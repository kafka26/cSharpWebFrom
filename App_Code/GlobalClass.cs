﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Reflection;
using System.Data.Linq;
/// <summary>
/// Summary description for ClassGlobal
/// </summary>
public class ClassGlobal
{         
    public static DataClassesDataContext db = new DataClassesDataContext();
    public static String Conn = ConfigurationManager.ConnectionStrings["PENA_Conn"].ConnectionString;
    public static SqlConnection cKon = new SqlConnection(Conn); // Unmodifiable
    public static SqlCommand xCmd = new SqlCommand();
    public static SqlDataAdapter sda = new SqlDataAdapter();
    public static DataTable data = new DataTable();
    public static DataTable Modul = new DataTable();
    public static DataView mView = new DataView();
    public static DataView DtView = new DataView();

    public static DataTable CekMenuByUser(string UserID, Int32 trnCode)
    { 
        data = ToDataTable((from m in db.m_transcodes
            join r in db.m_roledtls on m.trancode equals r.transcode
            join rd in db.m_roles on r.roleid equals rd.roleid
            join u in db.userlogindtls on r.roleid equals u.roleid
            join ul in db.userlogins on u.userid equals ul.userid
            where u.userid.ToLower() == UserID.ToLower() && m.trancode == trnCode && ul.m_flag.ToLower() == "aktif" && rd.m_flag.ToLower() == "aktif"
            select new
            {
                r.create_form,
                r.read_form,                               
                r.update_form,
                r.delete_form,
                r.req_approval,
                r.approval_user,
                r.special_akses,
                approval_form = (m.transtype.ToLower() == "standart" ? false : true )
            }).ToList());

        return data;
    }

    public static DataTable GetMenuByUser(string UserID, string sType, string sModul = "", string sMenuType = "")
    {        
        var dtmenu = (from m in db.m_transcodes
                      join r in db.m_roledtls on m.trancode equals r.transcode
                      join rd in db.m_roles on r.roleid equals rd.roleid
                      join u in db.userlogindtls on r.roleid equals u.roleid
                      join ul in db.userlogins on u.userid equals ul.userid
                      where u.userid.ToLower() == UserID.ToLower() && ul.m_flag.ToLower() == "aktif" && rd.m_flag.ToLower() == "aktif"
                      select new
                      {
                          m.menumodul_seq,
                          m.menumodul,
                          m.menumodul_icon,
                          m.menutype_seq,
                          m.menutype,
                          m.menutype_icon,
                          m.menufolder,
                          m.menufile,
                          m.type,
                          m.urutan
                      });

        if (sType == "modul")
        {
            data = ToDataTable(dtmenu.Select(x => new {
                x.menumodul_seq,
                x.menumodul,
                x.menumodul_icon
            }).Distinct().OrderBy(o => o.menumodul_seq).ToList());
        }
        else if (sType == "menutype")
        {
            data = ToDataTable(dtmenu.Where(w => w.menumodul == sModul).Select(x => new {
                x.menutype_seq,
                x.menutype,
                x.menutype_icon
            }).Distinct().OrderBy(o => o.menutype_seq).ToList());
        }
        else 
            data = ToDataTable(dtmenu.Where(w => w.menumodul == sModul && w.menutype == sMenuType).OrderBy(o => o.urutan).ToList()); 

        return data;
    }

    public static string GetNewID() => Guid.NewGuid().ToString(); 

    public static DateTime GetServerTime()
    { 
        return db.ExecuteQuery<DateTime>("SELECT GETDATE() AS tanggal").FirstOrDefault();
    }

    public static int CekDoubleClick(string TableName, string trnid)
    {        
        return db.ExecuteQuery<int>("Select COUNT(trnid) from "+ TableName + " Where trnid='"+ trnid.ToString() + "'").FirstOrDefault();
    } 

    public static int GenerateID(string TableName)
    {
        return db.ExecuteQuery<int>("Select ISNULL(MAX(sysid),0) from " + TableName + "").FirstOrDefault();
    }

    public static DateTime toDate(string dateTxt)
    {
        string[] arTgl = dateTxt.Split('/');
        //string tgl = arTgl[1] + "/" + arTgl[0] + "/" + arTgl[2];
        //new DateTime = int.Parse(arTgl[1]) , int.Parse(arTgl[0]) , arTgl[2];
        return new DateTime(int.Parse(arTgl[2]), int.Parse(arTgl[1]), int.Parse(arTgl[0]));
    }

    public void SetModalPopUp(AjaxControlToolkit.ModalPopupExtender Mpe, System.Web.UI.WebControls.Panel Panel, System.Web.UI.WebControls.Button Button, Boolean sBoolean)
    {
        Button.Visible = sBoolean;
        Panel.Visible = sBoolean;
        if (sBoolean)
        {
            Mpe.Show();
        }
        else
        {
            Mpe.Hide();
        }
    }

    public static DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);
        //Get all the properties by using reflection   
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names  
            dataTable.Columns.Add(prop.Name);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        return dataTable;
    }

    public static string GenerateTransNo(int transcode)
    {
        string sNo = ""; string prefix = ""; int maxid = 0; 
        m_transcode tblTrcode = new m_transcode();
        List<m_transcode> edt = db.m_transcodes.Where(x => x.trancode == transcode).ToList();
        foreach (var item in edt)
        {
            maxid = item.number_.Value;
            prefix = item.prefix;
        }

        sNo = NewGenNumberString(prefix + "-", DateTime.Now.ToString("yyMM") + "-", maxid + 1, 5);
        tblTrcode = db.m_transcodes.Where(x => x.trancode == transcode).FirstOrDefault();

        try
        {
            tblTrcode.number_ = maxid + 1;
            db.SubmitChanges();
        }  
        catch (Exception ex)
        {
            sNo = ex.ToString();
        }
        return sNo;
    }

    public static string ToMaskEdit(double sMoney, byte iType)
    {
        var ToMaskEdit = string.Format(sMoney.ToString(), "###,###,###,###.##");
        if (ToMaskEdit.ToString().Trim() == "")
        {
            ToMaskEdit = "0.00";
        }
        else if (ToMaskEdit.IndexOf(".") == -1)
        {
            ToMaskEdit += ".00";
        }
        else
        {
            if (ToMaskEdit.Length - ToMaskEdit.IndexOf(".") == 2)
            {
                ToMaskEdit += "0";
            }
        }
        return ToMaskEdit;
    }

    private static string NewGenNumberString(string sPrefix, string sMiddle, long lNo, int iDefaultCounter)
    {
        int iAdd = lNo.ToString().Length - iDefaultCounter;
        if (iAdd > 0)
        {
            iDefaultCounter += iAdd;
        }
        string sFormat = "";
        for (int i = 0; i < iDefaultCounter; i++)
        {
            sFormat = sFormat + "0";
        }
        string sNo = sPrefix + sMiddle + string.Format("{0:"+ sFormat + "}", lNo);
        return sNo;
    }

    public static string SetDBLogonCrv(ReportDocument CrvDoc)
    {
        Tables myTables = CrvDoc.Database.Tables;
        ConnectionInfo crConnInfo = new ConnectionInfo();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();

        crConnInfo.ServerName = ConfigurationManager.AppSettings["DB-Server"];
        crConnInfo.DatabaseName = ConfigurationManager.AppSettings["DB-Name"];
        crConnInfo.UserID = ConfigurationManager.AppSettings["DB-User"];
        crConnInfo.Password = ConfigurationManager.AppSettings["DB-Password"];

        foreach (CrystalDecisions.CrystalReports.Engine.Table CrvTable in myTables)
        {
            crtableLogoninfo = CrvTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnInfo;
            CrvTable.ApplyLogOnInfo(crtableLogoninfo);
        }
        return "OK";
    }

    public static DataTable GetDataTable(string sSql)
    {
        SqlDataAdapter mySqlDA = new SqlDataAdapter(sSql,Conn);
        DataTable sData = new DataTable();
        mySqlDA.Fill(sData);
        return sData;
    }

    public static string sMoney(decimal amount) => string.Format("{0:#,0.00}", Convert.ToDecimal(amount));
}