﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Coba.Startup))]
namespace Coba
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
