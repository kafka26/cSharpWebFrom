﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>javascript:window.history.forward();</script>
  <title>PeNa - LOGIN</title>
  <link href="../../Template/dist/img/AdminLTELogo.png" rel="shortcut icon"/>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="template/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="template/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Toastr -->
  <link rel="stylesheet" href="template/plugins/toastr/toastr.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="template/dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
   
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="index2.html" class="h1"><b>LOGIN</b></a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Sign in to start Application</p>

      <form runat="server" id="quickForm">
          <div class="row">
              <div class="col-12">
                  <div class="form-group input-group mb-3">
                   
                   <asp:Label runat="server" ID="Label1" Visible="false"></asp:Label>
                      <asp:TextBox runat="server" class="form-control"  placeholder="Enter Username" id="username" ClientIDMode="Static"></asp:TextBox>
                       <div class="input-group-append">
                           <div class="input-group-text">
                                <span class="fas fa-user"></span>
                           </div>
                       </div>
                  </div>

                  <div class="form-group input-group mb-3">
                      <asp:TextBox runat="server" class="form-control"  placeholder="Enter Password" TextMode="Password" id="password" ClientIDMode="Static"></asp:TextBox>
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-lock"></span>
                          </div>
                      </div>
                  </div>
                  </div>
                </div>
         
        <div class="row">          
          <!-- /.col -->
          <div class="col-4">
              <asp:Button runat="server" ID="signin" Text="Sign In" class="btn btn-primary btn-block" OnClick="signin_Click" />
          </div>
          <div >
              <asp:Label runat="server" ID="alertinvalidlogin" ForeColor="red" Text="Invalid username & password." />
          </div>
           
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->
   
<!-- jQuery -->
<script src="template/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="template/dist/js/adminlte.min.js"></script>

<!-- jquery-validation -->
<script src="template/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="../../template/plugins/jquery-validation/additional-methods.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="template/dist/js/demo.js"></script>
<!-- Page specific script -->

<script type="text/javascript">
    $(document).ready(function () {
        $('#<%=username.ClientID%>').focus();
    });
</script>

<script>
$(function () {
  //$.validator.setDefaults({
  //  submitHandler: function () {
  //    alert( "Change Password Successful Submitted!" );
  //  }
  //});
  $('#quickForm').validate({
    rules: {
        username: {
        required: true,
        //email: true,
      },
      password: {
        required: true,
        //minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
        username: {
        required: "Please enter a username",
        //email: "Please enter a vaild email address"
      },
        password: {
        required: "Please enter a password",
        //minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
</body>
</html>
