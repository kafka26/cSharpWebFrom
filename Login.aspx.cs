﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.IO;
using ASPSnippets.GoogleAPI;
using System.Web.Script.Serialization;

public partial class Login : System.Web.UI.Page
{

    DataClassesDataContext db = new DataClassesDataContext(); 
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Session.Clear();
        this.alertinvalidlogin.Visible = false;
    }

    protected void signin_Click(object sender, EventArgs e)
    {
        var Login = db.userlogins.Where(l => l.userid== username.Text.Trim().ToLower() && l.password== password.Text.ToLower().Trim()).ToList();
        if(Login.Count > 0)
        {
            foreach (var item in Login)
            {
                Session["username"] = item.username.ToUpper();
            }
            Response.Redirect("~/Other/Home/Home.aspx"); 
        }
        else
        {
            this.alertinvalidlogin.Visible = true;
        }
    }
}