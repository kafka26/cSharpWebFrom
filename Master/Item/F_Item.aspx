﻿<%@ Page Title="Master Katalog" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="F_Item.aspx.cs" Inherits="F_master_Item" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">Form Item</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-sm-12">
                                   <asp:label runat="server" ID="id_" Visible="false" />
                                   <asp:label runat="server" ID="flag_" Visible="false" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="col-form-label">Item Group</label> 
                                    <div class="input-group">
                                        <asp:TextBox id="itemgroupid" runat="server" CssClass="form-control" Visible="false" />
                                        <asp:TextBox id="itemgroupname" runat="server" CssClass="form-control" Enabled="false" />
                                        <span class="input-group-btn">
                                            <asp:button runat="server" ID="btnitemgroup" Text="search" class="btn btn-primary" OnClick="btnitemgroup_Click"/>
                                        </span>
                                    </div> 
                               </div>    
                                <div class="col-sm-6">
                                    <label class="col-form-label">Item Category</label>
                                    <div class="input-group">
                                        <asp:TextBox id="itemcategoryid" runat="server" CssClass="form-control" Visible="false" />
                                        <asp:TextBox id="itemcategoryname" runat="server" CssClass="form-control" Enabled="false" />
                                        <span class="input-group-btn">
                                            <asp:button runat="server" ID="btnitemcategory" Text="search" class="btn btn-sidebar" OnClick="btnitemcategory_Click"/>
                                        </span>
                                    </div> 
                               </div>  
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="col-form-label">Item Code</label> 
                                    <asp:TextBox runat="server" ID="itemcode" CssClass="form-control" /> 
                               </div>  
                                <div class="col-sm-6">
                                    <label class="col-form-label">Item Name</label> 
                                    <asp:TextBox runat="server" ID="itemname" CssClass="form-control" />
                               </div>  
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label">Unit Small</label> 
                                    <asp:DropDownList id="unit_small" runat="server" CssClass="form-control select2" ViewStateMode="Inherit" AutoPostBack="false" />
                               </div>  
                                <div class="col-sm-2">
                                    <label class="col-form-label">Unit Medium</label> 
                                    <asp:DropDownList id="unit_medium" runat="server" CssClass="form-control select2" ViewStateMode="Inherit" AutoPostBack="false" />
                               </div>  
                                <div class="col-sm-2">
                                    <label class="col-form-label">Unit Large</label> 
                                    <asp:DropDownList id="unit_large" runat="server" CssClass="form-control select2" ViewStateMode="Inherit" AutoPostBack="false" />
                               </div> 
                                <div class="col-sm-2">
                                    <label class="col-form-label">Conv Medium-Small</label> 
                                    <asp:TextBox runat="server" ID="unit_conv_MS" CssClass="form-control money" />
                               </div>  
                                <div class="col-sm-2">
                                    <label class="col-form-label">Conv Large-Medium</label> 
                                    <asp:TextBox runat="server" ID="unit_conv_LM" CssClass="form-control money" />
                               </div>  
                               <div class="col-sm-2">
                                   <div class="form-group clearfix">
                                       <label class="col-form-label">Flag</label> 
                                        <asp:RadioButtonList ID="rbflag" runat="server" CssClass="form-check">
                                            <asp:ListItem Selected="True">Aktif</asp:ListItem>
                                            <asp:ListItem>Tidak</asp:ListItem>
                                        </asp:RadioButtonList>
                                   </div>
                               </div> 
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-sm-5 right">
                                    <div class="form-group">
                                        <asp:Button runat="server" ID="BtnSave" CssClass="btn btn-success" Text="Save" OnClick="BtnSave_Click"/>
                                        <asp:Button runat="server" ID="BtnDel" CssClass="btn btn-danger" Text="Delete" onClientClick="return deleteConfirmed(this);" OnClick="BtnDel_Click" />
                                        <asp:Button runat="server" ID="BtnBack" CssClass="btn btn-secondary" Text="Back" OnClick="BtnBack_Click"/> 
                                    </div>
                                 </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                   <i class="col-form-label">Create By : <asp:Label runat="server" ID="lblcreate" /> </i>
                               </div>
                               <div class="col-sm-6">
                                   <i class="col-form-label">Last Update By : <asp:Label runat="server" ID="lblupdate" /> </i>
                               </div> 
                                <div class="col-sm-6">
                                   <i class="col-form-label">Create Time : <asp:Label runat="server" ID="lblcreatetime" /> </i>
                               </div>  
                                <div class="col-sm-6">
                                   <i class="col-form-label">Last Update Time : <asp:Label runat="server" ID="lblupdatetime" /> </i>
                               </div> 
                            </div>
                            <!---- Loading Bar ---->
                            <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                    <div id="processMessage" class="processMessage">
                                       <center>
                                           <span>
                                               <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"/>
                                               <br />Please Wait
                                           </span>
                                       </center>
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>

                            <!--- Start PopUp Item Group --->
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:Panel id="PanelItem" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">List Group item</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group row col-sm-12">
                                                    <div class="col-sm-2 custom-checkbox">
                                                        <label class="col-form-label">Filter</label>
                                                    </div>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <asp:DropDownList id="DDLfilterItemGroup" runat="server" CssClass="form-control select2">  
                                                                    <asp:ListItem value="itemgroupname"> Group </asp:ListItem> 
                                                                    <asp:ListItem value="itemgroupcode"> Kode </asp:ListItem>
                                                                </asp:DropDownList> 
                                                            </div>
                                                            <asp:TextBox runat="server" ID="txtfilterItemGroup" CssClass="form-control" />
                                                            <asp:Button ID="BtnFindItemGroup" runat="server" CssClass="btn btn-primary" Text="Find Data" OnClick="BtnFindItemGroup_Click"/>
                                                        </div>
                                                    </div>
                         
                                                     <br />
                                                     <div class="col-sm-12">
                                                       <div class="table-responsive">
                                                        <asp:GridView ID="gvitemgroup" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="itemgroupid,itemgroupname" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnSelectedIndexChanged="gvitemgroup_SelectedIndexChanged" OnPageIndexChanging="gvitemgroup_PageIndexChanging">
                                                            <Columns> 
                                                                <asp:CommandField ShowSelectButton="True" />
                                                                <asp:BoundField DataField="itemgroupcode" HeaderText="Code" /> 
                                                                <asp:BoundField DataField="itemgroupname" HeaderText="Group" />  
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <label>Data Not Found</label>
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div> 
                                                  </div>                       
                        
                                                </div> 
                                            </div>
                                            <div class="modal-footer">  
                                                <asp:Button ID="BtnCLoseItem" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCLoseItem_Click" />
                                            </div>
                                        </div>
                                    </asp:Panel> 

                                    <ajaxToolkit:ModalPopupExtender id="MPEItem" runat="server" Drag="True" PopupControlID="PanelItem" TargetControlID="beItem" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                    <asp:Button id="beItem" runat="server" Visible="False" CausesValidation="False" />      

                                     <asp:UpdateProgress runat="server" ID="UpdateProgress3" AssociatedUpdatePanelID="UpdatePanel2">
                                        <ProgressTemplate>
                                            <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                            <div id="processMessage" class="processMessage">
                                                <center><span><asp:Image ID="Image3" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><br />Please Wait</span></center><br />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>

                                </ContentTemplate>
                            </asp:UpdatePanel> 
                            <!--- End PopUp Item Group --->

                            <!--- Start PopUp Item Category --->
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:Panel id="PanelCategory" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">List Group item</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group row col-sm-12">
                                                    <div class="col-sm-2 custom-checkbox">
                                                        <label class="col-form-label">Filter</label>
                                                    </div>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <asp:DropDownList id="DDLfilterCategory" runat="server" CssClass="form-control select2">  
                                                                    <asp:ListItem value="itemcategoryname"> Category </asp:ListItem> 
                                                                    <asp:ListItem value="itemcategorycode"> Kode </asp:ListItem>
                                                                </asp:DropDownList> 
                                                            </div>
                                                            <asp:TextBox runat="server" ID="TxtFilterCategory" CssClass="form-control" />
                                                            <asp:Button ID="BtnFindCategory" runat="server" CssClass="btn btn-primary" Text="Find Data" OnClick="BtnFindCategory_Click"/>
                                                        </div>
                                                    </div>
                         
                                                     <br />
                                                     <div class="col-sm-12">
                                                       <div class="table-responsive">
                                                        <asp:GridView ID="GvCategory" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="itemcategoryid,itemcategoryname" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnSelectedIndexChanged="GvCategory_SelectedIndexChanged" OnPageIndexChanging="GvCategory_PageIndexChanging">
                                                           <Columns>
                                                                <asp:CommandField ShowSelectButton="True" />
                                                                <asp:BoundField DataField="itemcategorycode" HeaderText="Category Code" /> 
                                                                <asp:BoundField DataField="itemcategoryname" HeaderText="Category Name" />
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <label>Data Not Found</label>
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div> 
                                                  </div>                       
                        
                                                </div> 
                                            </div>
                                            <div class="modal-footer">  
                                                <asp:Button ID="BtnCloseCat" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCloseCat_Click"/>
                                            </div>
                                        </div>
                                    </asp:Panel> 

                                    <ajaxToolkit:ModalPopupExtender id="MpeCategory" runat="server" Drag="True" PopupControlID="PanelCategory" TargetControlID="BeCategory" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                    <asp:Button id="BeCategory" runat="server" Visible="False" CausesValidation="False" />      

                                    <asp:UpdateProgress runat="server" ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanel3">
                                        <ProgressTemplate>
                                            <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                            <div id="processMessage" class="processMessage">
                                                <center><span><asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><br />Please Wait</span></center><br />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>

                                </ContentTemplate>
                            </asp:UpdatePanel> 
                            <!--- End PopUp Item Category --->
     
                            <!--- Start PopUp Message --->
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:Panel id="PanelsMsg" runat="server" Visible="False" Width="70%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">
                                                    <asp:Label ID="lblInfo" runat="server" Text=""></asp:Label>
                                                </h4>
                                            </div>

                                            <div class="modal-body">                                                   
                                                <div class="col-sm-12">                                                         
                                                    <center>
                                                        <asp:Label ID="lblMessage" runat="server" Text="" />
                                                    </center> 
                                                </div>       
                                            </div> 

                                            <div class="modal-footer">   
                                                <div class="col-sm-12">
                                                 <center>
                                                     <asp:Button ID="BtnOK" runat="server" Text="OK" OnClick="BtnOK_Click" CssClass="btn btn-danger"/>
                                                </center>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </asp:Panel> 

                                    <ajaxToolkit:ModalPopupExtender id="MpesMsg" runat="server" Drag="True" PopupControlID="PanelsMsg" TargetControlID="BesMsg" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                    <asp:Button id="BesMsg" runat="server" Visible="False" CausesValidation="False" />       

                                </ContentTemplate>
                            </asp:UpdatePanel> 
                            <!--- End PopUp Item Group --->

                            <script type="text/javascript">
                                function deleteConfirmed(btndel) {
                                    if (btndel.dataset.confirmed) {
                                        btndel.dataset.confirmed = false;
                                        return true;
                                    } else {
                                        event.preventDefault();
                                        Swal.fire({
                                            title: 'Are you sure to Delete this data?',
                                            text: "Deleted data can't be returned!",
                                            type: 'warning',
                                            showCancelButton: true,
                                            confirmButtonText: 'Yes, Delete this data!'
                                        }).then((result) => {
                                            if (result.value) {
                                                $.blockUI({ message: bMsg });
                                                btndel.dataset.confirmed = true;
                                                btndel.click(); 
                                            }
                                        });
                                    }
                                    return false;
                                };
                            </script>

                        </ContentTemplate>
                    </asp:UpdatePanel>                
                </div>
            </div>
        </div>
    </section> 
</asp:Content>

