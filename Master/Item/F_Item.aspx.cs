﻿using System; 
using System.Linq; 
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient; 
using static ClassGlobal;
public partial class F_master_Item : System.Web.UI.Page
{
    DataClassesDataContext db = new DataClassesDataContext();
    m_item Tbl = new m_item(); 
    ClassGlobal sVar = new ClassGlobal();
    Int32 trnCode = 21;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {            
            if (!string.IsNullOrEmpty(Session["username"].ToString()))
            {
                data = CekMenuByUser(Session["username"].ToString(), trnCode);
                if (data.Rows.Count > 0)
                {
                    if (!this.IsPostBack)
                    {
                        this.GetDataUnit();
                        unit_conv_LM.Text = "1";
                        unit_conv_MS.Text = "1";
                        if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
                        {
                            flag_.Text = "1";
                            this.Filltexbox(Request.QueryString["id"]);
                        }
                        else
                            flag_.Text = "0";
                    }
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }                
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            Response.Redirect("~/Login.aspx");
        } 
    }

    private void Filltexbox(string itemid) {
        try
        {
            var data = (from i in db.m_items join g in db.m_itemgroups on i.item_group equals g.itemgroupid join c in db.m_itemcategories on i.item_category equals c.itemcategoryid where i.itemid == Int32.Parse(itemid) orderby i.itemname descending select new { i.itemid, g.itemgroupid, c.itemcategoryid, i.itemcode, i.itemname, g.itemgroupname, c.itemcategoryname, i.unit_small, i.unit_medium, i.unit_large, i.unit_conv_LM, i.unit_conv_MS, i.m_flag, i.createby, i.createtime, i.lastupdateby, i.lastupdatetime }).FirstOrDefault();
            id_.Text = data.itemid.ToString();
            itemname.Text = data.itemname.ToString();
            itemgroupid.Text = data.itemgroupid.ToString();
            itemcategoryid.Text = data.itemcategoryid.ToString();
            itemcode.Text = data.itemcode.ToString();
            itemgroupname.Text = data.itemgroupname.ToString();
            itemcategoryname.Text = data.itemcategoryname.ToString();
            unit_small.SelectedValue = data.unit_small.ToString();
            unit_medium.SelectedValue = data.unit_medium.ToString();
            unit_large.SelectedValue = data.unit_large.ToString();
            lblcreate.Text = data.createby.ToString();
            lblupdate.Text = data.lastupdateby.ToString();
            lblcreatetime.Text = data.createtime.ToString();
            lblupdatetime.Text = data.lastupdatetime.ToString();
        }
        catch (Exception ex)
        {         
            //this.CustomAlert(sender,ex.ToString());
            this.sMsg(ex.ToString(), "1");
            return;
        } 
    }

    //=== Start Getdata item group ===
    private void GetDataGroup()
    {
        data = ToDataTable((from i in db.m_itemgroups select new { i.itemgroupid, i.itemgroupcode, i.itemgroupname, i.createby }).ToList());
        DtView = data.DefaultView;
        if (txtfilterItemGroup.Text != "" )
        { 
            if (DDLfilterItemGroup.SelectedValue == "itemgroupname")
                DtView.RowFilter = DDLfilterItemGroup.SelectedValue + " LIKE '%" + txtfilterItemGroup.Text.Trim() + "%'";
            else
                DtView.RowFilter = DDLfilterItemGroup.SelectedValue + " LIKE '%" + txtfilterItemGroup.Text.Trim() + "%'";
        }
        data.AcceptChanges();
        gvitemgroup.DataSource = DtView.ToTable();
        gvitemgroup.DataBind();
        DtView.RowFilter = "";
    }

    protected void btnitemgroup_Click(object sender, EventArgs e)
    {
        txtfilterItemGroup.Text = "";
        this.GetDataGroup();
        sVar.SetModalPopUp(MPEItem, PanelItem, beItem, true);
    }

    protected void BtnCLoseItem_Click(object sender, EventArgs e)
    {
        gvitemgroup.DataSource = null;
        gvitemgroup.DataBind();
        sVar.SetModalPopUp(MPEItem, PanelItem, beItem, false);
    }

    protected void gvitemgroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        itemgroupid.Text = gvitemgroup.SelectedDataKey["itemgroupid"].ToString();
        itemgroupname.Text= gvitemgroup.SelectedDataKey["itemgroupname"].ToString();
        sVar.SetModalPopUp(MPEItem, PanelItem, beItem, false);
    }

    protected void gvitemgroup_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvitemgroup.PageIndex = e.NewPageIndex;
        this.GetDataGroup();
        sVar.SetModalPopUp(MPEItem, PanelItem, beItem, true);
    }

    protected void BtnFindItemGroup_Click(object sender, EventArgs e)
    {
        this.GetDataGroup();
        sVar.SetModalPopUp(MPEItem, PanelItem, beItem, true);
    }

    //=== End Getdata item group ===

    //=== Start Getdata item group ===
    private void GetDataCategory()
    {
        var DataItemCat = (from i in db.m_itemcategories select new { i.itemcategoryid, i.itemcategorycode, i.itemcategoryname, i.createby }).ToList();
        data = ToDataTable(DataItemCat);
        DtView = data.DefaultView;
        if (TxtFilterCategory.Text != "")
        {
            if (DDLfilterCategory.SelectedValue == "itemcategoryname")
                DtView.RowFilter = DDLfilterCategory.SelectedValue + " LIKE '%" + TxtFilterCategory.Text.Trim() + "%'";
            else
                DtView.RowFilter = DDLfilterCategory.SelectedValue + " LIKE '%" + TxtFilterCategory.Text.Trim() + "%'";
        }
        DtView.AllowEdit.ToString();
        GvCategory.DataSource = DtView.ToTable();
        GvCategory.DataBind();
        DtView.RowFilter = "";
    }
       
    protected void btnitemcategory_Click(object sender, EventArgs e)
    {
        TxtFilterCategory.Text = "";
        this.GetDataCategory();
        sVar.SetModalPopUp(MpeCategory, PanelCategory, BeCategory, true);
    }

    protected void GvCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        itemcategoryid.Text = GvCategory.SelectedDataKey["itemcategoryid"].ToString();
        itemcategoryname.Text = GvCategory.SelectedDataKey["itemcategoryname"].ToString();
        sVar.SetModalPopUp(MpeCategory, PanelCategory, BeCategory, false);
    }
    
    protected void BtnFindCategory_Click(object sender, EventArgs e)
    {
        this.GetDataCategory();
        sVar.SetModalPopUp(MpeCategory, PanelCategory, BeCategory, true);
    }

    protected void GvCategory_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.GetDataCategory();
        sVar.SetModalPopUp(MpeCategory, PanelCategory, BeCategory, true);
    }

    protected void BtnCloseCat_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeCategory, PanelCategory, BeCategory, false);
    }
    //=== End Getdata item group === 

    private void GetDataUnit()
    {
        try
        {
            var sData = (from p in db.m_generals where p.gengroup == "SATUAN" orderby p.genid descending select new { p.genid, p.genname }).ToList();
            unit_small.DataSource = sData;
            unit_small.DataTextField = "genname";
            unit_small.DataValueField = "genid";
            unit_small.DataBind();

            unit_medium.DataSource = sData;
            unit_medium.DataTextField = "genname";
            unit_medium.DataValueField = "genid";
            unit_medium.DataBind();

            unit_large.DataSource = sData;
            unit_large.DataTextField = "genname";
            unit_large.DataValueField = "genid";
            unit_large.DataBind();
        }
        catch (Exception ex)
        {
            //this.CustomAlert(sender,ex.ToString());
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {       
        if (flag_.Text!="0")
        { 
            Tbl = db.m_items.Single(i => i.itemid==Int32.Parse(id_.Text));
        }
        SqlTransaction ObjTrans; 
        if (cKon.State == ConnectionState.Closed) cKon.Open();
        ObjTrans = cKon.BeginTransaction();
        try
        {
            Tbl.itemcode = itemcode.Text.Trim().ToUpper();
            Tbl.itemname = itemname.Text.Trim().ToUpper();
            Tbl.itemcode = itemcode.Text;
            Tbl.itemname = itemname.Text;
            Tbl.item_group = Int32.Parse(itemgroupid.Text);
            Tbl.item_category = Int32.Parse(itemcategoryid.Text);
            Tbl.unit_small =Int32.Parse(unit_small.SelectedValue);
            Tbl.unit_medium = Int32.Parse(unit_medium.SelectedValue);
            Tbl.unit_large = Int32.Parse(unit_large.SelectedValue);
            Tbl.unit_conv_LM = Decimal.Parse(unit_conv_LM.Text);
            Tbl.unit_conv_MS = Decimal.Parse(unit_conv_MS.Text);
            Tbl.m_flag = rbflag.SelectedValue;
            Tbl.lastupdateby = "admin";
            Tbl.lastupdatetime = DateTime.Now;
            if (flag_.Text == "0")
            {
                Tbl.createby = Session["username"].ToString();
                Tbl.createtime = DateTime.Now;
                db.m_items.InsertOnSubmit(Tbl);
            }
            db.SubmitChanges();
            ObjTrans.Commit();
            cKon.Close();
            Response.Redirect("~/Master/Item/LF_Item.aspx");
        }
        catch(Exception ex)
        {
            ObjTrans.Rollback();
            cKon.Close();
            //this.CustomAlert(sender,ex.ToString());
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/Item/LF_Item.aspx");
    }

    protected void BtnDel_Click(object sender, EventArgs e)
    {
        if (flag_.Text != "0")
        {
            SqlTransaction ObjTrans;
            if (cKon.State == ConnectionState.Closed) cKon.Open();
            ObjTrans = cKon.BeginTransaction();

            try
            {                
                Tbl = db.m_items.Single(i => i.itemid == Int32.Parse(id_.Text));
                db.m_items.DeleteOnSubmit(Tbl);
                db.SubmitChanges();
                ObjTrans.Commit();
                cKon.Close();
                Response.Redirect("~/Master/Item/LF_Item.aspx");
            }
            catch(Exception ex)
            {
                ObjTrans.Rollback();
                cKon.Close();
                //this.CustomAlert(sender,ex.ToString());
                this.sMsg(ex.ToString(), "1");
                return;
            }          
        }
    }

    private void CustomAlert(object sender, string sMsg)
    {
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "alert", "Swal.fire('', '" + sMsg + "', 'error');", true);
    }

    protected void BtnOK_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, false);
    }

    private void sMsg(string sMsg, string type)
    {
        if (type == "1") lblInfo.Text = "ERROR";
        else lblInfo.Text = "INFORMATION";
        lblMessage.Text = sMsg.ToUpper().ToString();
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, true);
    }
}