﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="LF_Item.aspx.cs" Inherits="LF_master_Item" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="content-header">
    <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-dark">
                <asp:hyperlink runat="server" ID="NewData" CssClass="btn btn-success" NavigateUrl="~/Master/Item/F_Item.aspx?">
                    <i class="fas fa-plus-circle"> New Data</i>
                </asp:hyperlink> 
                List Item
            </h3>
        </div><!-- /.col -->
    </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body"> 
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="row"></div>
                
                        <div class="form-group row col-sm-12">
                             <div class="col-sm-1 custom-checkbox">
                                <label class="col-form-label">Filter</label>
                            </div>

                            <div class="col-sm-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <asp:DropDownList id="ddlfilter" runat="server" CssClass="form-control select2">  
                                            <asp:ListItem value="itemcode"> Code </asp:ListItem> 
                                            <asp:ListItem value="itemname"> Katalog </asp:ListItem>
                                            <asp:ListItem value="Created"> Create </asp:ListItem> 
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:TextBox runat="server" ID="txtfilter" CssClass="form-control" />
                                </div>
                            </div>
                             <asp:Button ID="btnadd" runat="server" Text="Find Data" OnClick="btnadd_Click" CssClass="btn btn-primary"/> 
                        </div>
                        <hr />

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvlist" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="itemid" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" AllowSorting="True" OnPageIndexChanging="gvlist_PageIndexChanging"> 
                                    <Columns>
                                       <asp:HyperLinkField DataNavigateUrlFields="itemid" DataNavigateUrlFormatString="~/Master/Item/F_Item.aspx?id={0}" DataTextField="itemcode" HeaderText="Code" SortExpression="itemcode" />
                                        <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                        <asp:BoundField DataField="createby" HeaderText="Created" SortExpression="createby" />
                                         <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                         <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                         <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                         <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                         <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                         <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                         <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                         <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                         <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                         <asp:BoundField DataField="itemname" HeaderText="Katalog" SortExpression="itemname" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <label>Data Not Found</label>
                                    </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div> 
                        <!---- Loading Bar ---->
                        <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                <div id="processMessage" class="processMessage">
                                    <center><span><asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif">
                                    </asp:Image><br />Please Wait</span></center><br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div> 
</section>
</asp:Content>

