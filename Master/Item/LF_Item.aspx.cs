﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using static ClassGlobal;

public partial class LF_master_Item : System.Web.UI.Page
{ 
    DataClassesDataContext db = new DataClassesDataContext();
    ClassGlobal sVar = new ClassGlobal();
    int trnCode = 21;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            data = CekMenuByUser(Session["username"].ToString(), trnCode);
            if (data.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Session["username"].ToString()))
                {
                    if (!this.IsPostBack)
                    {
                        //this.BindData();
                    }
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }
        }
        catch (Exception)
        {
            Response.Redirect("~/Login.aspx");
        }
    } 

    private void BindData()
    { 
        var dt = (from i in db.m_items select new { i.itemid,i.itemcode,i.itemname,i.createby }).ToList();
        data = ToDataTable(dt);
        Session["itemview"] = data;
        Session["item"] = data;
        gvlist.DataSource = Session["itemview"];
        gvlist.DataBind(); 
    }

    protected void btnadd_Click(object sender, EventArgs e)
    { 
        if (Session["itemview"] != null)
        {
            gvlist.DataSource = null;
            gvlist.DataBind();
            data = Session["itemview"] as DataTable;
            DataView DtView = data.DefaultView;
            DtView.RowFilter = ddlfilter.SelectedValue + " LIKE '%" + txtfilter.Text + "%'";
            data.AcceptChanges();
            gvlist.DataSource = data;
            gvlist.DataBind();
            DtView.RowFilter = "";
        }
        else
        {
            BindData();
        }
    }
    
    protected void gvlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvlist.PageIndex = e.NewPageIndex;
        gvlist.DataSource = Session["itemview"];
        gvlist.DataBind();
    }
     
}