﻿<%@ Page Title="Master Katalog" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="F_Produk.aspx.cs" Inherits="F_master_Item" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" runat="server" %> 


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Content Header (Page header) -->
<head runat="server"><title></title></head>
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0 text-dark">Form Produk</h1>
        </div><!-- /.col -->
    </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-sm-12">
                               <asp:label runat="server" ID="id_" Visible="false" />
                               <asp:label runat="server" ID="flag_" Visible="false" />
                            </div>
                        </div>
                         
                        <div class="row">
                             <div class="col-sm-2">
                                <label class="col-form-label">Tanggal</label>
                                <div class="input-group">                                                           
                                     <asp:TextBox runat="server" ID="tanggal" CssClass="form-control" Enabled="false"/>  
                                     <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" CssClass="btn btn-secondary"/>  
                                </div>
                            </div> 
                        </div>

                        <asp:CalendarExtender ID="CalendarExtender" runat="server" Enabled="True" TargetControlID="tanggal" PopupButtonID="CalPeriod2" Format="dd/MM/yyyy" /> 

                        <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="tanggal" Mask="99/99/9999" MaskType="Date" UserDateFormat="None"/>

                        <div class="row">
                           <div class="col-sm-3">
                                <label class="col-form-label">Item Code</label> 
                                <asp:TextBox runat="server" ID="itemcode" CssClass="form-control" /> 
                           </div>  

                            <div class="col-sm-4">
                                <label class="col-form-label">Item Name</label> 
                                <asp:TextBox runat="server" ID="itemname" CssClass="form-control" />
                           </div>  
                            <div class="col-sm-2">
                                <label class="col-form-label">Jumlah</label> 
                                <asp:TextBox runat="server" ID="jumlah" CssClass="form-control money" Text="0" />
                           </div> 
                        </div>

                           <div class="row">
                           <div class="col-sm-3">
                                <label class="col-form-label">Item Code</label> 
                                <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control" /> 
                           </div>  

                            <div class="col-sm-4">
                                <label class="col-form-label">Item Name</label> 
                                <asp:TextBox runat="server" ID="TextBox2" CssClass="form-control" />
                           </div>  
                            <div class="col-sm-2">
                                <label class="col-form-label">TextBox3</label> 
                                <asp:TextBox runat="server" ID="TextBox3" CssClass="form-control" />
                           </div> 
                        </div>

                        <hr>
                         <div class="row">
                           <div class="col-sm-2">                                
                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                           </div>

                            <div class="col-sm-2">                                
                                <asp:Label ID="Label8" runat="server" Text=""></asp:Label>
                           </div>

                           <div class="col-sm-6">                                
                                <asp:Label ID="Label15" runat="server" Text=""></asp:Label>
                           </div>

                         </div>

                        <div class="row">
                           <div class="col-sm-2">
                                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                           </div>

                            <div class="col-sm-2">                                
                                <asp:Label ID="Label9" runat="server" Text=""></asp:Label>
                           </div>
                         </div>

                        <div class="row">
                           <div class="col-sm-2">
                                <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                           </div>

                            <div class="col-sm-2">                                
                                <asp:Label ID="Label10" runat="server" Text=""></asp:Label>
                           </div>
                         </div>

                        <div class="row">
                           <div class="col-sm-2">
                                <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                           </div>

                            <div class="col-sm-2">                                
                                <asp:Label ID="Label11" runat="server" Text=""></asp:Label>
                           </div>
                         </div>

                        <div class="row">
                           <div class="col-sm-2">
                               <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                           </div>

                            <div class="col-sm-2">                                
                                <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                           </div>
                         </div>

                        <div class="row">
                           <div class="col-sm-2">
                                <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                           </div>

                            <div class="col-sm-2">                                
                                <asp:Label ID="Label13" runat="server" Text=""></asp:Label>
                           </div>
                         </div>

                        <div class="row">
                           <div class="col-sm-2">
                                <asp:Label ID="Label7" runat="server" Text=""></asp:Label>
                           </div>

                            <div class="col-sm-2">                                
                                <asp:Label ID="Label14" runat="server" Text=""></asp:Label>
                           </div> 
                         </div>
                         
                        <hr />
                        <div class="row">
                            <div class="col-sm-5 right">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="BtnGet" CssClass="btn btn-success load" Text="Button" OnClick="BtnGet_Click"/>
                                    <asp:Button runat="server" ID="BtnSave" CssClass="btn btn-success load" Text="Save" OnClick="BtnSave_Click"/>
                                    <asp:Button runat="server" ID="BtnEdit" CssClass="btn btn-success load" Text="Edit" OnClick="BtnEdit_Click"/>
                                    <asp:Button runat="server" ID="BtnDel" CssClass="btn btn-danger" Text="Delete" onClientClick="return deleteConfirmed(this);" OnClick="BtnDel_Click" />
                                    <asp:Button runat="server" ID="BtnBack" CssClass="btn btn-secondary load" Text="Back" OnClick="BtnBack_Click"/> 
                                </div>
                             </div>
                        </div>

                        <!---- Loading Bar ---->
                        <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                <div id="processMessage" class="processMessage">
                                   <center>
                                       <span>
                                           <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"/>
                                           <br />Please Wait
                                       </span>
                                   </center>
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </ContentTemplate>
                </asp:UpdatePanel>                
            </div>
        </div>
    </div>
</section> 
       
<script type="text/javascript">
    function deleteConfirmed(btndel) {
        if (btndel.dataset.confirmed) {
            btndel.dataset.confirmed = false;
            return true;
        } else {
            event.preventDefault();
            Swal.fire({
                title: 'Are you sure to Delete this data?',
                text: "Deleted data can't be returned!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Delete this data!'
            }).then((result) => {
                if (result.value) {
                    $.blockUI({ message: bMsg });
                    btndel.dataset.confirmed = true;
                    btndel.click(); 
                }
            });
        }
        return false;
    };
</script>
</asp:Content>

