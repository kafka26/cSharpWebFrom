﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using static ClassGlobal;
public partial class F_master_Item : System.Web.UI.Page
{
    DataClassesDataContext db = new DataClassesDataContext();
    m_item Tbl = new m_item();
    ClassGlobal sVar = new ClassGlobal();
    Int32 trnCode = 21;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
            {
                flag_.Text = "1";
                this.Filltexbox(Request.QueryString["id"]);
                BtnSave.Visible = false;
                BtnEdit.Visible = true;
            }
            else
            {
                flag_.Text = "0";
                BtnSave.Visible = true;
                BtnEdit.Visible = false;
            }
        }
        tanggal.Text = DateTime.Now.ToString("dd/MM/yyyy");        
    }

    public static int FindMaxSum(List<int> list)
    {
        throw new NotImplementedException("Waiting to be implemented.");
    }

    public static void Main1(string[] args)
    {
        try
        {
            List<int> list = new List<int> { 5, 9, 7, 11 };
            FindMaxSum(list);
        }
        catch (NotImplementedException notImp)
        {
            Console.WriteLine(notImp);
        }
    }

    public static string Transform(string input)
    {
        try
        {
            input.ToString();
        }
        catch (NotImplementedException ex)
        {
            throw new NotImplementedException("Waiting to be implemented.");
        }
        return input.ToString(); 
    }

    public static void Main(string[] args)
    {
        Console.WriteLine(Transform("abbcbbb"));
    }

    private void Looping()
    {
        {
            String inputString = TextBox1.Text;
            int count = 0;
            char[] ArrayChar = new char[inputString.Length];
            ArrayChar = inputString.ToCharArray();
            foreach (char temp in ArrayChar)
                if (char.IsLetter(temp) || !(char.IsLetterOrDigit(temp))) count++;
            TextBox2.Text = count.ToString();
            TextBox3.Text = TextBox1.Text.ToString().Substring(5,3);
        }
    }

    private void GetWord()
    {

        //cKon.Open();
        //SqlCommand xCmd = new SqlCommand("Select_Word");
        //xCmd.Parameters.AddWithValue("@Action", "SELECT");
        //xCmd.Parameters.AddWithValue("@KeyWord", "hello world");
        //SqlDataAdapter xsda = new SqlDataAdapter();
        //xCmd.CommandType = CommandType.StoredProcedure;
        //xCmd.Connection = cKon;
        //xsda.SelectCommand = xCmd;

        using (SqlConnection con = new SqlConnection(Conn))
        {
            using (SqlCommand cmd = new SqlCommand("Select_Word"))
            {
                cmd.Parameters.AddWithValue("@Action", "SELECT");
                cmd.Parameters.AddWithValue("@KeyWord", "hello world");
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            DtView = dt.DefaultView;
                            DtView.RowFilter= "kolom1='h'";
                            Label8.Text = DtView[0]["kolom1"].ToString() +"-" + DtView.Count;
                            DtView.RowFilter = "";

                            DtView.RowFilter = "kolom1='e'";
                            Label9.Text = DtView[0]["kolom1"].ToString() + "-" + DtView.Count;
                            DtView.RowFilter = "";

                            DtView.RowFilter = "kolom1='l'";
                            Label10.Text = DtView[0]["kolom1"].ToString() + "-" + DtView.Count;
                            DtView.RowFilter = "";

                            DtView.RowFilter = "kolom1='o'";
                            Label11.Text = DtView[0]["kolom1"].ToString() + "-" + DtView.Count;
                            DtView.RowFilter = "";

                            DtView.RowFilter = "kolom1='w'";
                            Label12.Text = DtView[0]["kolom1"].ToString() + "-" + DtView.Count;
                            DtView.RowFilter = "";

                            DtView.RowFilter = "kolom1='r'";
                            Label13.Text = DtView[0]["kolom1"].ToString() + "-" + DtView.Count;
                            DtView.RowFilter = "";

                            DtView.RowFilter = "kolom1='d'";
                            Label14.Text = DtView[0]["kolom1"].ToString() + "-" + DtView.Count;
                            DtView.RowFilter = "";
                        }
                    }
                }
            }
        }

        int angka;
        for (angka=1; angka <= 20; angka++)
        {
            if (angka % 5 == 0) Label15.Text += " IDIC";
            else if (angka % 6 == 0) Label15.Text += " LPS";
            else Label15.Text = Label15.Text + " " + angka;
        }
            
       itemcode.Text = itemcode.Text.ToLower().Substring(0, 1).ToUpper() + itemcode.Text.ToLower().Substring(1);
       itemname.Text = string.Join(" ", itemname.Text.ToLower().Split(' ').ToList().ConvertAll(word => word.Substring(0, 1).ToUpper() + word.Substring(1)));

    }

    private void Filltexbox(string itemid) { 
        using (SqlConnection con = new SqlConnection(Conn))
        {
            using (SqlCommand cmd = new SqlCommand("Produk_CRUD"))
            {
                cmd.Parameters.AddWithValue("@Action", "SELECT");
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt); 
                        if (dt.Rows.Count > 0)
                        {
                            DtView = dt.DefaultView;
                            DtView.RowFilter="id=" + itemid;
                            for (int i = 0; i < DtView.Count; i++)
                            {
                                string aDate = DtView[i]["tanggal"].ToString();
                                DateTime oDate = DateTime.Parse(aDate);
                                id_.Text = DtView[i]["id"].ToString();
                                itemname.Text = DtView[i]["nama_barang"].ToString();
                                itemcode.Text = DtView[i]["kode_barang"].ToString();
                                jumlah.Text = DtView[i]["jumlah_barang"].ToString();
                                tanggal.Text = "1/1/1900"; //oDate.ToString("dd/MM/yyyy");
                                Label1.Text = DtView[i]["kolom1"].ToString();
                                Label2.Text = DtView[i]["kolom2"].ToString();
                                Label3.Text = DtView[i]["kolom3"].ToString();
                                Label4.Text = DtView[i]["kolom4"].ToString();
                                Label5.Text = DtView[i]["kolom5"].ToString();
                                Label6.Text = DtView[i]["kolom6"].ToString();
                                Label7.Text = DtView[i]["kolom7"].ToString();
                            }
                        }
                    }
                }
            }
        }
        GetWord();
    }         

    protected void BtnSave_Click(object sender, EventArgs e)
    { 
        using (SqlConnection con = new SqlConnection(Conn))
        {
            using (SqlCommand cmd = new SqlCommand("Produk_CRUD"))
            {
                string[] arTgl = tanggal.Text.Split('/');
                tanggal.Text = arTgl[1] + "/" + arTgl[0] + "/" + arTgl[2];
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Action", "INSERT");
                cmd.Parameters.AddWithValue("@nama_barang", itemname.Text);
                cmd.Parameters.AddWithValue("@kode_barang", itemcode.Text);
                cmd.Parameters.AddWithValue("@jumlah_barang", jumlah.Text);
                cmd.Parameters.AddWithValue("@tanggal", tanggal.Text);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        //this.BindGrid();
        Response.Redirect("~/Master/Produk/LF_Produk.aspx");
    }

    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/Produk/LF_Produk.aspx");
    }

    protected void BtnDel_Click(object sender, EventArgs e)
    {
        if (flag_.Text != "0")
        { 
            using (SqlConnection con = new SqlConnection(Conn))
            {
                using (SqlCommand cmd = new SqlCommand("Produk_CRUD"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Action", "DELETE");
                    cmd.Parameters.AddWithValue("@id", id_.Text);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            Response.Redirect("~/Master/Produk/LF_Produk.aspx");
        }
    }

    protected void BtnEdit_Click(object sender, EventArgs e)
    { 
        using (SqlConnection con = new SqlConnection(Conn))
        {
            using (SqlCommand cmd = new SqlCommand("Produk_CRUD"))
            {
                string[] arTgl = tanggal.Text.Split('/');
                tanggal.Text = arTgl[1] + "/" + arTgl[0] + "/" + arTgl[2];
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Action", "UPDATE");
                cmd.Parameters.AddWithValue("@Id", id_.Text);
                cmd.Parameters.AddWithValue("@nama_barang", itemname.Text);
                cmd.Parameters.AddWithValue("@kode_barang", itemcode.Text);
                cmd.Parameters.AddWithValue("@jumlah_barang", jumlah.Text);
                cmd.Parameters.AddWithValue("@tanggal", tanggal.Text);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        Response.Redirect("~/Master/Produk/LF_Produk.aspx");
    }

    protected void BtnGet_Click(object sender, EventArgs e)
    {
        var sumOfNumber = (from number in Enumerable.Range(1, 4) group number by number % 2 into setofnumber select setofnumber.Sum()).Max();
        TextBox3.Text = sumOfNumber.ToString();
    }
}