﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="LF_Produk.aspx.cs" Inherits="LF_master_Produk" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0 text-dark">List Produk</h1>
        </div><!-- /.col -->
    </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body"> 
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:hyperlink runat="server" ID="NewData" CssClass="btn btn-success" NavigateUrl="~/Master/Produk/F_Produk.aspx?">
                                    <i class="fas fa-plus-circle"> <label class="control-label">New Data</label></i>
                                </asp:hyperlink>  
                            </div>
                        </div>
                        <br /> 
                
                        <div class="form-group row col-sm-12">
                            <div class="col-sm-1 custom-checkbox">
                                <label class="col-form-label">Filter</label>
                            </div>

                            <div class="col-sm-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <asp:DropDownList id="ddlfilter" runat="server" CssClass="form-control select2">  
                                            <asp:ListItem value="kode_barang"> Code </asp:ListItem> 
                                            <asp:ListItem value="nama_barang"> Katalog </asp:ListItem> 
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:TextBox runat="server" ID="txtfilter" CssClass="form-control" />
                                </div>
                            </div>
                            <asp:Button ID="btnadd" runat="server" Text="Find Data" OnClick="btnadd_Click" CssClass="btn btn-primary"/> 
                        </div>
                        <hr />

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvlist" CssClass="table table-hover table-bordered datatable" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="id" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Right" AllowSorting="True" OnPageIndexChanging="gvlist_PageIndexChanging"> 
                                    <Columns>
                                       <asp:HyperLinkField DataNavigateUrlFields="id" DataNavigateUrlFormatString="~/Master/Produk/F_Produk.aspx?id={0}" DataTextField="kode_barang" HeaderText="Code" SortExpression="itemcode" />
                                        <asp:BoundField DataField="nama_barang" HeaderText="Katalog" SortExpression="nama_barang" />
                                        <asp:BoundField DataField="jumlah_barang" HeaderText="Jumlah" SortExpression="jumlah_barang" ControlStyle-CssClass="money" />
                                       <asp:BoundField DataField="tanggal" HeaderText="Tanggal" SortExpression="tanggal" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <label>Data Not Found</label>
                                    </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div> 
                        <!---- Loading Bar ---->
                        <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                <div id="processMessage" class="processMessage">
                                    <center><span><asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><br />Please Wait</span></center><br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div> 
</section>
    <script type="text/javascript">
        function AddToDetail() {
            //alert("L_customer.aspx/GetCustomers");
            /*$.ajax({
                type: "POST",
                url: "L_customer.aspx/GetCustomers",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert("L_customer.aspx/GetCustomers");
                },
                error: function (response) {
                    alert("COK");
                }
            });*/
        };

        function OnSuccess(response) {
            //alert("COOOOK")
            //$("[id*=gvlist]").DataTable({
            //    bLengthChange: true,
            //    lengthMenu: [[5, 10, -1], [5, 10, "All"]],
            //    bFilter: true,
            //    bSort: true,
            //    bPaginate: true,
            //    data: response.d,
            //    columns: [
            //        { 'data': 'custcode' },
            //        { 'data': 'custname' },
            //        { 'data': 'cust_address' }
            //    ]
            //});
        };
    </script>
</asp:Content>

