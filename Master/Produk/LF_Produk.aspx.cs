﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using CrystalDecisions.CrystalReports.Engine; 
using System.Reflection;
using static ClassGlobal;

public partial class LF_master_Produk : System.Web.UI.Page
{ 
    DataClassesDataContext db = new DataClassesDataContext();
    ClassGlobal sVar = new ClassGlobal();
    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
        //    if (!string.IsNullOrEmpty(Session["username"].ToString()))
        //    {
        //        if (!this.IsPostBack)
        //        {
                   BindData();
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //    Response.Redirect("~/Login.aspx");
        //}
    } 
    
    private void BindData()
    {  
        using (SqlConnection con = new SqlConnection(Conn))
        {
            using (SqlCommand cmd = new SqlCommand("Produk_CRUD"))
            {
                cmd.Parameters.AddWithValue("@Action", "SELECT");
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        gvlist.DataSource = dt;
                        gvlist.DataBind();
                        Session["itemview"] = dt;
                    }
                }
            }
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    { 
        if (Session["itemview"] != null)
        {
            gvlist.DataSource = null;
            gvlist.DataBind();
            data = Session["itemview"] as DataTable;
            DataView DtView = data.DefaultView;
            DtView.RowFilter = ddlfilter.SelectedValue + " LIKE '%" + txtfilter.Text + "%'";
            data.AcceptChanges();
            gvlist.DataSource = data;
            gvlist.DataBind();
            DtView.RowFilter = "";
        }
        else BindData();
    }
    
    protected void gvlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvlist.PageIndex = e.NewPageIndex;
        gvlist.DataSource = Session["itemview"];
        gvlist.DataBind();
    }
     
}