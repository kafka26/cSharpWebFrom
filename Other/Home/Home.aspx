﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">Notification</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate> 
                             <%--<div class="row">
                                  <div class="col-lg-3 col-6">
                                    <!-- small box -->
                                    <div class="small-box bg-info">
                                      <div class="inner">
                                        <h3>150</h3>

                                        <p>New Orders</p>
                                      </div>
                                      <div class="icon">
                                        <i class="ion ion-bag"></i>
                                      </div>
                                      <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                  </div>

                                  <!-- ./col -->
                                  <div class="col-lg-3 col-6">
                                    <!-- small box -->
                                    <div class="small-box bg-success">
                                      <div class="inner">
                                        <h3>53<sup style="font-size: 20px">%</sup></h3>

                                        <p>Bounce Rate</p>
                                      </div>
                                      <div class="icon">
                                        <i class="ion ion-stats-bars"></i>
                                      </div>
                                      <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                  </div>

                                  <!-- ./col -->
                                  <div class="col-lg-3 col-6">
                                    <!-- small box -->
                                    <div class="small-box bg-warning">
                                      <div class="inner">
                                        <h3>44</h3>

                                        <p>User Registrations</p>
                                      </div>
                                      <div class="icon">
                                        <i class="ion ion-person-add"></i>
                                      </div>
                                      <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                  </div>

                                  <!-- ./col -->
                                  <div class="col-lg-3 col-6">
                                    <!-- small box -->
                                    <div class="small-box bg-danger">
                                      <div class="inner">
                                        <h3>65</h3>

                                        <p>Unique Visitors</p>
                                      </div>
                                      <div class="icon">
                                        <i class="ion ion-pie-graph"></i>
                                      </div>
                                      <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                  </div>
                                  <!-- ./col --> 
                            </div>--%>

                             <div class="row">
                                    <div class="col-12">
<%--                                        <div class="card">
                                          <div class="card-header">
                                            <h3 class="card-title">Fixed Header Table</h3>
                                        </div>--%>
                                        <!-- /.card-header -->
                                        <div class="card-body table-responsive" style="height: 200px;">
                                           <table class="table table-head-fixed text-nowrap">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>User</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Reason</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>183</td>
                                                <td>John Doe</td>
                                                <td>11-7-2014</td>
                                                <td><span class="tag tag-success">Approved</span></td>
                                                <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                                            </tr>
                                            <tr>
                                                <td>219</td>
                                                <td>Alexander Pierce</td>
                                                <td>11-7-2014</td>
                                                <td><span class="tag tag-warning">Pending</span></td>
                                                <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                                            </tr>
                                            <tr>
                                                <td>657</td>
                                                <td>Bob Doe</td>
                                                <td>11-7-2014</td>
                                                <td><span class="tag tag-primary">Approved</span></td>
                                                <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                                            </tr>
                                            <tr>
                                                <td>175</td>
                                                <td>Mike Doe</td>
                                                <td>11-7-2014</td>
                                                <td><span class="tag tag-danger">Denied</span></td>
                                                <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                                            </tr>
                                            <tr>
                                                <td>134</td>
                                                <td>Jim Doe</td>
                                                <td>11-7-2014</td>
                                                <td><span class="tag tag-success">Approved</span></td>
                                                <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                                            </tr>
                                            <tr>
                                                <td>494</td>
                                                <td>Victoria Doe</td>
                                                <td>11-7-2014</td>
                                                <td><span class="tag tag-warning">Pending</span></td>
                                                <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                                            </tr>
                                            <tr>
                                                <td>832</td>
                                                <td>Michael Doe</td>
                                                <td>11-7-2014</td>
                                                <td><span class="tag tag-primary">Approved</span></td>
                                                <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                                            </tr>
                                            <tr>
                                                <td>982</td>
                                                <td>Rocky Doe</td>
                                                <td>11-7-2014</td>
                                                <td><span class="tag tag-danger">Denied</span></td>
                                                <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                    </div>
                                </div>
                            <!---- Loading Bar ---->
                            <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                    <div id="processMessage" class="processMessage">
                                       <center>
                                           <span>
                                               <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"/>
                                               <br />Please Wait
                                           </span>
                                       </center>
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress> 

                        </ContentTemplate>
                    </asp:UpdatePanel>                
                </div>
            </div>
        </div>
    </section> 
</asp:Content>

