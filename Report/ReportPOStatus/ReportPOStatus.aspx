﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReportPOStatus.aspx.cs" Inherits="ReportPOStatus" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Content Header (Page header) -->
    <head runat="server"/> 
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Purchase Order Status</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate> 

                                <div class="row">
                                    <div class="col-sm-2">
                                        <label class="col-form-label">Type Report</label> 
                                        <asp:DropDownList id="TypeReport" runat="server" CssClass="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="TypeReport_SelectedIndexChanged">
                                            <asp:ListItem Value="SUMMARY">SUMMARY</asp:ListItem>
                                            <asp:ListItem Value="DETAIL">DETAIL</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>  
                                </div>

                                <div class="row">

                                    <div class="col-sm-3">
                                        <label class="col-form-label">Periode 1</label> 
                                        <div class="input-group">
                                           <asp:TextBox runat="server" ID="Periode1" CssClass="form-control" Enabled="false"/>  
                                           <asp:ImageButton id="CallPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" CssClass="btn btn-secondary"/>
                                        </div>
                                   </div>
                                    <ajaxToolkit:CalendarExtender ID="CEEPeriode1" runat="server" Enabled="True" TargetControlID="Periode1" PopupButtonID="CallPeriod1" Format="dd/MM/yyyy" />
                                    <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="Periode1" Mask="99/99/9999" MaskType="Date" UserDateFormat="None"/> 
                     
                                    <div class="col-sm-3">
                                         <label class="col-form-label">Periode 2</label> 
                                        <div class="input-group">
                                           <asp:TextBox runat="server" ID="Periode2" CssClass="form-control" Enabled="false"/>  
                                           <asp:ImageButton id="CallPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" CssClass="btn btn-secondary"/>
                                        </div>
                                    </div> 

                                    <ajaxToolkit:CalendarExtender ID="CEEPeriode2" runat="server" Enabled="True" TargetControlID="Periode2" PopupButtonID="CallPeriod2" Format="dd/MM/yyyy" />
                                    <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="Periode2" Mask="99/99/9999" MaskType="Date" UserDateFormat="None"/> 

                                    <div class="col-sm-3">
                                        <label class="col-form-label">Status</label>
                                        <asp:ListBox id="DDLStatus" runat="server" CssClass="form-control select2" multiple="multiple" SelectionMode="Multiple">
                                            <asp:ListItem Value="IN PROCESS" Text="IN PROCESS" />
                                            <asp:ListItem Value="APPROVED" Text="APPROVED" />
                                            <asp:ListItem Value="IN APPROVAL" Text="IN APPROVAL" />
                                            <asp:ListItem Value="REVISED" Text="REVISED" /> 
                                            <asp:ListItem Value="REJECTED" Text="REJECTED" /> 
                                        </asp:ListBox>
                                    </div> 

                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label class="col-form-label">Supplier</label> 
                                        <div class="input-group">
                                            <asp:Label ID="TxtSuppid" runat="server" Text="0" CssClass="col-form-label" Font-Bold="true" Visible="false"/> 
                                            <asp:TextBox id="TxtSuppname" runat="server" CssClass="form-control" Enabled="false" TextMode="MultiLine"/>
                                            <span class="input-group-btn">
                                               <asp:button runat="server" ID="BtnBindSupp" Text="Search" CssClass="btn btn-block btn-primary btn-sm btn-flat" OnClick="BtnBindSupp_Click" />
                                               <asp:button runat="server" ID="BtnEraseSupp" Text="Erase" CssClass="btn btn-block bg-gradient-secondary btn-sm" OnClick="BtnEraseSupp_Click" />
                                            </span>
                                        </div> 
                                    </div>  
                                    
                                    <div class="col-sm-3">
                                        <label class="col-form-label">No. PO</label> 
                                        <div class="input-group">
                                            <asp:Label ID="TxtPOId" runat="server" Text="0" CssClass="col-form-label" Font-Bold="true" Visible="false"/> 
                                            <asp:TextBox ID="TxtPO" runat="server" TextMode="MultiLine" CssClass="form-control" Enabled="false" />
                                             <span class="input-group-btn">                                 
                                                <asp:button runat="server" ID="BtnGetPO" Text="Search" CssClass="btn btn-block btn-primary btn-sm" OnClick="BtnGetPO_Click" />
                                                <asp:button runat="server" ID="BtnErasePO" Text="Erase" CssClass="btn btn-block bg-gradient-secondary btn-sm" OnClick="BtnErasePO_Click"/>
                                            </span>
                                        </div> 
                                    </div>  

                                    <div class="col-sm-3">
                                        <label class="col-form-label">No. IR</label> 
                                        <div class="input-group">
                                            <asp:Label ID="TxtIRId" runat="server" Text="0" CssClass="col-form-label" Font-Bold="true" Visible="false"/> 
                                            <asp:TextBox id="TxtIR" runat="server" CssClass="form-control" Enabled="false" TextMode="MultiLine"/>
                                             <span class="input-group-btn">                                 
                                                <asp:button runat="server" ID="BtnBindIR" Text="Search" CssClass="btn btn-block btn-primary btn-sm" OnClick="BtnBindIR_Click"/>
                                                <asp:button runat="server" ID="BtnEraseIR" Text="Erase" CssClass="btn btn-block bg-gradient-secondary btn-sm" OnClick="BtnEraseIR_Click"/>
                                            </span>
                                        </div> 
                                    </div> 
                                     
                                    <div class="col-sm-3">
                                        <label class="col-form-label">
                                            <asp:Label ID="LblItem" runat="server" Text="Nama Item" CssClass="col-form-label" Font-Bold="true" Visible="false"/> 
                                        </label>                               
                                        <div class="input-group">
                                            <asp:Label ID="TxtItemId" runat="server" Text="0" CssClass="col-form-label" Font-Bold="true" Visible="false"/> 
                                            <asp:TextBox id="TxtItem" runat="server" CssClass="form-control" Enabled="false" TextMode="MultiLine" Visible="false"/>
                                            <span class="input-group-btn">                                 
                                                <asp:button runat="server" ID="BtnBindItem" Text="Search" Visible="false" CssClass="btn btn-block btn-primary btn-sm" OnClick="BtnBindItem_Click" />
                                                <asp:button runat="server" ID="BtnEraseItem" Text="Erase" Visible="false" CssClass="btn btn-block bg-gradient-secondary btn-sm" OnClick="BtnEraseItem_Click" />
                                            </span>
                                        </div> 
                                    </div>  

                                </div>                                 

                                <hr />
                                <div class="row no-print" align="center">
                                    <div class="col-sm-12">  
                                        <asp:button runat="server" ID="BtnView" Text="View Data" CssClass="btn btn-primary" OnClick="BtnView_Click"/>
                                        <asp:button runat="server" ID="BtnToPDF" Text="Export PDF" class="btn btn-danger" OnClick="BtnToPDF_Click"/>
                                        <asp:button runat="server" ID="BtnToEXL" Text="Export Excel" class="btn btn-success" OnClick="BtnToEXL_Click" /> 
                                        <asp:button runat="server" ID="BtnClear" Text="Clear" class="btn btn-secondary" OnClick="BtnClear_Click"/> 
                                    </div>
                                </div> 
                                
                                <div class="row" align="center">
                                    <div class="col-sm-12"> 
                                        <div class="card-body table-responsive p-0" style="height: 100%;"> 
                                           <CR:CrystalReportViewer ID="CrvReportSOStatus" runat="server" Width="250px" Height="100%" AutoDataBind="True" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False" ToolPanelView="None" HasDrilldownTabs="False" />
                                        </div>
                                    </div>
                                </div>

                                <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div id="processMessage" class="processMessage">
                                            <center>
                                                <span>
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                                                </span>
                                            </center>
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                 
                                <!--- Start PopUp Message --->
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel id="PanelsMsg" runat="server" Visible="False" Width="70%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">
                                                        <asp:Label ID="lblInfo" runat="server" Text="" />
                                                    </h4>
                                                </div>

                                                <div class="modal-body">                                                   
                                                    <div class="col-sm-12">                                                         
                                                        <center>
                                                            <asp:Label ID="lblMessage" runat="server" Text="" />
                                                        </center> 
                                                    </div>       
                                                </div> 

                                                <div class="modal-footer">   
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <asp:Button ID="BtnOK" runat="server" CssClass="btn btn-danger" Text="OK" OnClick="BtnOK_Click"/>                      </center>
                                                    </div>                                                
                                                </div>
                                            </div>
                                        </asp:Panel> 

                                        <ajaxToolkit:ModalPopupExtender id="MpesMsg" runat="server" Drag="True" PopupControlID="PanelsMsg" TargetControlID="BesMsg" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                        <asp:Button id="BesMsg" runat="server" Visible="False" CausesValidation="False" />       

                                    </ContentTemplate>
                                </asp:UpdatePanel> 
                                <!--- End PopUp Message ---> 

                        </ContentTemplate> 
                        <Triggers>
                            <asp:PostBackTrigger ControlID="BtnToEXL" /> 
                            <asp:PostBackTrigger ControlID="BtnToPDF" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </section>

    <!-- Pop Up Data Supplier-->
    <asp:UpdatePanel ID="UpdatePanelSupp" runat="server">
        <ContentTemplate>
            <asp:Panel id="PanelSupp" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">List data Supplier</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row col-sm-12">
                            <div class="col-sm-2 custom-checkbox">
                                <label class="col-form-label">Filter</label>
                            </div>

                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <asp:DropDownList id="ddlfilterSupp" runat="server" CssClass="form-control select2">  
                                            <asp:ListItem value="suppname"> Supplier </asp:ListItem> 
                                            <asp:ListItem value="suppcode"> Kode </asp:ListItem>
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:TextBox runat="server" ID="txtfilterSupp" CssClass="form-control" />
                                </div>
                            </div>

                            <div class="col-sm-2 custom-checkbox">
                                <asp:Button ID="BtnFindSupp" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindSupp_Click" />
                            </div> 
                        </div>

                         <div class="row no-print" align="center">
                            <div class="col-sm-12">  
                                <asp:Button runat="server" ID="BtnCheckedALLSupp" Text="Checked ALL" CssClass="btn btn-primary" OnClick="BtnCheckedALLSupp_Click" />
                                <asp:button runat="server" ID="BtnCheckedNoneSupp" Text="Checked None" CssClass="btn btn-danger" OnClick="BtnCheckedNoneSupp_Click" />
                                <asp:button runat="server" ID="BtnCheckedViewSupp" Text="Checked View" CssClass="btn btn-success" OnClick="BtnCheckedViewSupp_Click" /> 
                            </div>
                        </div> 
                        <hr />
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <asp:GridView ID="gvSupp" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnPageIndexChanging="gvSupp_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbListSupp" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("suppid") %>'/>
                                            </ItemTemplate> 
                                        </asp:TemplateField> 
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <label>Data Not Found</label>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="BtnAddSupp" runat="server" CssClass="btn btn-default" Text="Add Data" OnClick="BtnAddSupp_Click" />   
                        <asp:Button ID="BtnCLoseSupp" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCLoseSupp_Click" />
                    </div>
                </div>
            </asp:Panel> 

            <ajaxToolkit:ModalPopupExtender id="MPESupp" runat="server" Drag="True" PopupControlID="PanelSupp" TargetControlID="BESupp" DropShadow="false" BackgroundCssClass="modalBackground" />
            <asp:Button id="BESupp" runat="server" Visible="False" CausesValidation="False" />

            <asp:UpdateProgress runat="server" ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanelSupp">
                <ProgressTemplate>
                    <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div id="processMessage" class="processMessage">
                        <center>
                            <span>
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                            </span>
                        </center>
                        <br />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- End Pop Up Data Supplier--> 

    <!-- Pop Up PO-->
    <asp:UpdatePanel ID="UpdatePanelPO" runat="server">
        <ContentTemplate>
            <asp:Panel id="PanelPO" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">List Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row col-sm-12">
                            <div class="col-sm-2 custom-checkbox">
                                <label class="col-form-label">Filter</label>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <asp:DropDownList id="DDLFilterPO" runat="server" CssClass="form-control select2">  
                                            <asp:ListItem value="orderid"> Draft </asp:ListItem>
                                            <asp:ListItem value="transno"> No. PO </asp:ListItem>  
                                            <asp:ListItem value="Suppname"> Supplier </asp:ListItem>  
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:TextBox runat="server" ID="TxtFilterPO" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4 custom-checkbox">
                                <asp:Button ID="BtnFindPO" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindPO_Click"/>
                            </div>
                        </div>

                          <div class="row no-print" align="center">
                            <div class="col-sm-12">  
                               <asp:Button runat="server" ID="BtnCheckedALLPO" Text="Checked ALL" CssClass="btn btn-primary" OnClick="BtnCheckedALLPO_Click"/>
                               <asp:button runat="server" ID="BtnCheckedNonePO" Text="Checked None" CssClass="btn btn-danger" OnClick="BtnCheckedNonePO_Click"/>
                               <asp:button runat="server" ID="BtnChekedViewPO" Text="View Checked" CssClass="btn btn-success" OnClick="BtnChekedViewPO_Click"/> 
                            </div>
                        </div> 
                        <hr />
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <asp:GridView ID="gvPO" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnPageIndexChanging="gvPO_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                               <asp:CheckBox ID="cbListDtlPO" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("orderid") %>'/>
                                            </ItemTemplate> 
                                        </asp:TemplateField>                                       
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <label>Data Not Found</label>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <asp:Button ID="BtnAddPO" runat="server" CssClass="btn btn-success" Text="Add Data" OnClick="BtnAddPO_Click"/>
                        <asp:Button ID="BtnCLosePO" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCLosePO_Click" /> 
                    </div>
                </div>
            </asp:Panel> 

            <ajaxToolkit:ModalPopupExtender id="MpePO" runat="server" Drag="True" PopupControlID="PanelPO" TargetControlID="BePO" DropShadow="false" BackgroundCssClass="modalBackground" />
            <asp:Button id="BePO" runat="server" Visible="False" CausesValidation="False" />

            <asp:UpdateProgress runat="server" ID="UpdateProgress3" AssociatedUpdatePanelID="UpdatePanelPO">
                <ProgressTemplate>
                    <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div id="processMessage" class="processMessage">
                        <center>
                            <span>
                               <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                            </span>
                        </center><br />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Pop Up PO-->

    <!-- Pop Up IR-->
    <asp:UpdatePanel ID="UpdatePanelIR" runat="server">
        <ContentTemplate>
            <asp:Panel id="PanelIR" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">List Item Received</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row col-sm-12">
                            <div class="col-sm-2 custom-checkbox">
                                <label class="col-form-label">Filter</label>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <asp:DropDownList id="DDLFilterIR" runat="server" CssClass="form-control select2">  
                                            <asp:ListItem value="irid"> Draft </asp:ListItem>
                                            <asp:ListItem value="irno"> IR. NO </asp:ListItem>  
                                            <asp:ListItem value="Suppname"> Supplier </asp:ListItem>  
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:TextBox runat="server" ID="TxtFilterIR" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4 custom-checkbox">
                                <asp:Button ID="BtnFindIR" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindIR_Click" />
                            </div>
                        </div>

                          <div class="row no-print" align="center">
                            <div class="col-sm-12">  
                               <asp:button runat="server" ID="BtnCheckedALLIR" Text="Checked ALL" CssClass="btn btn-primary" OnClick="BtnCheckedALLIR_Click" /> 
                               <asp:button runat="server" ID="BtnCheckedNoneIR" Text="Checked None" CssClass="btn btn-danger" OnClick="BtnCheckedNoneIR_Click" />
                               <asp:button runat="server" ID="BtnCheckedViewIR" Text="View Checked" CssClass="btn btn-success" OnClick="BtnCheckedViewIR_Click" /> 
                            </div>
                        </div> 
                        
                        <hr />
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <asp:GridView ID="gvIR" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnPageIndexChanging="gvIR_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                               <asp:CheckBox ID="cbListDtlIR" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("irid") %>'/>
                                            </ItemTemplate> 
                                        </asp:TemplateField> 
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <label>Data Not Found</label>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <asp:Button ID="BtnAddIR" runat="server" CssClass="btn btn-success" Text="Add Data" OnClick="BtnAddIR_Click" />
                        <asp:Button ID="BtnCloseIR" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCloseIR_Click" /> 
                    </div>
                </div>
            </asp:Panel> 

            <ajaxToolkit:ModalPopupExtender id="MpeIR" runat="server" Drag="True" PopupControlID="PanelIR" TargetControlID="BeIR" DropShadow="false" BackgroundCssClass="modalBackground" />
            <asp:Button id="BeIR" runat="server" Visible="False" CausesValidation="False" />

            <asp:UpdateProgress runat="server" ID="UpdateProgress4" AssociatedUpdatePanelID="UpdatePanelIR">
                <ProgressTemplate>
                    <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div id="processMessage" class="processMessage">
                        <center>
                            <span>
                               <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                            </span>
                        </center><br />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Pop Up IR-->

    <!-- Pop Up ITEM-->
    <asp:UpdatePanel ID="UpdatePanelItem" runat="server">
        <ContentTemplate>
            <asp:Panel id="PanelItem" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">List Item</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row col-sm-12">
                            <div class="col-sm-2 custom-checkbox">
                                <label class="col-form-label">Filter</label>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <asp:DropDownList id="DDLFilterItem" runat="server" CssClass="form-control select2">  
                                            <asp:ListItem value="itemcode"> Kode </asp:ListItem>
                                            <asp:ListItem value="itemname"> Nama </asp:ListItem>     
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:TextBox runat="server" ID="TxtFilterItem" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4 custom-checkbox">
                                <asp:Button ID="BtnFindItem" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindItem_Click" />
                            </div>
                        </div>

                          <div class="row no-print" align="center">
                            <div class="col-sm-12">  
                               <asp:button runat="server" ID="BtnCheckedALLItem" Text="Checked ALL" CssClass="btn btn-primary" OnClick="BtnCheckedALLItem_Click" /> 
                               <asp:button runat="server" ID="BtnCheckedNoneItem" Text="Checked None" CssClass="btn btn-danger" OnClick="BtnCheckedNoneItem_Click" />
                               <asp:button runat="server" ID="BtnCheckedViewItem" Text="View Checked" CssClass="btn btn-success" OnClick="BtnCheckedViewItem_Click" /> 
                            </div>
                        </div> 
                        <hr />
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <asp:GridView ID="gvItem" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnPageIndexChanging="gvItem_PageIndexChanging" >
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                               <asp:CheckBox ID="cbListDtlItem" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("itemid") %>'/>
                                            </ItemTemplate> 
                                        </asp:TemplateField> 
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <label>Data Not Found</label>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <asp:Button ID="BtnAddItem" runat="server" CssClass="btn btn-success" Text="Add Data" OnClick="BtnAddItem_Click" />
                        <asp:Button ID="BtnCloseItem" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCloseItem_Click" /> 
                    </div>
                </div>
            </asp:Panel> 

            <ajaxToolkit:ModalPopupExtender id="MpeItem" runat="server" Drag="True" PopupControlID="PanelItem" TargetControlID="BeItem" DropShadow="false" BackgroundCssClass="modalBackground" />
            <asp:Button id="BeItem" runat="server" Visible="False" CausesValidation="False" />

            <asp:UpdateProgress runat="server" ID="UpdateProgress5" AssociatedUpdatePanelID="UpdatePanelItem">
                <ProgressTemplate>
                    <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div id="processMessage" class="processMessage">
                        <center>
                            <span>
                               <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                            </span>
                        </center><br />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Pop Up ITEM-->
</asp:Content>

