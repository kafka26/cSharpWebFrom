﻿using System; 
using System.Web.UI.WebControls;
using System.Data;  
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI; 
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services; 
using static ClassGlobal;
using System.Reflection;

public partial class ReportPOStatus : System.Web.UI.Page
{
    private static ClassGlobal sVar = new ClassGlobal();
    private static ReportDocument ViewRpt = new ReportDocument();
    Int32 trnCode = 33;
    string sSql = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["username"].ToString()))
            {
                data = CekMenuByUser(Session["username"].ToString(), trnCode);
                if (data.Rows.Count > 0)
                {                   
                    if (!this.IsPostBack)
                    { 
                        Periode1.Text = DateTime.Now.ToString("01/MM/yyyy");
                        Periode2.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    } 
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            //this.sMsg(ex.ToString(), "1");
            Response.Redirect("~/Login.aspx");
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        //try
        //{
        //    if (ViewRpt != null)
        //    {
        //        if (ViewRpt.IsLoaded)
        //        {
        //            ViewRpt.Dispose();
        //            ViewRpt.Close();
        //        }
        //    }
        //}
        //catch
        //{
        //    ViewRpt.Dispose();
        //    ViewRpt.Close();
        //}
    }

    private void sMsg(string sMsg, string type)
    {
        if (type == "1")
        {
            lblInfo.Text = "ERROR";
        }
        else
        {
            lblInfo.Text = "INFORMATION";
        }
        lblMessage.Text = sMsg.ToUpper().ToString();
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, true);
    }

    protected void TypeReport_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.BindReport("VIEW", "PDF");
        if (TypeReport.SelectedValue == "DETAIL")
        {
            LblItem.Visible = true;
            TxtItem.Visible = true;
            BtnBindItem.Visible = true;
            BtnEraseItem.Visible = true;
        }
        else
        {
            LblItem.Visible = false;
            TxtItem.Visible = false;
            BtnBindItem.Visible = false;
            BtnEraseItem.Visible = false;
        }
    }
              
    private void UnloadView()
    {
        Response.Buffer = false;
        Response.ClearHeaders();
        Response.ClearContent();        
    }

    private void BindReport(string sType, string FileName)
    {
        try
        {
            string sGroup = "", fStatus = "";
            List<string> sStatus = new List<string>();
            DateTime sDate1 = toDate(Periode1.Text);
            DateTime sDate2 = toDate(Periode2.Text);
            if (TypeReport.SelectedValue.Trim() == "SUMMARY")
            {
                sSql = "SELECT om.sysid, ISNULL(im.sysid, 0) irid, om.trno, om.trdate trndate, ISNULL(im.trno, '-') irno, im.trdate irDate, UPPER(c.suppname) suppname, om.nettoamt, SUM(od.qty) QtyPO, SUM(ISNULL(im.qtyin, 0)) qtyin, SUM(od.qty) - SUM(ISNULL(im.qtyin, 0)) OSQTY, om.orderstatus FROM t_orderhdr om INNER JOIN m_supplier c ON c.suppid = om.custsuppid INNER JOIN t_orderdtl od ON om.sysid = od.sysid LEFT JOIN(Select im.sysid, im.trno, im.trdate, id.orderid, SUM(ISNULL(id.qtyin, 0.0)) qtyin, id.orderdtlid from t_inhdr im Inner Join t_indtl id ON id.sysid = im.sysid Where im.transcode = 2 Group By im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid) im ON im.orderid = om.sysid AND od.sysiddtl = im.orderdtlid WHERE om.transcode = 1";
                sGroup = " Group BY om.sysid, om.trno, om.trdate, UPPER(c.suppname), om.nettoamt, im.trno, im.trdate, om.orderstatus, im.sysid";
            }
            else
            {
                sSql = "Select om.sysid, om.trno, om.trdate trndate, UPPER(c.suppname) suppname, om.nettoamt, ISNULL(im.trno,'') irno, im.trdate irdate, i.itemid, i.itemcode, i.itemname, od.unitprice, od.totalamtdtl, od.discamtdtl, od.nettoamtdtl, od.qty, ISNULL(im.qtyin,0) qtyin, od.qty-ISNULL(im.qtyin,0) OSQTY, ISNULL(od.qty_akum,0.0) qty_akum, ISNULL(od.qty_akum_inv,0.0) qty_akum_inv, Isnull(od.qty_cancel,0.0) qty_cancel, ISNULL(od.qty_retur,0.0) qty_retur, u.genname, ISNULL(locationname,'') locationname FROM t_orderhdr om INNER JOIN m_supplier c ON c.suppid=om.custsuppid INNER JOIN t_orderdtl od ON om.sysid=od.sysid INNER JOIN m_items i ON i.itemid=od.itemid INNER JOIN m_general u ON u.genid=od.unitid AND u.gengroup='SATUAN' LEFT JOIN (Select im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, id.itemid, SUM(ISNULL(id.qtyin,0.0)) qtyin, l.locationname from t_inhdr im Inner Join t_indtl id ON id.sysid=im.sysid INNER JOIN m_location l ON l.locationid=id.locationid Where im.transcode=2 Group By im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, id.itemid, l.locationname) im ON im.orderid=om.sysid AND im.orderdtlid=od.sysiddtl WHERE om.transcode=1";
            }

            sSql += " AND om.trdate Between '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + "' AND '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + "'";

            if (TxtSuppid.Text != "0")
            {
                sSql += " And om.custsuppid In (" + TxtSuppid.Text + ") ";
            }


            if (TxtPOId.Text != "0")
            {
                sSql += " And om.sysid In (" + TxtPOId.Text + ") ";
            }
                       
            foreach (ListItem item in DDLStatus.Items)
            {
                if (item.Selected)
                {
                    //sStatus.Add(item.Value);
                    fStatus += "'" + item.Value.Trim() + "',";
                }
            }

            if (fStatus != "")
            {
                sSql += "AND orderstatus IN (" + fStatus.Substring(0, fStatus.Length - 1) + ")";
            }

            if (TxtIRId.Text != "0")
            {
                sSql += " And im.sysid In (" + TxtIRId.Text + ") ";
            }

            if (TypeReport.SelectedValue.Trim() == "DETAIL")
            {
                if (TxtItemId.Text != "0")
                {
                    sSql += " And i.itemid IN (" + TxtItemId.Text + ") ";
                }
            }

            sSql += sGroup + " ORDER BY om.trdate";
            ViewRpt.Load(Server.MapPath("~/Report/ReportPOStatus/RptPOStatus" + TypeReport.SelectedValue.Trim() + "_" + FileName + ".rpt"));
            DataView dtv = GetDataTable(sSql).DefaultView;
            ViewRpt.SetDataSource(dtv.ToTable());
            ViewRpt.SetParameterValue("UserID", Session["username"].ToString());
            ViewRpt.SetParameterValue("sStatus", "STATUS " + TypeReport.SelectedValue.Trim());
            ViewRpt.SetParameterValue("sPeriode", "Periode : " + Periode1.Text + " s/d " + Periode2.Text);
            SetDBLogonCrv(ViewRpt);

            if (sType == "XLS")
            {
                ViewRpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "ReportPOStatus" + DateTime.Now.ToString("dd/MM/yyyy")); 
            }
            else if (sType == "PDF")
            {
                ViewRpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "ReportPOStatus" + DateTime.Now.ToString("dd/MM/yyyy")); 
            }
            else
            {
                CrvReportSOStatus.ReportSource = ViewRpt;                
            }
            this.UnloadView();
        }
        catch (System.Threading.ThreadAbortException ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnView_Click(object sender, EventArgs e)
    {
        this.BindReport("VIEW", "PDF");
    }

    protected void BtnToPDF_Click(object sender, EventArgs e)
    {
        this.BindReport("PDF", "PDF");
    }

    protected void BtnToEXL_Click(object sender, EventArgs e)
    {
        this.BindReport("XLS", "XLS");
    }

    protected void BtnClear_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Report/ReportPOStatus/ReportPOStatus.aspx");
    }
    
    protected void BtnOK_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, false);
    }

    private void DrawTable(GridView gView, string[] HdrText, string[] DtField, string[] DtKeyNames)
    {
        if (gView.Columns.Count <= int.Parse(DtField.Length.ToString()))
        {
            gView.DataKeyNames = DtKeyNames;
            for (int i = 0; i < DtField.Length; i++)
            {
                System.Web.UI.WebControls.BoundField dRow = new BoundField();
                dRow.HeaderText = HdrText[i];
                dRow.HeaderStyle.Wrap = false;
                dRow.DataField = DtField[i];
                gView.Columns.Add(dRow);
            }
        }
    }

    //============= Start GetData Supplier =============//
    private void UpdateCheckedValueSupp()
    {
        DataTable objData = Session["GetFilterSuppPOStatus"] as DataTable;
        DataView objView = objData.DefaultView;
        if (objView.Count > 0)
        {
            for (int i = 0; i < gvSupp.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvSupp.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string suppid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            suppid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView.RowFilter = "suppid=" + suppid;
                    if (objView.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView[0]["checkvalue"] = "true";
                        }
                    }

                }
            }
        }
        objView.RowFilter = "";
        objData.AcceptChanges();
        Session["GetFilterSuppPOStatus"] = objData;

        //==================================//
        DataTable objData1 = Session["GetFilterSuppPOStatus_Hist"] as DataTable;
        DataView objView1 = objData1.DefaultView;
        if (objView1.Count > 0)
        {
            for (int i = 0; i < gvSupp.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvSupp.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string suppid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            suppid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView1.RowFilter = "suppid=" + suppid;
                    if (objView1.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView1[0]["checkvalue"] = "true";
                        }
                    }
                    objView1.RowFilter = "";
                    objData1.AcceptChanges();
                    Session["GetFilterSuppPOStatus_Hist"] = objData1;
                }
            }
        }
    }

    private void GetDataSupp()
    {
        try
        {
            string fStatus = "";
            List<string> sStatus = new List<string>();
            DateTime sDate1 = Convert.ToDateTime(Periode1.Text);
            DateTime sDate2 = Convert.ToDateTime(Periode2.Text);
            sSql = "SELECT 'false' checkvalue, c.suppid, c.suppcode, UPPER(c.suppname) suppname, UPPER(CAST(c.suppaddress AS varchar(MAX))) suppaddress FROM t_orderhdr om INNER JOIN m_supplier c ON c.suppid=om.custsuppid INNER JOIN t_orderdtl od ON om.sysid=od.sysid INNER JOIN m_items i ON i.itemid=od.itemid LEFT JOIN (Select im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, id.itemid from t_inhdr im Inner Join t_indtl id ON id.sysid=im.sysid INNER JOIN m_location l ON l.locationid=id.locationid Where im.transcode=2) im ON im.orderid=om.sysid AND im.orderdtlid=od.sysiddtl Where om.transcode=1 AND om.trdate Between '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + "' AND '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + "'";

            if (TxtPOId.Text != "0")
            {
                sSql += " And om.sysid In (" + TxtPOId.Text + ") ";
            }

            foreach (ListItem item in DDLStatus.Items)
            {
                if (item.Selected)
                {
                    fStatus += "'" + item.Value.Trim() + "',";
                }
            }

            if (fStatus != "")
            {
                sSql += "AND om.orderstatus IN (" + fStatus.Substring(0, fStatus.Length - 1) + ")";
            }

            if (TxtIRId.Text != "0")
            {
                sSql += " And im.sysid In (" + TxtIRId.Text + ") ";
            }

            if (TypeReport.SelectedValue.Trim() != "SUMMARY")
            {
                if (TxtItemId.Text != "0")
                {
                    sSql += " And i.itemid IN (" + TxtItemId.Text + ") ";
                }
            }

            sSql += "Group by c.suppid, c.suppcode, UPPER(c.suppname), CAST(c.suppaddress AS varchar(MAX))";

            string[] HdrText = { "Supp. Code", "Supplier", "Address" };
            string[] DtField = { "suppcode", "suppname", "suppaddress" };
            string[] DtKeyNames = { "suppid" };

            DrawTable(gvSupp, HdrText, DtField, DtKeyNames);
            gvSupp.DataSource = GetDataTable(sSql);
            gvSupp.DataBind();
            Session["GetFilterSuppPOStatus"] = GetDataTable(sSql);
            Session["GetFilterSuppPOStatus_Hist"] = GetDataTable(sSql);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnBindSupp_Click(object sender, EventArgs e)
    {
        try
        {
            this.GetDataSupp();
            sVar.SetModalPopUp(MPESupp, PanelSupp, BESupp, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnEraseSupp_Click(object sender, EventArgs e)
    {
        TxtSuppid.Text = "0";
        TxtSuppname.Text = "";
    }

    protected void BtnFindSupp_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetFilterSuppPOStatus"] as DataTable;
            if (objData.Rows.Count <= 0)
            {
                this.GetDataSupp();
            }
            else
            {
                this.UpdateCheckedValueSupp();
                DataView objView = objData.DefaultView;
                objView.RowFilter = ddlfilterSupp.SelectedValue.ToString() + " LIKE '%" + txtfilterSupp.Text.Trim() + "%'";
                Session["GetFilterSuppPOStatus_Hist"] = objView.ToTable();
                gvSupp.DataSource = Session["GetFilterSuppPOStatus_Hist"];
                gvSupp.DataBind();
                objView.RowFilter = "";
            }
            sVar.SetModalPopUp(MPESupp, PanelSupp, BESupp, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }
      
    protected void BtnCheckedALLSupp_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueSupp();
            DataTable objData = Session["GetFilterSuppPOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "suppid=" + objData.Rows[i]["suppid"];
                objView[0]["checkvalue"] = "true";
                objData.Rows[i]["checkvalue"] = "true";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetFilterSuppPOStatus"] = objData;
            Session["GetFilterSuppPOStatus_Hist"] = objData;
            gvSupp.DataSource = objData;
            gvSupp.DataBind();
            sVar.SetModalPopUp(MPESupp, PanelSupp, BESupp, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnCheckedNoneSupp_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetFilterSuppPOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "suppid=" + objData.Rows[i]["suppid"];
                objView[0]["checkvalue"] = "false";
                objData.Rows[i]["checkvalue"] = "false";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetFilterSuppPOStatus"] = objData;
            Session["GetFilterSuppPOStatus_Hist"] = objData;
            gvSupp.DataSource = objData;
            gvSupp.DataBind();
            sVar.SetModalPopUp(MPESupp, PanelSupp, BESupp, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedViewSupp_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueSupp();
            DataTable objData = Session["GetFilterSuppPOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count > 0)
            {
                Session["GetFilterSuppPOStatus_Hist"] = objView.ToTable();
                gvSupp.DataSource = Session["GetFilterSuppPOStatus_Hist"];
                gvSupp.DataBind();
                objView.RowFilter = "";
                sVar.SetModalPopUp(MPESupp, PanelSupp, BESupp, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnAddSupp_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueSupp();
            DataTable objData = Session["GetFilterSuppPOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count <= 0)
            {
                this.sMsg("Sorry, Please select list to add data..!!<br>", "2");
            }

            foreach (DataRowView sRow in objView)
            {
                if (TxtSuppid.Text != "0")
                {
                    TxtSuppid.Text += "," + sRow["suppid"].ToString();
                    TxtSuppname.Text += ";" + sRow["suppcode"].ToString();
                }
                else
                {
                    TxtSuppid.Text += "," + sRow["suppid"].ToString();
                    TxtSuppname.Text += ";" + sRow["suppcode"].ToString();
                }
            }
            objView.RowFilter = "";
            sVar.SetModalPopUp(MPESupp, PanelSupp, BESupp, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnCLoseSupp_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MPESupp, PanelSupp, BESupp, false);
    }

    protected void gvSupp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValueSupp();
        gvSupp.PageIndex = e.NewPageIndex;
        gvSupp.DataSource = Session["GetFilterSuppPOStatus_Hist"];
        gvSupp.DataBind();
        sVar.SetModalPopUp(MPESupp, PanelSupp, BESupp, true);
    }

    //============= End GetData Supplier =============//

    //============= Start GetData PO =============//
    private void GetDataPO()
    {
        try
        {
            string fStatus = "";
            List<string> sStatus = new List<string>();
            DateTime sDate1 = Convert.ToDateTime(Periode1.Text);
            DateTime sDate2 = Convert.ToDateTime(Periode2.Text); 
            sSql = "Select 'false' checkvalue, CAST(om.sysid AS varchar(20)) orderid, om.trno transno, om.trdate trndate, UPPER(c.suppname) suppname, om.orderstatus FROM t_orderhdr om INNER JOIN m_supplier c On c.suppid=om.custsuppid INNER JOIN t_orderdtl od On om.sysid=od.sysid LEFT JOIN (Select id.orderid, id.orderdtlid from t_inhdr im Inner Join t_indtl id ON id.sysid=im.sysid Where im.transcode=2 Group By im.sysid, id.orderid, id.orderdtlid) im ON im.orderid=om.sysid AND im.orderdtlid=od.sysiddtl WHERE om.transcode=1 AND om.trdate Between '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + "' AND '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + "'";
           
            foreach (ListItem item in DDLStatus.Items)
            {
                if (item.Selected)
                {
                    fStatus += "'" + item.Value.Trim() + "',";
                }
            }

            if (fStatus != "")
            {
                sSql += "AND om.orderstatus IN (" + fStatus.Substring(0, fStatus.Length - 1) + ")";
            }

            if (TxtIRId.Text != "0")
            {
                sSql += " And im.sysid In (" + TxtIRId.Text + ") ";
            }

            if (TypeReport.SelectedValue.Trim() == "SUMMARY")
            {
                if (TxtItemId.Text != "0")
                {
                    sSql += " And i.itemid IN (" + TxtItemId.Text + ") ";
                }
            }

            if (TxtSuppid.Text != "0")
            {
                sSql += " And om.custsuppid IN (" + TxtSuppid.Text + ") ";
            }

            sSql += " Group By om.sysid, om.trno, om.trdate, c.suppname, om.orderstatus ORDER BY om.sysid"; 
            string[] HdrText = { "Draft", "No. PO", "Tanggal", "Supplier", "Status" };
            string[] DtField = { "orderid", "transno", "trndate", "suppname", "orderstatus" };
            string[] DtKeyNames = { "orderid" };
            DrawTable(gvPO, HdrText, DtField, DtKeyNames);

            Session["GetFilterPOStatus_Hist"] = GetDataTable(sSql);
            Session["GetFilterPOStatus"] = GetDataTable(sSql);
            gvPO.DataSource = GetDataTable(sSql);
            gvPO.DataBind();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    private void UpdateCheckedValuePO()
    {
        DataTable objData = Session["GetFilterPOStatus"] as DataTable;
        DataView objView = objData.DefaultView;
        if (objView.Count > 0)
        {
            for (int i = 0; i < gvPO.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvPO.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string orderid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            orderid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView.RowFilter = "orderid=" + orderid;
                    if (objView.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView[0]["checkvalue"] = "true";
                        }
                    }

                }
            }
        }
        objView.RowFilter = "";
        objData.AcceptChanges();
        Session["GetFilterPOStatus"] = objData;

        //==================================//
        DataTable objData1 = Session["GetFilterPOStatus_Hist"] as DataTable;
        DataView objView1 = objData1.DefaultView;
        if (objView1.Count > 0)
        {
            for (int i = 0; i < gvPO.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvPO.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string orderid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            orderid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView1.RowFilter = "orderid=" + orderid;
                    if (objView1.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView1[0]["checkvalue"] = "true";
                        }
                    }
                    objView1.RowFilter = "";
                    objData1.AcceptChanges();
                    Session["GetFilterPOStatus_Hist"] = objData1;
                }
            }
        }
    }

    protected void BtnGetPO_Click(object sender, EventArgs e)
    {
        try
        {
            this.GetDataPO();
            sVar.SetModalPopUp(MpePO, PanelPO, BePO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnErasePO_Click(object sender, EventArgs e)
    {
        TxtPOId.Text = "0";
        TxtPO.Text = "";
    }

    protected void BtnFindPO_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetFilterPOStatus"] as DataTable;
            if (objData.Rows.Count <= 0)
            {
                this.GetDataPO();
            }
            else
            {
                this.UpdateCheckedValuePO();
                DataView objView = objData.DefaultView;
                objView.RowFilter = DDLFilterPO.SelectedValue.ToString() + " LIKE '%" + TxtFilterPO.Text.Trim() + "%'";
                Session["GetFilterPOStatus_Hist"] = objView.ToTable();
                gvPO.DataSource = Session["GetFilterPOStatus_Hist"];
                gvPO.DataBind();
                objView.RowFilter = "";
            }
            sVar.SetModalPopUp(MpePO, PanelPO, BePO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedALLPO_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValuePO();
            DataTable objData = Session["GetFilterPOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "orderid=" + objData.Rows[i]["orderid"];
                objView[0]["checkvalue"] = "true";
                objData.Rows[i]["checkvalue"] = "true";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetFilterPOStatus"] = objData;
            Session["GetFilterPOStatus_Hist"] = objData;
            gvPO.DataSource = objData;
            gvPO.DataBind();
            sVar.SetModalPopUp(MpePO, PanelPO, BePO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnCheckedNonePO_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetFilterPOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "orderid=" + objData.Rows[i]["orderid"];
                objView[0]["checkvalue"] = "false";
                objData.Rows[i]["checkvalue"] = "false";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetFilterPOStatus"] = objData;
            Session["GetFilterPOStatus_Hist"] = objData;
            gvPO.DataSource = objData;
            gvPO.DataBind();
            sVar.SetModalPopUp(MpePO, PanelPO, BePO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnChekedViewPO_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValuePO();
            DataTable objData = Session["GetFilterPOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count > 0)
            {
                Session["GetFilterPOStatus_Hist"] = objView.ToTable();
                gvPO.DataSource = Session["GetFilterPOStatus_Hist"];
                gvPO.DataBind();
                objView.RowFilter = "";
                sVar.SetModalPopUp(MpePO, PanelPO, BePO, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnAddPO_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValuePO();
            DataTable objData = Session["GetFilterPOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count <= 0)
            {
                this.sMsg("Sorry, Please select list to add data..!!<br>", "2");
            }

            foreach (DataRowView sRow in objView)
            {
                if (TxtSuppid.Text != "0")
                {
                    TxtPOId.Text += "," + sRow["orderid"].ToString();
                    TxtPO.Text += ";" + sRow["transno"].ToString();
                }
                else
                {
                    TxtPOId.Text += "," + sRow["orderid"].ToString();
                    TxtPO.Text += ";" + sRow["transno"].ToString();
                }
            }
            objView.RowFilter = "";
            sVar.SetModalPopUp(MpePO, PanelPO, BePO, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnCLosePO_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpePO, PanelPO, BePO, false);
    }

    protected void gvPO_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValuePO();
        gvPO.PageIndex = e.NewPageIndex;
        gvPO.DataSource = Session["GetFilterPOStatus_Hist"];
        gvPO.DataBind();
        sVar.SetModalPopUp(MpePO, PanelPO, BePO, true);
    }
    //============= End GetData PO =============//

    //============= Start GetData IR =============//
    private void UpdateCheckedValueIR()
    {

        DataTable objData = Session["GetIRPOStatus"] as DataTable;
        DataView objView = objData.DefaultView;
        if (objView.Count > 0)
        {
            for (int i = 0; i < gvIR.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvIR.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string irid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            irid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView.RowFilter = "irid=" + irid;
                    if (objView.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView[0]["checkvalue"] = "true";
                        }
                    }

                }
            }
        }
        objView.RowFilter = "";
        objData.AcceptChanges();
        Session["GetIRPOStatus"] = objData;

        //================//
        DataTable objData1 = Session["GetIRPOStatus_Hist"] as DataTable;
        DataView objView1 = objData1.DefaultView;
        if (objView1.Count > 0)
        {

            for (int i = 0; i < gvIR.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvIR.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string irid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            irid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView1.RowFilter = "irid=" + irid;
                    if (objView1.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView1[0]["checkvalue"] = "true";
                        }
                    }
                }
            }
        }
        objView1.RowFilter = "";
        objData1.AcceptChanges();
        Session["GetIRPOStatus_Hist"] = objData1;
    }

    private void GetDataIR()
    {
        try
        {
            DateTime sDate1 = Convert.ToDateTime(Periode1.Text);
            DateTime sDate2 = Convert.ToDateTime(Periode2.Text);
            List<string> sStatus = new List<string>();
            string fStatus = "";
            sSql = "SELECT 'false' checkvalue, CAST(im.sysid as varchar(20)) irid, im.orderid, ISNULL(im.trno, '') irno, im.trdate irdate, om.trno orderno, c.suppname FROM t_orderhdr om INNER JOIN m_supplier c ON c.suppid = om.custsuppid INNER JOIN t_orderdtl od ON om.sysid = od.sysid INNER JOIN (Select im.sysid, im.trno, im.trdate, id.orderid from t_inhdr im Inner Join t_indtl id ON id.sysid = im.sysid Where im.transcode = 2 Group By im.sysid, im.trno, im.trdate, id.orderid) im ON im.orderid = om.sysid WHERE om.transcode = 1 AND om.trdate Between '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + "' AND '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + "'";

            if (TxtPOId.Text != "0")
            {
                sSql += " And om.sysid In (" + TxtPOId.Text + ") ";
            }

            foreach (ListItem item in DDLStatus.Items)
            {
                if (item.Selected)
                {
                    fStatus += "'" + item.Value.Trim() + "',";
                }
            }

            if (fStatus != "")
            {
                sSql += "AND om.orderstatus IN (" + fStatus.Substring(0, fStatus.Length - 1) + ")";
            }

            if (TypeReport.SelectedValue.Trim() == "SUMMARY")
            {
                if (TxtItemId.Text != "0")
                {
                    sSql += " And i.itemid IN (" + TxtItemId.Text + ") ";
                }
            }

            if (TxtSuppid.Text != "0")
            {
                sSql += " And om.custsuppid IN (" + TxtSuppid.Text + ") ";
            }

            sSql += " Group by im.sysid, im.orderid, ISNULL(im.trno,''), im.trdate, om.trno, c.suppname Order By im.trdate";
            string[] HdrText = { "Draft", "No. MR", "Tanggal", "Supplier" };
            string[] DtField = { "irid", "irno", "irdate", "suppname" };
            string[] DtKeyNames = { "irid" };
            DrawTable(gvIR, HdrText, DtField, DtKeyNames);

            Session["GetIRPOStatus_Hist"] = GetDataTable(sSql);
            Session["GetIRPOStatus"] = GetDataTable(sSql);
            gvIR.DataSource = GetDataTable(sSql);
            gvIR.DataBind();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnBindIR_Click(object sender, EventArgs e)
    {
        try
        {
            this.GetDataIR();
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnEraseIR_Click(object sender, EventArgs e)
    {
        TxtIRId.Text = "0";
        TxtIR.Text = "";
    }

    protected void BtnFindIR_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetIRPOStatus"] as DataTable;
            if (objData.Rows.Count <= 0)
            {
                this.GetDataIR();
            }
            else
            {
                this.UpdateCheckedValueIR();
                DataView objView = objData.DefaultView;
                objView.RowFilter = DDLFilterIR.SelectedValue.ToString() + " LIKE '%" + TxtFilterIR.Text.Trim() + "%'";
                Session["GetIRPOStatus_Hist"] = objView.ToTable();
                gvIR.DataSource = Session["GetIRPOStatus_Hist"];
                gvIR.DataBind();
                objView.RowFilter = "";
            }
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedALLIR_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueIR();
            DataTable objData = Session["GetIRPOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "irid=" + objData.Rows[i]["irid"];
                objView[0]["checkvalue"] = "true";
                objData.Rows[i]["checkvalue"] = "true";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetIRPOStatus"] = objData;
            Session["GetIRPOStatus_Hist"] = objData;
            gvIR.DataSource = objData;
            gvIR.DataBind();
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnCheckedNoneIR_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetIRPOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "orderid=" + objData.Rows[i]["orderid"];
                objView[0]["checkvalue"] = "false";
                objData.Rows[i]["checkvalue"] = "false";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetIRPOStatus"] = objData;
            Session["GetIRPOStatus_Hist"] = objData;
            gvIR.DataSource = objData;
            gvIR.DataBind();
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedViewIR_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueIR();
            DataTable objData = Session["GetIRPOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count > 0)
            {
                Session["GetIRPOStatus_Hist"] = objView.ToTable();
                gvIR.DataSource = Session["GetIRPOStatus_Hist"];
                gvIR.DataBind();
                objView.RowFilter = "";
                sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnAddIR_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueIR();
            DataTable objData = Session["GetIRPOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count <= 0)
            {
                this.sMsg("Sorry, Please select list to add data..!!<br>", "2");
            }

            foreach (DataRowView sRow in objView)
            {
                if (TxtIRId.Text != "0")
                {
                    TxtIRId.Text += "," + sRow["irid"].ToString();
                    TxtIR.Text += ";" + sRow["irno"].ToString();
                }
                else
                {
                    TxtIRId.Text += "," + sRow["irid"].ToString();
                    TxtIR.Text += ";" + sRow["irno"].ToString();
                }
            }
            objView.RowFilter = "";
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnCloseIR_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, false);
    }

    protected void gvIR_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValueIR();
        gvIR.PageIndex = e.NewPageIndex;
        gvIR.DataSource = Session["GetIRPOStatus_Hist"];
        gvIR.DataBind();
        sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, true);
    }
    //============= End GetData IR =============//

    //============= Start GetData Item =============//

    private void GetDataItem()
    {
        try
        {
            DateTime sDate1 = Convert.ToDateTime(Periode1.Text);
            DateTime sDate2 = Convert.ToDateTime(Periode2.Text);
            List<string> sStatus = new List<string>();
            string fStatus = "";

            sSql = "SELECT 'false' checkvalue, i.itemid, i.itemcode, i.itemname, u.genname FROM t_orderhdr om INNER JOIN t_orderdtl od ON om.sysid=od.sysid INNER JOIN m_items i ON i.itemid = od.itemid INNER JOIN m_general u ON u.genid = od.unitid AND u.gengroup = 'SATUAN' LEFT JOIN (Select im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, id.itemid, SUM(ISNULL(id.qtyin, 0.0)) qtyin, l.locationname from t_inhdr im Inner Join t_indtl id ON id.sysid = im.sysid INNER JOIN m_location l ON l.locationid = id.locationid Where im.transcode = 2 Group By im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, id.itemid, l.locationname) im ON im.orderid = om.sysid AND im.orderdtlid = od.sysiddtl WHERE om.transcode = 1 AND om.trdate Between '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + "' AND '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + "'";

            if (TxtPOId.Text != "0")
            {
                sSql += " And om.sysid In (" + TxtPOId.Text + ") ";
            }

            foreach (ListItem item in DDLStatus.Items)
            {
                if (item.Selected)
                {
                    fStatus += "'" + item.Value.Trim() + "',";
                }
            }


            if (fStatus != "")
            {
                sSql += "AND om.orderstatus IN (" + fStatus.Substring(0, fStatus.Length - 1) + ")";
            }

            if (TxtIRId.Text != "0")
            {
                sSql += " And im.sysid In (" + TxtIRId.Text + ") ";
            }

            if (TxtSuppid.Text != "0")
            {
                sSql += " And om.custsuppid IN (" + TxtSuppid.Text + ") ";
            }
            sSql += " Group BY i.itemid, i.itemcode, i.itemname, u.genname Order By i.itemcode";

            string[] HdrText = { "Code", "Item Name", "Unit" };
            string[] DtField = { "itemcode", "itemname", "genname" };
            string[] DtKeyNames = { "itemid" };
            DrawTable(gvItem, HdrText, DtField, DtKeyNames);

            Session["GetItemPOStatus_Hist"] = GetDataTable(sSql);
            Session["GetItemPOStatus"] = GetDataTable(sSql);
            gvItem.DataSource = GetDataTable(sSql);
            gvItem.DataBind();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    private void UpdateCheckedValueItem()
    {

        DataTable objData = Session["GetItemPOStatus"] as DataTable;
        DataView objView = objData.DefaultView;
        if (objView.Count > 0)
        {
            for (int i = 0; i < gvItem.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string itemid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            itemid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView.RowFilter = "itemid=" + itemid;
                    if (objView.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView[0]["checkvalue"] = "true";
                        }
                    }

                }
            }
        }
        objView.RowFilter = "";
        objData.AcceptChanges();
        Session["GetItemPOStatus_Hist"] = objData;

        //================//
        DataTable objData1 = Session["GetItemPOStatus_Hist"] as DataTable;
        DataView objView1 = objData1.DefaultView;
        if (objView1.Count > 0)
        {

            for (int i = 0; i < gvItem.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string itemid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            itemid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView1.RowFilter = "itemid=" + itemid;
                    if (objView1.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView1[0]["checkvalue"] = "true";
                        }
                    }
                }
            }
        }
        objView1.RowFilter = "";
        objData1.AcceptChanges();
        Session["GetItemPOStatus_Hist"] = objData1;
    }

    protected void BtnBindItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.GetDataItem();
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnEraseItem_Click(object sender, EventArgs e)
    {
        TxtItem.Text = "";
        TxtItemId.Text = "0";
    }

    protected void BtnFindItem_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetItemPOStatus"] as DataTable;
            if (objData.Rows.Count <= 0)
            {
                this.GetDataItem();
            }
            else
            {
                this.UpdateCheckedValueItem();
                DataView objView = objData.DefaultView;
                objView.RowFilter = DDLFilterItem.SelectedValue.ToString() + " LIKE '%" + TxtFilterItem.Text.Trim() + "%'";
                Session["GetItemPOStatus_Hist"] = objView.ToTable();
                gvItem.DataSource = Session["GetItemPOStatus_Hist"];
                gvItem.DataBind();
                objView.RowFilter = "";
            }
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedALLItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueItem();
            DataTable objData = Session["GetItemPOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "itemid=" + objData.Rows[i]["itemid"];
                objView[0]["checkvalue"] = "True";
                objData.Rows[i]["checkvalue"] = "True";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetItemPOStatus"] = objData;
            Session["GetItemPOStatus_Hist"] = objData;
            gvItem.DataSource = objData;
            gvItem.DataBind();
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedNoneItem_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetItemPOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "itemid=" + objData.Rows[i]["itemid"];
                objView[0]["checkvalue"] = "false";
                objData.Rows[i]["checkvalue"] = "false";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetItemPOStatus"] = objData;
            Session["GetItemPOStatus_Hist"] = objData;
            gvItem.DataSource = objData;
            gvItem.DataBind();
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedViewItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueItem();
            DataTable objData = Session["GetItemPOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count > 0)
            {
                Session["GetItemPOStatus"] = objView.ToTable();
                gvItem.DataSource = Session["GetItemPOStatus_Hist"];
                gvItem.DataBind();
                objView.RowFilter = "";
                sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnAddItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueItem();
            DataTable objData = Session["GetItemPOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count <= 0)
            {
                this.sMsg("Sorry, Please select list to add data..!!<br>", "2");
            }

            foreach (DataRowView sRow in objView)
            {
                if (TxtItemId.Text != "0")
                {
                    TxtItemId.Text += "," + sRow["itemid"].ToString();
                    TxtItem.Text += ";" + sRow["itemcode"].ToString();
                }
                else
                {
                    TxtItemId.Text += "," + sRow["itemid"].ToString();
                    TxtItem.Text += ";" + sRow["itemcode"].ToString();
                }
            }
            objView.RowFilter = "";
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCloseItem_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
    }

    protected void gvItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValueItem();
        gvItem.PageIndex = e.NewPageIndex;
        gvItem.DataSource = Session["GetItemPOStatus_Hist"];
        gvItem.DataBind();
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
    }
    //============= End GetData Item =============//

    //DateTime sDate1 = toDate(Periode1.Text);
    //DateTime sDate2 = toDate(Periode2.Text);
    //string fDate1 = sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year;
    //string fDate2 = sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year;
    //if (TxtSuppid.Text != "0")
    //{
    //    sSql += " And om.custsuppid In (" + TxtSuppid.Text + ") ";
    //}

    //DataTable sData = new DataTable();

    //SqlConnection con = new SqlConnection(Conn);
    //SqlCommand cmd = new SqlCommand("ReportPOStatus");
    //cmd.Parameters.AddWithValue("@setAction", TypeReport.SelectedValue.ToString().Trim());
    //cmd.Parameters.AddWithValue("@fDate1", fDate1);
    //cmd.Parameters.AddWithValue("@fDate2", fDate2);
    //cmd.Parameters.AddWithValue("@fSupplier", sSql);
    //SqlDataAdapter sda = new SqlDataAdapter();
    //cmd.CommandType = CommandType.StoredProcedure;
    //cmd.Connection = con;
    //sda.SelectCommand = cmd;
    //sda.Fill(sData);         
    //cKon.Close();
}