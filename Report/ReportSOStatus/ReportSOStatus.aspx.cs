﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Reflection;
using static ClassGlobal;

public partial class Report_ReportSOStatus_ReportSOStatus : System.Web.UI.Page
{
    private static  ClassGlobal sVar = new ClassGlobal();
    private static ReportDocument ViewRpt = new ReportDocument();
    Int32 trnCode = 35;
    string sSql = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["username"].ToString()))
            {
                DataTable sdata = CekMenuByUser(Session["username"].ToString(), trnCode);
                if (sdata.Rows.Count > 0)
                {                   
                    if (!this.IsPostBack)
                    { 
                        Periode1.Text = DateTime.Now.ToString("01/MM/yyyy");
                        Periode2.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    } 
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            //this.sMsg(ex.ToString(), "1");
            Response.Redirect("~/Login.aspx");
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        //try
        //{
        //    if (ViewRpt != null)
        //    {
        //        if (ViewRpt.IsLoaded)
        //        {
        //            ViewRpt.Dispose();
        //            ViewRpt.Close();
        //        }
        //    }
        //}
        //catch
        //{
        //    ViewRpt.Dispose();
        //    ViewRpt.Close();
        //}
    }

    private void sMsg(string sMsg, string type)
    {
        if (type == "1") lblInfo.Text = "ERROR";
        else lblInfo.Text = "INFORMATION";
        lblMessage.Text = sMsg.ToUpper().ToString();
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, true);
    }

    protected void TypeReport_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.BindReport("VIEW", "PDF");
        if (TypeReport.SelectedValue == "DETAIL")
        {
            LblItem.Visible = true;
            TxtItem.Visible = true;
            BtnBindItem.Visible = true;
            BtnEraseItem.Visible = true;
        }
        else
        {
            LblItem.Visible = false;
            TxtItem.Visible = false;
            BtnBindItem.Visible = false;
            BtnEraseItem.Visible = false;
        }
    }

    private void DrawTable(GridView gView, string[] HdrText, string[] DtField, string[] DtKeyNames)
    {
        if (gView.Columns.Count <= int.Parse(DtField.Length.ToString()))
        {
            gView.DataKeyNames = DtKeyNames;
            for (int i = 0; i < DtField.Length; i++)
            {
                System.Web.UI.WebControls.BoundField dRow = new BoundField();
                dRow.HeaderText = HdrText[i];
                dRow.HeaderStyle.Wrap = false;
                dRow.DataField = DtField[i];
                gView.Columns.Add(dRow);
            }
        }
    }

    //=============Start GetData Customer =============//
    private void GetDataCust()
    {
        try
        {
            string fStatus = "";
            List<string> sStatus = new List<string>();
            DateTime sDate1 = Convert.ToDateTime(Periode1.Text);
            DateTime sDate2 = Convert.ToDateTime(Periode2.Text);
            sSql = "SELECT 'false' checkvalue, c.custid, c.custcode, UPPER(c.custname) custname, UPPER(CAST(c.cust_address AS varchar(MAX))) cust_address FROM t_orderhdr om INNER JOIN m_customer c ON c.custid=om.custsuppid INNER JOIN t_orderdtl od ON om.sysid=od.sysid INNER JOIN m_items i ON i.itemid=od.itemid LEFT JOIN (Select im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, id.itemid from t_inhdr im Inner Join t_indtl id ON id.sysid=im.sysid INNER JOIN m_location l ON l.locationid=id.locationid Where im.transcode=5) im ON im.orderid=om.sysid AND im.orderdtlid=od.sysiddtl Where om.transcode=4 AND om.trdate Between '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + "' AND '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + "'";

            if (TxtSOId.Text != "0") sSql += " And om.sysid In (" + TxtSOId.Text + ") ";

            foreach (ListItem item in DDLStatus.Items)
                if (item.Selected) fStatus += "'" + item.Value.Trim() + "',";

            if (fStatus != "")
                sSql += "AND om.orderstatus IN (" + fStatus.Substring(0, fStatus.Length - 1) + ")";

            if (TxtSJId.Text != "0") sSql += " And im.sysid In (" + TxtSJId.Text + ") ";

            if (TypeReport.SelectedValue.Trim() == "SUMMARY" & TxtItemId.Text != "0")
                sSql += " And i.itemid IN (" + TxtItemId.Text + ") ";

            sSql += "Group by c.custid, c.custcode, UPPER(c.custname), CAST(c.cust_address AS varchar(MAX))";
            string[] HdrText = { "Code", "Customer", "Address" };
            string[] DtField = { "custcode", "custname", "cust_address" };
            string[] DtKeyNames = { "custid" };

            DrawTable(gvCust, HdrText, DtField, DtKeyNames);

            gvCust.DataSource = GetDataTable(sSql);
            gvCust.DataBind();
            Session["GetFilterCustSOStatus"] = GetDataTable(sSql);
            Session["GetFilterCustSOStatus_Hist"] = GetDataTable(sSql);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnBindCust_Click(object sender, EventArgs e)
    {
        try
        {
            this.GetDataCust();
            sVar.SetModalPopUp(MPECust, PanelCust, BECust, true);
        }
        catch (Exception ex)
        { 
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnEraseCust_Click(object sender, EventArgs e)
    {
        TxtCustid.Text = "0";
        TxtCustname.Text = "";
    }

    protected void BtnCLoseCust_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MPECust, PanelCust, BECust, false);
    }
    
    private void UpdateCheckedValueCust()
    {
        DataTable objData = Session["GetFilterCustSOStatus"] as DataTable;
        DataView objView = objData.DefaultView;
        if (objView.Count > 0)
        {
            for (int i = 0; i < gvCust.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvCust.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string custid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            custid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView.RowFilter = "custid=" + custid;
                    if (objView.Count == 1 & cbCheck == true)
                        objView[0]["checkvalue"] = "true";

                }
            }
        }
        objView.RowFilter = "";
        objData.AcceptChanges();
        Session["GetFilterCustSOStatus"] = objData;

        //==================================//
        DataTable objData1 = Session["GetFilterCustSOStatus_Hist"] as DataTable;
        DataView objView1 = objData1.DefaultView;
        if (objView1.Count > 0)
        {
            for (int i = 0; i < gvCust.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvCust.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string custid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            custid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView1.RowFilter = "custid=" + custid;
                    if (objView1.Count == 1 && cbCheck == true)
                        objView1[0]["checkvalue"] = "true";
                    objView1.RowFilter = "";
                    objData1.AcceptChanges();
                    Session["GetFilterCustSOStatus_Hist"] = objData1;
                }
            }
        }
    }

    protected void BtnFindCust_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetFilterCustSOStatus"] as DataTable;
            if (objData.Rows.Count <= 0) this.GetDataCust();
            else
            {
                this.UpdateCheckedValueCust();
                DataView objView = objData.DefaultView;
                objView.RowFilter = ddlfilterCust.SelectedValue.ToString() + " LIKE '%" + txtfilterCust.Text.Trim() + "%'";
                Session["GetFilterCustSOStatus_Hist"] = objView.ToTable();
                gvCust.DataSource = Session["GetFilterCustSOStatus_Hist"];
                gvCust.DataBind();
                objView.RowFilter = "";
            }
            sVar.SetModalPopUp(MPECust, PanelCust, BECust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }        
    }

    protected void BtnCheckedALLCust_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueCust();
            DataTable objData = Session["GetFilterCustSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "custid=" + objData.Rows[i]["custid"];
                objView[0]["checkvalue"] = "true";
                objData.Rows[i]["checkvalue"] = "true";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetFilterCustSOStatus"] = objData;
            Session["GetFilterCustSOStatus_Hist"] = objData;
            gvCust.DataSource = objData;
            gvCust.DataBind();
            sVar.SetModalPopUp(MPECust, PanelCust, BECust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnCheckedNoneCust_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetFilterCustSOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "custid=" + objData.Rows[i]["custid"];
                objView[0]["checkvalue"] = "false";
                objData.Rows[i]["checkvalue"] = "false";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetFilterCustSOStatus"] = objData;
            Session["GetFilterCustSOStatus_Hist"] = objData;
            gvCust.DataSource = objData;
            gvCust.DataBind();
            sVar.SetModalPopUp(MPECust, PanelCust, BECust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedViewCust_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueCust();
            DataTable objData = Session["GetFilterCustSOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count > 0)
            {
                Session["GetFilterCustSOStatus_Hist"] = objView.ToTable();
                gvCust.DataSource = Session["GetFilterCustSOStatus_Hist"];
                gvCust.DataBind();
                objView.RowFilter = "";
                sVar.SetModalPopUp(MPECust, PanelCust, BECust, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnAddCust_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueCust();
            DataTable objData = Session["GetFilterCustSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count <= 0) this.sMsg("Sorry, Please select list to add data..!!<br>", "2");

            foreach (DataRowView sRow in objView)
            {
                if (TxtCustid.Text != "0")
                {
                    TxtCustid.Text += "," + sRow["custid"].ToString();
                    TxtCustname.Text += ";" + sRow["custcode"].ToString();
                }
                else
                {
                    TxtCustid.Text += "," + sRow["custid"].ToString();
                    TxtCustname.Text += ";" + sRow["custcode"].ToString();
                }
            }
            objView.RowFilter = "";
            sVar.SetModalPopUp(MPECust, PanelCust, BECust, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void gvCust_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValueCust();
        gvCust.PageIndex = e.NewPageIndex;
        gvCust.DataSource = Session["GetFilterCustSOStatus_Hist"];
        gvCust.DataBind();
        sVar.SetModalPopUp(MPECust, PanelCust, BECust, false);
    }

    //=============End GetData Customer =============//

    //=============Start GetData SO =============//
    private void GetDataSO()
    {
        try
        {
            string fStatus = "";
            List<string> sStatus = new List<string>();
            DateTime sDate1 = Convert.ToDateTime(Periode1.Text);
            DateTime sDate2 = Convert.ToDateTime(Periode2.Text); 
            sSql = "Select 'false' checkvalue, CAST(om.sysid AS varchar(20)) orderid, om.trno transno, om.trdate trndate, UPPER(c.custname) custname, om.orderstatus FROM t_orderhdr om INNER JOIN m_customer c On c.custid=om.custsuppid INNER JOIN t_orderdtl od On om.sysid=od.sysid LEFT JOIN (Select id.orderid, id.orderdtlid from t_inhdr im Inner Join t_indtl id ON id.sysid=im.sysid Where im.transcode=3 Group By im.sysid, id.orderid, id.orderdtlid) im ON im.orderid=om.sysid AND im.orderdtlid=od.sysiddtl WHERE om.transcode=4 AND om.trdate Between '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + "' AND '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + "'";

            foreach (ListItem item in DDLStatus.Items)
                if (item.Selected) fStatus += "'" + item.Value.Trim() + "',";

            if (fStatus != "") sSql += "AND om.orderstatus IN (" + fStatus.Substring(0, fStatus.Length - 1) + ")";

            if (TxtSJId.Text != "0") sSql += " And im.sysid In (" + TxtSJId.Text + ") ";

            if (TypeReport.SelectedValue.Trim() == "SUMMARY" && TxtItemId.Text != "0")
                sSql += " And i.itemid IN (" + TxtItemId.Text + ") ";

            if (TxtCustid.Text != "0") sSql += " And om.custsuppid IN (" + TxtCustid.Text + ") ";

            sSql += " Group By om.sysid, om.trno, om.trdate, c.custname, om.orderstatus ORDER BY om.sysid";
            string[] HdrText = { "Draft", "No. SO", "Tanggal", "Customer", "Status" };
            string[] DtField = { "orderid", "transno", "trndate", "custname", "orderstatus" };
            string[] DtKeyNames = { "orderid" };
            DrawTable(gvSO, HdrText, DtField, DtKeyNames);

            Session["GetFilterSOStatus_Hist"] = GetDataTable(sSql);
            Session["GetFilterSOStatus"] = GetDataTable(sSql);
            gvSO.DataSource = GetDataTable(sSql);
            gvSO.DataBind();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnGetSO_Click(object sender, EventArgs e)
    {
        try
        {
            this.GetDataSO();
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnEraseSO_Click(object sender, EventArgs e)
    {
        TxtSOId.Text = "0";
        TxtSO.Text = "";
    }

    protected void BtnCLoseSO_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, false);
    }

    private void UpdateCheckedValueSO()
    {
        DataTable objData = Session["GetFilterSOStatus"] as DataTable;
        DataView objView = objData.DefaultView;
        if (objView.Count > 0)
        {
            for (int i = 0; i < gvSO.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvSO.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string orderid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            orderid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView.RowFilter = "orderid=" + orderid;
                    if (objView.Count == 1 && cbCheck == true)
                        objView[0]["checkvalue"] = "true";

                }
            }
        }
        objView.RowFilter = "";
        objData.AcceptChanges();
        Session["GetFilterSOStatus"] = objData;

        //==================================//
        DataTable objData1 = Session["GetFilterSOStatus_Hist"] as DataTable;
        DataView objView1 = objData1.DefaultView;
        if (objView1.Count > 0)
        {
            for (int i = 0; i < gvSO.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvSO.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string orderid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            orderid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView1.RowFilter = "orderid=" + orderid;
                    if (objView1.Count == 1 && cbCheck == true)
                        objView1[0]["checkvalue"] = "true";
                    objView1.RowFilter = "";
                    objData1.AcceptChanges();
                    Session["GetFilterSOStatus_Hist"] = objData1;
                }
            }
        }
    }

    protected void BtnFindSO_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetFilterSOStatus"] as DataTable;
            if (objData.Rows.Count <= 0) this.GetDataSO();
            else
            {
                this.UpdateCheckedValueSO();
                DataView objView = objData.DefaultView;
                objView.RowFilter = DDLFilterSO.SelectedValue.ToString() + " LIKE '%" + TxtFilterSO.Text.Trim() + "%'";
                Session["GetFilterSOStatus_Hist"] = objView.ToTable();
                gvSO.DataSource = Session["GetFilterSOStatus_Hist"];
                gvSO.DataBind();
                objView.RowFilter = ""; 
            }
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        } 
    }

    protected void BtnCheckedALLSO_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueSO();
            DataTable objData = Session["GetFilterSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "orderid=" + objData.Rows[i]["orderid"];
                objView[0]["checkvalue"] = "true";
                objData.Rows[i]["checkvalue"] = "true";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetFilterSOStatus"] = objData;
            Session["GetFilterSOStatus_Hist"] = objData;
            gvSO.DataSource = objData;
            gvSO.DataBind();
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnCheckedNoneSO_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetFilterSOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "orderid=" + objData.Rows[i]["orderid"];
                objView[0]["checkvalue"] = "false";
                objData.Rows[i]["checkvalue"] = "false";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetFilterSOStatus"] = objData;
            Session["GetFilterSOStatus_Hist"] = objData;
            gvSO.DataSource = objData;
            gvSO.DataBind();
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnChekedViewSO_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueSO();
            DataTable objData = Session["GetFilterSOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count > 0)
            {
                Session["GetFilterSOStatus_Hist"] = objView.ToTable();
                gvSO.DataSource = Session["GetFilterSOStatus_Hist"];
                gvSO.DataBind();
                objView.RowFilter = "";
                sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnAddSO_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueSO();
            DataTable objData = Session["GetFilterSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count <= 0)
                this.sMsg("Sorry, Please select list to add data..!!<br>", "2");

            foreach (DataRowView sRow in objView)
            {
                if (TxtSOId.Text != "0")
                {
                    TxtSOId.Text += "," + sRow["orderid"].ToString();
                    TxtSO.Text += ";" + sRow["transno"].ToString();
                }
                else
                {
                    TxtSOId.Text += "," + sRow["orderid"].ToString();
                    TxtSO.Text += ";" + sRow["transno"].ToString();
                }
            }
            objView.RowFilter = "";
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void gvSO_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValueSO();
        gvSO.PageIndex = e.NewPageIndex;
        gvSO.DataSource = Session["GetFilterSOStatus_Hist"];
        gvSO.DataBind();
        sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, true);
    }

    //=============End GetData SO =============//

    //=============Start GetData DO =============//
    private void UpdateCheckedValueSJ()
    {

        DataTable objData = Session["GetSJSOStatus"] as DataTable;
        DataView objView = objData.DefaultView;
        if (objView.Count > 0)
        {
            for (int i = 0; i < gvSJ.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvSJ.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string sjid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            sjid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView.RowFilter = "sjid=" + sjid;
                    if (objView.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView[0]["checkvalue"] = "true";
                        }
                    }

                }
            }
        }
        objView.RowFilter = "";
        objData.AcceptChanges();
        Session["GetSJSOStatus"] = objData;

        //================//
        DataTable objData1 = Session["GetSJSOStatus_Hist"] as DataTable;
        DataView objView1 = objData1.DefaultView;
        if (objView1.Count > 0)
        {

            for (int i = 0; i < gvSJ.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvSJ.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string sjid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            sjid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView1.RowFilter = "sjid=" + sjid;
                    if (objView1.Count == 1 && cbCheck == true)
                        objView1[0]["checkvalue"] = "true";
                }
            }
        }
        objView1.RowFilter = "";
        objData1.AcceptChanges();
        Session["GetSJSOStatus_Hist"] = objData1;
    }

    private void GetDataSJ()
    {
        try
        {
            string fStatus = "";
            List<string> sStatus = new List<string>();
            DateTime sDate1 = Convert.ToDateTime(Periode1.Text);
            DateTime sDate2 = Convert.ToDateTime(Periode2.Text); 

            sSql = "SELECT 'false' checkvalue, CAST(im.sysid as varchar(20)) sjid, im.orderid, ISNULL(im.trno, '') sjno, im.trdate sjdate, om.trno orderno, c.custname FROM t_orderhdr om INNER JOIN m_customer c ON c.custid = om.custsuppid INNER JOIN t_orderdtl od ON om.sysid = od.sysid INNER JOIN (Select im.sysid, im.trno, im.trdate, id.orderid from t_inhdr im Inner Join t_indtl id ON id.sysid = im.sysid Where im.transcode = 5 Group By im.sysid, im.trno, im.trdate, id.orderid) im ON im.orderid = om.sysid WHERE om.transcode = 4 AND om.trdate Between '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + "' AND '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + "'";

            if (TxtSOId.Text != "0") sSql += " And om.sysid In (" + TxtSOId.Text + ") ";

            foreach (ListItem item in DDLStatus.Items)
                if (item.Selected) fStatus += "'" + item.Value.Trim() + "',";

            if (fStatus != "")
                sSql += "AND om.orderstatus IN (" + fStatus.Substring(0, fStatus.Length - 1) + ")";

            if (TypeReport.SelectedValue.Trim() == "SUMMARY" && TxtItemId.Text != "0")
                sSql += " And i.itemid IN (" + TxtItemId.Text + ") ";

            if (TxtCustid.Text != "0")
                sSql += " And om.custsuppid IN (" + TxtCustid.Text + ") ";

            sSql += " Group by im.sysid, im.orderid, ISNULL(im.trno,''), im.trdate, om.trno, c.custname Order By im.trdate";
            string[] HdrText = { "Draft", "No. SJ", "Tanggal", "Customer" };
            string[] DtField = { "sjid", "sjno", "sjdate", "custname" };
            string[] DtKeyNames = { "sjid" };
            DrawTable(gvSJ, HdrText, DtField, DtKeyNames);
            Session["GetSJSOStatus_Hist"] = GetDataTable(sSql);
            Session["GetSJSOStatus"] = GetDataTable(sSql);
            gvSJ.DataSource = GetDataTable(sSql);
            gvSJ.DataBind();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnBindSJ_Click(object sender, EventArgs e)
    {
        try
        {
            this.GetDataSJ();
            sVar.SetModalPopUp(MpeSJ, PanelSJ, BeSJ, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnEraseSJ_Click(object sender, EventArgs e)
    {
        TxtSJId.Text = "0";
        TxtSJ.Text = "";
    }

    protected void BtnCheckedALLSJ_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueSJ();
            DataTable objData = Session["GetSJSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "sjid=" + objData.Rows[i]["sjid"];
                objView[0]["checkvalue"] = "true";
                objData.Rows[i]["checkvalue"] = "true";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetSJSOStatus"] = objData;
            Session["GetSJSOStatus_Hist"] = objData;
            gvSJ.DataSource = objData;
            gvSJ.DataBind();
            sVar.SetModalPopUp(MpeSJ, PanelSJ, BeSJ, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnCheckedNoneSJ_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetSJSOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "sjid=" + objData.Rows[i]["sjid"];
                objView[0]["checkvalue"] = "false";
                objData.Rows[i]["checkvalue"] = "false";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetSJSOStatus"] = objData;
            Session["GetSJSOStatus_Hist"] = objData;
            gvSJ.DataSource = objData;
            gvSJ.DataBind();
            sVar.SetModalPopUp(MpeSJ, PanelSJ, BeSJ, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedViewSJ_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueSJ();
            DataTable objData = Session["GetSJSOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count > 0)
            {
                Session["GetSJSOStatus_Hist"] = objView.ToTable();
                gvSJ.DataSource = Session["GetSJSOStatus_Hist"];
                gvSJ.DataBind();
                objView.RowFilter = "";
                sVar.SetModalPopUp(MpeSJ, PanelSJ, BeSJ, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnFindSJ_Click(object sender, EventArgs e)
    {

        try
        {
            DataTable objData = Session["GetSJSOStatus_Hist"] as DataTable;
            if (objData.Rows.Count <= 0) this.GetDataSJ();
            else
            {
                this.UpdateCheckedValueSJ();
                DataView objView = objData.DefaultView;
                objView.RowFilter = DDLFilterSJ.SelectedValue.ToString() + " LIKE '%" + TxtFilterSJ.Text.Trim() + "%'";
                Session["GetSJSOStatus_Hist"] = objView.ToTable();
                gvSJ.DataSource = Session["GetSJSOStatus_Hist"];
                gvSJ.DataBind();
                objView.RowFilter = "";
            }
            sVar.SetModalPopUp(MpeSJ, PanelSJ, BeSJ, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnAddSJ_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueSJ();
            DataTable objData = Session["GetSJSOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count <= 0) this.sMsg("Sorry, Please select list to add data..!!<br>", "2");

            foreach (DataRowView sRow in objView)
            {
                if (TxtSJId.Text != "0")
                {
                    TxtSJId.Text += "," + sRow["sjid"].ToString();
                    TxtSJ.Text += ";" + sRow["sjno"].ToString();
                }
                else
                {
                    TxtSJId.Text += "," + sRow["sjid"].ToString();
                    TxtSJ.Text += ";" + sRow["sjno"].ToString();
                }
            }
            objView.RowFilter = "";
            sVar.SetModalPopUp(MpeSJ, PanelSJ, BeSJ, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    protected void BtnCloseSJ_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeSJ, PanelSJ, BeSJ, false);
    }

    protected void gvSJ_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValueSJ();
        gvSJ.PageIndex = e.NewPageIndex;
        gvSJ.DataSource = Session["GetSJSOStatus_Hist"];
        gvSJ.DataBind();
        sVar.SetModalPopUp(MpeSJ, PanelSJ, BeSJ, true);
    }

    //=============End GetData SO =============//

    //=============Start GetData Item =============//
    private void GetDataItem()
    {
        try
        {
            string fStatus = "";
            List<string> sStatus = new List<string>();
            DateTime sDate1 = Convert.ToDateTime(Periode1.Text);
            DateTime sDate2 = Convert.ToDateTime(Periode2.Text); 

            sSql = "SELECT 'false' checkvalue, i.itemid, i.itemcode, i.itemname, u.genname FROM t_orderhdr om INNER JOIN t_orderdtl od ON om.sysid=od.sysid INNER JOIN m_items i ON i.itemid = od.itemid INNER JOIN m_general u ON u.genid = od.unitid AND u.gengroup = 'SATUAN' LEFT JOIN (Select im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, id.itemid, SUM(ISNULL(id.qtyin, 0.0)) qtyin, l.locationname from t_inhdr im Inner Join t_indtl id ON id.sysid = im.sysid INNER JOIN m_location l ON l.locationid = id.locationid Where im.transcode = 4 Group By im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, id.itemid, l.locationname) im ON im.orderid = om.sysid AND im.orderdtlid = od.sysiddtl WHERE om.transcode = 4 AND om.trdate Between '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + "' AND '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + "'";

            if (TxtSOId.Text != "0") sSql += " And om.sysid In (" + TxtSOId.Text + ") ";

            foreach (ListItem item in DDLStatus.Items)
                if (item.Selected) fStatus += "'" + item.Value.Trim() + "',";

            if (fStatus != "")
                sSql += "AND om.orderstatus IN (" + fStatus.Substring(0, fStatus.Length - 1) + ")";

            if (TxtSJId.Text != "0")
                sSql += " And im.sysid In (" + TxtSJId.Text + ") ";

            if (TxtCustid.Text != "0")
                sSql += " And om.custsuppid IN (" + TxtCustid.Text + ") ";
            sSql += " Group BY i.itemid, i.itemcode, i.itemname, u.genname Order By i.itemcode";
            string[] HdrText = { "Code", "Item Name", "Unit" };
            string[] DtField = { "itemcode", "itemname", "genname" };
            string[] DtKeyNames = { "itemid" };
            DrawTable(gvItem, HdrText, DtField, DtKeyNames);
            Session["GetItemSOStatus_Hist"] = GetDataTable(sSql);
            Session["GetItemSOStatus"] = GetDataTable(sSql);
            gvItem.DataSource = GetDataTable(sSql);
            gvItem.DataBind();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    private void UpdateCheckedValueItem()
    {

        DataTable objData = Session["GetItemSOStatus"] as DataTable;
        DataView objView = objData.DefaultView;
        if (objView.Count > 0)
        {
            for (int i = 0; i < gvItem.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string itemid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            itemid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView.RowFilter = "itemid=" + itemid;
                    if (objView.Count == 1 && cbCheck == true)
                        objView[0]["checkvalue"] = "true";

                }
            }
        }
        objView.RowFilter = "";
        objData.AcceptChanges();
        Session["GetItemSOStatus"] = objData;

        //================//
        DataTable objData1 = Session["GetItemSOStatus_Hist"] as DataTable;
        DataView objView1 = objData1.DefaultView;
        if (objView1.Count > 0)
        {

            for (int i = 0; i < gvItem.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string itemid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            itemid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView1.RowFilter = "itemid=" + itemid;
                    if (objView1.Count == 1 && cbCheck == true)
                        objView1[0]["checkvalue"] = "true";
                }
            }
        }
        objView1.RowFilter = "";
        objData1.AcceptChanges();
        Session["GetItemSOStatus_Hist"] = objData1;
    }

    protected void BtnBindItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.GetDataItem();
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnEraseItem_Click(object sender, EventArgs e)
    {
        TxtItem.Text = "";
        TxtItemId.Text = "0";
    }

    protected void BtnFindItem_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetItemSOStatus"] as DataTable;
            if (objData.Rows.Count <= 0) this.GetDataItem();
            else
            {
                this.UpdateCheckedValueItem();
                DataView objView = objData.DefaultView;
                objView.RowFilter = DDLFilterItem.SelectedValue.ToString() + " LIKE '%" + TxtFilterItem.Text.Trim() + "%'";
                Session["GetItemSOStatus_Hist"] = objView.ToTable();
                gvItem.DataSource = Session["GetItemSOStatus_Hist"];
                gvItem.DataBind();
                objView.RowFilter = "";
            }
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedALLItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueItem();
            DataTable objData = Session["GetItemSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "itemid=" + objData.Rows[i]["itemid"];
                objView[0]["checkvalue"] = "True";
                objData.Rows[i]["checkvalue"] = "True";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetItemSOStatus"] = objData;
            Session["GetItemSOStatus_Hist"] = objData;
            gvItem.DataSource = objData;
            gvItem.DataBind();
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedNoneItem_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetItemSOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "itemid=" + objData.Rows[i]["itemid"];
                objView[0]["checkvalue"] = "false";
                objData.Rows[i]["checkvalue"] = "false";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetItemSOStatus"] = objData;
            Session["GetItemSOStatus_Hist"] = objData;
            gvItem.DataSource = objData;
            gvItem.DataBind();
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedViewItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueItem();
            DataTable objData = Session["GetItemSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count > 0)
            {
                Session["GetItemSOStatus_Hist"] = objView.ToTable();
                gvItem.DataSource = Session["GetItemSOStatus_Hist"];
                gvItem.DataBind();
                objView.RowFilter = "";
                sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnAddItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueItem();
            DataTable objData = Session["GetItemSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count <= 0)
                this.sMsg("Sorry, Please select list to add data..!!<br>", "2");

            foreach (DataRowView sRow in objView)
            {
                if (TxtItemId.Text != "0")
                {
                    TxtItemId.Text += "," + sRow["itemid"].ToString();
                    TxtItem.Text += ";" + sRow["itemcode"].ToString();
                }
                else
                {
                    TxtItemId.Text += "," + sRow["itemid"].ToString();
                    TxtItem.Text += ";" + sRow["itemcode"].ToString();
                }
            }
            objView.RowFilter = "";
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCloseItem_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
    }

    protected void gvItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValueItem();
        gvItem.PageIndex = e.NewPageIndex;
        gvItem.DataSource = Session["GetItemSOStatus_Hist"];
        gvItem.DataBind();
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
    }
    //=============End GetData Item =============//

    private void BindReport(string sType, string FileName)
    {
        try
        { 
            string sGroup = "", fStatus = "";
            List<string> sStatus = new List<string>();
            DateTime sDate1 = toDate(Periode1.Text);
            DateTime sDate2 = toDate(Periode2.Text);
            if (TypeReport.SelectedValue.Trim() == "SUMMARY")
            {
                sSql = "SELECT c.custid, c.custcode, om.sysid, ISNULL(im.sysid, 0) irid, om.trno, om.trdate, ISNULL(im.trno, '-') irno, sino,  im.trdate irDate, im.sidate, UPPER(c.custname) custname, om.nettoamt, SUM(od.qty) QtyPO, SUM(ISNULL(im.qtyin, 0)) qtyin, SUM(od.qty) - SUM(ISNULL(im.qtyin, 0)) OSQTY, Isnull(im.qtysi, 0) qtysi, om.orderstatus FROM t_orderhdr om INNER JOIN m_customer c ON c.custid = om.custsuppid INNER JOIN t_orderdtl od ON om.sysid = od.sysid LEFT JOIN (Select im.sysid, im.trno, ivh.trno sino, im.trdate, ivh.trdate sidate, id.orderid, SUM(ISNULL(id.qtyin, 0.0)) qtyin, SUM(ISNULL(ivd.qty, 0.0)) qtysi, id.orderdtlid from t_inhdr im Inner Join t_indtl id ON id.sysid = im.sysid Left Join t_invoicedtl ivd ON ivd.inhdriddtl = id.sysiddtl AND ivd.inhdrid = id.sysid Left Join t_invoicehdr ivh ON ivh.sysid = ivd.sysid AND ivh.transcode = 6 Where im.transcode = 5 Group By im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, ivh.trno, ivh.trdate) im ON im.orderid = om.sysid AND od.sysiddtl = im.orderdtlid WHERE om.transcode = 4";
                sGroup = " Group By om.sysid, om.trno, om.trdate, UPPER(c.custname), om.nettoamt, im.trno, im.trdate, om.orderstatus, im.sysid, im.sino, im.sidate, c.custid, im.qtysi, c.custcode";
            }
            else
            {
                sSql = "Select om.sysid, om.trno, om.trdate trndate, UPPER(c.custname) suppname, om.nettoamt, ISNULL(im.trno,'') irno, im.trdate irdate, i.itemid, i.itemcode, i.itemname, od.unitprice, od.totalamtdtl, od.discamtdtl, od.nettoamtdtl, od.qty, ISNULL(im.qtyin,0) qtyin, od.qty-ISNULL(im.qtyin,0) OSQTY, ISNULL(od.qty_akum,0.0) qty_akum, ISNULL(od.qty_akum_inv,0.0) qty_akum_inv, Isnull(od.qty_cancel,0.0) qty_cancel, ISNULL(od.qty_retur,0.0) qty_retur, u.genname, ISNULL(locationname,'') locationname FROM t_orderhdr om INNER JOIN m_customer c ON c.custid=om.custsuppid INNER JOIN t_orderdtl od ON om.sysid=od.sysid INNER JOIN m_items i ON i.itemid=od.itemid INNER JOIN m_general u ON u.genid=od.unitid AND u.gengroup='SATUAN' LEFT JOIN (Select im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, id.itemid, SUM(ISNULL(id.qtyin,0.0)) qtyin, l.locationname from t_inhdr im Inner Join t_indtl id ON id.sysid=im.sysid INNER JOIN m_location l ON l.locationid=id.locationid Where im.transcode=5 Group By im.sysid, im.trno, im.trdate, id.orderid, id.orderdtlid, id.itemid, l.locationname) im ON im.orderid=om.sysid AND im.orderdtlid=od.sysiddtl WHERE om.transcode=4";
            }

            sSql += " AND om.trdate Between '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + "' AND '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + "'";

            if (TxtCustid.Text != "0")
                sSql += " And om.custsuppid In (" + TxtCustid.Text + ") ";
            
            if (TxtSOId.Text != "0")
                sSql += " And om.sysid In (" + TxtSOId.Text + ") ";

            foreach (ListItem item in DDLStatus.Items)
                if (item.Selected) fStatus += "'" + item.Value.Trim() + "',";

            if (fStatus != "")
                sSql += "AND orderstatus IN (" + fStatus.Substring(0, fStatus.Length - 1) + ")";

            if (TxtSJId.Text != "0")
                sSql += " And im.sysid In (" + TxtSJId.Text + ") ";

            if (TypeReport.SelectedValue.Trim() == "SUMMARY" && TxtItemId.Text != "0")
                sSql += " And i.itemid IN (" + TxtItemId.Text + ") ";

            sSql += sGroup + " ORDER BY om.trdate";
            ViewRpt.Load(Server.MapPath("~/Report/ReportSOStatus/RptSOStatus" + TypeReport.SelectedValue.Trim() + "_" + FileName + ".rpt"));
            DataView dtv = GetDataTable(sSql).DefaultView;
            ViewRpt.SetDataSource(dtv.ToTable());
            ViewRpt.SetParameterValue("UserID", Session["username"].ToString());
            ViewRpt.SetParameterValue("sStatus", "STATUS " + TypeReport.SelectedValue.Trim());
            ViewRpt.SetParameterValue("sPeriode", "Periode : " + Periode1.Text + "s/d" + Periode2.Text);
            SetDBLogonCrv(ViewRpt);

            if (sType == "XLS")
                ViewRpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "ReportSOStatus" + DateTime.Now.ToString("dd/MM/yyyy"));
            else if (sType == "PDF")
                ViewRpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "ReportSOStatus" + DateTime.Now.ToString("dd/MM/yyyy"));
            else
                CrvReportSOStatus.ReportSource = ViewRpt;
            this.UnloadView();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    private void UnloadView()
    {
        Response.Buffer = false;
        Response.ClearHeaders();
        Response.ClearContent();
    }
     
    protected void BtnToPDF_Click(object sender, EventArgs e)
    {
        this.BindReport("PDF", "PDF");
    }

    protected void BtnToEXL_Click(object sender, EventArgs e)
    {
        this.BindReport("XLS", "XLS");
    }

    protected void BtnClear_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Report/ReportSOStatus/ReportSOStatus.aspx");
    }
    
    protected void BtnOK_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, false);
    }

    protected void BtnView_Click(object sender, EventArgs e)
    {
        this.BindReport("VIEW", "PDF");
    }
}