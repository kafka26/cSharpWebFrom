﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using static ClassGlobal;
using System.Reflection;

public partial class Report_ReportStok : System.Web.UI.Page
{
    public static DataClassesDataContext db = new DataClassesDataContext();
    private static  ClassGlobal sVar = new ClassGlobal();
    private static ReportDocument ViewRpt = new ReportDocument();
    Int32 trnCode = 36;
    string sSql = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["username"].ToString()))
            {
                DataTable sdata = CekMenuByUser(Session["username"].ToString(), trnCode);
                if (sdata.Rows.Count > 0)
                {
                    if (!this.IsPostBack)
                    {
                        Periode1.Text = DateTime.Now.ToString("01/MM/yyyy");
                        Periode2.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        GetddlGudang();
                    }                   
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }
        catch (Exception ex)
        { 
            Response.Redirect("~/Login.aspx");
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        //try
        //{
        //    if (ViewRpt != null)
        //    {
        //        if (ViewRpt.IsLoaded)
        //        {
        //            ViewRpt.Dispose();
        //            ViewRpt.Close();
        //        }
        //    }
        //}
        //catch
        //{
        //    ViewRpt.Dispose();
        //    ViewRpt.Close();
        //}
    }

    private void sMsg(string sMsg, string type)
    {
        if (type == "1")
        {
            lblInfo.Text = "ERROR";
        }
        else
        {
            lblInfo.Text = "INFORMATION";
        }
        lblMessage.Text = sMsg.ToUpper().ToString();
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, true);
    }

    protected void TypeReport_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.BindReport("VIEW", "PDF");
        //if (TypeReport.SelectedValue == "DETAIL")
        //{
        //    LblItem.Visible = true;
        //    TxtItem.Visible = true;
        //    BtnBindItem.Visible = true;
        //    BtnEraseItem.Visible = true;
        //}
        //else
        //{
        //    LblItem.Visible = false;
        //    TxtItem.Visible = false;
        //    BtnBindItem.Visible = false;
        //    BtnEraseItem.Visible = false;
        //}
    }

    private void GetddlGudang()
    { 
        List<m_location> sData = db.m_locations.Where(l => l.m_flag == "ACTIVE").ToList();
        locationid.DataSource = sData;
        locationid.DataTextField = "locationname";
        locationid.DataValueField = "locationid";
        locationid.DataBind();
    }

    private void DrawTable(GridView gView, string[] HdrText, string[] DtField, string[] DtKeyNames)
    {
        if (gView.Columns.Count <= int.Parse(DtField.Length.ToString()))
        {
            gView.DataKeyNames = DtKeyNames;
            for (int i = 0; i < DtField.Length; i++)
            {
                System.Web.UI.WebControls.BoundField dRow = new BoundField();
                dRow.HeaderText = HdrText[i];
                dRow.HeaderStyle.Wrap = false;
                dRow.DataField = DtField[i];
                gView.Columns.Add(dRow);
            }
        }
    }

    //=============Start GetData Item =============//
    private void GetItemData()
    {
        try
        { 
            sSql = "SELECT 'false' checkvalue, i.itemid, i.itemcode, i.itemname, u.genname FROM m_items i INNER JOIN m_general u ON u.genid = i.unit_small AND u.gengroup = 'SATUAN' ";
            string[] HdrText = { "Code", "Item Name", "Unit" };
            string[] DtField = { "itemcode", "itemname", "genname" };
            string[] DtKeyNames = { "itemid" };
            DrawTable(gvItem, HdrText, DtField, DtKeyNames);
            Session["GetItemSOStatus_Hist"] = GetDataTable(sSql);
            Session["GetItemSOStatus"] = GetDataTable(sSql);
            gvItem.DataSource = GetDataTable(sSql);
            gvItem.DataBind();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() + sSql, "1");
            return;
        }
    }

    private void UpdateCheckedValueItem()
    {

        DataTable objData = Session["GetItemSOStatus"] as DataTable;
        DataView objView = objData.DefaultView;
        if (objView.Count > 0)
        {
            for (int i = 0; i < gvItem.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string itemid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            itemid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView.RowFilter = "itemid=" + itemid;
                    if (objView.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView[0]["checkvalue"] = "true";
                        }
                    }

                }
            }
        }
        objView.RowFilter = "";
        objData.AcceptChanges();
        Session["GetItemSOStatus"] = objData;

        //================//
        DataTable objData1 = Session["GetItemSOStatus_Hist"] as DataTable;
        DataView objView1 = objData1.DefaultView;
        if (objView1.Count > 0)
        {

            for (int i = 0; i < gvItem.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string itemid = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            itemid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    objView1.RowFilter = "itemid=" + itemid;
                    if (objView1.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            objView1[0]["checkvalue"] = "true";
                        }
                    }
                }
            }
        }
        objView1.RowFilter = "";
        objData1.AcceptChanges();
        Session["GetItemSOStatus_Hist"] = objData1;
    }

    protected void BtnBindItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.GetItemData();
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnEraseItem_Click(object sender, EventArgs e)
    {
        TxtItem.Text = "";
        TxtItemId.Text = "0";
    }

    protected void BtnFindItem_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetItemSOStatus"] as DataTable;
            if (objData.Rows.Count <= 0)
            {
                this.GetItemData();
            }
            else
            {
                this.UpdateCheckedValueItem();
                DataView objView = objData.DefaultView;
                objView.RowFilter = DDLFilterItem.SelectedValue.ToString() + " LIKE '%" + TxtFilterItem.Text.Trim() + "%'";
                Session["GetItemSOStatus_Hist"] = objView.ToTable();
                gvItem.DataSource = Session["GetItemSOStatus_Hist"];
                gvItem.DataBind();
                objView.RowFilter = "";
            }
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedALLItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueItem();
            DataTable objData = Session["GetItemSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "itemid=" + objData.Rows[i]["itemid"];
                objView[0]["checkvalue"] = "True";
                objData.Rows[i]["checkvalue"] = "True";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetItemSOStatus"] = objData;
            Session["GetItemSOStatus_Hist"] = objData;
            gvItem.DataSource = objData;
            gvItem.DataBind();
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedNoneItem_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable objData = Session["GetItemSOStatus_Hist"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.AllowEdit = true;
            objView.RowFilter = "";
            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objView.RowFilter = "itemid=" + objData.Rows[i]["itemid"];
                objView[0]["checkvalue"] = "false";
                objData.Rows[i]["checkvalue"] = "false";
                objView.RowFilter = "";
            }
            objData.AcceptChanges();
            Session["GetItemSOStatus"] = objData;
            Session["GetItemSOStatus_Hist"] = objData;
            gvItem.DataSource = objData;
            gvItem.DataBind();
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCheckedViewItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueItem();
            DataTable objData = Session["GetItemSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count > 0)
            {
                Session["GetItemSOStatus_Hist"] = objView.ToTable();
                gvItem.DataSource = Session["GetItemSOStatus_Hist"];
                gvItem.DataBind();
                objView.RowFilter = "";
                sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnAddItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValueItem();
            DataTable objData = Session["GetItemSOStatus"] as DataTable;
            DataView objView = objData.DefaultView;
            objView.RowFilter = "checkvalue='true'";
            if (objView.Count <= 0)
            {
                this.sMsg("Sorry, Please select list to add data..!!<br>", "2");
            }

            foreach (DataRowView sRow in objView)
            {
                if (TxtItemId.Text != "0")
                {
                    TxtItemId.Text += "," + sRow["itemid"].ToString();
                    TxtItem.Text += ";" + sRow["itemcode"].ToString();
                }
                else
                {
                    TxtItemId.Text += "," + sRow["itemid"].ToString();
                    TxtItem.Text += ";" + sRow["itemcode"].ToString();
                }
            }
            objView.RowFilter = "";
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCloseItem_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
    }

    protected void gvItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValueItem();
        gvItem.PageIndex = e.NewPageIndex;
        gvItem.DataSource = Session["GetItemSOStatus_Hist"];
        gvItem.DataBind();
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
    }
    //=============End GetData Item =============//

    private void BindReport(string sType, string TypeFile)
    {
        try
        { 
            List<string> sLocation = new List<string>(); 
            DateTime sDate1 = toDate(Periode1.Text);
            DateTime sDate2 = toDate(Periode2.Text);
            string sField = "", sGroup = "", OrderBy = "", fLocation=""; 

            if (TypeReport.SelectedValue.Trim() == "SUMMARY")
            {
                sField = " itemname, itemid, itemcode, locationid, locationname, genname, SUM(SaldoAwal) SaldoAwal, SUM(qtyin) qtyin, SUM(qtyout) qtyout";
                sGroup = " Group By itemname, itemid, itemcode, locationid, locationname, genname";
                OrderBy = " Order by itemname";
            }
            else
            {
                sField = " trdate, itemname, itemid, itemcode, locationid, locationname, trno, detailnote, genname, SaldoAwal, qtyin, qtyout, trstatus";
                OrderBy = " Order by trdate, itemname";
            }                

            sSql = "Select " + sField + " from ( SELECT '" + sDate1.Year + "-" + sDate1.Month + "-" + sDate1.Day + " 00:00:00' trdate, i.itemid, i.itemcode, i.itemname, ISNULL(s.locationid, 0) locationid, ISNULL(s.locationname,'') locationname, 'Saldo Awal' trno, '' detailnote, u.genname, SUM(isnull(s.qtyin,0.0)) - SUM(isnull(qtyout,0.0)) SaldoAwal, 0.0 qtyin, 0.0 qtyout, trstatus FROM m_items i INNER JOIN m_general u ON u.genid = i.unit_small AND u.gengroup = 'SATUAN' LEFT JOIN (Select dt.itemid, hd.trno, trdate, dt.unitid, hd.locationid, dt.qtyin, dt.qtyout, dt.detailnote, hd.lastupdatetime, l.locationname, hd.trstatus From t_inhdr hd INNER JOIN t_indtl dt ON hd.sysid=dt.sysid LEFT JOIN m_location l ON l.locationid=dt.locationid ) s ON s.itemid=i.itemid AND s.unitid=u.genid AND s.lastupdatetime < '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + " 00:00:00' GROUP BY i.itemid, i.itemcode, i.itemname, u.genname, s.locationid, s.locationname, s.locationid, s.trstatus UNION ALL SELECT ISNULL(s.lastupdatetime, Getdate()) trdate, i.itemid, i.itemcode, i.itemname, ISNULL(s.locationid, 0) locationid, ISNULL(s.locationname,'') locationname, ISNULL(s.trno,'') trno, CAST(s.detailnote as varchar(500)) detailnote, u.genname, 0.0 SaldoAwal, isnull(s.qtyin,0.0) qtyin, isnull(qtyout,0.0) qtyout, trstatus FROM m_items i INNER JOIN m_general u ON u.genid = i.unit_small AND u.gengroup = 'SATUAN' LEFT JOIN (Select dt.itemid, hd.trno, trdate, dt.unitid, hd.locationid, dt.qtyin, dt.qtyout, dt.detailnote, hd.lastupdatetime, l.locationname, trstatus From t_inhdr hd INNER JOIN t_indtl dt ON hd.sysid=dt.sysid LEFT JOIN m_location l ON l.locationid=dt.locationid) s ON s.itemid=i.itemid AND s.unitid=u.genid AND s.lastupdatetime >= '" + sDate1.Month + "-" + sDate1.Day + "-" + sDate1.Year + " 00:00:00' AND s.lastupdatetime <= '" + sDate2.Month + "-" + sDate2.Day + "-" + sDate2.Year + " 23:59:59' ) d Where trstatus='APPROVED'";

            //List<string> sStatus = new List<string>();
            //foreach (ListItem item in DDLStatus.Items)
            //{
            //    if (item.Selected)
            //    {
            //        sStatus.Add(item.Value);
            //    }
            //}

            foreach (ListItem item in locationid.Items)
            {
                if (item.Selected)
                {
                    sLocation.Add(item.Value);
                    fLocation += "'" + item.Value.Trim() + "',";
                }
            } 

            if (fLocation != "")
                sSql += "AND locationid IN (" + fLocation.Substring(0, fLocation.Length - 1) + ")";

            if (TxtItemId.Text != "0") 
                sSql += " And d.itemid IN (" + TxtItemId.Text + ") "; 

            sSql += sGroup + OrderBy;
            ViewRpt.Load(Server.MapPath("~/Report/ReportStok/RptReportStok_" + TypeReport.SelectedValue.Trim() + "_" + TypeFile + ".rpt"));
            DataView dtv = GetDataTable(sSql).DefaultView;
            ViewRpt.SetDataSource(dtv.ToTable());
            ViewRpt.SetParameterValue("UserID", Session["username"].ToString());
            ViewRpt.SetParameterValue("sStatus", TypeReport.SelectedValue.Trim());
            ViewRpt.SetParameterValue("sPeriode", "Periode : " + Periode1.Text + " s/d " + Periode2.Text);
            SetDBLogonCrv(ViewRpt);

            if (sType == "XLS")
            {
                ViewRpt.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "ReportStok" + DateTime.Now.ToString("dd/MM/yyyy"));
            }
            else if (sType == "PDF")
            {
                ViewRpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "ReportStok" + DateTime.Now.ToString("dd/MM/yyyy"));
            }
            else
            {
                CrvReportStok.ReportSource = ViewRpt;

            }
            this.UnloadView();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString() +" => <br>"+ sSql, "1");
            return;
        }
    }

    private void UnloadView()
    {
        Response.Buffer = false;
        Response.ClearHeaders();
        Response.ClearContent();
    }
     
    protected void BtnToPDF_Click(object sender, EventArgs e)
    {
        this.BindReport("PDF", "PDF");
    }

    protected void BtnToEXL_Click(object sender, EventArgs e)
    {
        this.BindReport("XLS", "XLS");
    }

    protected void BtnClear_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Report/ReportStok/ReportStok.aspx");
    }
    
    protected void BtnOK_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, false);
    }

    protected void BtnView_Click(object sender, EventArgs e)
    {
        this.BindReport("VIEW", "PDF");
    }
}