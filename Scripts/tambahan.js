$(document).ready(function () {
	//treeview-sidebar auto select
	const url = window.location;
	$('ul.nav-sidebar a').filter(function () {
		return this.href == url;
	}).parent().addClass('active');
	$('ul.nav-treeview a').filter(function () {
		return this.href == url;
	}).parentsUntil(".sidebar-menu > .nav-treeview").addClass('menu-open');
	$('ul.nav-treeview a').filter(function () {
		return this.href == url;
	}).addClass('active');
	$('li.has-treeview a').filter(function () {
		return this.href == url;
	}).addClass('active');
	$('ul.nav-treeview a').filter(function () {
		return this.href == url;
	}).parentsUntil(".sidebar-menu > .nav-treeview").children(0).addClass('active');
});

var bMsg = '<h2><span class="fa fa-spinner fa-spin"></span> Please wait...</h2>';
$(document).ajaxStart(function () {
    $.blockUI({
        baseZ: 1000,
        message: bMsg
    })
}).ajaxStop($.unblockUI);
$(document).ajaxStop($.unblockUI);

$(function () {
    //Initialize Select2 Elements
    $('.select2').select2();

    //Initialize InputMask Elements
    $('.money').inputmask('decimal', {
        autoGroup: true,
        groupSeparator: ",",
        removeMaskOnSubmit: true,
        autoUnmask: true
    });
});

var prm = Sys.WebForms.PageRequestManager.getInstance();
if (prm != null) {
    prm.add_endRequest(function (sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize InputMask Elements
            $('.money').inputmask('decimal', {
                autoGroup: true,
                groupSeparator: ",",
                removeMaskOnSubmit: true,
                autoUnmask: true
            });
        }
    });
};