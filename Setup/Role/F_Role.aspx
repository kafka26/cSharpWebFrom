﻿<%@ Page Title="Master Role" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="F_Role.aspx.cs" Inherits="F_Setup_Role" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">Form Role</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-sm-12">
                                   <asp:label runat="server" ID="id_" Visible="false" />
                                   <asp:label runat="server" ID="flag_" Visible="false" />
                                </div>
                            </div>                             

                            <div class="row"> 
                               <div class="col-sm-4">
                                    <label class="col-form-label">Role Name</label> 
                                    <asp:TextBox runat="server" ID="rolename" CssClass="form-control" />
                               </div> 

                               <div class="col-sm-2">
                                   <div class="form-group clearfix">
                                       <label class="col-form-label">Flag</label> 
                                        <asp:DropDownList ID="rbflag" runat="server" CssClass="form-control select2">
                                            <asp:ListItem value="Aktif"> Aktif </asp:ListItem> 
                                            <asp:ListItem value="Tidak"> Tidak </asp:ListItem>  
                                        </asp:DropDownList>
                                   </div>
                               </div> 
                            </div>
                            <hr />
                            
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <asp:Button runat="server" ID="BtnSave" CssClass="btn btn-success load" Text="Save" OnClick="BtnSave_Click"/>
                                        <asp:Button runat="server" ID="btnChangeHdr" CssClass="btn btn-secondary" Text="Change Header" Enabled="false" OnClick="btnChangeHdr_Click"/>
                                    </div>
                                 </div>
                            </div>
                            <!---- Detail Role ---->
                            <hr />
                            <div class="row">
                               <div class="col-sm-6">
                                    <label class="col-form-label">Search For Multiple Add</label><br />
                                    <asp:Button runat="server" ID="BtnMultiTranscode" CssClass="btn btn-info" Text="Add Multiple Data" Enabled="true" OnClick="BtnMultiTranscode_Click"/>
                               </div> 
                            </div>

                            <hr />
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-body table-responsive p-0" style="height: 200px;"> 
                                        <asp:GridView ID="gvDtl" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="roledtlid,trancode" OnSelectedIndexChanged="gvDtl_SelectedIndexChanged"> 
                                        <Columns>                                            
                                            <asp:BoundField DataField="roledtlid" HeaderText="" ReadOnly="True" Visible="false" /> 
                                            <asp:BoundField DataField="trancode" HeaderText="Code" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" /> 
                                            <asp:BoundField DataField="type" HeaderText="Menu" ItemStyle-Wrap="false" />
                                            <asp:BoundField DataField="menutype" HeaderText="Type" /> 
                                            <asp:TemplateField HeaderText="Special akses" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="special_akses_dtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("special_akses")) %>' ToolTip='<%# Eval("special_akses") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                        <asp:TemplateField HeaderText="Create" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="create_form_dtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("create_form")) %>' ToolTip='<%# Eval("create_form") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                        <asp:TemplateField HeaderText="Read" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="read_form_dtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("read_form")) %>' ToolTip='<%# Eval("read_form") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                        <asp:TemplateField HeaderText="Update" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="update_form_dtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("update_form")) %>' ToolTip='<%# Eval("update_form") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                        <asp:TemplateField HeaderText="Delete" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="delete_form_dtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("delete_form")) %>' ToolTip='<%# Eval("delete_form") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                        <asp:TemplateField HeaderText="Approval" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="approval_user_dtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("approval_user")) %>' ToolTip='<%# Eval("approval_user") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                        <asp:TemplateField HeaderText="Req approval_dtl" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="req_approval_dtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("req_approval")) %>' ToolTip='<%# Eval("req_approval") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                            <asp:CommandField ButtonType="Link" ShowSelectButton="True" SelectText="Delete" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <label>Data Not Found</label>
                                        </EmptyDataTemplate>
                                        </asp:GridView> 
                                    </div>                                    
                                </div>
                            </div>

                            <br />
                            <div class="row">
                                <div class="col-sm-5 right">
                                    <div class="form-group">                                        
                                        <asp:Button runat="server" ID="BtnDel" CssClass="btn btn-danger" Text="Delete" onClientClick="return deleteConfirmed(this);" OnClick="BtnDel_Click" />
                                        <asp:Button runat="server" ID="BtnBack" CssClass="btn btn-secondary load" Text="Back" OnClick="BtnBack_Click"/> 
                                    </div>
                                 </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-sm-6">
                                   <i class="col-form-label">Create By : <asp:Label runat="server" ID="lblcreate" /> </i>
                               </div>
                               <div class="col-sm-6">
                                   <i class="col-form-label">Last Update By : <asp:Label runat="server" ID="lblupdate" /> </i>
                               </div> 
                                <div class="col-sm-6">
                                   <i class="col-form-label">Create Time : <asp:Label runat="server" ID="lblcreatetime" /> </i>
                               </div>  
                                <div class="col-sm-6">
                                   <i class="col-form-label">Last Update Time : <asp:Label runat="server" ID="lblupdatetime" /> </i>
                               </div> 
                            </div>

                            <!---- Loading Bar ---->
                            <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                    <div id="processMessage" class="processMessage">
                                       <center>
                                           <span>
                                               <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"/>
                                               <br />Please Wait
                                           </span>
                                       </center>
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress> 

                            <!--- Start PopUp transcode --->
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:Panel id="PanelTranscode" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">List Group item</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group row col-sm-12">
                                                    <div class="col-sm-2 custom-checkbox">
                                                        <label class="col-form-label">Filter</label>
                                                    </div>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <asp:DropDownList id="DDLFilterTranscode" runat="server" CssClass="form-control select2">  
                                                                    <asp:ListItem value="menutype"> Menu Type </asp:ListItem> 
                                                                    <asp:ListItem value="Type"> TYpe </asp:ListItem>
                                                                </asp:DropDownList> 
                                                            </div>
                                                            <asp:TextBox runat="server" ID="txtfilterTranscode" CssClass="form-control" />
                                                            <asp:Button ID="BtnFindTranscode" runat="server" CssClass="btn btn-primary" Text="Find Data" OnClick="BtnFindTranscode_Click"/>
                                                        </div>
                                                    </div> 
                                                                         
                        
                                                </div> 

                                                <div class="form-group row col-sm-12">
                                                       <div class="table-responsive">
                                                        <asp:GridView ID="gvtranscode" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnPageIndexChanging="gvtranscode_PageIndexChanging">
                                                            <Columns> 
                                                                <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                       <asp:CheckBox ID="cbDtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("trancode") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField> 
                                                                <asp:BoundField DataField="trancode" HeaderText="Code" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" /> 
                                                                <asp:BoundField DataField="type" HeaderText="Menu" ItemStyle-Wrap="false" />
                                                                <asp:BoundField DataField="menutype" HeaderText="Type" ItemStyle-Wrap="false" />  

                                                                <asp:TemplateField HeaderText="Special akses" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                       <asp:CheckBox ID="special_akses" runat="server" Checked='<%# Convert.ToBoolean(Eval("special_akses")) %>' ToolTip='<%# Eval("special_akses") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField> 

                                                                <asp:TemplateField HeaderText="Create" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                       <asp:CheckBox ID="create_form" runat="server" Checked='<%# Convert.ToBoolean(Eval("create_form")) %>' ToolTip='<%# Eval("create_form") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField> 

                                                                <asp:TemplateField HeaderText="Read" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                       <asp:CheckBox ID="read_form" runat="server" Checked='<%# Convert.ToBoolean(Eval("read_form")) %>' ToolTip='<%# Eval("read_form") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField> 

                                                                <asp:TemplateField HeaderText="Update" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                       <asp:CheckBox ID="update_form" runat="server" Checked='<%# Convert.ToBoolean(Eval("update_form")) %>' ToolTip='<%# Eval("update_form") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField> 

                                                                <asp:TemplateField HeaderText="Delete" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                       <asp:CheckBox ID="delete_form" runat="server" Checked='<%# Convert.ToBoolean(Eval("delete_form")) %>' ToolTip='<%# Eval("delete_form") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField> 

                                                                <asp:TemplateField HeaderText="Approval" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                       <asp:CheckBox ID="approval_user" runat="server" Checked='<%# Convert.ToBoolean(Eval("approval_user")) %>' ToolTip='<%# Eval("approval_user") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField> 

                                                                <asp:TemplateField HeaderText="Req approval" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                       <asp:CheckBox ID="req_approval" runat="server" Checked='<%# Convert.ToBoolean(Eval("req_approval")) %>' ToolTip='<%# Eval("req_approval") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField> 
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <label>Data Not Found</label>
                                                            </EmptyDataTemplate>
                                                            <PagerStyle HorizontalAlign="Left" />
                                                        </asp:GridView>
                                                    </div> 
                                                  </div>   

                                            </div> 
                                            <div class="modal-footer">  
                                                <asp:Button ID="BtnAddToDtl" runat="server" CssClass="btn btn-default btn-md" Text="Add To Detail" OnClick="BtnAddToDtl_Click" />
                                                <asp:Button ID="BtnClosed" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnClosed_Click"/>
                                            </div>
                                        </div>
                                    </asp:Panel> 

                                    <ajaxToolkit:ModalPopupExtender id="MPETranscode" runat="server" Drag="True" PopupControlID="PanelTranscode" TargetControlID="beTranscode" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                    <asp:Button id="beTranscode" runat="server" Visible="False" CausesValidation="False" />      

                                     <asp:UpdateProgress runat="server" ID="UpdateProgress3" AssociatedUpdatePanelID="UpdatePanel2">
                                        <ProgressTemplate>
                                            <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                            <div id="processMessage" class="processMessage">
                                                <center>
                                                    <span>
                                                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/loading_animate.gif" />
                                                        <br />Please Wait
                                                    </span>
                                                </center>
                                                <br />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>

                                </ContentTemplate>
                            </asp:UpdatePanel> 
                            <!--- End PopUp Item Group --->

                            <!--- Start PopUp Message --->
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:Panel id="PanelsMsg" runat="server" Visible="False" Width="70%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">
                                                    <asp:Label ID="lblInfo" runat="server" Text=""></asp:Label>
                                                </h4>
                                            </div>

                                            <div class="modal-body">                                                   
                                                <div class="col-sm-12">                                                         
                                                    <center>
                                                        <asp:Label ID="lblMessage" runat="server" Text="" />
                                                    </center> 
                                                </div>       
                                            </div> 

                                            <div class="modal-footer">   
                                                <div class="col-sm-12">
                                                 <center>
                                                    <asp:Button ID="BtnOK" runat="server" CssClass="btn btn-danger" Text="OK" OnClick="BtnOK_Click" />
                                                </center>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </asp:Panel> 

                                    <ajaxToolkit:ModalPopupExtender id="MpesMsg" runat="server" Drag="True" PopupControlID="PanelsMsg" TargetControlID="BesMsg" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                    <asp:Button id="BesMsg" runat="server" Visible="False" CausesValidation="False" />       

                                </ContentTemplate>
                            </asp:UpdatePanel> 
                            <!--- End PopUp Item Group ---> 

                        </ContentTemplate>
                    </asp:UpdatePanel>                
                </div>
            </div>
        </div>
    </section> 
<script type="text/javascript">
    function deleteConfirmed(btndel) {
        if (btndel.dataset.confirmed) {
            btndel.dataset.confirmed = false;
            return true;
        } else {
            event.preventDefault();
            Swal.fire({
                title: 'Are you sure to Delete this data?',
                text: "Deleted data can't be returned!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Delete this data!'
            }).then((result) => {
                if (result.value) {
                    $.blockUI({ message: bMsg });
                    btndel.dataset.confirmed = true;
                    btndel.click(); 
                }
            });
        }
        return false;
    };
</script>
</asp:Content>

