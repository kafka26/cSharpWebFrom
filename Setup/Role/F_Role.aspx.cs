﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using CrystalDecisions.CrystalReports.Engine;
using System.Reflection;

public partial class F_Setup_Role : System.Web.UI.Page
{
    DataClassesDataContext db = new DataClassesDataContext();
    m_role TblHdr = new m_role();
    m_roledtl Tbldtl = new m_roledtl();
    ClassGlobal sVar = new ClassGlobal();
    Int32 trnCode = 18;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["username"].ToString()))
            {
                ClassGlobal.data = ClassGlobal.CekMenuByUser(Session["username"].ToString(), trnCode);
                if (ClassGlobal.data.Rows.Count > 0)
                {
                    if (!this.IsPostBack)
                    {
                        if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
                        {
                            flag_.Text = "1";
                            this.Filltexbox(Request.QueryString["id"]);
                            this.FillTexDtl(Request.QueryString["id"]);
                            BtnSave.Enabled = false;
                            btnChangeHdr.Enabled = true;
                        }
                        else
                        {
                            flag_.Text = "0";
                            BtnSave.Enabled = true;
                            btnChangeHdr.Enabled = false;
                            BtnMultiTranscode.Enabled = false;
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                } 
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/Login.aspx");
        } 
    } 

    private void Filltexbox(string iduser) {
        try {
            TblHdr = db.m_roles.Where(r => r.roleid == int.Parse(iduser)).FirstOrDefault();
            id_.Text = TblHdr.roleid.ToString();
            rolename.Text = TblHdr.rolename.ToString();
            rbflag.SelectedValue = TblHdr.m_flag.ToString();
            lblcreate.Text = TblHdr.createby.ToString();
            lblupdate.Text = TblHdr.lastupdateby.ToString();
            lblcreatetime.Text = TblHdr.createtime.ToString();
            lblupdatetime.Text = TblHdr.lastupdatetime.ToString();
        }
        catch (Exception ex)
        {   
            //this.CustomAlert(sender, ex.ToString());
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    private void FillTexDtl(string idHdr)
    {
        var data = (from r in db.m_roledtls join t in db.m_transcodes on r.transcode equals t.trancode where r.roleid==int.Parse(idHdr) orderby r.transcode select new { 
            r.roledtlid, r.roleid, t.trancode, type=t.type.ToUpper(), menutype=t.menutype.ToUpper(), menufolder=t.menufolder.ToUpper(), menufile=t.menufile.ToUpper(),
            r.special_akses,
            r.read_form,
            r.create_form,
            r.update_form,
            r.delete_form,
            r.req_approval,
            r.approval_user
        }).ToList(); 
        gvDtl.DataSource = data;
        gvDtl.DataBind();
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        var Msg = "";
        if (rolename.Text.ToString().Trim()=="") 
            Msg += "Please, fill role name first..!!<br />"; 

        if (Msg!="")
        {
            //this.CustomAlert(sender, Msg);
            this.sMsg(Msg, "2");
            return;
        }

        if (flag_.Text!="0")
        {
            TblHdr = db.m_roles.Single(i => i.roleid == int.Parse(id_.Text));
        }
        else
        {
            int idNew = db.m_roles.Max(i => i.roleid)+1;
            id_.Text = idNew.ToString();
        }
         
        try
        {
            TblHdr.rolename = rolename.Text.Trim().ToUpper();
            TblHdr.m_flag = rbflag.SelectedValue;
            TblHdr.lastupdateby = "ADMIN";
            TblHdr.lastupdatetime = DateTime.Now;
            if (flag_.Text == "0")
            {
                TblHdr.createby = "admin";
                TblHdr.createtime = DateTime.Now;
                db.m_roles.InsertOnSubmit(TblHdr);
            }
            db.SubmitChanges(); 
        }        
        catch(Exception ex)
        { 
            this.CustomAlert(sender, ex.ToString());
            //this.sMsg(ex.ToString(), "1");
            return;
        }
        btnChangeHdr.Enabled = true;
        BtnMultiTranscode.Enabled = true;
        Response.Redirect("~/Setup/Role/F_Role.aspx?id=" + id_.Text);
    }
    
    protected void btnChangeHdr_Click(object sender, EventArgs e)
    {
        this.BtnSave_Click(sender, e);
    }

    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Setup/Role/LF_Role.aspx");
    }

    protected void BtnDel_Click(object sender, EventArgs e)
    {
        if (flag_.Text != "0")
        { 
            try
            {
                TblHdr = db.m_roles.Single(i => i.roleid == int.Parse(id_.Text));
                db.m_roles.DeleteOnSubmit(TblHdr);

                Tbldtl = db.m_roledtls.Single(i => i.roleid == int.Parse(id_.Text));  
                db.m_roledtls.DeleteOnSubmit(Tbldtl); 
                Response.Redirect("~/Setup/Role/LF_Role.aspx");
            }
            catch (Exception ex)
            {  
                this.sMsg(ex.ToString(), "1");
                return; 
            }
        } 
    }

    //=== PopUP Detail=//

    private void UpdateCheckedValue()
    {
        if (!string.IsNullOrEmpty(Session["MdlTranscode"].ToString()))
        {
            ClassGlobal.data = Session["MdlTranscode"] as DataTable;
            ClassGlobal.DtView = ClassGlobal.data.DefaultView;
            for (int i = 0; i < gvtranscode.Rows.Count; i++)
            {
              System.Web.UI.WebControls.GridViewRow row = gvtranscode.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls; 
                    Boolean cbCheck = false;
                    string trancode = "";
                   
                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            trancode = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    ClassGlobal.DtView.RowFilter = "trancode='" + trancode + "'";
                    if (ClassGlobal.DtView.Count ==1 )
                    {
                        if (cbCheck==true)
                        {
                            ClassGlobal.DtView[0]["checkvalue"] = "true";
                        }
                    }
                    ClassGlobal.DtView.RowFilter = "";
                    ClassGlobal.data.AcceptChanges();
                    Session["MdlTranscode"] = ClassGlobal.data;
                }
            }
        }

        if (!string.IsNullOrEmpty(Session["MdlTranscode_Hist"].ToString()))
        {
            ClassGlobal.data = Session["MdlTranscode_Hist"] as DataTable;
            ClassGlobal.DtView = ClassGlobal.data.DefaultView;
            for (int i = 0; i < gvtranscode.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvtranscode.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string trancode = "";

                    foreach (System.Web.UI.Control myControl in cc)
                    {
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            trancode = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        }
                    }
                    ClassGlobal.DtView.RowFilter = "trancode='" + trancode + "'";
                    if (ClassGlobal.DtView.Count == 1)
                    {
                        if (cbCheck == true)
                        {
                            ClassGlobal.DtView[0]["checkvalue"] = "true";
                        }
                    }
                    ClassGlobal.DtView.RowFilter = "";
                    ClassGlobal.data.AcceptChanges();
                    Session["MdlTranscode_Hist"] = ClassGlobal.data;
                }
            }
        } 
    }

    private void GetTranscode()
    {
        DataTable sData = ClassGlobal.ToDataTable((from t in db.m_transcodes orderby t.trancode descending select new { 
            checkvalue = "false", trancode = int.Parse(t.trancode.ToString()), type=t.type.ToUpper(), 
            menutype = t.menutype.ToUpper(), menufolder=t.menufolder.ToUpper(), menufile=t.menufile.ToUpper(),
            special_akses = false,
            read_form = false,
            create_form = false,
            update_form = false,
            delete_form = false,
            req_approval = false,
            approval_user = false
        }).ToList());
        gvtranscode.DataSource = sData;
        gvtranscode.DataBind();
        Session["MdlTranscode"] = sData;
        Session["MdlTranscode_Hist"] = sData; 
    }

    protected void BtnClosed_Click(object sender, EventArgs e)
    {
        txtfilterTranscode.Text = ""; 
        sVar.SetModalPopUp(MPETranscode, PanelTranscode, beTranscode, false);
    }

    protected void gvtranscode_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValue();
        gvtranscode.PageIndex = e.NewPageIndex;
        gvtranscode.DataSource = Session["MdlTranscode_Hist"];
        gvtranscode.DataBind();
        MPETranscode.Show();
    }

    private static Boolean isOnDtl(int trancode, GridView GvData)
    {
        for (int Index = 0; Index < GvData.Rows.Count; Index++)
        {
            if (trancode == Convert.ToInt32(GvData.DataKeys[Index]["trancode"]))
            {
                return true;
            }
        }
        return false;
    }

    protected void BtnAddToDtl_Click(object sender, EventArgs e)
    {
        this.UpdateCheckedValue();
        string sMsg = "";

        if (!string.IsNullOrEmpty(Session["MdlTranscode"].ToString()))
        {
            ClassGlobal.data = Session["MdlTranscode"] as DataTable;
            ClassGlobal.DtView = ClassGlobal.data.DefaultView;
            ClassGlobal.DtView.RowFilter = "checkvalue='True'";
            if (ClassGlobal.DtView.Count<=0)
            {  
                this.sMsg("Sorry, can't delete this data..!!<br>", "2");
            }

            if (ClassGlobal.DtView.Count > 0)
            {
                for (int c1 = 0; c1 < ClassGlobal.DtView.Count; c1++)
                {
                    if (isOnDtl(Convert.ToInt32(ClassGlobal.DtView[c1]["trancode"]), gvDtl) == true)
                    {
                        sMsg += "Menu " + ClassGlobal.DtView[c1]["type"] + " has been added !<br />";
                    }
                }                
            }
        }

        ClassGlobal.DtView.RowFilter = ""; 

        try
        {
            if (!string.IsNullOrEmpty(Session["MdlTranscode"].ToString()))
            {
                ClassGlobal.data = Session["MdlTranscode"] as DataTable;
                ClassGlobal.DtView = ClassGlobal.data.DefaultView;
                ClassGlobal.DtView.RowFilter = "checkvalue='True'";
                //if (ClassGlobal.DtView.Count > 0)
                //{
                //    for (int i = 0; i < ClassGlobal.DtView.Count; i++)
                //    {
                //        Tbldtl.roleid = int.Parse(id_.Text);
                //        Tbldtl.transcode = int.Parse(ClassGlobal.DtView[i]["trancode"].ToString());
                //        if (flag_.Text == "0")
                //        {
                //            Tbldtl.approval_user = true;
                //            Tbldtl.create_form = true;
                //            Tbldtl.delete_form = true;
                //            Tbldtl.read_form = true;
                //            Tbldtl.req_approval = true;
                //            Tbldtl.special_akses = true;
                //            Tbldtl.update_form = true;
                //            Tbldtl.createby = Session["username"].ToString();
                //            Tbldtl.createtime = DateTime.Now;
                //        }
                //        Tbldtl.lastupdateby = Session["username"].ToString();
                //        Tbldtl.lastupdatetime = DateTime.Now;
                //        db.m_roledtls.InsertOnSubmit(Tbldtl);
                //        db.SubmitChanges();
                //    }
                //}                
            }            
        }
        catch (Exception ex)
        { 
            this.sMsg(ex.ToString(), "1");
            return;
        }
        txtfilterTranscode.Text = "";
        sVar.SetModalPopUp(MPETranscode, PanelTranscode, beTranscode, false);
        Response.Redirect("~/Setup/Role/F_Role.aspx?id=" + id_.Text);
    }

    protected void BtnMultiTranscode_Click(object sender, EventArgs e)
    {
        txtfilterTranscode.Text = "";
        this.GetTranscode();
        sVar.SetModalPopUp(MPETranscode, PanelTranscode, beTranscode, true);
    }
         
    //=== PopUP Detail=//

    private void sMsg(string sMsg, string type)
    {
        if (type=="1")
        {
            lblInfo.Text = "ERROR";            
        }
        else
        {
            lblInfo.Text = "INFORMATION";
        }
        lblMessage.Text = sMsg.ToUpper().ToString();
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, true);
    }
    
    protected void BtnFindTranscode_Click(object sender, EventArgs e)
    {
        this.UpdateCheckedValue();
        ClassGlobal.data = Session["MdlTranscode"] as DataTable;
        ClassGlobal.DtView = ClassGlobal.data.DefaultView;
        if (ClassGlobal.data.Rows.Count>0)
        {            
            if (txtfilterTranscode.Text != "")
            {
                ClassGlobal.DtView.RowFilter = DDLFilterTranscode.SelectedValue.ToString() + " LIKE '%" + txtfilterTranscode.Text.ToString().Trim() + "%'";
            }
            Session["MdlTranscode"] = ClassGlobal.DtView.ToTable();
            gvtranscode.DataSource = Session["MdlTranscode"];
            gvtranscode.DataBind();
            MPETranscode.Show();
        } 
    }

    protected void BtnOK_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, false);
    }

    private void CustomAlert(object sender, string sMsg )
    { 
        ScriptManager.RegisterClientScriptBlock(this , typeof(Page), "alert", "Swal.fire('', '" + sMsg + "', 'error');", true);
    }

    protected void gvDtl_SelectedIndexChanged(object sender, EventArgs e)
    {
        Tbldtl = db.m_roledtls.Single(i => i.roledtlid == int.Parse(gvDtl.SelectedDataKey["roledtlid"].ToString()));
        SqlTransaction ObjTrans;
        if (ClassGlobal.cKon.State == ConnectionState.Closed)
        {
            ClassGlobal.cKon.Open();
        }
        ObjTrans = ClassGlobal.cKon.BeginTransaction();
        try
        {
            db.m_roledtls.DeleteOnSubmit(Tbldtl);
            db.SubmitChanges();
            ObjTrans.Commit();
            ClassGlobal.cKon.Close();           
        }
        catch (Exception ex)
        {
            ObjTrans.Rollback();
            ClassGlobal.cKon.Close();
            //this.CustomAlert(sender,ex.ToString());
            this.sMsg(ex.ToString(), "1");
            return;
        }
        Response.Redirect("~/Setup/Role/F_Role.aspx?id=" + id_.Text);
    } 
}