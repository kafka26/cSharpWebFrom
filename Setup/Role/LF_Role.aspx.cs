﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using CrystalDecisions.CrystalReports.Engine; 
using System.Reflection;

public partial class LF_setup_Role : System.Web.UI.Page
{ 
    DataClassesDataContext db = new DataClassesDataContext();
    ClassGlobal sVar = new ClassGlobal();
    Int32 trnCode = 18;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ClassGlobal.data = ClassGlobal.CekMenuByUser(Session["username"].ToString(), trnCode);
            if (ClassGlobal.data.Rows.Count > 0) 
                if (!string.IsNullOrEmpty(Session["username"].ToString())) 
                    if (!this.IsPostBack) 
                        this.BindData();   
            else 
                Response.Redirect("~/Login.aspx"); 
        }
        catch (Exception ex)
        {
            Response.Redirect("~/Login.aspx");
        }
    } 
     
    private void BindData()
    { 
        var dt = (from u in db.m_roles select new { u.roleid, rolename = u.rolename.ToUpper(), u.m_flag, u.createtime, createby=u.createby.ToUpper(), u.lastupdatetime, lastupdateby=u.lastupdateby.ToUpper() }).ToList();
        ClassGlobal.data = ClassGlobal.ToDataTable(dt);
        Session["roleview"] = ClassGlobal.data;
        Session["role"] = ClassGlobal.data;
        gvlist.DataSource = Session["roleview"];
        gvlist.DataBind(); 
    }

    protected void btnadd_Click(object sender, EventArgs e)
    { 
        if (Session["roleview"] != null)
        {
            gvlist.DataSource = null;
            gvlist.DataBind();
            ClassGlobal.data = Session["roleview"] as DataTable;
            DataView DtView = ClassGlobal.data.DefaultView;
            DtView.RowFilter = ddlfilter.SelectedValue + " LIKE '%" + txtfilter.Text + "%'";
            ClassGlobal.data.AcceptChanges();
            gvlist.DataSource = ClassGlobal.data;
            gvlist.DataBind();
            DtView.RowFilter = "";
        }
        else
        {
            BindData();
        }
    }
    
    protected void gvlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvlist.PageIndex = e.NewPageIndex;
        gvlist.DataSource = Session["roleview"];
        gvlist.DataBind();
    }
     
}