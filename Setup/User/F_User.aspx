﻿<%@ Page Title="Master Katalog" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="F_User.aspx.cs" Inherits="F_User_User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">Form User</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-sm-12">
                                   <asp:label runat="server" ID="id_" Visible="false" />
                                   <asp:label runat="server" ID="flag_" Visible="false" />
                                </div>
                            </div>
                             

                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="col-form-label">Userid</label> 
                                    <asp:TextBox runat="server" ID="Userid" CssClass="form-control" /> 
                               </div>  
                               <div class="col-sm-4">
                                    <label class="col-form-label">Username</label> 
                                    <asp:TextBox runat="server" ID="Username" CssClass="form-control" />
                               </div>
                              
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="col-form-label">Password</label> 
                                    <asp:TextBox runat="server" ID="Password" CssClass="form-control" TextMode="Password" />
                               </div> 

                                <div class="col-sm-2">
                                    <label class="col-form-label">Gender</label> 
                                    <asp:DropDownList id="gender" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="male"> Male </asp:ListItem> 
                                        <asp:ListItem value="female"> female </asp:ListItem> 
                                    </asp:DropDownList> 
                               </div>  
                                
                               <div class="col-sm-2">
                                   <div class="form-group clearfix">
                                       <label class="col-form-label">Flag</label> 
                                        <asp:DropDownList ID="rbflag" runat="server" CssClass="form-control select2">
                                            <asp:ListItem value="Aktif"> Aktif </asp:ListItem> 
                                            <asp:ListItem value="Tidak"> Tidak </asp:ListItem>  
                                        </asp:DropDownList>
                                   </div>
                               </div> 
                            </div>

                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <asp:Button runat="server" ID="BtnSave" CssClass="btn btn-success load" Text="Save" OnClick="BtnSave_Click"/>
                                        <asp:Button runat="server" ID="btnChangeHdr" CssClass="btn btn-secondary" Text="Change Header" Enabled="false" OnClick="btnChangeHdr_Click"/>
                                    </div>
                                 </div>
                            </div>

                            <!---- Detail Role ---->
                            <hr />
                            <div class="row">
                               <div class="col-sm-6">
                                    <label class="col-form-label">Search For Multiple Add</label><br />
                                    <asp:Button runat="server" ID="BtnMultiRole" CssClass="btn btn-info" Text="Add Multiple Data" Enabled="true" OnClick="BtnMultiRole_Click"/>
                               </div> 
                            </div>

                            <hr />
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <asp:GridView ID="gvDtl" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="userdtlid" OnRowDeleting="gvDtl_RowDeleting"> 
                                        <Columns>                                            
                                            <asp:BoundField DataField="userdtlid" HeaderText="" ReadOnly="True" HeaderStyle-Wrap="false" Visible="false"/>
                                            <asp:BoundField DataField="rolename" HeaderText="Code" /> 
                                            <asp:BoundField DataField="m_flag" HeaderText="Menu" />                                           
                                            <asp:CommandField ButtonType="Link" ShowDeleteButton="true" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <label>Data Not Found</label>
                                        </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <hr />
                            <div class="row">
                                <div class="col-sm-5 right">
                                    <div class="form-group">                                        
                                        <asp:Button runat="server" ID="BtnDel" CssClass="btn btn-danger" Text="Delete" onClientClick="return deleteConfirmed(this);" OnClick="BtnDel_Click" />
                                        <asp:Button runat="server" ID="BtnBack" CssClass="btn btn-secondary load" Text="Back" OnClick="BtnBack_Click"/> 
                                    </div>
                                 </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                   <i class="col-form-label">Create By : <asp:Label runat="server" ID="lblcreate" /> </i>
                               </div>
                               <div class="col-sm-6">
                                   <i class="col-form-label">Last Update By : <asp:Label runat="server" ID="lblupdate" /> </i>
                               </div> 
                                <div class="col-sm-6">
                                   <i class="col-form-label">Create Time : <asp:Label runat="server" ID="lblcreatetime" /> </i>
                               </div>  
                                <div class="col-sm-6">
                                   <i class="col-form-label">Last Update Time : <asp:Label runat="server" ID="lblupdatetime" /> </i>
                               </div> 
                            </div>

                            <!---- Loading Bar ---->
                            <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                    <div id="processMessage" class="processMessage">
                                       <center>
                                           <span>
                                               <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"/>
                                               <br />Please Wait
                                           </span>
                                       </center>
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress> 

                             <!--- Start PopUp Role --->
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:Panel id="PanelRole" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">List Group item</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group row col-sm-12">
                                                    <div class="col-sm-2 custom-checkbox">
                                                        <label class="col-form-label">Filter</label>
                                                    </div>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <asp:DropDownList id="DDLFilterRole" runat="server" CssClass="form-control select2">  
                                                                    <asp:ListItem value="rolename"> Rolename </asp:ListItem> 
                                                                </asp:DropDownList> 
                                                            </div>
                                                            <asp:TextBox runat="server" ID="txtfilterRole" CssClass="form-control" />
                                                            <asp:Button ID="BtnFindRole" runat="server" CssClass="btn btn-primary" Text="Find Data" OnClick="BtnFindRole_Click"/>
                                                        </div>
                                                    </div>
                         
                                                     <br />
                                                     <div class="col-sm-12">
                                                       <div class="table-responsive">
                                                        <asp:GridView ID="gvRole" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnPageIndexChanging="gvRole_PageIndexChanging">
                                                            <Columns> 
                                                                  <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="cbDtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("roleid") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField> 
                                                                <asp:BoundField DataField="rolename" HeaderText="Role" /> 
                                                                <asp:BoundField DataField="createby" HeaderText="Create" />
                                                                <asp:BoundField DataField="m_flag" HeaderText="Status" />  
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <label>Data Not Found</label>
                                                            </EmptyDataTemplate>
                                                            <PagerStyle HorizontalAlign="Left" />
                                                        </asp:GridView>
                                                    </div> 
                                                  </div>                       
                        
                                                </div> 
                                            </div> 
                                            <div class="modal-footer">  
                                                <asp:Button ID="BtnAddToDtl" runat="server" CssClass="btn btn-default btn-md" Text="Add To Detail" OnClick="BtnAddToDtl_Click"/>
                                                <asp:Button ID="BtnClosed" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnClosed_Click"/>
                                            </div>
                                        </div>
                                    </asp:Panel> 

                                    <ajaxToolkit:ModalPopupExtender id="MPERole" runat="server" Drag="True" PopupControlID="PanelRole" TargetControlID="beRole" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                    <asp:Button id="beRole" runat="server" Visible="False" CausesValidation="False" />      

                                     <asp:UpdateProgress runat="server" ID="UpdateProgress3" AssociatedUpdatePanelID="UpdatePanel2">
                                        <ProgressTemplate>
                                            <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                            <div id="processMessage" class="processMessage">
                                                <center>
                                                    <span>
                                                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/loading_animate.gif" />
                                                        <br />Please Wait
                                                    </span>
                                                </center>
                                                <br />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>

                                </ContentTemplate>
                            </asp:UpdatePanel> 
                            <!--- End PopUp Item Group --->

                            <!--- Start PopUp Message --->
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:Panel id="PanelsMsg" runat="server" Visible="False" Width="70%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">
                                                    <asp:Label ID="lblInfo" runat="server" Text=""></asp:Label>
                                                </h4>
                                            </div>
                                            
                                            <div class="modal-body">                                                   
                                                <div class="col-sm-12">                                                         
                                                    <center>
                                                        <asp:Label ID="lblMessage" runat="server" Text="" />
                                                    </center> 
                                                </div>       
                                            </div> 

                                            <div class="modal-footer">   
                                                <div class="col-sm-12">
                                                 <center>
                                                    <asp:Button ID="BtnOK" runat="server" Text="OK" OnClick="BtnOK_Click" CssClass="btn btn-danger" />
                                                </center>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </asp:Panel> 

                                    <ajaxToolkit:ModalPopupExtender id="MpesMsg" runat="server" Drag="True" PopupControlID="PanelsMsg" TargetControlID="BesMsg" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                    <asp:Button id="BesMsg" runat="server" Visible="False" CausesValidation="False" />       

                                </ContentTemplate>
                            </asp:UpdatePanel> 
                            <!--- End PopUp Item Group --->
     
                            <script type="text/javascript">
                                function deleteConfirmed(btndel) {
                                    if (btndel.dataset.confirmed) {
                                        btndel.dataset.confirmed = false;
                                        return true;
                                    } else {
                                        event.preventDefault();
                                        Swal.fire({
                                            title: 'Are you sure to Delete this data?',
                                            text: "Deleted data can't be returned!",
                                            type: 'warning',
                                            showCancelButton: true,
                                            confirmButtonText: 'Yes, Delete this data!'
                                        }).then((result) => {
                                            if (result.value) {
                                                $.blockUI({ message: bMsg });
                                                btndel.dataset.confirmed = true;
                                                btndel.click(); 
                                            }
                                        });
                                    }
                                    return false;
                                };
                            </script>

                        </ContentTemplate>
                    </asp:UpdatePanel>                
                </div>
            </div>
        </div>
    </section> 
</asp:Content>

