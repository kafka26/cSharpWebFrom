﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using CrystalDecisions.CrystalReports.Engine;
using System.Reflection;
using static ClassGlobal;
public partial class F_User_User : System.Web.UI.Page
{
    DataClassesDataContext db = new DataClassesDataContext();
    userlogin Tbl = new userlogin();
    userlogindtl Tbldtl = new userlogindtl();
    ClassGlobal sVar = new ClassGlobal();
    Int32 trnCode = 14;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["username"].ToString()))
            {
                if (!this.IsPostBack)
                {
                    if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
                    {
                        flag_.Text = "1";
                        this.Filltexbox(Request.QueryString["id"]);
                        this.FillTexDtl(Request.QueryString["id"]);
                        BtnSave.Enabled = false;
                        btnChangeHdr.Enabled = true;
                    }
                    else
                    {
                        flag_.Text = "0";
                        BtnSave.Enabled = true;
                        btnChangeHdr.Enabled = false;
                        BtnMultiRole.Enabled = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/Login.aspx");
        } 
    }

    private void Filltexbox(string iduser) {        
        try
        {
            Tbl = db.userlogins.Where(o => o.userid == iduser).FirstOrDefault();
            id_.Text = Tbl.userid.ToString().ToUpper();
            Userid.Text = Tbl.userid.ToString().ToUpper();
            Username.Text = Tbl.username.ToString().ToUpper();
            Password.Text = Tbl.password.ToString().ToUpper();
            gender.SelectedValue = Tbl.password.ToString().ToUpper();
            rbflag.SelectedValue = Tbl.m_flag.ToString();
            lblcreate.Text = Tbl.createby.ToString().ToUpper();
            lblupdate.Text = Tbl.lastupdateby.ToString().ToUpper();
            lblcreatetime.Text = Tbl.createtime.ToString();
            lblupdatetime.Text = Tbl.lastupdatetime.ToString();
            Userid.Enabled = false;
            Password.Enabled = false;
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }        
    }

    private void FillTexDtl(string iduser)
    {
        var data = (from r in db.userlogindtls join t in db.m_roles on r.roleid equals t.roleid where r.userid==iduser.ToString() orderby r.roleid select new { r.userdtlid, r.roleid, t.rolename, r.userid, t.m_flag }).ToList();
        gvDtl.DataSource = data;
        gvDtl.DataBind();
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {       
        if (flag_.Text!="0") 
            Tbl = db.userlogins.Single(i => i.userid == id_.Text.ToString()); 
        SqlTransaction ObjTrans; 
        if (cKon.State == ConnectionState.Closed) 
            cKon.Open(); 
        ObjTrans = cKon.BeginTransaction();

        try
        {
            Tbl.username = Username.Text.Trim().ToUpper();
            Tbl.gender = gender.SelectedValue.ToUpper(); 
            Tbl.m_flag = rbflag.SelectedValue;
            Tbl.lastupdateby = "ADMIN";
            Tbl.lastupdatetime = DateTime.Now;
            if (flag_.Text == "0")
            {
                Tbl.password = Password.Text.Trim().ToUpper();
                Tbl.userid = Userid.Text.Trim().ToUpper();
                Tbl.createby = Session["username"].ToString();
                Tbl.createtime = DateTime.Now;
                db.userlogins.InsertOnSubmit(Tbl);
            }

            db.SubmitChanges();
            ObjTrans.Commit();
            cKon.Close();            
        }
        catch(Exception ex)
        {
            ObjTrans.Rollback();
            cKon.Close();
            this.sMsg(ex.ToString(), "1");
            return;
        }
        btnChangeHdr.Enabled = true;
        BtnMultiRole.Enabled = true;
        Response.Redirect("~/Setup/User/F_User.aspx?id=" + id_.Text);
    }

    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Setup/User/LF_User.aspx");
    }

    protected void BtnDel_Click(object sender, EventArgs e)
    {
        if (flag_.Text != "0")
        {
            SqlTransaction ObjTrans;
            if (cKon.State == ConnectionState.Closed) 
                cKon.Open(); 
            ObjTrans = cKon.BeginTransaction();

            try
            {                
                Tbl = db.userlogins.Single(i => i.userid == id_.Text.Trim().ToUpper());
                db.userlogins.DeleteOnSubmit(Tbl);
                db.SubmitChanges();
                ObjTrans.Commit();
                cKon.Close(); 
            }
            catch(Exception ex)
            {
                ObjTrans.Rollback();
                cKon.Close();
                this.sMsg(ex.ToString(), "1");
                return;
            }
            Response.Redirect("~/Setup/User/LF_User.aspx");
        }
    }

    //== POP UP VALIDASI ==//
    private void sMsg(string sMsg, string type)
    {
        if (type == "1") 
            lblInfo.Text = "ERROR"; 
        else if (type == "2") 
            lblInfo.Text = "INFORMATION"; 
        else 
            lblInfo.Text = "INFORMATION"; 
        lblMessage.Text = sMsg.ToUpper().ToString();
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, true);
    }

    protected void BtnOK_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, false);
    }

    private void CustomAlert(object sender, string sMsg)
    {
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "alert", "Swal.fire('', '" + sMsg + "', 'error');", true);
    }

    //== END POP UP VALIDASI ==//

    //== START POP UP ROLE ==// 
    private void UpdateCheckedValue()
    {
        if (!string.IsNullOrEmpty(Session["MdlRole"].ToString()))
        {
            data = Session["MdlRole"] as DataTable;
            DtView = data.DefaultView;
            for (int i = 0; i < gvRole.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvRole.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string trancode = "";

                    foreach (System.Web.UI.Control myControl in cc) 
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            trancode = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        } 
                    DtView.RowFilter = "roleid=" + int.Parse(trancode);
                    if (DtView.Count == 1) 
                        if (cbCheck == true) 
                            DtView[0]["checkvalue"] = "true";  

                    DtView.RowFilter = "";
                    data.AcceptChanges();
                    Session["MdlRole"] = data;
                }
            }
        }

        if (!string.IsNullOrEmpty(Session["MdlRole_Hist"].ToString()))
        {
            data = Session["MdlRole_Hist"] as DataTable;
            DtView = data.DefaultView;
            for (int i = 0; i < gvRole.Rows.Count; i++)
            {
                System.Web.UI.WebControls.GridViewRow row = gvRole.Rows[i];
                if (row.RowType == DataControlRowType.DataRow)
                {
                    System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                    Boolean cbCheck = false;
                    string trancode = "";

                    foreach (System.Web.UI.Control myControl in cc) 
                        if (myControl is System.Web.UI.WebControls.CheckBox)
                        {
                            cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                            trancode = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                        } 
                    DtView.RowFilter = "roleid=" + int.Parse(trancode);
                    if (DtView.Count == 1) 
                        if (cbCheck == true) 
                            DtView[0]["checkvalue"] = "true"; 
                    DtView.RowFilter = "";
                    data.AcceptChanges();
                    Session["MdlRole_Hist"] = data;
                }
            }
        }
    }

    private void GetRole()
    {
        data = ToDataTable((from i in db.m_roles where i.m_flag == "Aktif" orderby i.roleid descending select new { checkvalue = "false", i.roleid, rolename = i.rolename.ToUpper(), i.m_flag, createby = i.createby.ToUpper(), i.createtime, lastupdateby = i.lastupdateby.ToUpper(), i.lastupdatetime }).ToList());
        gvRole.DataSource = data;
        gvRole.DataBind();
        Session["MdlRole"] = data;
        Session["MdlRole_Hist"] = data;
    }
    
    protected void gvRole_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValue();
        gvRole.PageIndex = e.NewPageIndex;
        gvRole.DataSource = Session["MdlRole_Hist"];
        gvRole.DataBind();
        MPERole.Show();
    }

    protected void BtnFindRole_Click(object sender, EventArgs e)
    {
        this.UpdateCheckedValue();
        data = Session["MdlRole"] as DataTable;
        DtView = data.DefaultView;
        if (data.Rows.Count > 0)
        {
            if (txtfilterRole.Text != "") 
                DtView.RowFilter = DDLFilterRole.SelectedValue.ToString() + " LIKE '%" + txtfilterRole.Text.ToString().Trim() + "%'"; 
            Session["MdlRole"] = DtView.ToTable();
            gvRole.DataSource = Session["MdlRole"];
            gvRole.DataBind();
            MPERole.Show();
        }
    }

    protected void BtnClosed_Click(object sender, EventArgs e)
    {
        txtfilterRole.Text = "";
        sVar.SetModalPopUp(MPERole, PanelRole, beRole, false);
    }

    private static Boolean isCekDtl(int idrole, GridView gvDtl)
    {
        foreach (DataRow row in gvDtl.Rows) 
            if (idrole == int.Parse(row["roleid"].ToString())) 
                return true; 
        return false;
    }

    protected void BtnAddToDtl_Click(object sender, EventArgs e)
    {
        this.UpdateCheckedValue();
        var sMsg = "";
        if (!string.IsNullOrEmpty(Session["MdlRole"].ToString()))
        {
            data = Session["MdlRole"] as DataTable;
            DtView = data.DefaultView;
            DtView.RowFilter = "checkvalue='True'";

            if (DtView.Count <= 0) 
                sMsg += "Sorry, can't delete this data..!!<br>"; 

            foreach (DataRow row in DtView)
            {
                Boolean idnya = isCekDtl(Convert.ToInt32(row["roleid"]), gvDtl);
                if (idnya == true) sMsg += "Item " + row["rolename"] + " has been added !<br />"; 
            }

            if (sMsg != "")
            {
                this.CustomAlert(sender, sMsg);
                return;
                //this.sMsg(/*"Maaf, data tidak bisa di delete..!!<br>"*/, "1");
            }

        }

        DtView.RowFilter = "";
        SqlTransaction ObjTrans;
        if (cKon.State == ConnectionState.Closed) 
            cKon.Open(); 

        ObjTrans = cKon.BeginTransaction();
        try
        {
            if (!string.IsNullOrEmpty(Session["MdlRole"].ToString()))
            {
                data = Session["MdlRole"] as DataTable;
                DtView = data.DefaultView;
                DtView.RowFilter = "checkvalue='True'";
                if (DtView.Count > 0)
                {
                    for (int i = 0; i < DtView.Count; i++)
                    {
                        //Tbldtl = db.userlogindtls.Single(i => i.userid == id_.Text.ToString() & i.userdtlid == int.Parse(cell.ToString()));
                        Tbldtl.roleid = int.Parse(DtView[i]["roleid"].ToString());
                        Tbldtl.userid = id_.Text.ToString().ToUpper();
                        if (flag_.Text == "0")
                        {
                            Tbldtl.createby = Session["username"].ToString();
                            Tbldtl.createtime = DateTime.Now;
                        }
                        Tbldtl.lastupdateby= Session["username"].ToString();
                        Tbldtl.lastupdatetime = DateTime.Now;
                        db.userlogindtls.InsertOnSubmit(Tbldtl);
                        db.SubmitChanges();
                    }
                }
                ObjTrans.Commit();
                cKon.Close();
            }
        }
        catch (Exception ex)
        {
            ObjTrans.Rollback();
            cKon.Close();
            //CustomAlert(sender, ex.ToString());
            this.sMsg(ex.ToString(), "1");
            return;
        }
        txtfilterRole.Text = "";
        sVar.SetModalPopUp(MPERole, PanelRole, beRole, false);
        Response.Redirect("~/Setup/user/F_User.aspx?id=" + id_.Text);
    }

    protected void btnChangeHdr_Click(object sender, EventArgs e)
    {
        this.BtnSave_Click(sender, e);
    } 

    protected void gvDtl_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {       
        SqlTransaction ObjTrans;
        if (cKon.State == ConnectionState.Closed) cKon.Open(); 
        ObjTrans = cKon.BeginTransaction();
        try
        {
            string ccc = gvDtl.Rows[e.RowIndex].Cells[0].ToString();
            TableCell cell = gvDtl.Rows[e.RowIndex].Cells[0];
            Tbldtl = db.userlogindtls.Single(i => i.userid == id_.Text.ToString() & i.userdtlid==Convert.ToInt32(cell));

            db.userlogindtls.DeleteOnSubmit(Tbldtl);
            db.SubmitChanges();
            ObjTrans.Commit();
            cKon.Close();
        }
        catch (Exception ex)
        {
            ObjTrans.Rollback();
            cKon.Close();
            //this.CustomAlert(sender,ex.ToString());
            this.sMsg(ex.ToString(), "1");
            return;
        }
        Response.Redirect("~/Setup/User/F_User.aspx?id=" + id_.Text);
    }

    protected void BtnMultiRole_Click(object sender, EventArgs e)
    {
        txtfilterRole.Text = "";
        this.GetRole();
        sVar.SetModalPopUp(MPERole, PanelRole, beRole, true);
    }
    //== END POP UP ROLE ==//
}