﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using CrystalDecisions.CrystalReports.Engine; 
using System.Reflection;
using static ClassGlobal;

public partial class LF_setup_user : System.Web.UI.Page
{ 
    DataClassesDataContext db = new DataClassesDataContext();
    ClassGlobal sVar = new ClassGlobal();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["username"].ToString())) 
                if (!this.IsPostBack) 
                    this.BindData();  
        }
        catch (Exception ex)
        {
            Response.Redirect("~/Login.aspx");
        }
    } 
     
    private void BindData()
    { 
        data = ToDataTable((from u in db.userlogins select new { userid = u.userid.ToUpper(), u.username, u.password, u.m_flag, u.createtime, u.createby, u.lastupdatetime, u.lastupdateby }).ToList());
        Session["userview"] = data;
        Session["user"] = data;
        gvlist.DataSource = Session["userview"];
        gvlist.DataBind(); 
    }

    protected void btnadd_Click(object sender, EventArgs e)
    { 
        if (Session["userview"] != null)
        {
            gvlist.DataSource = null;
            gvlist.DataBind();
            data = Session["userview"] as DataTable;
            DataView DtView = data.DefaultView;
            DtView.RowFilter = ddlfilter.SelectedValue + " LIKE '%" + txtfilter.Text + "%'";
            data.AcceptChanges();
            gvlist.DataSource = data;
            gvlist.DataBind();
            DtView.RowFilter = "";
        }
        else
        {
            this.BindData();
        }
    }
    
    protected void gvlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvlist.PageIndex = e.NewPageIndex;
        gvlist.DataSource = Session["userview"];
        gvlist.DataBind();
    }
     
}