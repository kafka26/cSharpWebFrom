﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="F_PurchaseInvoice.aspx.cs" Inherits="Transaction_PurchaseInvoice_F_PurchaseInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<!-- Content Header (Page header) -->
<head runat="server">
    <title></title>
</head> 
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Form Purchase Invoice</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">

                <asp:UpdatePanel ID="UpdatePanelPI" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlHeader" runat="server">
                            <div class="row">
                                <div class="col-sm-12">
                                   <asp:label runat="server" ID="trnid" Visible="false" Text="" />
                                   <asp:label runat="server" ID="id_" Visible="false" Text="0" />
                                   <asp:label runat="server" ID="flag_" Visible="false" Text="0" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">PI No</label> 
                                    <asp:TextBox runat="server" ID="trno" CssClass="form-control" Enabled="false" />
                               </div> 
                                 
                               <div class="col-sm-3">
                                    <label class="col-form-label">PI Date</label> 
                                    <div class="input-group">
                                       <asp:TextBox runat="server" ID="trdate" CssClass="form-control" Enabled="false"/>  
                                       <asp:ImageButton id="CallPeriod" runat="server" ImageUrl="~/Images/oCalendar.gif" CssClass="btn btn-secondary"/>
                                    </div>
                               </div> 

                                <ajaxToolkit:CalendarExtender ID="CEtrdate" runat="server" Enabled="True" TargetControlID="trdate" PopupButtonID="CallPeriod" Format="dd/MM/yyyy" />
                                <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="trdate" Mask="99/99/9999" MaskType="Date" UserDateFormat="None"/>

                               <div class="col-sm-6">
                                    <label class="col-form-label">Supplier</label> 
                                    <div class="input-group">
                                        <asp:TextBox id="suppid" runat="server" CssClass="form-control" Visible="false" />
                                        <asp:TextBox id="suppname" runat="server" CssClass="form-control" Enabled="false" />
                                        <span class="input-group-btn">
                                            <asp:button runat="server" ID="BtnSupp" Text="search" class="btn btn-default" OnClick="BtnSupp_Click"/>
                                        </span>
                                    </div> 
                               </div>   
                            </div>

                            <div class="row"> 
                                <div class="col-sm-3">
                                    <label class="col-form-label">Total Amount</label> 
                                    <asp:TextBox runat="server" ID="TotalAmount" CssClass="form-control" Enabled="false" Text ="0.00" />
                                </div>  
                                <div class="col-sm-3">
                                    <label class="col-form-label">Total Discount</label> 
                                    <asp:TextBox runat="server" ID="AmtDisc" CssClass="form-control" Enabled="false" Text ="0.00" />
                                </div> 
                                <div class="col-sm-3">
                                    <label class="col-form-label">Tax Type</label> 
                                    <asp:DropDownList id="taxid" runat="server" CssClass="form-control select2" Enabled="true" OnSelectedIndexChanged="taxid_SelectedIndexChanged" AutoPostBack="true"/>
                               </div>  
                               <div class="col-sm-3">
                                    <label class="col-form-label">Tax Value</label> 
                                    <asp:TextBox runat="server" ID="taxable" CssClass="form-control" Enabled="false" Text="0" />
                               </div> 
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Total DPP</label> 
                                    <asp:TextBox runat="server" ID="amtDPP" CssClass="form-control" Enabled="false" Text ="0.00" />
                                </div> 
                                <div class="col-sm-3">
                                    <label class="col-form-label">Total Tax</label> 
                                    <asp:TextBox runat="server" ID="amtTax" CssClass="form-control" Enabled="false" Text ="0.00" /> 
                                </div> 
                                <div class="col-sm-3">
                                    <label class="col-form-label">Grand Total Netto</label> 
                                    <asp:TextBox runat="server" ID="TotalNettoAmt" CssClass="form-control" Enabled="false" Text ="0.00" />
                                </div> 
                                <div class="col-sm-3">
                                    <label class="col-form-label">Status</label> 
                                    <asp:TextBox runat="server" ID="orderstatus" CssClass="form-control" Enabled="false"/>
                                </div> 
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="col-form-label">Header Note</label> 
                                    <asp:TextBox runat="server" ID="headernote" CssClass="form-control" TextMode="MultiLine" Height="50" /> 
                                </div>
                            </div>  

                             <hr />
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                       <asp:Button runat="server" ID="BtnSaveHdr" CssClass="btn btn-success" Text="Save" Enabled="true" OnClick="BtnSaveHdr_Click"/>
                                       <asp:Button runat="server" ID="BtnEditHdr" CssClass="btn btn-secondary" Text="Edit Header" Enabled="false" OnClick="BtnEditHdr_Click"/>
                                    </div>
                                </div>
                            </div>
                
                            <hr />
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="col-form-label">IR No</label> 
                                    <div class="input-group">
                                        <asp:TextBox id="irid" runat="server" CssClass="form-control" Visible="false" Text="0" />
                                        <asp:TextBox id="irno" runat="server" CssClass="form-control" Enabled="false" />
                                        <span class="input-group-btn">
                                            <asp:button runat="server" ID="BtnIR" Text="search" class="btn btn-default" OnClick="BtnIR_Click"/>
                                        </span>
                                    </div> 
                                </div>  
                       
                               <div class="col-sm-6">
                                    <label class="col-form-label">Search Item For Multiple Add</label><br />
                                    <asp:Button runat="server" ID="BtnMultiItem" CssClass="btn btn-info" Text="Add Multiple Item" Enabled="false" OnClick="BtnMultiItem_Click"/>
                               </div> 
                            </div> 
                            <hr />

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-body table-responsive p-0" style="height: 200px;">
                                        <asp:GridView ID="gvDtl" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" OnRowDeleting="gvDtl_RowDeleting">
                                        <EmptyDataTemplate>
                                            <label>Data Not Found</label>
                                        </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <asp:Panel ID="pnlReason" runat="server">
                                <div class="row">
                                    <div class="col-sm-6">
                                      <asp:Label ID="LblReason" runat="server" Text="Reason" CssClass="col-form-label" Visible="false" />
                                      <asp:TextBox runat="server" ID="txtreason" CssClass="form-control" TextMode="MultiLine" Height="50" Visible="false" />
                                    </div>
                                </div>
                            </asp:Panel>

                            <hr />
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                       <asp:Button runat="server" ID="BtnBack" CssClass="btn btn-secondary" Text="Back" Visible="true" OnClick="BtnBack_Click"/>
                                       <asp:Button runat="server" ID="BtnDel" CssClass="btn btn-danger" Text="Delete" Visible="false" OnClick="BtnDel_Click"/>
                                       <asp:Button runat="server" ID="BtnSendApp" CssClass="btn btn-success" Text="Send Approval" Visible="false" OnClick="BtnSendApp_Click"/>
                                       <asp:Button runat="server" ID="BtnApproval" CssClass="btn btn-success" Text="Approved" Visible="false" OnClick="BtnApproval_Click"/>
                                       <asp:Button runat="server" ID="BtnRevise" CssClass="btn btn-info" Text="Revised" Visible="false" OnClick="BtnRevise_Click" />
                                       <asp:Button runat="server" ID="BtnReject" CssClass="btn btn-danger" Text="Rejected" Visible="false" OnClick="BtnReject_Click" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <i class="col-form-label">Create By : <asp:Label runat="server" ID="lblcreate" /></i>
                                </div>
                                <div class="col-sm-6">
                                    <i class="col-form-label">Last Update By : <asp:Label runat="server" ID="lblupdate" /></i>
                                </div> 
                                <div class="col-sm-6">
                                    <i class="col-form-label">Create Time : <asp:Label runat="server" ID="lblcreatetime" /></i>
                                </div>  
                                <div class="col-sm-6">
                                    <i class="col-form-label">Last Update Time : <asp:Label runat="server" ID="lblupdatetime" /></i>
                                </div> 
                            </div> 

                            <!--- Start PopUp Message --->
                            <asp:UpdatePanel ID="UpdatePanelMsg" runat="server">
                                <ContentTemplate>
                                    <asp:Panel id="PanelsMsg" runat="server" Visible="False" Width="70%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">
                                                    <asp:Label ID="lblInfo" runat="server" Text="" />
                                                </h4>
                                            </div>

                                            <div class="modal-body">                                                   
                                                <div class="col-sm-12">                                                         
                                                    <center>
                                                        <asp:Label ID="lblMessage" runat="server" Text="" />
                                                    </center> 
                                                </div>       
                                            </div> 
                                            
                                            <div class="modal-footer">   
                                                <div class="col-sm-12">
                                                    <center>
                                                        <asp:Button ID="BtnOK" runat="server" CssClass="btn btn-danger" Text="OK" OnClick="BtnOK_Click"/>
                                                    </center>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </asp:Panel> 

                                    <ajaxToolkit:ModalPopupExtender id="MpesMsg" runat="server" Drag="True" PopupControlID="PanelsMsg" TargetControlID="BesMsg" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                    <asp:Button id="BesMsg" runat="server" Visible="False" CausesValidation="False" />       

                                </ContentTemplate>
                            </asp:UpdatePanel> 
                            <!--- End PopUp Message --->

                            <asp:UpdateProgress runat="server" ID="UpdateProgressPI" AssociatedUpdatePanelID="UpdatePanelPI">
                                <ProgressTemplate>
                                    <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                    <div id="processMessage" class="processMessage">
                                        <center>
                                            <span>
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                                            </span>
                                        </center><br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </asp:Panel>                                             
                    </ContentTemplate>

                    <%--<Triggers>
                        <asp:PostBackTrigger ControlID="BtnSave" />
                    </Triggers>--%>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>
</section>
<!-- End Main content -->

<!-- Pop Up  Supplier-->
<asp:UpdatePanel ID="UpdatePanelSupp" runat="server">
    <ContentTemplate>
        <asp:Panel id="pnlSupp" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List Supplier</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="ddlfilterSupp" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="supplier"> Nama </asp:ListItem> 
                                        <asp:ListItem value="suppcode"> Kode </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="txtfilterSupp" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindSupp" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindSupp_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvSupp" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged" OnPageIndexChanging="gvSupp_PageIndexChanging">
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <asp:Button ID="BtnCloseSupp" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCloseSupp_Click" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="mpeSupp" runat="server" Drag="True" PopupControlID="pnlSupp" TargetControlID="BeSupp" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="BeSupp" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgressSupp" AssociatedUpdatePanelID="UpdatePanelSupp">
            <ProgressTemplate>
                <div ID="progressBackgroundFilterSupp" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                           <asp:Image ID="ImageSupp" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center><br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
<!-- Pop Up Supplier-->

<!-- Pop Up IR-->
<asp:UpdatePanel ID="UpdatePanelIR" runat="server">
    <ContentTemplate>
        <asp:Panel id="PanelIR" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List Item Received</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="DDLFilterIR" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="sysid"> Draft IR </asp:ListItem> 
                                        <asp:ListItem value="trno"> No IR </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="TxtFilterIR" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindIR" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindIR_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvIR" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnSelectedIndexChanged="gvIR_SelectedIndexChanged" OnPageIndexChanging="gvIR_PageIndexChanging" > 
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnCloseIR" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCloseIR_Click" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="MpeIR" runat="server" Drag="True" PopupControlID="PanelIR" TargetControlID="BeIR" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="BeIR" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgressIR" AssociatedUpdatePanelID="UpdatePanelIR">
            <ProgressTemplate>
                <div ID="progressBackgroundFilterIR" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                           <asp:Image ID="ImageIR" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center><br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
<!-- Pop Up IR-->

<!-- Pop Up Data Item-->
<asp:UpdatePanel ID="UpdatePanelItem" runat="server">
    <ContentTemplate>
        <asp:Panel id="PanelItem" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List data item</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="ddlfilterItem" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="itemname"> Item </asp:ListItem> 
                                        <asp:ListItem value="itemcode"> Kode </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="txtfilterItem" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindItem" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindItem_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvItem" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnRowDataBound="gvItem_RowDataBound" OnPageIndexChanging="gvItem_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbItemDtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("itemid") %>' Enabled="false"/>
                                        </ItemTemplate> 
                                    </asp:TemplateField> 
                                </Columns>
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnAddToDtl" runat="server" CssClass="btn btn-default" Text="Add To Detail" OnClick="BtnAddToDtl_Click"/> 
                    <asp:Button ID="BtnCLoseItem" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCLoseItem_Click" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="MpeItem" runat="server" Drag="True" PopupControlID="PanelItem" TargetControlID="BeItem" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="BeItem" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanelItem">
            <ProgressTemplate>
                <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center>
                    <br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="BtnAddToDtl" />
    </Triggers>
</asp:UpdatePanel>
<!-- End Pop Up  Data Item--> 
</asp:Content>

