﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="LF_PurchaseInvoice.aspx.cs" Inherits="Transaction_PurchaseInvoice_LF_PurchaseInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<head runat="server">
    <title>

    </title>
</head> 
<div class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="text-dark">List Purchase Invoice</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
            
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">

                <asp:UpdatePanel ID="UpdatePanel_PI" runat="server">
                    <ContentTemplate>
                        <div class="form-group row col-sm-12">
                            <asp:hyperlink runat="server" ID="CreateNewData" CssClass="btn btn-success btn-sm" NavigateUrl="~/Transaction/PurchaseInvoice/F_PurchaseInvoice.aspx?">
                                <i class="fas fa-plus-circle"> 
                                    <label class="control-label">New Data</label>
                                </i>
                            </asp:hyperlink>
                        </div> 

                      <%--  <div class="form-group row col-sm-12">
                            <div class="col-sm-1 custom-checkbox">
                                <label class="col-form-label">Filter</label>
                            </div>

                             <div class="col-sm-5">
                                <div class="input-group">
                                    <asp:TextBox runat="server" ID="startdate" CssClass="form-control"/>
                                    <div class="input-group-prepend">
                                        <asp:ImageButton id="CallPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" CssClass="btn btn-secondary"/>
                                        <span class="input-group-text">S/D</span>
                                    </div>
                                    <asp:TextBox runat="server" ID="enddate" CssClass="form-control"/>
                                    <asp:ImageButton id="CallPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" CssClass="btn btn-secondary"/>
                                </div>
                            </div>
                        </div>
                
                         <ajaxToolkit:CalendarExtender ID="CEEPeriode1" runat="server" TargetControlID="startdate" PopupButtonID="CallPeriod1" Format="dd/MM/yyyy" />
                         <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="startdate" Mask="99/99/9999" MaskType="Date" UserDateFormat="None"/> 

                         <ajaxToolkit:CalendarExtender ID="CEEPeriode2" runat="server" TargetControlID="enddate" PopupButtonID="CallPeriod2" Format="dd/MM/yyyy" />
                         <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="enddate" Mask="99/99/9999" MaskType="Date" UserDateFormat="None"/> --%>

                        <div class="form-group row col-sm-12">
                            <div class="col-sm-1 custom-checkbox">
                                <label class="col-form-label">Periode</label>
                            </div>

                            <div class="col-sm-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <asp:DropDownList id="ddlfilter" runat="server" CssClass="form-control select2">   
                                            <asp:ListItem value="trno"> No. PI </asp:ListItem> 
                                            <asp:ListItem value="sysid"> No. Draft </asp:ListItem> 
                                            <asp:ListItem value="suppname"> Supplier</asp:ListItem> 
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:TextBox runat="server" ID="txtfilter" CssClass="form-control" />
                                </div>
                            </div>
                             <asp:Button ID="BtnFind" runat="server" Text="Find Data" CssClass="btn btn-primary" OnClick="BtnFind_Click"/>
                        </div>
                        
                        <hr />
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvlist" CssClass="table table-hover table-bordered table-stripedtable table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="trnid,sysid" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Left" AllowSorting="True" OnPageIndexChanging="gvlist_PageIndexChanging">  
                                    <EmptyDataTemplate>
                                        <label>Data Not Found</label>
                                    </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <asp:UpdateProgress runat="server" ID="UpdateProgressPI" AssociatedUpdatePanelID="UpdatePanel_PI">
                            <ProgressTemplate>
                                <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                <div id="processMessage" class="processMessage">
                                    <center><span><asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><br />Please Wait</span></center><br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
</asp:Content>

