﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using CrystalDecisions.CrystalReports.Engine;
using System.Reflection;
using static ClassGlobal;

public partial class Transaction_PurchaseInvoice_LF_PurchaseInvoice : System.Web.UI.Page
{
    private static DataClassesDataContext db = new DataClassesDataContext();
    t_invoicehdr TblHdr = new t_invoicehdr();
    t_invoicedtl Tbldtl = new t_invoicedtl();
    ClassGlobal sVar = new ClassGlobal();
    Int32 trnCode = 3;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        { 
            data = CekMenuByUser(Session["username"].ToString(), trnCode);
            if (data.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Session["username"].ToString()))
                {
                    if (!this.IsPostBack)
                    {
                        //startdate.Text = DateTime.Now.ToString("01/MM/yyyy");
                        //enddate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        gvlist.DataSource = BindData(gvlist);
                        gvlist.DataBind();
                    }
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    private static DataTable BindData(GridView gvNya)
    {
        DataTable objData = ToDataTable((from pi in db.t_invoicehdrs
            join s in db.m_suppliers on pi.custsuppid equals s.suppid
            where pi.transcode == 3
            select new
            {
                pi.trnid,
                pi.sysid,
                pi.trno,
                pi.trdate,
                s.suppname,
                pi.trstatus,
                pi.headernote,
                pi.createby,
                pi.totalamt,
                pi.discamt,
                pi.taxamt,
                pi.nettoamt
            }).ToList());

        string[] HdrText = { "NO. PI", "Tanggal", "Supplier", "Total Amount", "Discount", "Tax Amount", "Netto", "Keterangan", "Status" };
        string[] DtField = { "trno", "trdate", "suppname", "totalamt", "discamt", "taxamt", "nettoamt", "headernote", "trstatus" };
        string[] DtKeyNames = { "trnid", "sysid" };

        gvNya.Columns.Clear();
        System.Web.UI.WebControls.HyperLinkField dLink = new HyperLinkField();
        dLink.DataTextField = "sysid";
        dLink.SortExpression = "sysid";
        dLink.DataNavigateUrlFormatString = "~/Transaction/PurchaseInvoice/F_PurchaseInvoice.aspx?id={0}";
        dLink.DataNavigateUrlFields = new string[] { "trnid" };
        dLink.HeaderText = "Draft";
        dLink.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        gvNya.Columns.Add(dLink);

        for (int i = 0; i < HdrText.Length; i++)
        {
            System.Web.UI.WebControls.BoundField dRow = new BoundField();
            dRow.HeaderText = HdrText[i];
            dRow.HeaderStyle.Wrap = false;

            if (DtField[i] == "totalamt" || DtField[i] == "discamt" || DtField[i] == "taxamt" || DtField[i] == "nettoamt")
            {
                dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
            }
            else
            {
                dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                dRow.ItemStyle.Wrap = false;
            }

            dRow.DataField = DtField[i];
            gvNya.Columns.Add(dRow);
        }

        for (int i = 0; i < objData.Rows.Count; i++)
        {
            objData.Rows[i]["trdate"] = Convert.ToDateTime(objData.Rows[i]["trdate"]).ToString("dd/MM/yyy");
            objData.Rows[i]["suppname"] = objData.Rows[i]["suppname"].ToString().ToUpper();
            objData.Rows[i]["totalamt"] = Convert.ToDouble(objData.Rows[i]["totalamt"]).ToString("N2");
            objData.Rows[i]["discamt"] = Convert.ToDouble(objData.Rows[i]["discamt"]).ToString("N2");
            objData.Rows[i]["taxamt"] = Convert.ToDouble(objData.Rows[i]["taxamt"]).ToString("N2");
            objData.Rows[i]["nettoamt"] = Convert.ToDouble(objData.Rows[i]["nettoamt"]).ToString("N2");
            objData.Rows[i]["headernote"] = objData.Rows[i]["headernote"].ToString().ToUpper();
        }
        return objData;

    }

    protected void BtnFind_Click(object sender, EventArgs e)
    {
        DataView DtView = BindData(gvlist).DefaultView;
        DtView.RowFilter = ddlfilter.SelectedValue + " LIKE '%" + txtfilter.Text + "%'";
        // AND trdate>='"+ startdate.Text + "' AND trdate<='"+ enddate.Text + "'";
        BindData(gvlist).AcceptChanges();
        gvlist.DataSource = DtView.ToTable();
        gvlist.DataBind();
    }

    protected void gvlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {        
        gvlist.PageIndex = e.NewPageIndex;
        gvlist.DataSource = BindData(gvlist);
        gvlist.DataBind();
    }
}