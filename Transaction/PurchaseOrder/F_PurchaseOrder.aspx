﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="F_PurchaseOrder.aspx.cs" Inherits="Transaction_PurchaseOrder_F_PurchaseOrder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Content Header (Page header) -->
<head runat="server"/> 
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">Form Purchase Order</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlHeader" runat="server">
                            <div class="row">
                                <div class="col-sm-12">
                                   <asp:label runat="server" ID="trnid" Visible="false" Text="" />
                                   <asp:label runat="server" ID="id_" Visible="false" Text="0" />
                                   <asp:label runat="server" ID="flag_" Visible="false" Text="0" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">PO No</label> 
                                    <asp:TextBox runat="server" ID="trno" CssClass="form-control" Enabled="false" />
                               </div> 
                                 
                               <div class="col-sm-3">
                                    <label class="col-form-label">PO Date</label> 
                                    <div class="input-group">
                                       <asp:TextBox runat="server" ID="trdate" CssClass="form-control" Enabled="false"/>  
                                       <asp:ImageButton id="CallPeriod" runat="server" ImageUrl="~/Images/oCalendar.gif" CssClass="btn btn-secondary"/>
                                    </div>
                               </div> 

                                <ajaxToolkit:CalendarExtender ID="CETrdate" runat="server" Enabled="True" TargetControlID="trdate" PopupButtonID="CallPeriod" Format="dd/MM/yyyy" />
                                <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="trdate" Mask="99/99/9999" MaskType="Date" UserDateFormat="None"/>

                               <div class="col-sm-6">
                                    <label class="col-form-label">Supplier</label> 
                                    <div class="input-group">
                                        <asp:TextBox id="suppid" runat="server" CssClass="form-control" Visible="false" />
                                        <asp:TextBox id="suppname" runat="server" CssClass="form-control" Enabled="false" />
                                        <span class="input-group-btn">
                                            <asp:button runat="server" ID="BtnCust" Text="search" class="btn btn-default" OnClick="BtnCust_Click"/>
                                        </span>
                                    </div> 
                               </div>   
                            </div>

                            <div class="row"> 
                                <div class="col-sm-3">
                                    <label class="col-form-label">Total Amount</label> 
                                    <asp:TextBox runat="server" ID="TotalAmount" CssClass="form-control" Enabled="false" Text ="0.00" />
                                </div>  
                                <div class="col-sm-3">
                                    <label class="col-form-label">Total Discount</label> 
                                    <asp:TextBox runat="server" ID="AmtDisc" CssClass="form-control" Enabled="false" Text ="0.00" />
                                </div> 
                                <div class="col-sm-3">
                                    <label class="col-form-label">Tax Type</label> 
                                    <asp:DropDownList id="taxid" runat="server" CssClass="form-control select2" ViewStateMode="Enabled" AutoPostBack="true" OnSelectedIndexChanged="taxid_SelectedIndexChanged" />
                               </div>  
                               <div class="col-sm-3">
                                    <label class="col-form-label">Tax Value</label> 
                                    <asp:TextBox runat="server" ID="taxable" CssClass="form-control" Enabled="false" />
                               </div> 
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Total DPP</label> 
                                    <asp:TextBox runat="server" ID="amtDPP" CssClass="form-control" Enabled="false" Text ="0.00" />
                                </div> 
                                <div class="col-sm-3">
                                    <label class="col-form-label">Total Tax</label> 
                                    <asp:TextBox runat="server" ID="amtTax" CssClass="form-control" Enabled="false" Text ="0.00" /> 
                                </div> 
                                <div class="col-sm-3">
                                    <label class="col-form-label">Grand Total Netto</label> 
                                    <asp:TextBox runat="server" ID="TotalNettoAmt" CssClass="form-control" Enabled="false" Text ="0.00" />
                                </div> 
                                <div class="col-sm-3">
                                    <label class="col-form-label">Status</label> 
                                    <asp:TextBox runat="server" ID="orderstatus" CssClass="form-control" Enabled="false"/>
                                </div> 
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="col-form-label">Header Note</label> 
                                    <asp:TextBox runat="server" ID="headernote" CssClass="form-control" TextMode="MultiLine" Height="50" /> 
                                </div>
                            </div>  

                             <hr />
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <asp:Button runat="server" ID="BtnSave" CssClass="btn btn-success" Text="Save" OnClick="BtnSave_Click"/>
                                        <asp:Button runat="server" ID="btnChangeHdr" CssClass="btn btn-secondary" Text="Change Header" Enabled="false" OnClick="btnChangeHdr_Click"/>
                                    </div>
                                </div>
                            </div>
                
                            <hr />
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="col-form-label">Search Item For Multiple Add</label><br />
                                    <asp:Button runat="server" ID="BtnMutiItem" CssClass="btn btn-info" Text="Add Multiple Item" OnClick="BtnMutiItem_Click"/>
                                </div> 
                            </div>

                            <hr />

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-body table-responsive p-0" style="height: 200px;"> 
                                        <asp:GridView ID="gvDtl" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="sysiddtl,itemid,unitid" OnRowEditing="gvDtl_RowEditing" OnRowUpdating="gvDtl_RowUpdating" OnRowCancelingEdit="gvDtl_RowCancelingEdit" OnRowDeleting="gvDtl_RowDeleting">
                                        <Columns>
                                            <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true"/>
                                            <asp:BoundField DataField="itemcode" HeaderText="Item Code" ReadOnly="True" HeaderStyle-Wrap="false"/>
                                            <asp:BoundField DataField="itemname" HeaderText="Description" ReadOnly="True" ItemStyle-Wrap="false"/>
                                            <asp:BoundField DataField="unit" HeaderText="Unit" ReadOnly="True" ItemStyle-Wrap="false"/>
                                            <%-- <asp:TemplateField HeaderText="Unit">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="txtUnit" runat="server" Enabled="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                             <asp:TemplateField HeaderText="Qty" ControlStyle-CssClass="">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtQty" runat="server" Text='<%# Eval("qty") %>' OnBlur="Totaldtl()" Enabled="false" Width="110" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Price" ControlStyle-CssClass="">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtUnitPrice" runat="server" Text='<%# Eval("unitprice") %>' OnBlur="Totaldtl()" Enabled="false" Width="110" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Total Detail" ControlStyle-CssClass="">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtAmtDtl" runat="server" Text='<%# Eval("totalamtdtl") %>' CssClass="money" Enabled="false" Width="110"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Disc (%)" ControlStyle-CssClass="">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtDiscPct" runat="server" Text='<%# Eval("discpctdtl") %>' OnBlur="discPctdtl()" Enabled="false" Width="110" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Disc Amt" ControlStyle-CssClass="">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtDiscAmt" runat="server" Text='<%# Eval("discamtdtl") %>' OnBlur="discAmtdtl()" Enabled="false" Width="110" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Netto Detail" ControlStyle-CssClass="">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtTotalNetto" runat="server" Text='<%# Eval("nettoamtdtl") %>' Enabled="false" Width="110" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Detail Note">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtDtlNote" runat="server" Text='<%# Eval("detailnote") %>' Enabled="false"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbdtDtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("itemid") %>' Enabled="false"/>
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <label>Data Not Found</label>
                                        </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <asp:Panel ID="pnlReason" runat="server">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <asp:Label ID="LblReason" runat="server" Text="Reason" CssClass="col-form-label" Visible="false" /> 
                                        <asp:TextBox ID="txtreason" runat="server" TextMode="MultiLine" CssClass="form-control" Visible="false" Height="50"/>
                                    </div>
                                </div>
                            </asp:Panel>

                            <hr />
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <asp:Button runat="server" ID="BtnBack" CssClass="btn btn-secondary" Text="Back" Visible="true" OnClick="BtnBack_Click"/>
                                        <asp:Button runat="server" ID="BtnDel" CssClass="btn btn-danger" Text="Delete" Visible="false" OnClick="BtnDel_Click"/>
                                        <asp:Button runat="server" ID="BtnSendApp" CssClass="btn btn-success" Text="Send Approval" Visible="false" OnClick="BtnSendApp_Click"/>
                                        <asp:Button runat="server" ID="BtnApproval" CssClass="btn btn-success" Text="Approved" Visible="false" OnClick="BtnApproval_Click"/>
                                        <asp:Button runat="server" ID="BtnRevise" CssClass="btn btn-info" Text="Revised" Visible="false" OnClick="BtnRevise_Click" />
                                        <asp:Button runat="server" ID="BtnReject" CssClass="btn btn-danger" Text="Rejected" Visible="false" OnClick="BtnReject_Click" />
                                    </div>
                                    </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <i class="col-form-label">Create By : <asp:Label runat="server" ID="lblcreate" /></i>
                                </div>
                                <div class="col-sm-6">
                                    <i class="col-form-label">Last Update By : <asp:Label runat="server" ID="lblupdate" /></i>
                                </div> 
                                <div class="col-sm-6">
                                    <i class="col-form-label">Create Time : <asp:Label runat="server" ID="lblcreatetime" /></i>
                                </div>  
                                <div class="col-sm-6">
                                    <i class="col-form-label">Last Update Time : <asp:Label runat="server" ID="lblupdatetime" /></i>
                                </div> 
                            </div> 

                            <!--- Start PopUp Message --->
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:Panel id="PanelsMsg" runat="server" Visible="False" Width="70%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">
                                                    <asp:Label ID="lblInfo" runat="server" Text="" />
                                                </h4>
                                            </div>

                                            <div class="modal-body">                                                   
                                                <div class="col-sm-12">                                                         
                                                    <center>
                                                        <asp:Label ID="lblMessage" runat="server" Text="" />
                                                    </center> 
                                                </div>       
                                            </div> 

                                            <div class="modal-footer">   
                                                <div class="col-sm-12">
                                                    <center>
                                                    <asp:Button ID="BtnOK" runat="server" CssClass="btn btn-danger" Text="OK" OnClick="BtnOK_Click"/>
                                                </center>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </asp:Panel> 

                                    <ajaxToolkit:ModalPopupExtender id="MpesMsg" runat="server" Drag="True" PopupControlID="PanelsMsg" TargetControlID="BesMsg" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                    <asp:Button id="BesMsg" runat="server" Visible="False" CausesValidation="False" />       

                                </ContentTemplate>
                            </asp:UpdatePanel> 
                            <!--- End PopUp Message --->

                            <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                    <div id="processMessage" class="processMessage">
                                        <center>
                                            <span>
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                                            </span>
                                        </center><br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </asp:Panel>                                             
                    </ContentTemplate>

                    <Triggers>
                        <asp:PostBackTrigger ControlID="BtnSave" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>
</section>
<!-- End Main content -->
     
<!-- Pop Up  Supplier-->
<asp:UpdatePanel ID="UpdatePanel3" runat="server">
    <ContentTemplate>
        <asp:Panel id="pnlCust" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List Supplier</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="ddlfilterCust" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="suppname"> Nama </asp:ListItem> 
                                        <asp:ListItem value="suppcode"> Kode </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="txtfilterCust" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindCust" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindCust_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvcust" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnSelectedIndexChanged="gvcust_SelectedIndexChanged" OnPageIndexChanging="gvcust_PageIndexChanging">
                               
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnCloseCust" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="btnCloseCust_Click" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="mpeCust" runat="server" Drag="True" PopupControlID="pnlCust" TargetControlID="beCust" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="beCust" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgress3" AssociatedUpdatePanelID="UpdatePanel3">
            <ProgressTemplate>
                <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                           <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center><br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
<!-- Pop Up Supplier-->

<!-- Pop Up Data Item-->
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <asp:Panel id="PanelItem" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable" >
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List data item</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="ddlfilterItem" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="itemname"> Item </asp:ListItem> 
                                        <asp:ListItem value="itemcode"> Kode </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="txtfilterItem" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindItem" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindItem_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvItem" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="itemid" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnRowDataBound="gvItem_RowDataBound" OnPageIndexChanging="gvItem_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbListDtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("itemid") %>'/>
                                        </ItemTemplate> 
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="itemcode" HeaderText="Code" ItemStyle-Wrap="false" /> 
                                    <asp:BoundField DataField="itemname" HeaderText="Item" ItemStyle-Wrap="false" /> 
                                    <asp:TemplateField HeaderText="Unit" HeaderStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:DropDownList id="tbunitdtl" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                    </asp:TemplateField> 

                                    <asp:TemplateField HeaderText="Quantity" HeaderStyle-Wrap="false" ControlStyle-CssClass="">
                                    <ItemTemplate>
                                        <asp:TextBox id="tbMatQty" runat="server" Text='<%# Eval("unitqty") %>' OnBlur="Totaldetail()" Width="110"/>
                                    </ItemTemplate>
                                    </asp:TemplateField> 

                                    <asp:TemplateField HeaderText="Price" HeaderStyle-Wrap="false" ControlStyle-CssClass="">
                                        <ItemTemplate>
                                            <asp:TextBox id="tbMatPrice" runat="server" Text='<%# Eval("unitprice") %>' Width="110" OnBlur="Totaldetail()"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>  

                                    <asp:TemplateField HeaderText="Total Amount" HeaderStyle-Wrap="false" ControlStyle-CssClass="">
                                    <ItemTemplate>
                                        <asp:TextBox id="totalamtdtl" runat="server" Text='<%# Eval("totalamtdtl") %>' Enabled="false" Width="110" />                   
                                    </ItemTemplate>
                                    </asp:TemplateField> 
                                 
                                    <asp:TemplateField HeaderText="Disc(%)" HeaderStyle-Wrap="false" ControlStyle-CssClass="">
                                        <ItemTemplate>
                                            <asp:TextBox id="tbDiscPct" runat="server" Text='<%# Eval("discpctdtl") %>' OnBlur="discPctdetail()" Width="110" />
                                        </ItemTemplate>
                                    </asp:TemplateField> 

                                    <asp:TemplateField HeaderText="Disc Amt" HeaderStyle-Wrap="false" ControlStyle-CssClass="">
                                    <ItemTemplate>
                                        <asp:TextBox id="tbDiscAmt" runat="server" Text='<%# Eval("discamtdtl") %>' OnBlur="discAmtdetail()" Width="110" />
                                    </ItemTemplate>
                                    </asp:TemplateField> 

                                    <asp:TemplateField HeaderText="Total Netto" HeaderStyle-Wrap="false" ControlStyle-CssClass="">
                                    <ItemTemplate>
                                        <asp:TextBox id="tbTotalNetto" runat="server" Text='<%# Eval("nettoamtdtl") %>' Enabled="false" Width="110" />
                                    </ItemTemplate>
                                    </asp:TemplateField> 

                                    <asp:TemplateField HeaderText="Note" HeaderStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:TextBox id="tbnotedtl" runat="server" Text='<%# Eval("detailnote") %>'/> 
                                    </ItemTemplate>
                                    </asp:TemplateField> 
                                </Columns>
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnAddToDtl" runat="server" CssClass="btn btn-default btn-md" Text="Add To Detail" OnClick="BtnAddToDtl_Click" />   
                    <asp:Button ID="BtnCLoseItem" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCLoseItem_Click" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="MPEItem" runat="server" Drag="True" PopupControlID="PanelItem" TargetControlID="beItem" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="beItem" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanel2">
            <ProgressTemplate>
                <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center>
                    <br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="BtnAddToDtl" />
    </Triggers>
</asp:UpdatePanel>
<!-- End Pop Up  Data Item--> 

<script type="text/javascript">
    function deleteConfirmed(btndel, sflag) {
        if (btndel.dataset.confirmed) {
            btndel.dataset.confirmed = false;
            return true;
        } else {
            event.preventDefault();
            Swal.fire({
                title: 'Are you sure to ' + sflag + ' this data?',
                text: sflag + ' data can not be returned!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, ' + sflag + ' this data!'
            }).then((result) => {
                if (result.value) {
                    btndel.dataset.confirmed = true;
                    btndel.click(); 
                }
            });
        }
        return false;
    };

    //popup barang
    function discPctdetail() {
        var tbMatPrice = 0.0, tbMatQty = 0.0, TotalAmt = 0.0, DiscAmt = 0.0, DiscPct = 0.0, AmtDisc = 0.0, AmtNetto = 0.0, PctDisc = 0.0;
        $("#<%=gvItem.ClientID%> input[id*='cbListDtl']:checkbox").each(function (index) {
            if ($(this).is(':checked')) {                  
                tbMatPrice = $("input[id*=tbMatPrice]")[index].value;
                tbMatQty = $("input[id*=tbMatQty]")[index].value;
                DiscPct = $("input[id*=tbDiscPct]")[index].value;
                DiscAmt = $("input[id*=tbDiscAmt]")[index].value;
                TotalAmt = parseFloat(tbMatPrice) * parseFloat(tbMatQty);

                if (parseFloat(DiscPct) > 0) {
                    TotalNya = parseFloat(DiscPct) / 100;
                    AmtDisc = parseFloat(TotalAmt) * TotalNya;
                    $("input[id*=tbDiscAmt]")[index].value = parseFloat(AmtDisc).toFixed(2);
                    $("input[id*=tbDiscPct]")[index].value = parseFloat(DiscPct).toFixed(2);
                } else {
                    $("input[id*=tbDiscPct]")[index].value = 0;
                    $("input[id*=tbDiscAmt]")[index].value = 0;
                    AmtDisc = 0;
                }

                AmtNetto = TotalAmt - AmtDisc;
                $("input[id*=totalamtdtl]")[index].value = TotalAmt.toFixed(2);
                $("input[id*=tbTotalNetto]")[index].value = AmtNetto.toFixed(2);
                     
            }                   
        }); 
        return false;
    }

    function discAmtdetail() {
        var tbMatPrice = 0.0, tbMatQty = 0.0, TotalAmt = 0.0, DiscAmt = 0.0, DiscPct = 0.0, AmtDisc = 0.0, AmtNetto = 0.0, PctDisc = 0.0;
        $("#<%=gvItem.ClientID%> input[id*='cbListDtl']:checkbox").each(function (index) {
            if ($(this).is(':checked')) {                  
                tbMatPrice = $("input[id*=tbMatPrice]")[index].value;
                tbMatQty = $("input[id*=tbMatQty]")[index].value;
                DiscPct = $("input[id*=tbDiscPct]")[index].value;
                DiscAmt = $("input[id*=tbDiscAmt]")[index].value;
                TotalAmt = parseFloat(tbMatPrice) * parseFloat(tbMatQty);

                if (parseFloat(DiscAmt) > 0) {
                    PctDisc = (parseFloat(DiscAmt) / (parseFloat(TotalAmt))) * 100;
                    AmtDisc = DiscAmt
                    $("input[id*=tbDiscPct]")[index].value = parseFloat(PctDisc).toFixed(2);
                    $("input[id*=tbDiscAmt]")[index].value = parseFloat(DiscAmt).toFixed(2);
                } else {
                    $("input[id*=tbDiscPct]")[index].value = 0;
                    $("input[id*=tbDiscAmt]")[index].value = 0;
                    AmtDisc = 0;
                }

                AmtNetto = TotalAmt - AmtDisc;
                $("input[id*=totalamtdtl]")[index].value = TotalAmt.toFixed(2);
                $("input[id*=tbTotalNetto]")[index].value = AmtNetto.toFixed(2);
            }                   
        }); 
        return false;
    }

    function Totaldetail() {
        var tbMatPrice = 0.0, tbMatQty = 0.0, TotalAmt = 0.0, DiscAmt = 0.0, DiscPct = 0.0, AmtDisc = 0.0, AmtNetto = 0.0, PctDisc = 0.0;
        var TotalNya = 0.0;
        $("#<%=gvItem.ClientID%> input[id*='cbListDtl']:checkbox").each(function (index) {
            if ($(this).is(':checked')) {                  
                tbMatPrice = $("input[id*=tbMatPrice]")[index].value;
                tbMatQty = $("input[id*=tbMatQty]")[index].value;
                DiscPct = $("input[id*=tbDiscPct]")[index].value;
                DiscAmt = $("input[id*=tbDiscAmt]")[index].value;
                TotalAmt = parseFloat(tbMatPrice) * parseFloat(tbMatQty);

                if (parseFloat(DiscPct) > 0 && parseFloat(DiscAmt) > 0) {
                    TotalNya = parseFloat(DiscPct) / 100;
                    AmtDisc = parseFloat(TotalAmt) * TotalNya;
                    $("input[id*=tbDiscAmt]")[index].value = parseFloat(AmtDisc).toFixed(2);
                    $("input[id*=tbDiscPct]")[index].value = parseFloat(DiscPct).toFixed(2);
                }

                AmtNetto = TotalAmt - AmtDisc;
                $("input[id*=totalamtdtl]")[index].value = TotalAmt.toFixed(2);
                $("input[id*=tbTotalNetto]")[index].value = AmtNetto.toFixed(2);
                     
            }                   
        }); 
        return false;                                    
    }

    //grid detail
     function discPctdtl() {
            var txtUnitPrice = 0.0, txtQty = 0.0, TxtAmtDtl = 0.0, txtDiscPct = 0.0, txtDiscAmt = 0.0, AmtDisc = 0.0, PctDisc = 0.0, TxtTotalNetto = 0.0, TotalNya = 0.0;
            $("#<%=gvDtl.ClientID%> input[id*='cbdtDtl']:checkbox").each(function (i) {
                if ($(this).is(':checked')) {
                    txtUnitPrice = $("input[id*=txtUnitPrice]")[i].value;
                    txtQty = $("input[id*=txtQty]")[i].value;
                    DiscAmt = $("input[id*=txtDiscAmt]")[i].value;
                    DiscPct = $("input[id*=txtDiscPct]")[i].value;
                    TxtAmtDtl = parseFloat(txtUnitPrice) * parseFloat(txtQty);

                    if (parseFloat(DiscPct) > 0) {
                        TotalNya = parseFloat(DiscPct) / 100;
                        AmtDisc = parseFloat(TxtAmtDtl) * TotalNya;
                        $("input[id*=txtDiscAmt]")[i].value = parseFloat(AmtDisc).toFixed(2);
                        $("input[id*=txtDiscPct]")[i].value = parseFloat(DiscPct).toFixed(2);
                    } else {
                        $("input[id*=txtDiscPct]")[i].value = 0;
                        $("input[id*=txtDiscAmt]")[i].value = 0;
                        AmtDisc = 0;
                    } 

                    TxtTotalNetto = TxtAmtDtl - AmtDisc;
                    $("input[id*=TxtAmtDtl]")[i].value = TxtAmtDtl.toFixed(2);
                    $("input[id*=TxtTotalNetto]")[i].value = TxtTotalNetto.toFixed(2);
                }
            });
            return false;
     };

    function discAmtdtl() {
            var txtUnitPrice = 0.0, txtQty = 0.0, TxtAmtDtl = 0.0, txtDiscPct = 0.0, txtDiscAmt = 0.0, AmtDisc = 0.0, PctDisc = 0.0, TxtTotalNetto = 0.0, TotalNya = 0.0;
            $("#<%=gvDtl.ClientID%> input[id*='cbdtDtl']:checkbox").each(function (i) {
                if ($(this).is(':checked')) {
                    txtUnitPrice = $("input[id*=txtUnitPrice]")[i].value;
                    txtQty = $("input[id*=txtQty]")[i].value;
                    DiscAmt = $("input[id*=txtDiscAmt]")[i].value;
                    DiscPct = $("input[id*=txtDiscPct]")[i].value;
                    TxtAmtDtl = parseFloat(txtUnitPrice) * parseFloat(txtQty);

                    if (parseFloat(DiscAmt) > 0) {
                        PctDisc = (parseFloat(DiscAmt) / parseFloat(TxtAmtDtl)) * 100;
                        AmtDisc = DiscAmt
                        console.log(parseFloat(PctDisc).toFixed(2));
                        $("input[id*=txtDiscPct]")[i].value = parseFloat(PctDisc).toFixed(2);
                        $("input[id*=txtDiscAmt]")[i].value = parseFloat(DiscAmt).toFixed(2);
                    } else {
                        $("input[id*=txtDiscPct]")[i].value = 0;
                        $("input[id*=txtDiscAmt]")[i].value = 0;
                        AmtDisc = 0;
                    } 

                    TxtTotalNetto = TxtAmtDtl - AmtDisc;
                    $("input[id*=TxtAmtDtl]")[i].value = TxtAmtDtl.toFixed(2);
                    $("input[id*=TxtTotalNetto]")[i].value = TxtTotalNetto.toFixed(2);
                }
            });
            return false;
     };

    function Totaldtl() {
            var txtUnitPrice = 0.0, txtQty = 0.0, TxtAmtDtl = 0.0, txtDiscPct = 0.0, txtDiscAmt = 0.0, AmtDisc = 0.0, PctDisc = 0.0, TxtTotalNetto = 0.0, TotalNya = 0.0;
            $("#<%=gvDtl.ClientID%> input[id*='cbdtDtl']:checkbox").each(function (i) {
                if ($(this).is(':checked')) {
                    txtUnitPrice = $("input[id*=txtUnitPrice]")[i].value;
                    txtQty = $("input[id*=txtQty]")[i].value;
                    DiscAmt = $("input[id*=txtDiscAmt]")[i].value;
                    DiscPct = $("input[id*=txtDiscPct]")[i].value;
                    TxtAmtDtl = parseFloat(txtUnitPrice) * parseFloat(txtQty);
                    console.log(txtUnitPrice + "=>" + txtQty);
                    if (parseFloat(DiscPct) > 0 && parseFloat(DiscAmt) > 0) {
                        TotalNya = parseFloat(DiscPct) / 100;
                        AmtDisc = parseFloat(TxtAmtDtl) * TotalNya;
                        $("input[id*=txtDiscAmt]")[i].value = parseFloat(AmtDisc).toFixed(2);
                        $("input[id*=txtDiscPct]")[i].value = parseFloat(DiscPct).toFixed(2);
                    }

                    TxtTotalNetto = TxtAmtDtl - AmtDisc;
                    $("input[id*=TxtAmtDtl]")[i].value = TxtAmtDtl.toFixed(2);
                    $("input[id*=TxtTotalNetto]")[i].value = TxtTotalNetto.toFixed(2);
                }
            });
            return false;
        };
</script>
</asp:Content>


