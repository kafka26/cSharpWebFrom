﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="LF_PurchaseOrder.aspx.cs" Inherits="Transaction_PurchaseOrder_LF_PurchaseOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0 text-dark">List Purchase Order</h1>
        </div><!-- /.col -->
    </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body"> 
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:hyperlink runat="server" ID="NewData" CssClass="btn btn-success" NavigateUrl="~/Transaction/PurchaseOrder/F_PurchaseOrder.aspx?">
                                    <i class="fas fa-plus-circle"> <label class="control-label">New Data</label></i>
                                </asp:hyperlink>  
                            </div>
                        </div>
                        <br /> 
                
                        <div class="form-group row col-sm-12">
                             <div class="col-sm-1 custom-checkbox">
                                <label class="col-form-label">Filter</label>
                            </div>

                            <div class="col-sm-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <asp:DropDownList id="ddlfilter" runat="server" CssClass="form-control select2">   
                                            <asp:ListItem value="trno"> No. PO </asp:ListItem> 
                                            <asp:ListItem value="sysid"> No. Draft </asp:ListItem> 
                                            <asp:ListItem value="suppname"> Supplier</asp:ListItem> 
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:TextBox runat="server" ID="txtfilter" CssClass="form-control" />
                                </div>
                            </div>
                             <asp:Button ID="BtnFind" runat="server" Text="Find Data" CssClass="btn btn-primary" OnClick="BtnFind_Click"/> 
                        </div>
                        <hr />

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvlist" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Right" AllowSorting="True" OnPageIndexChanging="gvlist_PageIndexChanging">  
                                    <EmptyDataTemplate>
                                        <label>Data Not Found</label>
                                    </EmptyDataTemplate>
                                        <PagerStyle HorizontalAlign="Left" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div> 
                        <!---- Loading Bar ---->
                        <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                <div id="processMessage" class="processMessage">
                                    <center>
                                        <span>
                                          <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                                        </span>
                                    </center><br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div> 
</section>
</asp:Content>

