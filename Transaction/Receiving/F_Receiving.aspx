﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="F_Receiving.aspx.cs" Inherits="Transaction_Receiving_F_Receiving" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
 <head runat="server" />
 <div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">Form Receiving</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate> 
                            <div class="row">
                                <div class="col-sm-12">
                                   <asp:label runat="server" ID="trnid" Visible="false" />
                                   <asp:label runat="server" ID="id_" Visible="false" />
                                   <asp:label runat="server" ID="flag_" Visible="false" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">IR No</label> 
                                    <asp:TextBox runat="server" ID="trno" CssClass="form-control" Enabled="false" />
                               </div>  

                               <div class="col-sm-3">
                                    <label class="col-form-label">IR Date</label> 
                                    <div class="input-group">
                                       <asp:TextBox runat="server" ID="trdate" CssClass="form-control" Enabled="false"/>  
                                       <asp:ImageButton id="CallPeriod" runat="server" ImageUrl="~/Images/oCalendar.gif" CssClass="btn btn-secondary"/>
                                    </div>
                                   <ajaxToolkit:CalendarExtender ID="CalendarExtender" runat="server" Enabled="True" TargetControlID="trdate" PopupButtonID="CallPeriod" Format="dd/MM/yyyy" />
                                   <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="trdate" Mask="99/99/9999" MaskType="Date" UserDateFormat="None"/>
                               </div>   
                               
                               <div class="col-sm-6">
                                    <label class="col-form-label">Supplier</label> 
                                    <div class="input-group">
                                        <asp:TextBox id="suppid" runat="server" CssClass="form-control" Visible="false" Text="0" />
                                        <asp:TextBox id="suppname" runat="server" CssClass="form-control" Enabled="false" Text="" />
                                        <span class="input-group-btn">
                                            <asp:button runat="server" ID="BtnSupp" Text="search" CssClass="btn btn-default" OnClick="BtnSupp_Click"/>
                                        </span>
                                    </div> 
                               </div>   

                            </div>

                            <div class="row">         

                               <div class="col-sm-3">
                                    <label class="col-form-label">Gudang</label> 
                                    <asp:DropDownList id="locationid" runat="server" CssClass="form-control select2" />
                               </div> 

                                <div class="col-sm-3">
                                    <label class="col-form-label">Total Qty</label> 
                                    <asp:TextBox runat="server" ID="totalqty" CssClass="form-control" Text="0.0" Enabled="false" />
                                </div> 

                                 <div class="col-sm-3">
                                    <label class="col-form-label">Status</label> 
                                    <asp:TextBox runat="server" ID="orderstatus" CssClass="form-control" Enabled="false" />
                                </div> 

                            </div>

                             <div class="row"> 
                                <div class="col-sm-6">
                                    <label class="col-form-label">Header Note</label> 
                                    <asp:TextBox runat="server" ID="headernote" CssClass="form-control" TextMode="MultiLine" Height="50" />
                                </div>
                            </div>

                        
                        <hr />
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="Btnsave" CssClass="btn btn-success" Text="Save" OnClick="Btnsave_Click" />
                                    <asp:Button runat="server" ID="BtnChangeHdr" CssClass="btn btn-secondary" Text="Edit Header" Enabled="false" OnClick="BtnChangeHdr_Click"/>
                                </div>
                             </div>
                        </div> 

                        <hr />
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="col-form-label">PO No</label> 
                                <div class="input-group">
                                    <asp:TextBox id="poid" runat="server" CssClass="form-control" Visible="false" Text="0" />
                                    <asp:TextBox id="pono" runat="server" CssClass="form-control" Enabled="false" />
                                    <span class="input-group-btn">
                                        <asp:button runat="server" ID="BtnPO" Text="search" class="btn btn-default" OnClick="BtnPO_Click" Enabled="false"/>
                                    </span>
                                </div> 
                            </div>  
                       
                           <div class="col-sm-6">
                                <label class="col-form-label">Search Item For Multiple Add</label><br />
                                <asp:Button runat="server" ID="BtnMultiItem" CssClass="btn btn-info" Text="Add Multiple Item" Enabled="false" OnClick="BtnMultiItem_Click"/>
                           </div> 
                        </div>
                        <hr />

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-body table-responsive p-0" style="height: 200px;"> 
                                    <asp:GridView ID="gvDtl" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="sysiddtl,itemid" OnRowEditing="gvDtl_RowEditing" OnRowUpdating="gvDtl_RowUpdating" OnRowCancelingEdit="gvDtl_RowCancelingEdit" OnRowDeleting="gvDtl_RowDeleting"> 
                                    <Columns>
                                        <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true"/>
                                        <asp:BoundField DataField="trno" HeaderText="No. PO" ReadOnly="True" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"/>
                                        <asp:BoundField DataField="itemcode" HeaderText="Item Code" ReadOnly="True" HeaderStyle-Wrap="false"/>
                                        <asp:BoundField DataField="itemname" HeaderText="Description" ReadOnly="True" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"/>
                                        <asp:BoundField DataField="unit" HeaderText="Unit" ReadOnly="True" HeaderStyle-Wrap="false"/>
                                        <asp:BoundField DataField="orderqty" HeaderText="Qty PO" ReadOnly="True" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"/>

                                        <asp:TemplateField HeaderText="Qty" ControlStyle-CssClass="" HeaderStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtQtyIR" runat="server" Text='<%# Eval("qtyir") %>' Enabled="false" Width="110" />
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                                                                
                                        <asp:TemplateField HeaderText="Detail Note">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDetailNote" runat="server" Text='<%# Eval("detailnote") %>' Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbDtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("itemid") %>' Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                    </Columns>
                                    <EmptyDataTemplate>
                                        <label>Data Not Found</label>
                                    </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                        <asp:Panel ID="pnlReason" runat="server">
                            <div class="row">
                                <div class="col-sm-6">
                                    <asp:Label ID="LblReason" runat="server" Text="Reason" CssClass="col-form-label" Visible="false" /> 
                                    <asp:TextBox runat="server" ID="txtreason" CssClass="form-control" TextMode="MultiLine" Height="50" Visible="false" />
                                </div>
                            </div>
                        </asp:Panel>

                        <hr />
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="Btnback" CssClass="btn btn-secondary" Text="Back" OnClick="Btnback_Click"/>
                                    <asp:Button runat="server" ID="Btndel" CssClass="btn btn-danger" Text="Delete" Visible="false" OnClick="Btndel_Click" />
                                    <asp:Button runat="server" ID="BtnSendApp" CssClass="btn btn-success" Text="Send Approval" Visible="false" OnClick="BtnSendApp_Click" />
                                    <asp:Button runat="server" ID="BtnApproved" CssClass="btn btn-success" Text="Approved" Visible="false" OnClick="BtnApproved_Click" />
                                    <asp:Button runat="server" ID="BtnRevise" CssClass="btn btn-info" Text="Revised" Visible="false" OnClick="BtnRevise_Click" />
                                    <asp:Button runat="server" ID="BtnReject" CssClass="btn btn-danger" Text="Rejected" Visible="false" OnClick="BtnReject_Click" />
                                </div>
                             </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                               <i class="col-form-label">Create By : <asp:Label runat="server" ID="lblcreate"></asp:Label> </i>
                           </div>
                           <div class="col-sm-6">
                               <i class="col-form-label">Last Update By : <asp:Label runat="server" ID="lblupdate"></asp:Label> </i>
                           </div> 
                            <div class="col-sm-6">
                               <i class="col-form-label">Create Time : <asp:Label runat="server" ID="lblcreatetime"></asp:Label> </i>
                           </div>  
                            <div class="col-sm-6">
                               <i class="col-form-label">Last Update Time : <asp:Label runat="server" ID="lblupdatetime"></asp:Label> </i>
                           </div> 
                        </div>

                        <!--- Start PopUp Message --->
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:Panel id="PanelsMsg" runat="server" Visible="False" Width="70%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">
                                                <asp:Label ID="lblInfo" runat="server" Text="" />
                                            </h4>
                                        </div>

                                        <div class="modal-body">                                                   
                                            <div class="col-sm-12">                                                         
                                                <center>
                                                    <asp:Label ID="lblMessage" runat="server" Text="" />
                                                </center> 
                                            </div>       
                                        </div> 

                                        <div class="modal-footer">   
                                            <div class="col-sm-12">
                                                <center>
                                                    <asp:Button ID="BtnOK" runat="server" CssClass="btn btn-danger" Text="OK" OnClick="BtnOK_Click"/>
                                                </center>
                                            </div>                                                
                                        </div>
                                    </div>
                                </asp:Panel> 

                                <ajaxToolkit:ModalPopupExtender id="MpesMsg" runat="server" Drag="True" PopupControlID="PanelsMsg" TargetControlID="BesMsg" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                <asp:Button id="BesMsg" runat="server" Visible="False" CausesValidation="False" />       

                            </ContentTemplate>
                        </asp:UpdatePanel> 
                        <!--- End PopUp Message --->

                        <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div ID="progressBackgroundFilterIR" class="progressBackgroundFilter"></div>
                                <div id="processMessage" class="processMessage">
                                    <center><span><asp:Image ID="ImageIR" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><br />Please Wait</span></center><br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>
</section>
<!-- End Main content -->

<!-- Pop Up  Supplier-->
<asp:UpdatePanel ID="UpdatePanelSupp" runat="server">
    <ContentTemplate>
        <asp:Panel id="pnlSupp" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List Supplier</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="ddlfilterSupp" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="suppname"> Nama </asp:ListItem> 
                                        <asp:ListItem value="suppcode"> Kode </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="txtfilterSupp" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindSupp" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindSupp_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvSupp" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged" OnPageIndexChanging="gvSupp_PageIndexChanging" > 
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnCloseSupp" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCloseSupp_Click" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="mpeSupp" runat="server" Drag="True" PopupControlID="pnlSupp" TargetControlID="BeSupp" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="BeSupp" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgressSupp" AssociatedUpdatePanelID="UpdatePanelSupp">
            <ProgressTemplate>
                <div ID="progressBackgroundFilterSupp" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                           <asp:Image ID="ImageSupp" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center><br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
<!-- Pop Up Supplier-->

<!-- Pop Up  PO-->
<asp:UpdatePanel ID="UpdatePanelPO" runat="server">
    <ContentTemplate>
        <asp:Panel id="PanelPO" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List PO</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="DDLFilterPO" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="sysid"> Draft PO </asp:ListItem> 
                                        <asp:ListItem value="trno"> No PO </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="TxtFilterPO" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindPO" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindPO_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="GVpo" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnSelectedIndexChanged="GVpo_SelectedIndexChanged" OnPageIndexChanging="GVpo_PageIndexChanging" > 
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnClosePO" runat="server" CssClass="btn btn-danger" Text="Close" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="MpePO" runat="server" Drag="True" PopupControlID="PanelPO" TargetControlID="BePO" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="BePO" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgressPO" AssociatedUpdatePanelID="UpdatePanelPO">
            <ProgressTemplate>
                <div ID="progressBackgroundFilterPO" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                           <asp:Image ID="ImagePO" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center><br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
<!-- Pop Up PO-->

<!-- Pop Up Data Item-->
<asp:UpdatePanel ID="UpdatePanelItem" runat="server">
    <ContentTemplate>
        <asp:Panel id="PanelItem" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List data item</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="ddlfilterItem" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="itemname"> Item </asp:ListItem> 
                                        <asp:ListItem value="itemcode"> Kode </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="txtfilterItem" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindItem" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindItem_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvItem" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="itemid" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbListDtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("itemid") %>'/>
                                        </ItemTemplate> 
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="itemcode" HeaderText="Code" ItemStyle-Wrap="false" /> 
                                    <asp:BoundField DataField="itemname" HeaderText="Item" ItemStyle-Wrap="false" /> 
                                   <%-- <asp:TemplateField HeaderText="Unit" HeaderStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:DropDownList id="tbunitdtl" runat="server">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>  --%>
                                    <asp:BoundField DataField="qtypo" HeaderText="PO Qty" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="qty_akum" HeaderText="IR Qty" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Right" /> 
                                    <asp:TemplateField HeaderText="OS Qty" HeaderStyle-Wrap="false" ControlStyle-CssClass="">
                                        <ItemTemplate>
                                            <asp:TextBox id="tbQtyir" runat="server" Text='<%# Eval("qtyir") %>' OnBlur="discPctdetail()" Width="110" />
                                        </ItemTemplate>
                                    </asp:TemplateField>   

                                    <asp:TemplateField HeaderText="Note" HeaderStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:TextBox id="tbnotedtl" runat="server" Text='<%# Eval("detailnote") %>'/> 
                                    </ItemTemplate>
                                    </asp:TemplateField> 
                                </Columns>
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnAddToDtl" runat="server" CssClass="btn btn-default btn-md" Text="Add To Detail" OnClick="BtnAddToDtl_Click" />   
                    <asp:Button ID="BtnCLoseItem" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCLoseItem_Click" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="MpeItem" runat="server" Drag="True" PopupControlID="PanelItem" TargetControlID="BeItem" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="BeItem" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanelItem">
            <ProgressTemplate>
                <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center>
                    <br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="BtnAddToDtl" />
    </Triggers>
</asp:UpdatePanel>
<!-- End Pop Up  Data Item--> 
</asp:Content>

