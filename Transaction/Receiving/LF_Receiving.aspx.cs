﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using CrystalDecisions.CrystalReports.Engine;
using static ClassGlobal;
using System.Reflection;

public partial class Transaction_Receiving_LF_Receiving : System.Web.UI.Page
{
    private static DataClassesDataContext db = new DataClassesDataContext();
    t_inhdr Tbl = new t_inhdr();
    t_indtl Tbldtl = new t_indtl();
    ClassGlobal sVar = new ClassGlobal();
    Int32 trnCode = 2;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            data = CekMenuByUser(Session["username"].ToString(), trnCode);
            if (data.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Session["username"].ToString()))
                {
                    if (!this.IsPostBack)
                    {
                        //startdate.Text = DateTime.Now.ToString("01/MM/yyyy");
                        //enddate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        gvlist.DataSource = BindData(gvlist);
                        gvlist.DataBind();
                    }
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    private static DataTable BindData(GridView gvNya)
    {        
        DataTable objData = ToDataTable((from ir in db.t_inhdrs
                join s in db.m_suppliers on ir.custsuppid equals s.suppid
                where ir.transcode == 2
                select new
                {
                    ir.trnid,
                    ir.sysid,
                    ir.trno,
                    ir.trdate,
                    s.suppname,
                    ir.trstatus,
                    ir.headernote,
                    ir.createby,
                    ir.totalqtyin
                }).ToList());

        string[] HdrText = { "NO. MR", "Tanggal", "Supplier", "Total Qty", "Note", "Status" };
        string[] DtField = { "trno", "trdate", "suppname", "totalqtyin", "headernote", "trstatus" };
        string[] DtKeyNames = { "trnid", "sysid" };

        gvNya.Columns.Clear();
        System.Web.UI.WebControls.HyperLinkField dLink = new HyperLinkField();
        dLink.DataTextField = "sysid";
        dLink.SortExpression = "sysid";
        dLink.DataNavigateUrlFormatString = "~/Transaction/Receiving/F_Receiving.aspx?id={0}";
        dLink.DataNavigateUrlFields = new string[] { "trnid" };
        dLink.HeaderText = "Draft";
        dLink.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        dLink.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        gvNya.Columns.Add(dLink);

        for (int i = 0; i < HdrText.Length; i++)
        {
            System.Web.UI.WebControls.BoundField dRow = new BoundField();
            dRow.HeaderText = HdrText[i];
            dRow.HeaderStyle.Wrap = false;

            if (DtField[i] == "totalqtyin")
            {
                dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
            }
            else
            {
                dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                dRow.ItemStyle.Wrap = false;
            }

            dRow.DataField = DtField[i];
            gvNya.Columns.Add(dRow);
        }

        for (int i = 0; i < objData.Rows.Count; i++)
        {
            objData.Rows[i]["suppname"] = objData.Rows[i]["suppname"].ToString().ToUpper();
            objData.Rows[i]["headernote"] = objData.Rows[i]["headernote"].ToString().ToUpper();
            objData.Rows[i]["totalqtyin"] = Convert.ToDouble(objData.Rows[i]["totalqtyin"]).ToString("N2");
            objData.Rows[i]["trdate"] = Convert.ToDateTime(objData.Rows[i]["trdate"]).ToString("dd/MM/yyy");
        }

        gvNya.DataSource = objData;
        gvNya.DataBind();
        return objData;
    }

    protected void BtnFind_Click(object sender, EventArgs e)
    {
        DataView DtView = BindData(gvlist).DefaultView;
        DtView.RowFilter = ddlfilter.SelectedValue + " LIKE '%" + txtfilter.Text + "%'";
        // AND trdate>='"+ startdate.Text + "' AND trdate<='"+ enddate.Text + "'";
        BindData(gvlist).AcceptChanges();
        gvlist.DataSource = DtView.ToTable();
        gvlist.DataBind();
    }

    protected void gvlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvlist.PageIndex = e.NewPageIndex;
        gvlist.DataSource = BindData(gvlist);
        gvlist.DataBind();
    }
}