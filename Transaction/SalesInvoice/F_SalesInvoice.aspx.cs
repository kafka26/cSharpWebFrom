﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using static ClassGlobal;
using System.Reflection;

public partial class Transaction_SalesInvoice_F_SalesInvoice : System.Web.UI.Page
{
    public static DataClassesDataContext db = new DataClassesDataContext();
    ClassGlobal sVar = new ClassGlobal();
    t_invoicehdr TblHdr = new t_invoicehdr();
    t_invoicedtl Tbldtl = new t_invoicedtl();
    m_customer TblSupp = new m_customer();       
    Int32 trnCode = 6;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["username"].ToString()))
            {
                DataTable sData = CekMenuByUser(Session["username"].ToString(), trnCode);
                if (sData.Rows.Count > 0)
                {
                    if (!this.IsPostBack)
                    {
                        if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
                        {
                            flag_.Text = "1";
                            this.ddlTax();
                            this.Filltexbox(Convert.ToString(Request.QueryString["id"]));
                            this.FillTexDtl(Convert.ToString(Request.QueryString["id"]));
                            if (orderstatus.Text == "IN PROCESS")
                            {
                                BtnSendApp.Visible = true;
                            }
                            BtnSaveHdr.Visible = false;
                            this.CekUser(sData);
                        }
                        else
                        {
                            flag_.Text = "0";
                            trnid.Text = GetNewID().ToString();
                            trdate.Text = GetServerTime().ToString("dd/MM/yyyy");
                            orderstatus.Text = "IN PROCESS";
                            this.ddlTax();
                            this.taxid_SelectedIndexChanged(sender, e);
                            this.CekUser(sData);
                        }
                        
                    }
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }
        catch (Exception ex)
        { 
            Response.Redirect("~/Login.aspx");
        }
    }

    private void CekUser(DataTable sData)
    {
        try {
            if (Boolean.Parse(sData.Rows[0]["approval_form"].ToString()) == true && orderstatus.Text == "IN PROCESS")
            {
                if (orderstatus.Text == "IN PROCESS" || orderstatus.Text == "REVISED")
                {
                    BtnDel.Visible = true;
                    BtnSendApp.Visible = true;
                    BtnApproval.Visible = false;
                    BtnRevise.Visible = false;
                    BtnReject.Visible = false;
                    txtreason.Visible = false;
                    LblReason.Visible = false;
                }
                else if (orderstatus.Text == "IN APPROVAL")
                {
                    BtnDel.Visible = false;
                    BtnSendApp.Visible = false;
                    BtnApproval.Visible = true;
                    BtnRevise.Visible = true;
                    BtnReject.Visible = true;
                    txtreason.Visible = true;
                    LblReason.Visible = true;
                }
                else if (orderstatus.Text == "APPROVED")
                {
                    BtnSendApp.Visible = false;
                    BtnApproval.Visible = false;
                    BtnRevise.Visible = false;
                    BtnReject.Visible = false;
                    txtreason.Visible = false;
                    LblReason.Visible = false;
                    BtnDel.Visible = false;
                }
                BtnMultiItem.Visible = false;
                gvDtl.Columns[0].Visible = false;
            }
            else if (Boolean.Parse(sData.Rows[0]["approval_user"].ToString()) == true)
            {
                if (orderstatus.Text == "IN PROCESS" || orderstatus.Text == "REVISED")
                {
                    BtnSendApp.Visible = true;
                    BtnDel.Visible = true;
                    BtnApproval.Visible = false;
                    BtnRevise.Visible = false;
                    BtnReject.Visible = false;
                    txtreason.Visible = false;
                    LblReason.Visible = false;
                    BtnEditHdr.Visible = true;
                }
                else if (orderstatus.Text == "IN APPROVAL")
                {
                    BtnDel.Visible = false;
                    BtnSendApp.Visible = false;
                    BtnApproval.Visible = true;
                    BtnRevise.Visible = true;
                    BtnReject.Visible = true;
                    txtreason.Visible = true;
                    LblReason.Visible = true;
                    BtnEditHdr.Visible = false;
                }
                else if (orderstatus.Text == "APPROVED")
                {
                    BtnSendApp.Visible = false;
                    BtnApproval.Visible = false;
                    BtnRevise.Visible = false;
                    BtnReject.Visible = false;
                    txtreason.Visible = false;
                    LblReason.Visible = false;
                    BtnDel.Visible = false;
                    BtnEditHdr.Visible = false;
                }
                BtnMultiItem.Visible = false;
                gvDtl.Columns[0].Visible = false;
            }
            else if (Boolean.Parse(sData.Rows[0]["read_form"].ToString()))
            {
                BtnDel.Visible = false;
                BtnApproval.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                BtnEditHdr.Visible = false;
                BtnMultiItem.Visible = false; 
                txtreason.Visible = false;
                LblReason.Visible = false;
                gvDtl.Columns[0].Visible = false;
            }
            else if (Boolean.Parse(sData.Rows[0]["update_form"].ToString()) == true)
            {
                if (orderstatus.Text == "APPROVED" || orderstatus.Text == "IN APPROVAL")
                {
                    BtnDel.Visible = false;
                    BtnApproval.Visible = false;
                    BtnRevise.Visible = false;
                    BtnReject.Visible = false;
                    BtnEditHdr.Visible = false;
                    txtreason.Visible = false;
                    LblReason.Visible = false;
                    gvDtl.Columns[0].Visible = false;
                }
                else
                {
                    BtnDel.Visible = false;
                    BtnApproval.Visible = false;
                    BtnRevise.Visible = false;
                    BtnReject.Visible = false;
                    BtnEditHdr.Visible = true;
                    BtnEditHdr.Enabled = true;
                    txtreason.Visible = false;
                    LblReason.Visible = false;
                    gvDtl.Columns[0].Visible = true;
                }

            }
            else if (Boolean.Parse(sData.Rows[0]["delete_form"].ToString()) == true && orderstatus.Text == "IN PROCESS")
            {
                if (orderstatus.Text == "APPROVED" || orderstatus.Text == "IN APPROVAL")
                {
                    BtnDel.Visible = false;
                    BtnApproval.Visible = false;
                    BtnRevise.Visible = false;
                    BtnReject.Visible = false;
                    BtnEditHdr.Visible = false;
                    txtreason.Visible = false;
                    LblReason.Visible = false;
                    gvDtl.Columns[0].Visible = false;
                }
                else
                {
                    BtnDel.Visible = true;
                    BtnApproval.Visible = false;
                    BtnRevise.Visible = false;
                    BtnReject.Visible = false;
                    BtnEditHdr.Visible = false;
                    txtreason.Visible = false;
                    LblReason.Visible = false;
                    gvDtl.Columns[0].Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
        
    }

    private void Filltexbox(string idhdr)
    {
        try
        {
            TblHdr = db.t_invoicehdrs.Single(o => o.trnid == idhdr && o.transcode == trnCode);
            trnid.Text = TblHdr.trnid.ToString();
            id_.Text = TblHdr.sysid.ToString();
            trno.Text = TblHdr.trno;
            trdate.Text = TblHdr.trdate.Value.ToString("dd/MM/yyyy");
            custid.Text = TblHdr.custsuppid.ToString();
            taxid.SelectedValue = TblHdr.taxid.ToString();
            orderstatus.Text = (TblHdr.trstatus ?? "");
            headernote.Text = (TblHdr.headernote ?? "");
            taxable.Text = Convert.ToDouble(TblHdr.taxable).ToString("N2");
            TotalAmount.Text = Convert.ToDouble(TblHdr.totalamt).ToString("N2");
            AmtDisc.Text = Convert.ToDouble(TblHdr.discamt).ToString("N2");
            amtDPP.Text = Convert.ToDouble(TblHdr.dppamt).ToString("N2");
            amtTax.Text = Convert.ToDouble(TblHdr.taxamt).ToString("N2");
            TotalNettoAmt.Text = Convert.ToDouble(TblHdr.nettoamt).ToString("N2"); 
            lblcreate.Text = TblHdr.createby.ToString();
            lblcreatetime.Text = TblHdr.createtime.Value.ToString("yyyy-MM-dd HH:mm:ss");
            lblupdate.Text = TblHdr.lastupdateby;
            lblupdatetime.Text = TblHdr.lastupdatetime.Value.ToString("yyyy-MM-dd HH:mm:ss");
            TblSupp = db.m_customers.Single(c => c.custid == int.Parse(custid.Text));
            custname.Text = TblSupp.custname.ToString().ToUpper(); 
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }

    }

    private void FillTexDtl(string idhdr)
    {
        try
        {
            DataTable objData = ToDataTable((
                from pvd in db.t_invoicedtls
                join pv in db.t_invoicehdrs on pvd.sysid equals pv.sysid
                join ird in db.t_indtls on new { sysid = (int)pvd.inhdrid, sysiddtl = (int)pvd.inhdriddtl } equals new { ird.sysid, ird.sysiddtl }
                join ir in db.t_inhdrs on ird.sysid equals ir.sysid
                join i in db.m_items on pvd.itemid equals i.itemid
                join g in db.m_generals on pvd.unitid equals g.genid
                where pv.trnid == idhdr && pv.transcode == trnCode
                select new
                {
                    checkvalue = "False",
                    pvd.sysid,
                    pvd.sysiddtl,
                    pvd.orderid,
                    pvd.orderiddtl,
                    pvd.inhdrid,
                    pvd.inhdriddtl,
                    pvd.unitid,
                    pvd.itemid,
                    ir.trno,
                    itemcode = i.itemcode.ToString().ToUpper(),
                    itemname= i.itemname.ToString().ToUpper(),
                    genname = g.genname.ToString().ToUpper(),
                    pvd.qty,
                    pvd.unitprice,
                    pvd.discpctdtl,
                    pvd.discamt,
                    pvd.totalamtdtl,
                    pvd.nettoamtdtl,
                    pvd.detailnote
                }).ToList());

            string[] HdrText = { "SJ. No", "Code", "Item", "unit", "SI Qty", "Price", "Disc(%)", "Disc Amt", "Netto Amt" };
            string[] DtField = { "trno", "itemcode", "itemname", "genname", "qty", "unitprice", "totalamtdtl", "discpctdtl", "discamt", "nettoamtdtl" };
            string[] DtKeyNames = { "sysid", "sysiddtl" };

            gvDtl.Columns.Clear();
            gvDtl.DataKeyNames = DtKeyNames;
            System.Web.UI.WebControls.CommandField cField = new CommandField();
            cField.ButtonType = ButtonType.Link; 
            cField.ShowDeleteButton = true;

            gvDtl.Columns.Add(cField);

            for (int i = 0; i < HdrText.Length; i++)
            {
                System.Web.UI.WebControls.BoundField dRow = new BoundField(); 
                dRow.HeaderText = HdrText[i];
                dRow.HeaderStyle.Wrap = false;

                if (dRow.DataField == "qty" || dRow.DataField == "unitprice" || dRow.DataField == "discpctdtl" || dRow.DataField == "discamt" || dRow.DataField == "nettoamtdtl")
                {
                    dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    dRow.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
                } 
                else
                {
                    dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    dRow.ItemStyle.Wrap = false;
                }
                dRow.DataField = DtField[i];
                gvDtl.Columns.Add(dRow);
            }

            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objData.Rows[i]["detailnote"] = objData.Rows[i]["detailnote"].ToString().ToUpper();
                objData.Rows[i]["qty"] = Convert.ToDouble(objData.Rows[i]["qty"]).ToString("N2");
                objData.Rows[i]["unitprice"] = Convert.ToDouble(objData.Rows[i]["unitprice"]).ToString("N2");
                objData.Rows[i]["totalamtdtl"] = Convert.ToDouble(objData.Rows[i]["totalamtdtl"]).ToString("N2");
                objData.Rows[i]["discpctdtl"] = Convert.ToDouble(objData.Rows[i]["discpctdtl"]).ToString("N2");
                objData.Rows[i]["discamt"] = Convert.ToDouble(objData.Rows[i]["discamt"]).ToString("N2");
                objData.Rows[i]["nettoamtdtl"] = Convert.ToDouble(objData.Rows[i]["nettoamtdtl"]).ToString("N2");
            }
            gvDtl.DataSource = objData;
            gvDtl.DataBind(); 
           
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    private void sMsg(string sMsg, string type)
    {
        if (type == "1")
        {
            lblInfo.Text = "ERROR";
        }
        else
        {
            lblInfo.Text = "INFORMATION";
        }
        lblMessage.Text = sMsg.ToUpper().ToString();
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, true);
    }

    protected void BtnOK_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, false);
    }

    protected void BtnSaveHdr_Click(object sender, EventArgs e)
    {
        var sMessage = "";
        if (custname.Text.Trim() == "") 
            sMessage += "- Supplier belum dipilih! <br>"; 

        if (flag_.Text.ToString() == "0") 
            //int CekDouble = CekDoubleClick("t_orderhdrs", trnid.Text);
            if (CekDoubleClick("t_invoicehdr", trnid.Text) > 0) 
                sMessage += "- Maaf, anda sudah save data ini..!! <br>";  

        if (sMessage != "")
        {
            this.sMsg(sMessage, "2");
            return;
        }

        if (flag_.Text.ToString() != "0")
        {
            TblHdr = db.t_invoicehdrs.Single(o => o.sysid == Int32.Parse(id_.Text));
        }
        else
        {
            //int idnew = db.t_invoicehdrs.Max(i => i.sysid) + 1;
            int idnew = GenerateID("t_invoicehdr") + 1;
            id_.Text = idnew.ToString();
        }
         
        if (trno.Text == "")
        {
            trno.Text = GenerateTransNo(trnCode);
        }

        try
        {
            TblHdr.trdate = toDate(trdate.Text);
            TblHdr.transcode = trnCode;
            TblHdr.trno = trno.Text;
            TblHdr.custsuppid = Convert.ToInt32(custid.Text);
            TblHdr.totalamt = Convert.ToDecimal(TotalAmount.Text);
            TblHdr.discamt = Convert.ToDecimal(AmtDisc.Text);
            TblHdr.dppamt = Convert.ToDecimal(amtDPP.Text);
            TblHdr.taxid = Convert.ToInt32(taxid.SelectedValue);
            TblHdr.taxamt = Convert.ToDecimal(amtTax.Text);
            TblHdr.taxable = Convert.ToDecimal(taxable.Text);
            TblHdr.nettoamt = Convert.ToDecimal(TotalNettoAmt.Text);
            TblHdr.trstatus = orderstatus.Text;
            TblHdr.trnextstatus = "On Progress";
            if (orderstatus.Text == "APPROVED")
            {
                TblHdr.approved_by = Session["username"].ToString();
                TblHdr.approved_time = DateTime.Now;
            }
            else if (orderstatus.Text == "REVISED")
            {
                TblHdr.revised_by = Session["username"].ToString();
                TblHdr.revised_reason = txtreason.Text.Trim();
                TblHdr.revised_time = DateTime.Now;
            }
            else if (orderstatus.Text == "REJECTED")
            {
                TblHdr.rejected_by = Session["username"].ToString();
                TblHdr.rejected_reason = txtreason.Text.Trim();
                TblHdr.rejected_time = DateTime.Now;
            }

            TblHdr.headernote = headernote.Text;
            TblHdr.lastupdateby = Session["username"].ToString();
            TblHdr.lastupdatetime = DateTime.Now;
            if (flag_.Text == "0")
            {
                TblHdr.trnid = trnid.Text.ToString();
                TblHdr.sysid = Convert.ToInt32(id_.Text);
                TblHdr.createby = Session["username"].ToString();
                TblHdr.createtime = DateTime.Now;
                db.t_invoicehdrs.InsertOnSubmit(TblHdr);
            }
            db.SubmitChanges();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
        if (orderstatus.Text == "IN PROCESS")
        {
            Response.Redirect("~/Transaction/SalesInvoice/F_SalesInvoice.aspx?id=" + trnid.Text.ToString());
        }
        else
        {
            Response.Redirect("~/Transaction/SalesInvoice/LF_SalesInvoice.aspx");
        }

    }

    protected void taxid_SelectedIndexChanged(object sender, EventArgs e)
    {
        var genother = db.m_generals.Where(g => g.genid == int.Parse(taxid.SelectedValue)).ToList();
        if (genother.Count > 0)
        {
            foreach (var data in genother)
            {
                taxable.Text = data.genother.ToString();
            }
            //this.ContHdrAmt();
        }
    }

    protected void BtnEditHdr_Click(object sender, EventArgs e)
    {
        BtnSaveHdr.Enabled = true;
        BtnEditHdr.Enabled = false;
        BtnMultiItem.Enabled = false;
    }

    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Transaction/SalesInvoice/LF_SalesInvoice.aspx");
    }

    protected void BtnDel_Click(object sender, EventArgs e)
    {
        TblHdr = db.t_invoicehdrs.Where(o => o.sysid == Int32.Parse(id_.Text)).FirstOrDefault();
        Tbldtl = db.t_invoicedtls.Where(d => d.sysid == Int32.Parse(id_.Text)).FirstOrDefault();

        try
        {
            if (db.t_invoicedtls.Where(d => d.sysid == Int32.Parse(id_.Text)).Count() > 0)
            {
                db.t_invoicedtls.DeleteOnSubmit(Tbldtl);
                db.SubmitChanges();
            }
            if (db.t_invoicehdrs.Where(o => o.sysid == Int32.Parse(id_.Text)).Count() > 0)
            {
                db.t_invoicehdrs.DeleteOnSubmit(TblHdr);
                db.SubmitChanges();
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
        Response.Redirect("~/Transaction/SalesInvoice/LF_SalesInvoice.aspx");
    }

    protected void BtnSendApp_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "IN APPROVAL";
        BtnSaveHdr_Click(sender, e);
    }

    protected void BtnApproval_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "APPROVED";
        BtnSaveHdr_Click(sender, e);
    }

    protected void BtnRevise_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "REVISED";
        BtnSaveHdr_Click(sender, e);
    }

    protected void BtnReject_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "REJECTED";
        BtnSaveHdr_Click(sender, e);
    }

    //== Get Data Supplier ==//
    private void ddlTax()
    {
        var sData = (from p in db.m_generals where p.gengroup == "TAX" orderby p.genid descending select new { p.genid, p.genname }).ToList();
        taxid.DataSource = sData;
        taxid.DataTextField = "genname";
        taxid.DataValueField = "genid";
        taxid.DataBind();
    }

    private static DataTable GetDataCust(GridView gvCust)
    { 
        DataTable sdata = ToDataTable(
        (from c in db.m_customers
            join ir in db.t_inhdrs on c.custid equals ir.custsuppid
            where ir.trstatus=="APPROVED" && ir.transcode==5
            select new
            {
                c.custid,
                c.custcode,
                c.custname,
                c.cust_address
            }).Distinct().ToList());
            
        string[] HdrText = { "Supp. Code", "Supplier", "Address" };
        string[] DtField = { "custcode", "custname", "cust_address" };
        string[] DtKeyNames = { "custid", "custname" };

        gvCust.Columns.Clear();
        gvCust.DataKeyNames = DtKeyNames;

        System.Web.UI.WebControls.CommandField cField = new CommandField();
        cField.ShowSelectButton = true;
        gvCust.Columns.Add(cField);
        for (int i = 0; i < DtField.Length; i++)
        {
            System.Web.UI.WebControls.BoundField dRow = new BoundField(); 
            dRow.HeaderText = HdrText[i];
            dRow.HeaderStyle.Wrap = false;
            dRow.DataField = DtField[i];
            gvCust.Columns.Add(dRow);
        }             
        return sdata;
    }

    protected void BtnFindCust_Click(object sender, EventArgs e)
    {
        try
        {
            DataView DtView = GetDataCust(gvCust).DefaultView;
            DtView.RowFilter = ddlfilterCust.SelectedValue + " LIKE '%" + txtfilterCust.Text + "%'";
            // AND trdate>='"+ startdate.Text + "' AND trdate<='"+ enddate.Text + "'";
            GetDataCust(gvCust).AcceptChanges();
            gvCust.DataSource = DtView.ToTable();
            gvCust.DataBind();
            sVar.SetModalPopUp(mpeCust, pnlCust, BeCust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(mpeCust, pnlCust, BeCust, false);
            return;
        }
    }

    protected void BtnCloseCust_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(mpeCust, pnlCust, BeCust, false);
    }
       
    protected void BtnCust_Click(object sender, EventArgs e)
    {
        try
        {
            gvCust.DataSource = GetDataCust(gvCust);
            gvCust.DataBind();
            sVar.SetModalPopUp(mpeCust, pnlCust, BeCust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(mpeCust, pnlCust, BeCust, false);
            return;
        }
    }

    protected void gvCust_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BtnIR.Enabled = true;
            custid.Text = gvCust.SelectedDataKey["custid"].ToString();
            custname.Text = gvCust.SelectedDataKey["custname"].ToString();
            sVar.SetModalPopUp(mpeCust, pnlCust, BeCust, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }
     
    protected void gvCust_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvCust.PageIndex = e.NewPageIndex;
            gvCust.DataSource = GetDataCust(gvCust);
            gvCust.DataBind();
            sVar.SetModalPopUp(mpeCust, pnlCust, BeCust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(mpeCust, pnlCust, BeCust, false);
            return;
        }
    }
    //== End Data Supplier ==//

    //== Get Data Received ==//
    private static DataTable GetDataSJ(GridView gvNya, int taxid, int suppid)
    { 
        DataTable objData = ToDataTable((from ir in db.t_inhdrs
            join ird in db.t_indtls on ir.sysid equals ird.sysid
            join od in db.t_orderdtls on new { sysid = (int)ird.orderid, sysiddtl = (int)ird.orderdtlid } equals new { od.sysid, od.sysiddtl }
            join om in db.t_orderhdrs on od.sysid equals om.sysid
            join s in db.m_customers on ir.custsuppid equals s.custid    
            where ir.custsuppid==suppid && om.taxid==taxid && ir.trstatus=="APPROVED" && om.transcode == 4
            group ir by new
                { ir.trnid, ir.sysid, ir.trno, ir.trdate, s.custname, ir.trstatus, om.taxid, om.custsuppid, ir.totalqtyin } 
            into gc

            select new
                { gc.Key.trnid, gc.Key.sysid, gc.Key.trno, gc.Key.trdate, gc.Key.custname, gc.Key.trstatus, gc.Key.totalqtyin }
            ).ToList());

        string[] HdrText = { "Draft", "NO. IR", "Tanggal", "Supplier", "Total Qty", "Status" };
        string[] DtField = { "sysid", "trno", "trdate", "custname", "totalqtyin", "trstatus" };
        string[] DtKeyNames = { "sysid", "trno" };

        gvNya.Columns.Clear();
        gvNya.DataKeyNames = DtKeyNames;

        System.Web.UI.WebControls.CommandField cField = new CommandField();
        cField.ShowSelectButton = true;
        cField.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        gvNya.Columns.Add(cField);

        for (int i = 0; i < HdrText.Length; i++)
        {
            System.Web.UI.WebControls.BoundField dRow = new BoundField();
            dRow.HeaderText = HdrText[i];
            dRow.HeaderStyle.Wrap = false;

            if (DtField[i] == "totalqtyin")
            {
                dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
            }
            else if (DtField[i] == "sysid")
            {
                dRow.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            }
            else
            {
                dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                dRow.ItemStyle.Wrap = false;
            }

            dRow.DataField = DtField[i];
            gvNya.Columns.Add(dRow);
        }      

        for (int i = 0; i < objData.Rows.Count; i++)
        {
            objData.Rows[i]["trdate"] = Convert.ToDateTime(objData.Rows[i]["trdate"]).ToString("dd/MM/yyy");
            objData.Rows[i]["custname"] = objData.Rows[i]["custname"].ToString().ToUpper();
            objData.Rows[i]["totalqtyin"] = Convert.ToDouble(objData.Rows[i]["totalqtyin"]).ToString("N2"); 
        }
        return objData; 
    }

    protected void BtnIR_Click(object sender, EventArgs e)
    {
        try
        {
            if (id_.Text=="0")
            {
                this.sMsg("Maaf, tolong save data header dulu", "2");
                return;
            }
            gvIR.DataSource = GetDataSJ(gvIR, int.Parse(taxid.SelectedValue), int.Parse(custid.Text));
            gvIR.DataBind();
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, false);
            return;
        }
    }

    protected void BtnFindIR_Click(object sender, EventArgs e)
    {
        try
        {
            DataView DtView = GetDataSJ(gvIR, int.Parse(taxid.SelectedValue), int.Parse(custid.Text)).DefaultView;
            DtView.RowFilter = DDLFilterIR.SelectedValue + " LIKE '%" + TxtFilterIR.Text.Trim() + "%'";
            // AND trdate>='"+ startdate.Text + "' AND trdate<='"+ enddate.Text + "'";
            GetDataSJ(gvIR, int.Parse(taxid.SelectedValue), int.Parse(custid.Text)).AcceptChanges();
            gvIR.DataSource = DtView.ToTable();            
            gvIR.DataBind();
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, true);
            DtView.RowFilter = "";
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, false);
            return;
        }
    }

    protected void BtnCloseIR_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, false);
    }

    protected void gvIR_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BtnIR.Enabled = false;
            BtnMultiItem.Enabled = true;
            BtnMultiItem.Visible = true;
            irid.Text = gvIR.SelectedDataKey["sysid"].ToString();
            irno.Text = gvIR.SelectedDataKey["trno"].ToString();
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void gvIR_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvIR.PageIndex = e.NewPageIndex;
            gvIR.DataSource = GetDataSJ(gvIR, int.Parse(taxid.SelectedValue), int.Parse(custid.Text));
            gvIR.DataBind();
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(MpeIR, PanelIR, BeIR, false);
            return;
        }
    }
    //== End Get Data Received ==//

    //== Get Data Item ==//
    private static DataTable GetDataItem(GridView gvNya, int trnid, int suppid)
    {
        DataTable sData = ToDataTable((
            from ird in db.t_indtls
            join ir in db.t_inhdrs on ird.sysid equals ir.sysid
            join i in db.m_items on ird.itemid equals i.itemid
            join g in db.m_generals on ird.unitid equals g.genid
            join od in db.t_orderdtls on new { orderid = (int)ird.orderid, orderdtlid = (int)ird.orderdtlid } 
            equals new { orderid = (int) od.sysid, orderdtlid = (int)od.sysiddtl }
            where ir.transcode==5 && (ird.qtyin - od.qty_akum_inv)>0 /*&& (od.qty-od.qty_akum)==0*/
            && ir.custsuppid==suppid && ir.sysid==trnid
            select new {
                checkvalue = "true",
                ir.sysid,
                ird.sysiddtl,
                ird.orderid,
                ird.orderdtlid,
                i.itemid,
                i.itemcode,
                i.itemname,
                od.unitid,
                g.genname,
                ird.qtyin,
                orderqty = od.qty,
                ird.hpp,
                ird.totalhppdtl,
                od.unitprice,
                od.totalamtdtl,
                od.discpctdtl,
                od.discamtdtl,
                nettodtlamt = (od.unitprice * ird.qtyin) - od.discamtdtl,
                od.nettoamtdtl,
                ird.detailnote
            }).ToList());

        string[] HdrText = { "Code", "Item", "unit", "IR Qty", "Price", "Disc(%)", "Disc Amt", "Netto Amt" };
        string[] DtField = { "itemcode", "itemname", "genname", "qtyin", "unitprice", "discpctdtl", "discamtdtl", "nettodtlamt" };
        string[] DtKeyNames = { "sysid", "sysiddtl" };

        //gvNya.Columns.Clear();
        gvNya.DataKeyNames = DtKeyNames;

        //System.Web.UI.WebControls.TemplateField tField = new TemplateField();
        //tField.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        //gvNya.Columns.Add(tField);

        if (gvNya.Columns.Count <= int.Parse(HdrText.Length.ToString()))
        {
            for (int i = 0; i < HdrText.Length; i++)
            {
                System.Web.UI.WebControls.BoundField dRow = new BoundField();
                //if (dRow.DataField == "qtyin")
                //{
                //    System.Web.UI.WebControls.TemplateField txtField = new TemplateField();
                //    txtField.HeaderText = "IR Qty";
                //    txtField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                //    HdrText[i] = txtField.HeaderText;
                //    DtField[i] = "";
                //    gvNya.Columns.Add(txtField);
                //}
                dRow.HeaderText = HdrText[i];
                dRow.HeaderStyle.Wrap = false;

                if (dRow.DataField == "qtyin" || dRow.DataField == "unitprice" || dRow.DataField == "discpctdtl" || dRow.DataField == "discamtdtl" || dRow.DataField == "nettodtlamt")
                {
                    dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                    dRow.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
                }
                else if (dRow.DataField == "sysid")
                {
                    dRow.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else
                {
                    dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                    dRow.ItemStyle.Wrap = false;
                }
                dRow.DataField = DtField[i];
                gvNya.Columns.Add(dRow);
            }

            for (int i = 0; i < sData.Rows.Count; i++)
            {
                sData.Rows[i]["qtyin"] = Convert.ToDouble(sData.Rows[i]["qtyin"]).ToString("N2");
                sData.Rows[i]["unitprice"] = Convert.ToDouble(sData.Rows[i]["unitprice"]).ToString("N2");
                sData.Rows[i]["discpctdtl"] = Convert.ToDouble(sData.Rows[i]["discpctdtl"]).ToString("N2");
                sData.Rows[i]["discamtdtl"] = Convert.ToDouble(sData.Rows[i]["discamtdtl"]).ToString("N2");
                sData.Rows[i]["nettodtlamt"] = Convert.ToDouble(sData.Rows[i]["nettodtlamt"]).ToString("N2");
            }
        }
        return sData;
    }

    private void UpdateCheckedValue()
    {
        if (Session["GetItem_IR" + trnid.Text].ToString() != null || Session["GetItem_IR" + trnid.Text].ToString() != "")
        {
            DataTable sdata = Session["GetItem_IR" + trnid.Text] as DataTable;
            DataView DtView = sdata.DefaultView;
            if (DtView.Count > 0)
            {
                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.ControlCollection cc = row.Cells[0].Controls; 
                        Boolean cbCheck = false;
                        int itemid = 0;

                        foreach (System.Web.UI.Control myControl in cc)
                        {
                            if (myControl is System.Web.UI.WebControls.CheckBox)
                            {
                                cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                                itemid = int.Parse(((System.Web.UI.WebControls.CheckBox)myControl).ToolTip);
                            }
                        }

                        DtView.RowFilter = "itemid=" + itemid;
                        if (DtView.Count == 1)
                        {
                            if (cbCheck == true)
                            {                                
                                DtView[0]["checkvalue"] = "true";
                                //DtView[0]["qtyin"] = (row.FindControl("tbMatQty") as TextBox).Text;
                            }
                        }

                    }
                }
            }
            DtView.RowFilter = "";
            sdata.AcceptChanges();
            Session["GetItem_IR" + trnid.Text] = sdata;
        }

        if (!string.IsNullOrEmpty(Session["GetItem_IR_Hist" + trnid.Text].ToString()))
        {
            DataTable sdata = Session["GetItem_IR_Hist" + trnid.Text] as DataTable;
            DataView DtView = sdata.DefaultView;
            if (DtView.Count > 0)
            {
                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                        Boolean cbCheck = false;
                        int itemid = 0;

                        foreach (System.Web.UI.Control myControl in cc)
                        {
                            if (myControl is System.Web.UI.WebControls.CheckBox)
                            {
                                cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                                itemid = int.Parse(((System.Web.UI.WebControls.CheckBox)myControl).ToolTip);
                            }
                        }
                        DtView.RowFilter = "itemid=" + itemid + "";
                        if (DtView.Count == 1)
                        {
                            if (cbCheck == true)
                            {
                                DtView[0]["checkvalue"] = "true";
                                //DtView[0]["qtyin"] = Convert.ToDouble(((TextBox)row.FindControl("tbMatQty")).Text);
                            }
                        }
                    }
                }
            }
            DtView.RowFilter = "";
            sdata.AcceptChanges();
            Session["GetItem_IR_Hist" + trnid.Text] = sdata;
        }
    }

    protected void BtnMultiItem_Click(object sender, EventArgs e)
    { 
        try
        {
            if (id_.Text == "0")
            {
                this.sMsg("Maaf, tolong save data header dulu", "2");
                return;
            }
            gvItem.DataSource = GetDataItem(gvItem, int.Parse(irid.Text), int.Parse(custid.Text));
            gvItem.DataBind();
            Session["GetItem_IR" + trnid.Text] = GetDataItem(gvItem, int.Parse(irid.Text), int.Parse(custid.Text));
            Session["GetItem_IR_Hist" + trnid.Text] = GetDataItem(gvItem, int.Parse(irid.Text), int.Parse(custid.Text));
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
        }
        catch (Exception ex)
        {            
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
            return;
        }
    }

    protected void gvItem_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //CheckBox cbItemDtl = new CheckBox();
            //cbItemDtl.ID = "cbItemDtl"; 
            //cbItemDtl.ToolTip = (e.Row.DataItem as DataRowView).Row["itemid"].ToString();
            //cbItemDtl.Checked = Convert.ToBoolean((e.Row.DataItem as DataRowView).Row["checkvalue"].ToString());
            //e.Row.Cells[0].Controls.Add(cbItemDtl);

            //TextBox tbMatQty = new TextBox();
            //tbMatQty.ID = "tbMatQty";
            //tbMatQty.Width = 90;
            //tbMatQty.Text = (e.Row.DataItem as DataRowView).Row["qtyin"].ToString();
            //tbMatQty.ToolTip = (e.Row.DataItem as DataRowView).Row["qtyin"].ToString();
            //tbMatQty.Enabled=false;
            //e.Row.Cells[4].Controls.Add(tbMatQty);
        }
    }

    private static Boolean isOnDtl(int itemid, GridView GvData)
    {
        for (int Index = 0; Index < GvData.Rows.Count; Index++)
        {
            if (itemid == Convert.ToInt32(GvData.DataKeys[Index]["itemid"]))
            {
                return true;
            }
        }
        return false;
    }

    protected void BtnAddToDtl_Click(object sender, EventArgs e)
    { 
        try
        {
            this.UpdateCheckedValue();
            string sMsg = "";
            if (!string.IsNullOrEmpty(Session["GetItem_IR" + trnid.Text].ToString()))
            {
                DataTable sdata = Session["GetItem_IR" + trnid.Text] as DataTable;
                DataView sDtView = sdata.DefaultView;
                sDtView.RowFilter = "checkvalue='True'";
                if (sDtView.Count <= 0)
                {
                    this.sMsg("Maaf, data tidak bisa di delete..!!<br>", "1");
                }

                if (sDtView.Count > 0)
                {
                    for (int c1 = 0; c1 < sDtView.Count; c1++)
                    {
                        if (isOnDtl(Convert.ToInt32(sDtView[c1]["itemid"]), gvDtl) == true)
                        {
                            sMsg += "Item " + sDtView[c1]["itemname"] + " has been added !<br />";
                        }

                        if (Decimal.Parse(sDtView[c1]["qtyin"].ToString()) <= 0)
                        {
                            sMsg += "Qty Item " + sDtView[c1]["itemname"] + " Cant be 0 !<br />";
                        }

                        if (Decimal.Parse(sDtView[c1]["unitprice"].ToString()) <= 0)
                        {
                            sMsg += "Price Item " + sDtView[c1]["itemname"] + " Cant be 0 !<br />";
                        }
                    }
                }
                sDtView.RowFilter = "";
            }

            if (sMsg != "")
            {
                this.sMsg(sMsg.ToString(), "2");
                return;
            }

            if (!string.IsNullOrEmpty(Session["GetItem_IR" + trnid.Text].ToString()))
            {
                DataTable data = Session["GetItem_IR" + trnid.Text] as DataTable;
                DataView DtView = data.DefaultView;
                DtView.RowFilter = "checkvalue='True'";
                Int32 iSeq = gvDtl.Rows.Count + 1;
                if (DtView.Count > 0)
                {
                    for (int i = 0; i < DtView.Count; i++)
                    { 
                        t_invoicedtl dtdtl = new t_invoicedtl(); 
                        dtdtl.orderid = int.Parse(DtView[i]["orderid"].ToString());
                        dtdtl.orderiddtl = int.Parse(DtView[i]["orderdtlid"].ToString());
                        dtdtl.inhdrid = int.Parse(irid.Text);
                        dtdtl.inhdriddtl = int.Parse(DtView[i]["sysiddtl"].ToString());
                        dtdtl.itemid = int.Parse(DtView[i]["itemid"].ToString());
                        dtdtl.sysid = int.Parse(id_.Text);
                        dtdtl.seq = iSeq;
                        dtdtl.qty = Decimal.Parse(DtView[i]["qtyin"].ToString());
                        dtdtl.unitprice = Decimal.Parse(DtView[i]["unitprice"].ToString());
                        dtdtl.totalamtdtl = Decimal.Parse(dtdtl.qty.ToString()) * Decimal.Parse(dtdtl.unitprice.ToString());
                        dtdtl.discpctdtl = Decimal.Parse(DtView[i]["discpctdtl"].ToString());
                        dtdtl.discamt = Decimal.Parse(DtView[i]["discamtdtl"].ToString());
                        dtdtl.nettoamtdtl = Decimal.Parse(dtdtl.totalamtdtl.ToString()) - Decimal.Parse(dtdtl.discamt.ToString());
                        dtdtl.detailnote = DtView[i]["detailnote"].ToString().Trim();
                        dtdtl.unitid = int.Parse(DtView[i]["unitid"].ToString());
                        dtdtl.qty_akum = 0;
                        dtdtl.lastupdateby = Session["username"].ToString();
                        dtdtl.lastupdatetime = DateTime.Now;
                        db.t_invoicedtls.InsertOnSubmit(dtdtl);
                        db.SubmitChanges();
                        iSeq += 1;
                    }
                } 
                this.ContHdrAmt();
                this.FillTexDtl(trnid.Text.ToString());
                this.Filltexbox(trnid.Text.ToString());
                DtView.RowFilter = "";
            }
        }
        catch (InvalidOperationException ex)
        { 
            this.sMsg(ex.ToString(), "1");
            return;
        }
        txtfilterItem.Text = "";
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
        //Response.Redirect("~/Transaction/PurchaseOrder/F_PurchaseOrder?id=" + id_.Text);
    } 

    private void ContHdrAmt()
    {
        if (id_.Text != "0")
        {
            Decimal totalamt = 0, discamt = 0, dppamt = 0, totaltaxamt = 0, nettoamt = 0;
            TblHdr = db.t_invoicehdrs.Single(o => o.sysid == Int32.Parse(id_.Text));
            List<t_invoicedtl> dtl = (db.t_invoicedtls.Where(d => d.sysid == Int32.Parse(id_.Text))).ToList();
            if (dtl.Count > 0)
            {
                foreach (var item in dtl)
                {
                    totalamt += Convert.ToDecimal(item.totalamtdtl);
                    discamt += Convert.ToDecimal(item.discamt);
                    dppamt += Convert.ToDecimal(item.nettoamtdtl);
                }

                if (Convert.ToDecimal(taxable.Text) > 0)
                {
                    totaltaxamt = dppamt * (Convert.ToDecimal(taxable.Text) / 100);
                }
                nettoamt = (dppamt + totaltaxamt);
            }
             
            try
            {
                TblHdr.totalamt = totalamt;
                TblHdr.discamt = discamt;
                TblHdr.dppamt = dppamt;
                TblHdr.taxamt = totaltaxamt;
                TblHdr.nettoamt = nettoamt;
                TblHdr.taxid = Convert.ToInt32(taxid.SelectedValue);
                TblHdr.taxable = Convert.ToDecimal(taxable.Text);
                TblHdr.lastupdateby = Session["username"].ToString();
                TblHdr.lastupdatetime = DateTime.Now;
                db.SubmitChanges();  
            }
            catch (InvalidOperationException ex)
            { 
                this.sMsg(ex.ToString(), "1");
                return;
            }
        }
    }

    protected void gvItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValue();
        gvItem.PageIndex = e.NewPageIndex;
        gvItem.DataSource = Session["GetItem_IR_Hist" + trnid.Text];
        gvItem.DataBind();
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
    } 

    protected void BtnCLoseItem_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
    }

    protected void BtnFindItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValue();
            DataTable data = Session["GetItem_IR" + trnid.Text] as DataTable;
            DataView DtView = data.DefaultView;
            if (data.Rows.Count > 0)
            {
                if (txtfilterItem.Text != "")
                {
                    DtView.RowFilter = ddlfilterItem.SelectedValue.ToString() + " LIKE '%" + txtfilterItem.Text.ToString().Trim() + "%'";
                }
                Session["GetItem_IR" + trnid.Text] = DtView.ToTable();
                gvItem.DataSource = Session["GetItem_IR" + trnid.Text];
                gvItem.DataBind();
                sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }
    //== End Data Item ==//

    protected void gvDtl_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int sydIdDtl = int.Parse(gvDtl.DataKeys[e.RowIndex].Values[0].ToString());
        Tbldtl = db.t_invoicedtls.Where(d => d.sysiddtl == sydIdDtl).FirstOrDefault();
        try
        {
            db.t_invoicedtls.DeleteOnSubmit(Tbldtl);
            db.SubmitChanges();
            this.ContHdrAmt();
            this.FillTexDtl(trnid.Text.ToString());
            this.Filltexbox(trnid.Text.ToString());
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }










   
}