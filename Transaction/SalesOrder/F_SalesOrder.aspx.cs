﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using CrystalDecisions.CrystalReports.Engine;
using static ClassGlobal;
using System.Reflection;

public partial class Transaction_SalesOrder_F_SalesOrder : System.Web.UI.Page
{
    private static DataClassesDataContext db = new DataClassesDataContext();
    t_orderhdr TblHdr = new t_orderhdr();
    t_orderdtl Tbldtl = new t_orderdtl();
    m_customer TblCust = new m_customer();    
    ClassGlobal sVar = new ClassGlobal();
    Int32 trnCode = 4;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["username"].ToString()))
            {
                DataTable sData = CekMenuByUser(Session["username"].ToString(), trnCode);
                if (sData.Rows.Count > 0)
                {
                    if (!this.IsPostBack)
                    {
                        this.ddlTax();
                        if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
                        {
                            BtnSave.Visible = false;
                            flag_.Text = "1"; 
                            this.Filltexbox(Convert.ToString(Request.QueryString["id"]));
                            this.FillTexDtl(Convert.ToString(Request.QueryString["id"])); 
                            this.CekUser(sData);
                            if (orderstatus.Text== "IN PROCESS")
                            {
                                BtnSendApp.Visible = true;
                            }                            
                        }
                        else
                        {
                            flag_.Text = "0";
                            trnid.Text = GetNewID().ToString();
                            trdate.Text = GetServerTime().ToString("dd/MM/yyyy");
                            orderstatus.Text = "IN PROCESS"; 
                            this.taxid_SelectedIndexChanged(sender, e);
                            this.CekUser(sData);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            //this.sMsg(ex.ToString(), "2");
            Response.Redirect("~/Login.aspx");
        }
    }
    
    private void CekUser(DataTable sData)
    {
        if (Boolean.Parse(sData.Rows[0]["approval_form"].ToString()) == true && orderstatus.Text == "IN PROCESS")
        {
            if (orderstatus.Text == "IN PROCESS" || orderstatus.Text == "REVISED")
            {               
                BtnDel.Visible = true;
                BtnSendApp.Visible = true;
                BtnApproval.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
            }
            else if (orderstatus.Text == "IN APPROVAL")
            {
                BtnDel.Visible = false;
                BtnSendApp.Visible = false;
                BtnApproval.Visible = true;
                BtnRevise.Visible = true;
                BtnReject.Visible = true;
                txtreason.Visible = true;
                LblReason.Visible = true;
            }
            else if (orderstatus.Text == "APPROVED")
            {
                BtnSendApp.Visible = false;
                BtnApproval.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                BtnDel.Visible = false; 
            }
            BtnMutiItem.Visible = false;
            gvDtl.Columns[0].Visible = false;
        }
        else if (Boolean.Parse(sData.Rows[0]["approval_user"].ToString()) == true)
        {
            if (orderstatus.Text == "IN PROCESS" || orderstatus.Text == "REVISED")
            {
                BtnSendApp.Visible = true;
                BtnDel.Visible = true;
                BtnApproval.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                btnChangeHdr.Visible = true;
            }
            else if (orderstatus.Text == "IN APPROVAL")
            {
                BtnDel.Visible = false;
                BtnSendApp.Visible = false;
                BtnApproval.Visible = true;
                BtnRevise.Visible = true;
                BtnReject.Visible = true;
                txtreason.Visible = true;
                LblReason.Visible = true;
                btnChangeHdr.Visible = false;
            }
            else if (orderstatus.Text == "APPROVED")
            {
                BtnSendApp.Visible = false;
                BtnApproval.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                BtnDel.Visible = false;
                btnChangeHdr.Visible = false;
            }
            BtnMutiItem.Visible = false;
            gvDtl.Columns[0].Visible = false;
        }
        else if(Boolean.Parse(sData.Rows[0]["read_form"].ToString()))
        {
            BtnDel.Visible = false;
            BtnApproval.Visible = false;
            BtnRevise.Visible = false;
            BtnReject.Visible = false;
            btnChangeHdr.Visible = false;
            BtnMutiItem.Visible = false;
            gvDtl.Columns[0].Visible = false;
            txtreason.Visible = false;
            LblReason.Visible = false;
            gvDtl.Columns[0].Visible = false; 
        }
        else if(Boolean.Parse(sData.Rows[0]["update_form"].ToString()) == true )
        {
            if (orderstatus.Text == "APPROVED" || orderstatus.Text == "IN APPROVAL")
            {
                BtnDel.Visible = false;
                BtnApproval.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                btnChangeHdr.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                gvDtl.Columns[0].Visible = false;
            }
            else 
            {
                BtnDel.Visible = false;
                BtnApproval.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                btnChangeHdr.Visible = true;
                txtreason.Visible = false;
                LblReason.Visible = false;
                gvDtl.Columns[0].Visible = true;
            }
                
        }
        else if (Boolean.Parse(sData.Rows[0]["delete_form"].ToString()) == true && orderstatus.Text == "IN PROCESS")
        {
            if (orderstatus.Text == "APPROVED" || orderstatus.Text == "IN APPROVAL")
            {
                BtnDel.Visible = false;
                BtnApproval.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                btnChangeHdr.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                gvDtl.Columns[0].Visible = false;
            }
            else
            {
                BtnDel.Visible = true;
                BtnApproval.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                btnChangeHdr.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                gvDtl.Columns[0].Visible = true;
            }
            
        }
    }

    private void Filltexbox(string idhdr)
    {
        try
        {
            TblHdr = db.t_orderhdrs.Single(o => o.trnid == idhdr && o.transcode==trnCode);
            trnid.Text = TblHdr.trnid;
            id_.Text = TblHdr.sysid.ToString();
            trno.Text = TblHdr.trno;
            trdate.Text = TblHdr.trdate.Value.ToString("dd/MM/yyyy");
            custid.Text = TblHdr.custsuppid.ToString();
            taxid.SelectedValue = TblHdr.taxid.ToString();
            orderstatus.Text = (TblHdr.orderstatus ?? "");
            headernote.Text = (TblHdr.headernote ?? "");
            taxable.Text = Convert.ToDouble(TblHdr.taxable).ToString("N2");
            TotalAmount.Text = Convert.ToDouble(TblHdr.totalamt).ToString("N2");
            AmtDisc.Text = Convert.ToDouble(TblHdr.discamt).ToString("N2");
            amtDPP.Text = Convert.ToDouble(TblHdr.dppamt).ToString("N2");
            amtTax.Text = Convert.ToDouble(TblHdr.taxamt).ToString("N2");
            TotalNettoAmt.Text = Convert.ToDouble(TblHdr.nettoamt).ToString("N2");
            lblcreate.Text = TblHdr.createby.ToString();
            lblcreatetime.Text = TblHdr.createtime.Value.ToString("yyyy-MM-dd HH:mm:ss");
            lblupdate.Text = TblHdr.lastupdateby;
            lblupdatetime.Text = TblHdr.lastupdatetime.Value.ToString("yyyy-MM-dd HH:mm:ss");
            TblCust = db.m_customers.Single(c => c.custid==int.Parse(custid.Text));
            custname.Text = TblCust.custname.ToString().ToUpper(); 
        }
        catch (Exception ex)
        {  
            this.sMsg(ex.ToString(), "1");
            return;
        }       
    }

    private void FillTexDtl(string idhdr)
    {
        try
        {
            DataTable objData = ToDataTable((from od in db.t_orderdtls
                join om in db.t_orderhdrs on od.sysid equals om.sysid
                join i in db.m_items on od.itemid equals i.itemid
                join g in db.m_generals on od.unitid equals g.genid
                where om.trnid == idhdr
                select new
                {
                    om.trnid,
                    checkvalue = "False",
                    od.sysiddtl,
                    i.itemid,
                    i.itemcode,
                    i.itemname,
                    od.unitid,
                    unit = g.genname,
                    qty = od.qty.GetValueOrDefault(0),
                    unitprice = od.unitprice.GetValueOrDefault(0),
                    totalamtdtl = od.totalamtdtl.GetValueOrDefault(0),
                    discpctdtl = od.discpctdtl.GetValueOrDefault(0),
                    discamtdtl = od.discamtdtl.GetValueOrDefault(0),
                    nettoamtdtl = od.nettoamtdtl.GetValueOrDefault(0),
                    od.detailnote
                }).ToList());

            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objData.Rows[i]["qty"] = Convert.ToDouble(objData.Rows[i]["qty"]).ToString("N2");
                objData.Rows[i]["unitprice"] = Convert.ToDouble(objData.Rows[i]["unitprice"]).ToString("N2");
                objData.Rows[i]["totalamtdtl"] = Convert.ToDouble(objData.Rows[i]["totalamtdtl"]).ToString("N2");
                objData.Rows[i]["discpctdtl"] = Convert.ToDouble(objData.Rows[i]["discpctdtl"]).ToString("N2");
                objData.Rows[i]["discamtdtl"] = Convert.ToDouble(objData.Rows[i]["discamtdtl"]).ToString("N2");
                objData.Rows[i]["nettoamtdtl"] = Convert.ToDouble(objData.Rows[i]["nettoamtdtl"]).ToString("N2");
            }
            gvDtl.DataSource = objData;
            gvDtl.DataBind();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
            //Response.Redirect("~/Login.aspx");
        }

    }

    private void CustomAlert(object sender, string sMsg)
    {
        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "alert", "Swal.fire('', '" + sMsg + "', 'error');", true);
    }

    protected void BtnOK_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, false);
    }

    //== Get Data Customer ==//
    private static DataTable GetDataCustomer(GridView gvcust)
    {
        DataTable sdata = ToDataTable((from c in db.m_customers
            select new
            {
                c.custid,
                c.custcode,
                c.custname,
                c.cust_address
            }).ToList());

        string[] HdrText = { "Cust Code", "Customer", "Address" };
        string[] DtField = { "custcode", "custname", "cust_address" };
        string[] DtKeyNames = { "custid", "custname" };

        gvcust.Columns.Clear();
        gvcust.DataKeyNames = DtKeyNames;

        System.Web.UI.WebControls.CommandField cField = new CommandField();
        cField.ShowSelectButton = true;
        gvcust.Columns.Add(cField);
        for (int i = 0; i < DtField.Length; i++)
        {
            System.Web.UI.WebControls.BoundField dRow = new BoundField();
            dRow.HeaderText = HdrText[i];
            dRow.HeaderStyle.Wrap = false;
            dRow.DataField = DtField[i];
            gvcust.Columns.Add(dRow);
        }
        return sdata;
    } 

    protected void gvcust_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            custid.Text = gvcust.SelectedDataKey["custid"].ToString();
            custname.Text = gvcust.SelectedDataKey["custname"].ToString();
            sVar.SetModalPopUp(mpeCust, pnlCust, beCust, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCust_Click(object sender, EventArgs e)
    {
        try
        {
            DataView DtView = GetDataCustomer(gvcust).DefaultView;
            DtView.RowFilter = ddlfilterCust.SelectedValue + " LIKE '%" + txtfilterCust.Text + "%'";
            // AND trdate>='"+ startdate.Text + "' AND trdate<='"+ enddate.Text + "'";
            GetDataCustomer(gvcust).AcceptChanges();
            gvcust.DataSource = DtView.ToTable();
            gvcust.DataBind();
            sVar.SetModalPopUp(mpeCust, pnlCust, beCust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(mpeCust, pnlCust, beCust, false);
            return;
        } 
    }

    protected void BtnFindCust_Click(object sender, EventArgs e)
    {
        try
        {
            DataView DtView = GetDataCustomer(gvcust).DefaultView;
            DtView.RowFilter = ddlfilterCust.SelectedValue + " LIKE '%" + txtfilterCust.Text + "%'";
            // AND trdate>='"+ startdate.Text + "' AND trdate<='"+ enddate.Text + "'";
            GetDataCustomer(gvcust).AcceptChanges();
            gvcust.DataSource = DtView.ToTable();
            gvcust.DataBind();
            sVar.SetModalPopUp(mpeCust, pnlCust, beCust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(mpeCust, pnlCust, beCust, false);
            return;
        }
    }

    protected void btnCloseCust_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(mpeCust, pnlCust, beCust, false);
    }

    protected void gvcust_SelectedIndexChanged(object sender, EventArgs e)
    {
        custid.Text = gvcust.SelectedDataKey["custid"].ToString();
        custname.Text = gvcust.SelectedDataKey["custname"].ToString();
        sVar.SetModalPopUp(mpeCust, pnlCust, beCust, false);
    }
    //== End Get Data Customer

    private void ddlTax() {
        var sData = (from p in db.m_generals where p.gengroup == "TAX" orderby p.genid descending select new { p.genid, p.genname }).ToList();
        taxid.DataSource = sData;
        taxid.DataTextField = "genname";
        taxid.DataValueField = "genid";
        taxid.DataBind();
    }

    private void sMsg(string sMsg, string type)
    {
        if (type == "1")
        {
            lblInfo.Text = "ERROR";
        }
        else
        {
            lblInfo.Text = "INFORMATION";
        }
        lblMessage.Text = sMsg.ToUpper().ToString();
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, true);
    }
    
    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Transaction/SalesOrder/LF_SalesOrder.aspx");
    }

    protected void taxid_SelectedIndexChanged(object sender, EventArgs e)
    {
        var genother = db.m_generals.Where(g => g.genid == int.Parse(taxid.SelectedValue)).ToList();
        if (genother.Count>0)
        {
            foreach (var data in genother)
            {
                taxable.Text = data.genother.ToString();
            }
            //this.ContHdrAmt();
        } 
    }

    //== Get Data item ==//
    private void UpdateCheckedValue()
    {
        if (Session["GetItem_SO"+ trnid.Text].ToString() !=null || Session["GetItem_SO" + trnid.Text].ToString() !="")
        {
            data = Session["GetItem_SO" + trnid.Text] as DataTable;
            DtView = data.DefaultView;
            if (DtView.Count>0)
            {
                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                        Boolean cbCheck = false;
                        string itemid = "";

                        foreach (System.Web.UI.Control myControl in cc)
                        {
                            if (myControl is System.Web.UI.WebControls.CheckBox)
                            {
                                cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                                itemid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                            }
                        }

                        DtView.RowFilter = "itemid=" + itemid;
                        if (DtView.Count == 1)
                        {
                            if (cbCheck == true)
                            {
                                DtView[0]["checkvalue"] = "true";
                                DtView[0]["unitqty"] = Convert.ToDouble(((TextBox)row.FindControl("tbMatQty")).Text);
                                DtView[0]["unitid"] = ((DropDownList)row.FindControl("tbunitdtl")).SelectedValue;
                                DtView[0]["unitprice"] = Convert.ToDouble(((TextBox)row.FindControl("tbMatPrice")).Text);
                                DtView[0]["totalamtdtl"] = Convert.ToDouble(((TextBox)row.FindControl("totalamtdtl")).Text);
                                DtView[0]["discpctdtl"] = Convert.ToDouble(((TextBox)row.FindControl("tbDiscPct")).Text);
                                DtView[0]["discamtdtl"] = Convert.ToDouble(((TextBox)row.FindControl("tbDiscAmt")).Text);
                                DtView[0]["nettoamtdtl"] = Convert.ToDouble(((TextBox)row.FindControl("tbTotalNetto")).Text);
                                DtView[0]["detailnote"] = ((TextBox)row.FindControl("tbnotedtl")).Text;
                            }
                        }

                    }
                }
            }
            DtView.RowFilter = "";
            data.AcceptChanges();
            Session["GetItem_SO" + trnid.Text] = data;
        }

        if (!string.IsNullOrEmpty(Session["GetItem_SO_Hist" + trnid.Text].ToString()))
        {
            data = Session["GetItem_SO_Hist" + trnid.Text] as DataTable;
            DtView = data.DefaultView;
            if (DtView.Count>0)
            {
                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                        Boolean cbCheck = false;
                        string itemid = "";

                        foreach (System.Web.UI.Control myControl in cc)
                        {
                            if (myControl is System.Web.UI.WebControls.CheckBox)
                            {
                                cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                                itemid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                            }
                        }
                        DtView.RowFilter = "itemid='" + itemid + "'";
                        if (DtView.Count == 1)
                        {
                            if (cbCheck == true)
                            {
                                DtView[0]["checkvalue"] = "true";
                                DtView[0]["unitqty"] = Convert.ToDouble(((TextBox)row.FindControl("tbMatQty")).Text);
                                DtView[0]["unitid"] = ((DropDownList)row.FindControl("tbunitdtl")).SelectedValue;
                                DtView[0]["unitprice"] = Convert.ToDouble(((TextBox)row.FindControl("tbMatPrice")).Text);
                                DtView[0]["totalamtdtl"] = Convert.ToDouble(((TextBox)row.FindControl("totalamtdtl")).Text);
                                DtView[0]["discpctdtl"] = Convert.ToDouble(((TextBox)row.FindControl("tbDiscPct")).Text);
                                DtView[0]["discamtdtl"] = Convert.ToDouble(((TextBox)row.FindControl("tbDiscAmt")).Text);
                                DtView[0]["nettoamtdtl"] = Convert.ToDouble(((TextBox)row.FindControl("tbTotalNetto")).Text);
                                DtView[0]["detailnote"] = ((TextBox)row.FindControl("tbnotedtl")).Text;
                            }
                        }                        
                    }
                }
            }
            DtView.RowFilter = "";
            data.AcceptChanges();
            Session["GetItem_SO_Hist" + trnid.Text] = data;
        }
    }
    
    private void GetDataMaterial()
    {
        data = ToDataTable((from i in db.m_items join g in db.m_generals on i.unit_small equals g.genid select new { checkvalue = "False", sysiddtl = 0,i.itemid, i.itemcode, i.itemname, unit = g.genname, unitqty = 0.00, unitprice = 0.00, totalamtdtl = 0.00, discpctdtl = 0.00, discamtdtl = 0.00, nettoamtdtl = 0.00, detailnote = "", unitid = g.genid}).ToList());
        gvItem.DataSource = data;
        gvItem.DataBind();
        Session["GetItem_SO" + trnid.Text] = data;
        Session["GetItem_SO_Hist" + trnid.Text] = data;
    }

    protected void BtnMutiItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.GetDataMaterial();
            sVar.SetModalPopUp(MPEItem, PanelItem, beItem, true); 
        }
        catch (Exception ex)
        {
            //this.CustomAlert(sender, ex.ToString());
            this.sMsg(ex.ToString(), "1");
            return;
        } 
    }

    protected void gvItem_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            int itemid = int.Parse(rowView["itemid"].ToString());
            DropDownList ddlUnit = (DropDownList)e.Row.FindControl("tbunitdtl");
            var unit1 = (from g in db.m_generals join i in db.m_items on g.genid equals i.unit_small where i.itemid == itemid select new { g.genid, g.genname });
            var unit2 = (from g in db.m_generals join i in db.m_items on g.genid equals i.unit_medium where i.itemid == itemid select new { g.genid, g.genname });
            var unit3 = (from g in db.m_generals join i in db.m_items on g.genid equals i.unit_large where i.itemid == itemid select new { g.genid, g.genname });
            var unit = unit1.Union(unit2.Union(unit3).Distinct()).Distinct().ToList();
            ddlUnit.DataSource = unit;
            ddlUnit.DataTextField = "genname";
            ddlUnit.DataValueField = "genid";
            ddlUnit.DataBind(); 
        }
    }

    protected void BtnFindItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValue();
            data = Session["GetItem_SO" + trnid.Text] as DataTable;
            DtView = data.DefaultView;
            if (data.Rows.Count > 0)
            {
                if (txtfilterItem.Text != "")
                {
                    DtView.RowFilter = ddlfilterItem.SelectedValue.ToString() + " LIKE '%" + txtfilterItem.Text.ToString().Trim() + "%'";
                }
                Session["GetItem_SO" + trnid.Text] = DtView.ToTable();
                gvItem.DataSource = Session["GetItem_SO" + trnid.Text];
                gvItem.DataBind();
                sVar.SetModalPopUp(MPEItem, PanelItem, beItem, true);
            }
        }
        catch (Exception ex)
        { 
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }
    
    protected void BtnCLoseItem_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MPEItem, PanelItem, beItem, false);
    }

    private static Boolean isOnDtl(int itemid, GridView GvData)
    {        
        for (int Index = 0; Index < GvData.Rows.Count; Index++)
        { 
            if (itemid == Convert.ToInt32(GvData.DataKeys[Index]["itemid"]))
            {
                return true;
            } 
        }
        return false;
    }

    protected void BtnAddToDtl_Click(object sender, EventArgs e)
    {
        this.UpdateCheckedValue();
        string sMsg = "";
        if (!string.IsNullOrEmpty(Session["GetItem_SO" + trnid.Text].ToString()))
        {
            data = Session["GetItem_SO" + trnid.Text] as DataTable;
            DtView = data.DefaultView;
            DtView.RowFilter = "checkvalue='True'";
            if (DtView.Count <= 0)
            { 
                this.sMsg("Maaf, data tidak bisa di delete..!!<br>", "1");
            }

            if (DtView.Count > 0)
            {
                for (int c1 = 0; c1 < DtView.Count; c1++)
                {
                    if (isOnDtl(Convert.ToInt32(DtView[c1]["itemid"]), gvDtl) ==true){
                        sMsg += "Item " + DtView[c1]["itemname"] + " has been added !<br />";
                    } 

                    if (Decimal.Parse(DtView[c1]["unitqty"].ToString())==0)
                    {
                        sMsg += "Qty Item " + DtView[c1]["itemname"] + " Cant be 0 !<br />";
                    }

                    if (Decimal.Parse(DtView[c1]["unitprice"].ToString()) == 0)
                    {
                        sMsg += "Price Item " + DtView[c1]["itemname"] + " Cant be 0 !<br />";
                    }
                }
            }
            DtView.RowFilter = "";
        }        

        if (sMsg!="")
        {
            this.sMsg(sMsg.ToString(), "2");
            return;
        }
       
        try
        {
            if (!string.IsNullOrEmpty(Session["GetItem_SO" + trnid.Text].ToString()))
            {
                data = Session["GetItem_SO" + trnid.Text] as DataTable;
                DtView = data.DefaultView;
                DtView.RowFilter = "checkvalue='True'";
                Int32 iSeq = gvDtl.Rows.Count + 1;
                if (DtView.Count > 0)
                {
                    for (int i = 0; i < DtView.Count; i++)
                    {
                        t_orderdtl dtdtl = new t_orderdtl();
                        dtdtl.itemid = int.Parse(DtView[i]["itemid"].ToString());
                        dtdtl.sysid = int.Parse(id_.Text);
                        dtdtl.seq = iSeq;
                        dtdtl.qty = Decimal.Parse(DtView[i]["unitqty"].ToString());
                        dtdtl.unitprice = Decimal.Parse(DtView[i]["unitprice"].ToString());
                        dtdtl.totalamtdtl = Decimal.Parse(dtdtl.qty.ToString()) * Decimal.Parse(dtdtl.unitprice.ToString());
                        dtdtl.discpctdtl = Decimal.Parse(DtView[i]["discpctdtl"].ToString());
                        dtdtl.discamtdtl = Decimal.Parse(DtView[i]["discamtdtl"].ToString());
                        dtdtl.nettoamtdtl = Decimal.Parse(dtdtl.totalamtdtl.ToString()) - Decimal.Parse(dtdtl.discamtdtl.ToString());
                        dtdtl.detailnote = DtView[i]["detailnote"].ToString().Trim();
                        dtdtl.qty_akum = 0;
                        dtdtl.qty_akum_inv = 0;
                        dtdtl.qty_cancel = 0;
                        dtdtl.qty_retur = 0;
                        dtdtl.qty_retur_inv = 0;
                        dtdtl.unitid = int.Parse(DtView[i]["unitid"].ToString());
                        dtdtl.lastupdateby = Session["username"].ToString();
                        dtdtl.lastupdatetime = DateTime.Now;
                        db.t_orderdtls.InsertOnSubmit(dtdtl);
                        db.SubmitChanges();
                        iSeq += 1;
                    }
                } 
                this.ContHdrAmt();
                this.FillTexDtl(trnid.Text.ToString());
                this.Filltexbox(trnid.Text.ToString());
            }                 
        }
        catch (Exception ex)
        { 
            this.sMsg(ex.ToString(), "1");
            return;
        }
        txtfilterItem.Text = "";
        sVar.SetModalPopUp(MPEItem, PanelItem, beItem, false);
        //Response.Redirect("~/Transaction/SalesOrder/F_SalesOrder?id=" + id_.Text);
    }
    
    private void ContHdrAmt()
    {
        Decimal totalamt=0, discamt=0, dppamt=0, totaltaxamt=0, nettoamt=0;
        TblHdr = db.t_orderhdrs.Single(o => o.sysid == Int32.Parse(id_.Text));
        List<t_orderdtl> dtl = (db.t_orderdtls.Where(d => d.sysid == Int32.Parse(id_.Text))).ToList();
        int vbv = dtl.Count;
        foreach (var item in dtl)
        {
            totalamt += Convert.ToDecimal(item.totalamtdtl);
            discamt += Convert.ToDecimal(item.discamtdtl);
            dppamt += Convert.ToDecimal(item.nettoamtdtl);
        }

        if (Convert.ToDecimal(taxable.Text)>0)
        {
            totaltaxamt = dppamt * (Convert.ToDecimal(taxable.Text) / 100);
        }
        nettoamt = (dppamt + totaltaxamt);

        try
        {
            TblHdr.totalamt = totalamt;
            TblHdr.discamt = discamt;
            TblHdr.dppamt = dppamt;
            TblHdr.taxamt = totaltaxamt;
            TblHdr.nettoamt = nettoamt;  
            TblHdr.taxid = Convert.ToInt32(taxid.SelectedValue);
            TblHdr.taxable = Convert.ToDecimal(taxable.Text); 
            TblHdr.lastupdateby = Session["username"].ToString();
            TblHdr.lastupdatetime = DateTime.Now;
            db.SubmitChanges();            
        }
        catch (Exception ex)
        { 
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }    
   
    protected void gvItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.UpdateCheckedValue();
        gvItem.PageIndex = e.NewPageIndex;
        gvItem.DataSource = Session["GetItem_SO_Hist" + trnid.Text];
        gvItem.DataBind();
        sVar.SetModalPopUp(MPEItem, PanelItem, beItem, true);
    }
    //== End Data Item ==//  

    protected void gvDtl_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvDtl.EditIndex = e.NewEditIndex;
            this.FillTexDtl(trnid.Text.ToString());
            GridViewRow row = gvDtl.Rows[e.NewEditIndex];
            ((TextBox)row.FindControl("txtQty")).Enabled = true;
            ((TextBox)row.FindControl("txtUnitPrice")).Enabled = true;
            ((TextBox)row.FindControl("txtDiscPct")).Enabled = true;
            ((TextBox)row.FindControl("txtDiscAmt")).Enabled = true;
            ((TextBox)row.FindControl("txtDtlNote")).Enabled = true;
            ((CheckBox)row.FindControl("cbdtDtl")).Enabled = true;
            ((CheckBox)row.FindControl("cbdtDtl")).Checked = true;
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }     
    }

    protected void gvDtl_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        gvDtl.EditIndex = -1;
        GridViewRow row = gvDtl.Rows[e.RowIndex];
        int sydIdDtl = int.Parse(gvDtl.DataKeys[e.RowIndex].Values[0].ToString()); 
        Tbldtl = db.t_orderdtls.Where(d => d.sysiddtl == sydIdDtl).FirstOrDefault();

        SqlTransaction ObjTrans;
        if (cKon.State == ConnectionState.Closed)
        {
            cKon.Open();
        }
        ObjTrans = cKon.BeginTransaction();
        try
        {
            Tbldtl.qty = Decimal.Parse(((TextBox)row.FindControl("txtQty")).Text);
            Tbldtl.unitprice = Decimal.Parse(((TextBox)row.FindControl("txtUnitPrice")).Text);
            Tbldtl.totalamtdtl = (Tbldtl.qty * Tbldtl.unitprice);
            Tbldtl.discpctdtl = Decimal.Parse(((TextBox)row.FindControl("txtDiscPct")).Text);
            Tbldtl.discamtdtl = Decimal.Parse(((TextBox)row.FindControl("txtDiscAmt")).Text); ;
            Tbldtl.nettoamtdtl = (Tbldtl.qty * Tbldtl.unitprice) - Tbldtl.discamtdtl;
            Tbldtl.detailnote = ((TextBox)row.FindControl("txtDtlNote")).Text.ToString().Trim();
            Tbldtl.lastupdateby = Session["username"].ToString();
            Tbldtl.lastupdatetime = DateTime.Now;
            db.SubmitChanges();
            ObjTrans.Commit();
            cKon.Close();
            this.ContHdrAmt();
            this.FillTexDtl(trnid.Text.ToString());
            this.Filltexbox(trnid.Text.ToString());
        }
        catch (Exception ex)
        {
            ObjTrans.Rollback();
            cKon.Close();
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void gvDtl_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvDtl.EditIndex = -1;
            this.FillTexDtl(trnid.Text.ToString());
            GridViewRow row = gvDtl.Rows[e.RowIndex];
            ((TextBox)row.FindControl("txtQty")).Enabled = false;
            ((TextBox)row.FindControl("txtUnitPrice")).Enabled = false;
            ((TextBox)row.FindControl("txtDiscPct")).Enabled = false;
            ((TextBox)row.FindControl("txtDiscAmt")).Enabled = false;
            ((TextBox)row.FindControl("txtDtlNote")).Enabled = false;
            ((CheckBox)row.FindControl("cbdtDtl")).Enabled = false;
            ((CheckBox)row.FindControl("cbdtDtl")).Checked = false;
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }
    
    protected void gvDtl_RowDeleting(object sender, GridViewDeleteEventArgs e)
    { 
        int sydIdDtl = int.Parse(gvDtl.DataKeys[e.RowIndex].Values[0].ToString());
        Tbldtl = db.t_orderdtls.Where(d => d.sysiddtl == sydIdDtl).FirstOrDefault();

        SqlTransaction ObjTrans;
        if (cKon.State == ConnectionState.Closed)
        {
            cKon.Open();
        }
        ObjTrans = cKon.BeginTransaction();
        try
        {
            db.t_orderdtls.DeleteOnSubmit(Tbldtl);
            db.SubmitChanges();
            ObjTrans.Commit();
            cKon.Close();
            this.ContHdrAmt();
            this.FillTexDtl(trnid.Text.ToString());
            this.Filltexbox(trnid.Text.ToString());
        }
        catch (Exception ex)
        {
            ObjTrans.Rollback();
            cKon.Close();
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }
    
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        var sMessage = "";
        if (custname.Text.Trim() == "")
        {
            sMessage += "- Customer belum dipilih! <br>";
        }

        if (flag_.Text.ToString() == "0")
        {
            //int CekDouble = CekDoubleClick("t_orderhdrs", trnid.Text);
            if (CekDoubleClick("t_orderhdr", trnid.Text) > 0)
            {
                sMessage += "- Maaf, anda sudah save data ini..!! <br>";
            }
        }

        if (sMessage != "")
        {
            this.sMsg(sMessage, "2");
            return;
        }

        if (flag_.Text.ToString() != "0")
        {
            TblHdr = db.t_orderhdrs.Single(o => o.sysid == Int32.Parse(id_.Text));
        }
        else
        {
            //int idnew = db.t_orderhdrs.Max(i => i.sysid) + 1;
            int idnew = GenerateID("t_orderhdr") + 1;
            id_.Text = idnew.ToString();  
        }

        var a = toDate(trdate.Text);
        if (trno.Text == "")
        {
            trno.Text = GenerateTransNo(trnCode);
        }

        try
        {
            TblHdr.trdate = toDate(trdate.Text);
            TblHdr.transcode = trnCode;
            TblHdr.trno = trno.Text;
            TblHdr.custsuppid = Convert.ToInt32(custid.Text);
            TblHdr.totalamt = Convert.ToDecimal(TotalAmount.Text);
            TblHdr.discamt = Convert.ToDecimal(AmtDisc.Text);
            TblHdr.dppamt = Convert.ToDecimal(amtDPP.Text);
            TblHdr.nettoamt = Convert.ToDecimal(TotalNettoAmt.Text);
            TblHdr.taxamt = Convert.ToDecimal(amtTax.Text);
            TblHdr.taxid = Convert.ToInt32(taxid.SelectedValue);
            TblHdr.taxable = Convert.ToDecimal(taxable.Text);
            TblHdr.orderstatus = orderstatus.Text;
            TblHdr.ordernextstatus = "On Progress";
            if (orderstatus.Text == "APPROVED")
            {               
                TblHdr.approved_by = Session["username"].ToString();
                TblHdr.approved_time = DateTime.Now;
            }
            else if (orderstatus.Text == "REVISED")
            {
                TblHdr.revised_by = Session["username"].ToString();
                TblHdr.revised_reason = txtreason.Text.Trim();
                TblHdr.revised_time = DateTime.Now;
            }
            else if (orderstatus.Text == "REJECTED")
            {
                TblHdr.rejected_by = Session["username"].ToString();
                TblHdr.rejected_reason = txtreason.Text.Trim();
                TblHdr.rejected_time = DateTime.Now;
            }

            TblHdr.headernote = headernote.Text;
            TblHdr.lastupdateby = Session["username"].ToString();
            TblHdr.lastupdatetime = DateTime.Now;
            if (flag_.Text == "0")
            {
                TblHdr.trnid = trnid.Text.ToString();
                TblHdr.sysid = Convert.ToInt32(id_.Text);
                TblHdr.createby = Session["username"].ToString();
                TblHdr.createtime = DateTime.Now;
                db.t_orderhdrs.InsertOnSubmit(TblHdr);
            }
            db.SubmitChanges(); 
        }
        catch (Exception ex)
        { 
            this.sMsg(ex.ToString(), "1");
            return;
        }
        Response.Redirect("~/Transaction/SalesOrder/F_SalesOrder.aspx?id="+ trnid.Text);
    }

    protected void btnChangeHdr_Click(object sender, EventArgs e)
    {
        BtnSave.Enabled = true;
        btnChangeHdr.Enabled = false;
        BtnMutiItem.Enabled = false;
    }

    protected void BtnDel_Click(object sender, EventArgs e)
    {
        TblHdr = db.t_orderhdrs.Where(o => o.sysid == Int32.Parse(id_.Text)).FirstOrDefault();
        Tbldtl = db.t_orderdtls.Where(d => d.sysid == Int32.Parse(id_.Text)).FirstOrDefault();

        SqlTransaction ObjTrans;
        if (cKon.State == ConnectionState.Closed)
        {
            cKon.Open();
        }
        ObjTrans = cKon.BeginTransaction();
        try
        {
            if (db.t_orderdtls.Where(d => d.sysid == Int32.Parse(id_.Text)).Count()>0)
            {
                db.t_orderdtls.DeleteOnSubmit(Tbldtl);
                db.SubmitChanges();
            }
            if (db.t_orderhdrs.Where(o => o.sysid == Int32.Parse(id_.Text)).Count()> 0)
            {
                db.t_orderhdrs.DeleteOnSubmit(TblHdr);
                db.SubmitChanges(); 
            }

            ObjTrans.Commit();
            cKon.Close(); 
        }
        catch (Exception ex)
        {
            ObjTrans.Rollback();
            cKon.Close();
            this.sMsg(ex.ToString(), "1");
            return;
        }
        Response.Redirect("~/Transaction/SalesOrder/LF_SalesOrder.aspx");
    }

    protected void BtnSendApp_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "IN APPROVAL";
        BtnSave_Click(sender, e);
    }

    protected void BtnApproval_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "APPROVED";
        BtnSave_Click(sender, e);
    }

    protected void BtnRevise_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "REVISED";
        BtnSave_Click(sender, e);
    }

    protected void BtnReject_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "REJECTED";
        BtnSave_Click(sender, e);
    }
    
}