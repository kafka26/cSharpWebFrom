﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using CrystalDecisions.CrystalReports.Engine;
using System.Reflection;
using static ClassGlobal;
public partial class Transaction_SalesOrder_LF_SalesOrder : System.Web.UI.Page
{
    private static DataClassesDataContext db = new DataClassesDataContext();
    t_orderhdr Tbl = new t_orderhdr();
    t_orderdtl Tbldtl = new t_orderdtl(); 
    Int32 trnCode = 4;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            data = CekMenuByUser(Session["username"].ToString(), trnCode);
            if (data.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Session["username"].ToString()))
                {
                    if (!this.IsPostBack)
                    {
                        //startdate.Text = DateTime.Now.ToString("01/MM/yyyy");
                        //enddate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        gvlist.DataSource = BindData(gvlist);
                        gvlist.DataBind();
                    }
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }
        }
        catch (Exception ex)
        { 
            Response.Redirect("~/Login.aspx");
        }
    }

    private static DataTable BindData(GridView gvNya)
    {
        DataTable objData = ToDataTable((from som in db.t_orderhdrs join c in db.m_customers on som.custsuppid equals c.custid where som.transcode == 4 select new { som.trnid, som.sysid, som.trno, trdate = som.trdate.ToString(), custcode = c.custcode.ToUpper(), custname = c.custname.ToUpper(), totalamt = som.totalamt.GetValueOrDefault(0), discamt = som.discamt.GetValueOrDefault(0), dppamt = som.dppamt.GetValueOrDefault(0), som.taxable, taxamt = som.taxamt.GetValueOrDefault(0), nettoamt = som.nettoamt.GetValueOrDefault(0), som.headernote, som.orderstatus, som.ordernextstatus, som.createtime, createby = som.createby.ToUpper(), som.lastupdatetime, lastupdateby = som.lastupdateby.ToUpper() }).ToList());

        string[] HdrText = { "NO. SO", "Tanggal", "Supplier", "Total Amount", "Discount", "DPP", "Tax Amount", "Netto", "Keterangan", "Status" };
        string[] DtField = { "trno", "trdate", "custname", "totalamt", "discamt", "dppamt", "taxamt", "nettoamt", "headernote", "orderstatus" };
        string[] DtKeyNames = { "trnid", "sysid" };

        gvNya.Columns.Clear();
        System.Web.UI.WebControls.HyperLinkField dLink = new HyperLinkField();
        dLink.DataTextField = "sysid";
        dLink.SortExpression = "sysid";
        dLink.DataNavigateUrlFormatString = "~/Transaction/SalesOrder/F_SalesOrder.aspx?id={0}";
        dLink.DataNavigateUrlFields = new string[] { "trnid" };
        dLink.HeaderText = "Draft";
        dLink.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        gvNya.Columns.Add(dLink);

        for (int i = 0; i < HdrText.Length; i++)
        {
            System.Web.UI.WebControls.BoundField dRow = new BoundField();
            dRow.HeaderText = HdrText[i];
            dRow.HeaderStyle.Wrap = false;

            if (DtField[i] == "totalamt" || DtField[i] == "discamt" || DtField[i] == "taxamt" || DtField[i] == "nettoamt") 
                dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
            else
            {
                dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                dRow.ItemStyle.Wrap = false;
            }

            dRow.DataField = DtField[i];
            gvNya.Columns.Add(dRow);
        }

        for (int i = 0; i < objData.Rows.Count; i++)
        {
            objData.Rows[i]["totalamt"] = Convert.ToDouble(objData.Rows[i]["totalamt"]).ToString("N2");
            objData.Rows[i]["discamt"] = Convert.ToDouble(objData.Rows[i]["discamt"]).ToString("N2");
            objData.Rows[i]["taxamt"] = Convert.ToDouble(objData.Rows[i]["taxamt"]).ToString("N2");
            objData.Rows[i]["dppamt"] = Convert.ToDouble(objData.Rows[i]["dppamt"]).ToString("N2");
            objData.Rows[i]["nettoamt"] = Convert.ToDouble(objData.Rows[i]["nettoamt"]).ToString("N2");
            objData.Rows[i]["trdate"] = Convert.ToDateTime(objData.Rows[i]["trdate"]).ToString("dd/MM/yyy");
        }

        gvNya.DataSource = objData;
        gvNya.DataBind();
        return objData;
    } 

    protected void BtnFind_Click(object sender, EventArgs e)
    {
        DataView DtView = BindData(gvlist).DefaultView;
        DtView.RowFilter = ddlfilter.SelectedValue + " LIKE '%" + txtfilter.Text + "%'";
        // AND trdate>='"+ startdate.Text + "' AND trdate<='"+ enddate.Text + "'";
        BindData(gvlist).AcceptChanges();
        gvlist.DataSource = DtView.ToTable();
        gvlist.DataBind();
        DtView.RowFilter = "";
    }

    protected void gvlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvlist.PageIndex = e.NewPageIndex;
        gvlist.DataSource = BindData(gvlist);
        gvlist.DataBind();
    }
}