﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="F_Shipment.aspx.cs" Inherits="Transaction_Shipment_F_Shipment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
 <head runat="server" />
 <div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">Form Shipment (SJ)</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate> 
                            <div class="row">
                                <div class="col-sm-12">
                                   <asp:label runat="server" ID="trnid" Visible="false" />
                                   <asp:label runat="server" ID="id_" Visible="false" />
                                   <asp:label runat="server" ID="flag_" Visible="false" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">IR No</label> 
                                    <asp:TextBox runat="server" ID="trno" CssClass="form-control" Enabled="false" />
                               </div>  

                               <div class="col-sm-3">
                                    <label class="col-form-label">IR Date</label> 
                                    <div class="input-group">
                                       <asp:TextBox runat="server" ID="trdate" CssClass="form-control" Enabled="false"/>  
                                       <asp:ImageButton id="CallPeriod" runat="server" ImageUrl="~/Images/oCalendar.gif" CssClass="btn btn-secondary"/>
                                    </div>
                                   <ajaxToolkit:CalendarExtender ID="CalendarExtender" runat="server" Enabled="True" TargetControlID="trdate" PopupButtonID="CallPeriod" Format="dd/MM/yyyy" />
                                   <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="trdate" Mask="99/99/9999" MaskType="Date" UserDateFormat="None"/>
                               </div>   
                               
                               <div class="col-sm-6">
                                    <label class="col-form-label">Customer</label> 
                                    <div class="input-group">
                                        <asp:TextBox id="custid" runat="server" CssClass="form-control" Visible="false" Text="0" />
                                        <asp:TextBox id="custname" runat="server" CssClass="form-control" Enabled="false" Text="" />
                                        <span class="input-group-btn">
                                            <asp:button runat="server" ID="BtnCust" Text="search" CssClass="btn btn-default" OnClick="BtnCust_Click"/>
                                        </span>
                                    </div> 
                               </div>   

                            </div>

                            <div class="row">         

                               <div class="col-sm-3">
                                    <label class="col-form-label">Gudang</label> 
                                    <asp:DropDownList id="locationid" runat="server" CssClass="form-control select2" />
                               </div> 

                                <div class="col-sm-3">
                                    <label class="col-form-label">Total Qty</label> 
                                    <asp:TextBox runat="server" ID="totalqty" CssClass="form-control" Text="0.0" Enabled="false" />
                                </div> 

                                 <div class="col-sm-3">
                                    <label class="col-form-label">Status</label> 
                                    <asp:TextBox runat="server" ID="orderstatus" CssClass="form-control" Enabled="false" />
                                </div> 

                            </div>

                             <div class="row"> 
                                <div class="col-sm-6">
                                    <label class="col-form-label">Header Note</label> 
                                    <asp:TextBox runat="server" ID="headernote" CssClass="form-control" TextMode="MultiLine" Height="50" />
                                </div>
                            </div>

                        
                        <hr />
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="Btnsave" CssClass="btn btn-success" Text="Save" OnClick="Btnsave_Click" />
                                    <asp:Button runat="server" ID="BtnChangeHdr" CssClass="btn btn-secondary" Text="Change Header" Enabled="false" OnClick="BtnChangeHdr_Click"/>
                                </div>
                             </div>
                        </div> 

                        <hr />
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="col-form-label">SO No</label> 
                                <div class="input-group">
                                    <asp:TextBox id="soid" runat="server" CssClass="form-control" Visible="false" Text="0" />
                                    <asp:TextBox id="sono" runat="server" CssClass="form-control" Enabled="false" />
                                    <span class="input-group-btn">
                                        <asp:button runat="server" ID="BtnSO" Text="search" class="btn btn-default" Enabled="false" OnClick="BtnSO_Click"/>
                                    </span>
                                </div> 
                            </div>  
                       
                           <div class="col-sm-6">
                                <label class="col-form-label">Search Item For Multiple Add</label><br />
                                <asp:Button runat="server" ID="BtnMultiItem" CssClass="btn btn-info" Text="Add Multiple Item" Enabled="false" OnClick="BtnMultiItem_Click"/>
                           </div> 
                        </div>
                        <hr />

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-body table-responsive p-0" style="height: 200px;"> 
                                    <asp:GridView ID="gvDtl" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="sysiddtl,itemid" OnRowEditing="gvDtl_RowEditing" OnRowUpdating="gvDtl_RowUpdating" OnRowCancelingEdit="gvDtl_RowCancelingEdit" OnRowDeleting="gvDtl_RowDeleting"> 
                                    <Columns>
                                        <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true"/>
                                        <asp:BoundField DataField="trno" HeaderText="No. PO" ReadOnly="True" HeaderStyle-Wrap="false"/>
                                        <asp:BoundField DataField="itemcode" HeaderText="Item Code" ReadOnly="True" HeaderStyle-Wrap="false"/>
                                        <asp:BoundField DataField="itemname" HeaderText="Description" ReadOnly="True" HeaderStyle-Wrap="false"/>
                                        <asp:BoundField DataField="unit" HeaderText="Unit" ReadOnly="True" HeaderStyle-Wrap="false"/>
                                        <asp:BoundField DataField="orderqty" HeaderText="Qty SO" ReadOnly="True" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"/>

                                        <asp:TemplateField HeaderText="Qty" ControlStyle-CssClass="" HeaderStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtqtysj" runat="server" Text='<%# Eval("orderqty") %>' Enabled="false" Width="110" />
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                                                                
                                        <asp:TemplateField HeaderText="Detail Note">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDetailNote" runat="server" Text='<%# Eval("detailnote") %>' Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbDtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("itemid") %>' Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                    </Columns>
                                    <EmptyDataTemplate>
                                        <label>Data Not Found</label>
                                    </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                        <asp:Panel ID="pnlReason" runat="server">
                            <div class="row">
                                <div class="col-sm-6">
                                    <asp:Label ID="LblReason" runat="server" Text="Reason" CssClass="col-form-label" Visible="false" /> 
                                    <asp:TextBox runat="server" ID="txtreason" CssClass="form-control" TextMode="MultiLine" Height="50" Visible="false" />
                                </div>
                            </div>
                        </asp:Panel>

                        <hr />
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="Btnback" CssClass="btn btn-secondary" Text="Back" OnClick="Btnback_Click"/>
                                    <asp:Button runat="server" ID="Btndel" CssClass="btn btn-danger" Text="Delete" Visible="false" OnClick="Btndel_Click" />
                                    <asp:Button runat="server" ID="BtnSendApp" CssClass="btn btn-success" Text="Send Approval" Visible="false" OnClick="BtnSendApp_Click" />
                                    <asp:Button runat="server" ID="BtnApproved" CssClass="btn btn-success" Text="Approved" Visible="false" OnClick="BtnApproved_Click" />
                                    <asp:Button runat="server" ID="BtnRevise" CssClass="btn btn-info" Text="Revised" Visible="false" OnClick="BtnRevise_Click" />
                                    <asp:Button runat="server" ID="BtnReject" CssClass="btn btn-danger" Text="Rejected" Visible="false" OnClick="BtnReject_Click" />
                                </div>
                             </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                               <i class="col-form-label">Create By : <asp:Label runat="server" ID="lblcreate"></asp:Label> </i>
                           </div>
                           <div class="col-sm-6">
                               <i class="col-form-label">Last Update By : <asp:Label runat="server" ID="lblupdate"></asp:Label> </i>
                           </div> 
                            <div class="col-sm-6">
                               <i class="col-form-label">Create Time : <asp:Label runat="server" ID="lblcreatetime"></asp:Label> </i>
                           </div>  
                            <div class="col-sm-6">
                               <i class="col-form-label">Last Update Time : <asp:Label runat="server" ID="lblupdatetime"></asp:Label> </i>
                           </div> 
                        </div>

                        <!--- Start PopUp Message --->
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:Panel id="PanelsMsg" runat="server" Visible="False" Width="70%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">
                                                <asp:Label ID="lblInfo" runat="server" Text="" />
                                            </h4>
                                        </div>

                                        <div class="modal-body">                                                   
                                            <div class="col-sm-12">                                                         
                                                <center>
                                                    <asp:Label ID="lblMessage" runat="server" Text="" />
                                                </center> 
                                            </div>       
                                        </div> 

                                        <div class="modal-footer">   
                                            <div class="col-sm-12">
                                                <center>
                                                    <asp:Button ID="BtnOK" runat="server" CssClass="btn btn-danger" Text="OK" OnClick="BtnOK_Click"/>
                                                </center>
                                            </div>                                                
                                        </div>
                                    </div>
                                </asp:Panel> 

                                <ajaxToolkit:ModalPopupExtender id="MpesMsg" runat="server" Drag="True" PopupControlID="PanelsMsg" TargetControlID="BesMsg" DropShadow="false" BackgroundCssClass="modalBackground" /> 
                                <asp:Button id="BesMsg" runat="server" Visible="False" CausesValidation="False" />       

                            </ContentTemplate>
                        </asp:UpdatePanel> 
                        <!--- End PopUp Message --->

                        <asp:UpdateProgress runat="server" ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div ID="progressBackgroundFilterIR" class="progressBackgroundFilter"></div>
                                <div id="processMessage" class="processMessage">
                                    <center><span><asp:Image ID="ImageIR" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><br />Please Wait</span></center><br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>
</section>
<!-- End Main content -->

<!-- Pop Up  Supplier-->
<asp:UpdatePanel ID="UpdatePanelCust" runat="server">
    <ContentTemplate>
        <asp:Panel id="PnlCust" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List Customer</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="ddlfilterCust" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="custname"> Nama </asp:ListItem> 
                                        <asp:ListItem value="custcode"> Kode </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="txtfilterCust" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindCust" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindCust_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvCust" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnSelectedIndexChanged="gvCust_SelectedIndexChanged" OnPageIndexChanging="gvCust_PageIndexChanging" > 
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnCloseCust" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCloseCust_Click" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="MpeCUst" runat="server" Drag="True" PopupControlID="PnlCust" TargetControlID="BeCust" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="BeCust" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgressCust" AssociatedUpdatePanelID="UpdatePanelCust">
            <ProgressTemplate>
                <div ID="progressBackgroundFilterCust" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                           <asp:Image ID="ImageCust" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center><br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
<!-- Pop Up Supplier-->

<!-- Pop Up  PO-->
<asp:UpdatePanel ID="UpdatePanelSO" runat="server">
    <ContentTemplate>
        <asp:Panel id="PanelSO" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List PO</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="DDLFilterSO" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="sysid"> Draft SO </asp:ListItem> 
                                        <asp:ListItem value="trno"> No SO </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="TxtFilterSO" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindSO" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindSO_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="GVso" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left" OnSelectedIndexChanged="GVso_SelectedIndexChanged" OnPageIndexChanging="GVso_PageIndexChanging" > 
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnCloseSO" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCloseSO_Click" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="MpeSO" runat="server" Drag="True" PopupControlID="PanelSO" TargetControlID="BeSO" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="BeSO" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgressSO" AssociatedUpdatePanelID="UpdatePanelSO">
            <ProgressTemplate>
                <div ID="progressBackgroundFilterSO" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                           <asp:Image ID="ImagePO" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center><br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
<!-- Pop Up PO-->

<!-- Pop Up Data Item-->
<asp:UpdatePanel ID="UpdatePanelItem" runat="server">
    <ContentTemplate>
        <asp:Panel id="PanelItem" runat="server" Visible="False" Width="100%" CssClass="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">List data item</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row col-sm-12">
                        <div class="col-sm-2 custom-checkbox">
                            <label class="col-form-label">Filter</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <asp:DropDownList id="ddlfilterItem" runat="server" CssClass="form-control select2">  
                                        <asp:ListItem value="itemname"> Item </asp:ListItem> 
                                        <asp:ListItem value="itemcode"> Kode </asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                                <asp:TextBox runat="server" ID="txtfilterItem" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2 custom-checkbox">
                            <asp:Button ID="BtnFindItem" runat="server" CssClass="btn btn-secondary" Text="Find Data" OnClick="BtnFindItem_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvItem" CssClass="table table-hover table-bordered table-striped" runat="server" AutoGenerateColumns="False" EmptyDataText="Data Not Found" Width="100%" DataKeyNames="itemid" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Left">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbListDtl" runat="server" Checked='<%# Convert.ToBoolean(Eval("checkvalue")) %>' ToolTip='<%# Eval("itemid") %>'/>
                                        </ItemTemplate> 
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="itemcode" HeaderText="Code" ItemStyle-Wrap="false" /> 
                                    <asp:BoundField DataField="itemname" HeaderText="Item" ItemStyle-Wrap="false" /> 
                                   <%-- <asp:TemplateField HeaderText="Unit" HeaderStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:DropDownList id="tbunitdtl" runat="server">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>  --%>
                                    <asp:BoundField DataField="orderqty" HeaderText="SO Qty" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="qty_akum" HeaderText="SJ Qty" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Right" /> 
                                    <asp:TemplateField HeaderText="OS Qty" HeaderStyle-Wrap="false" ControlStyle-CssClass="">
                                        <ItemTemplate>
                                            <asp:TextBox id="tbqtysj" runat="server" Text='<%# Eval("qtysj") %>' OnBlur="discPctdetail()" Width="110" />
                                        </ItemTemplate>
                                    </asp:TemplateField>   

                                    <asp:TemplateField HeaderText="Note" HeaderStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:TextBox id="tbnotedtl" runat="server" Text='<%# Eval("detailnote") %>'/> 
                                    </ItemTemplate>
                                    </asp:TemplateField> 
                                </Columns>
                                <EmptyDataTemplate>
                                    <label>Data Not Found</label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnAddToDtl" runat="server" CssClass="btn btn-default btn-md" Text="Add To Detail" OnClick="BtnAddToDtl_Click" />   
                    <asp:Button ID="BtnCLoseItem" runat="server" CssClass="btn btn-danger" Text="Close" OnClick="BtnCLoseItem_Click" />
                </div>
            </div>
        </asp:Panel> 

        <ajaxToolkit:ModalPopupExtender id="MpeItem" runat="server" Drag="True" PopupControlID="PanelItem" TargetControlID="BeItem" DropShadow="false" BackgroundCssClass="modalBackground" />
        <asp:Button id="BeItem" runat="server" Visible="False" CausesValidation="False" />

        <asp:UpdateProgress runat="server" ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanelItem">
            <ProgressTemplate>
                <div ID="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div id="processMessage" class="processMessage">
                    <center>
                        <span>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />Please Wait
                        </span>
                    </center>
                    <br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="BtnAddToDtl" />
    </Triggers>
</asp:UpdatePanel>
<!-- End Pop Up  Data Item--> 
</asp:Content>

