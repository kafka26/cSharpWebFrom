﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using CrystalDecisions.CrystalReports.Engine;
using static ClassGlobal;
using System.Reflection;

public partial class Transaction_Shipment_F_Shipment : System.Web.UI.Page
{
    public static DataClassesDataContext db = new DataClassesDataContext();
    t_inhdr TblHdr = new t_inhdr();
    t_indtl Tbldtl = new t_indtl();
    m_customer TblCust = new m_customer();
    ClassGlobal sVar = new ClassGlobal();
    Int32 trnCode = 5;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["username"].ToString()))
            {
                DataTable sData =CekMenuByUser(Session["username"].ToString(), trnCode);
                if (sData.Rows.Count > 0)
                { 
                    if (!this.IsPostBack)
                    {
                        if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
                        {                            
                            flag_.Text = "1";
                            this.Filltexbox(Convert.ToString(Request.QueryString["id"]));
                            this.FillTexDtl(Convert.ToString(Request.QueryString["id"]));
                            this.CekUser(sData); 
                            this.GetddlGudang();
                        }
                        else
                        {
                            flag_.Text = "0";
                            orderstatus.Text = "IN PROCESS";
                            trnid.Text =GetNewID().ToString();                           
                            trdate.Text =GetServerTime().ToString("dd/MM/yyyy");
                            this.GetddlGudang();
                            this.CekUser(sData);
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }
        catch (Exception ex)
        { 
            Response.Redirect("~/Login.aspx");
        }
    }

    private void CekUser(DataTable sData)
    {
        if (Boolean.Parse(sData.Rows[0]["approval_form"].ToString()) == true)
        {
            if (orderstatus.Text == "IN PROCESS" || orderstatus.Text == "REVISED")
            { 
                Btndel.Visible = true;
                BtnSendApp.Visible = true;
                BtnApproved.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                BtnChangeHdr.Visible = false;
                BtnSO.Enabled = false;
                BtnMultiItem.Visible = true;
            }
            else if (orderstatus.Text == "IN APPROVAL")
            {
                Btnsave.Visible = false;
                Btndel.Visible = false;
                BtnSendApp.Visible = false;
                BtnApproved.Visible = true;
                BtnRevise.Visible = true;
                BtnReject.Visible = true;
                txtreason.Visible = true;
                LblReason.Visible = true;
                BtnChangeHdr.Visible = false;
                BtnSO.Enabled = false;
                BtnMultiItem.Visible = false;
                gvDtl.Columns[0].Visible = false;
            }
            else if (orderstatus.Text == "APPROVED" || orderstatus.Text == "REJECTED")
            {
                BtnSendApp.Visible = false;
                BtnApproved.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                Btndel.Visible = false;
                BtnChangeHdr.Visible = false;
                BtnSO.Enabled = false;
                BtnMultiItem.Visible = false;
                gvDtl.Columns[0].Visible = false; 
            }            
        }
        else if (Boolean.Parse(sData.Rows[0]["approval_user"].ToString()) == true)
        {
            if (orderstatus.Text == "IN PROCESS" || orderstatus.Text == "REVISED")
            {
                BtnSendApp.Visible = true;
                Btndel.Visible = true;
                BtnApproved.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                BtnChangeHdr.Visible = true;
                BtnChangeHdr.Enabled = true;
                BtnSO.Enabled = true;
                BtnMultiItem.Visible = true;
                gvDtl.Columns[0].Visible = false;
            }
            else if (orderstatus.Text == "IN APPROVAL")
            {
                Btndel.Visible = false;
                BtnSendApp.Visible = false;
                BtnApproved.Visible = true;
                BtnRevise.Visible = true;
                BtnReject.Visible = true;
                txtreason.Visible = true;
                LblReason.Visible = true;
                BtnChangeHdr.Visible = false;
                BtnSO.Enabled = false;
                BtnMultiItem.Visible = false;
                gvDtl.Columns[0].Visible = false;
            }
            else if (orderstatus.Text == "APPROVED")
            {
                BtnSendApp.Visible = false;
                BtnApproved.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                Btndel.Visible = false;
                BtnChangeHdr.Visible = false;
                BtnSO.Enabled = false;
                BtnMultiItem.Visible = false;
                gvDtl.Columns[0].Visible = false;
            } 
        }
        else if (Boolean.Parse(sData.Rows[0]["read_form"].ToString()))
        {
            Btndel.Visible = false;
            BtnApproved.Visible = false;
            BtnRevise.Visible = false;
            BtnReject.Visible = false;
            BtnChangeHdr.Visible = false;
            BtnMultiItem.Visible = false;
            gvDtl.Columns[0].Visible = false;
            txtreason.Visible = false;
            LblReason.Visible = false;
            gvDtl.Columns[0].Visible = false;
            BtnSO.Enabled = false;
        }
        else if (Boolean.Parse(sData.Rows[0]["update_form"].ToString()) == true)
        {
            if (orderstatus.Text == "APPROVED" || orderstatus.Text == "IN APPROVAL" || orderstatus.Text == "REJECTED")
            {
                Btndel.Visible = false;
                BtnApproved.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                BtnChangeHdr.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                gvDtl.Columns[0].Visible = false;
                BtnSO.Enabled = false;
                BtnMultiItem.Visible = false;
            }
            else
            {
                Btndel.Visible = false;
                BtnApproved.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                BtnChangeHdr.Visible = true;
                txtreason.Visible = false;
                LblReason.Visible = false;
                gvDtl.Columns[0].Visible = true;
                BtnSO.Enabled = true;
                BtnMultiItem.Visible = true;
            }

        }
        else if (Boolean.Parse(sData.Rows[0]["delete_form"].ToString()) == true)
        {
            if (orderstatus.Text == "APPROVED" || orderstatus.Text == "IN APPROVAL" || orderstatus.Text == "REJECTED")
            {
                Btndel.Visible = false;
                BtnApproved.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                BtnChangeHdr.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                gvDtl.Columns[0].Visible = false;
            }
            else
            {
                Btndel.Visible = true;
                BtnApproved.Visible = false;
                BtnRevise.Visible = false;
                BtnReject.Visible = false;
                BtnChangeHdr.Visible = false;
                txtreason.Visible = false;
                LblReason.Visible = false;
                gvDtl.Columns[0].Visible = true;
            }

        }
    }

    private void sMsg(string sMsg, string type)
    {
        if (type == "1")
        {
            lblInfo.Text = "ERROR";
        }
        else
        {
            lblInfo.Text = "INFORMATION";
        }
        lblMessage.Text = sMsg.ToUpper().ToString();
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, true);
    }

    protected void BtnOK_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpesMsg, PanelsMsg, BesMsg, false);
    }

    private void GetddlGudang()
    {
        var sData = (from p in db.m_locations orderby p.locationname descending select new { p.locationid, p.locationname }).ToList();
        locationid.DataSource = sData;
        locationid.DataTextField = "locationname";
        locationid.DataValueField = "locationid";
        locationid.DataBind();
    }

    //== Get Data Supplier ==//
    private static DataTable GetDataCustomer(GridView gvCust)
    {
        DataTable sdata =ToDataTable((from c in db.m_customers
            join p in db.t_orderhdrs on c.custid equals p.custsuppid
            where p.transcode == 4 && p.orderstatus == "APPROVED"
            select new
            {
                c.custid,
                c.custcode,
                c.custname,
                c.cust_address
            }).Distinct().ToList());

        string[] HdrText = { "Cust. Code", "Customer", "Address" };
        string[] DtField = { "custcode", "custname", "cust_address" };
        string[] DtKeyNames = { "custid", "custname" };

        gvCust.Columns.Clear();
        gvCust.DataKeyNames = DtKeyNames;

        System.Web.UI.WebControls.CommandField cField = new CommandField();
        cField.ShowSelectButton = true;
        gvCust.Columns.Add(cField);
        for (int i = 0; i < DtField.Length; i++)
        {
            System.Web.UI.WebControls.BoundField dRow = new BoundField();
            dRow.HeaderText = HdrText[i];
            dRow.HeaderStyle.Wrap = false;
            dRow.DataField = DtField[i];
            gvCust.Columns.Add(dRow);
        }
        return sdata;
    }

    protected void BtnFindCust_Click(object sender, EventArgs e)
    {
        try
        {
            DataView DtView = GetDataCustomer(gvCust).DefaultView;
            DtView.RowFilter = ddlfilterCust.SelectedValue + " LIKE '%" + txtfilterCust.Text + "%'";
            // AND trdate>='"+ startdate.Text + "' AND trdate<='"+ enddate.Text + "'";
            GetDataCustomer(gvCust).AcceptChanges();
            gvCust.DataSource = DtView.ToTable();
            gvCust.DataBind();
            sVar.SetModalPopUp(MpeCUst, PnlCust, BeCust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(MpeCUst, PnlCust, BeCust, false);
            return;
        }
    } 

    protected void gvCust_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BtnSO.Enabled = true;
            custid.Text = gvCust.SelectedDataKey["custid"].ToString();
            custname.Text = gvCust.SelectedDataKey["custname"].ToString();
            sVar.SetModalPopUp(MpeCUst, PnlCust, BeCust, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void gvCust_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvCust.PageIndex = e.NewPageIndex;
            gvCust.DataSource = GetDataCustomer(gvCust);
            gvCust.DataBind();
            sVar.SetModalPopUp(MpeCUst, PnlCust, BeCust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(MpeCUst, PnlCust, BeCust, false);
            return;
        }
    }

    protected void BtnCust_Click(object sender, EventArgs e)
    {
        try
        {
            gvCust.DataSource = GetDataCustomer(gvCust);
            gvCust.DataBind();
            sVar.SetModalPopUp(MpeCUst, PnlCust, BeCust, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(MpeCUst, PnlCust, BeCust, false);
            return;
        }
    }

    protected void BtnCloseCust_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeCUst, PnlCust, BeCust, false);
    }
    //== Get Data Supplier ==//

    //== Get Data PO ==//

    private static DataTable GetDataSO(GridView gvNya, int custid)
    {
        DataTable sdata =ToDataTable((from c in db.m_customers
            join p in db.t_orderhdrs on c.custid equals p.custsuppid 
            where p.transcode == 4 && c.custid == custid && p.orderstatus == "APPROVED"           
            select new
            {
                p.sysid,
                p.trno,
                trdate = string.Format(p.trdate.ToString(), "dd/MM/yyyy"),
                custname = c.custname.ToUpper(),
                p.orderstatus
            }).ToList());

        string[] HdrText = { "Draft", "NO. SO", "Tanggal", "Customer", "Status" };
        string[] DtField = { "sysid", "trno", "trdate", "custname", "orderstatus" };
        string[] DtKeyNames = { "sysid", "trno" };

        gvNya.Columns.Clear();
        gvNya.DataKeyNames = DtKeyNames;

        System.Web.UI.WebControls.CommandField cField = new CommandField();
        cField.ShowSelectButton = true;
        cField.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        gvNya.Columns.Add(cField);

        for (int i = 0; i < HdrText.Length; i++)
        {
            System.Web.UI.WebControls.BoundField dRow = new BoundField();
            dRow.HeaderText = HdrText[i];
            dRow.HeaderStyle.Wrap = false; 
            dRow.DataField = DtField[i];
            dRow.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
            dRow.ItemStyle.Wrap = false;
            gvNya.Columns.Add(dRow);
        }

        for (int i = 0; i < sdata.Rows.Count; i++)
        {
            sdata.Rows[i]["trdate"] = Convert.ToDateTime(sdata.Rows[i]["trdate"]).ToString("dd/MM/yyy"); 
        }
        return sdata;
    }

    protected void BtnSO_Click(object sender, EventArgs e)
    {
        try
        {
            var sMsg = "";
            if (id_.Text == "0")
            {
                sMsg += "Maaf, tolong save data header dulu<br>";
            }
            if (custid.Text == "0")
            {
                sMsg += "Maaf, tolong pilih supplier dulu<br>";
            }
            if (sMsg != "")
            {
                this.sMsg(sMsg, "2");
                return;
            }
            GVso.DataSource = GetDataSO(GVso, int.Parse(custid.Text));
            GVso.DataBind();
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, false);
            return;
        }
    }
     
    protected void BtnFindSO_Click(object sender, EventArgs e)
    {
        try
        {
            DataView DtView = GetDataSO(GVso, int.Parse(custid.Text)).DefaultView;
            DtView.RowFilter = DDLFilterSO.SelectedValue + " LIKE '%" + TxtFilterSO.Text.Trim() + "%'";
            // AND trdate>='"+ startdate.Text + "' AND trdate<='"+ enddate.Text + "'";
            GetDataSO(GVso, int.Parse(custid.Text)).AcceptChanges();
            GVso.DataSource = DtView.ToTable();
            GVso.DataBind();
            DtView.RowFilter = "";
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, false);
            return;
        }
    }

    protected void BtnCloseSO_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, false);
    }

    protected void GVso_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BtnSO.Enabled = false;
            BtnMultiItem.Enabled = true;
            BtnMultiItem.Visible = true;
            soid.Text = GVso.SelectedDataKey["sysid"].ToString();
            sono.Text = GVso.SelectedDataKey["trno"].ToString();
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, false);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void GVso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GVso.PageIndex = e.NewPageIndex;
            GVso.DataSource = GetDataSO(GVso, int.Parse(custid.Text));
            GVso.DataBind();
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, true);
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            sVar.SetModalPopUp(MpeSO, PanelSO, BeSO, false);
            return;
        }
    }

    private void CountQtyHdr()
    {
        if (id_.Text != "0")
        {
            Decimal totalqtyin = 0, totalhpp=0;
            TblHdr = db.t_inhdrs.Single(o => o.sysid == Int32.Parse(id_.Text));
            List<t_indtl> dtl = (db.t_indtls.Where(d => d.sysid == Int32.Parse(id_.Text))).ToList();
            if (dtl.Count > 0)
            {
                foreach (var item in dtl)
                {
                    totalqtyin += Convert.ToDecimal(item.qtyin);
                    totalhpp += Convert.ToDecimal(item.totalhppdtl);
                } 
            }
             
            try
            {
                TblHdr.totalqtyin = totalqtyin;
                TblHdr.totalhpp = totalhpp;
                TblHdr.lastupdateby = Session["username"].ToString();
                TblHdr.lastupdatetime = DateTime.Now;
                db.SubmitChanges();  
            }
            catch (InvalidOperationException ex)
            { 
                this.sMsg(ex.ToString(), "1");
                return;
            }
        }        
    }

    //== Get Data Item ==//
    private void UpdateCheckedValue()
    {
        if (Session["GetItem" + trnid.Text].ToString() != null || Session["GetItem" + trnid.Text].ToString() != "")
        {
            DataTable sdata = Session["GetItem" + trnid.Text] as DataTable;
            DataView sDtView = sdata.DefaultView;
            if (sDtView.Count > 0)
            {
                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                        Boolean cbCheck = false;
                        string itemid = "";

                        foreach (System.Web.UI.Control myControl in cc)
                        {
                            if (myControl is System.Web.UI.WebControls.CheckBox)
                            {
                                cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                                itemid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                            }
                        }

                        sDtView.RowFilter = "itemid=" + itemid;
                        if (sDtView.Count == 1)
                        {
                            if (cbCheck == true)
                            {
                                sDtView[0]["checkvalue"] = "true";
                                sDtView[0]["qtysj"] = Convert.ToDouble(((TextBox)row.FindControl("tbqtysj")).Text); 
                                sDtView[0]["detailnote"] = ((TextBox)row.FindControl("tbnotedtl")).Text;
                            }
                        }

                    }
                }
            }
            sDtView.RowFilter = "";
            sdata.AcceptChanges();
            Session["GetItem" + trnid.Text] = sdata;
        }

        if (!string.IsNullOrEmpty(Session["GetItem_Hist" + trnid.Text].ToString()))
        {
            DataTable idata = Session["GetItem_Hist" + trnid.Text] as DataTable;
            DataView iDtView = idata.DefaultView;
            if (iDtView.Count > 0)
            {
                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    System.Web.UI.WebControls.GridViewRow row = gvItem.Rows[i];
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        System.Web.UI.ControlCollection cc = row.Cells[0].Controls;
                        Boolean cbCheck = false;
                        string itemid = "";

                        foreach (System.Web.UI.Control myControl in cc)
                        {
                            if (myControl is System.Web.UI.WebControls.CheckBox)
                            {
                                cbCheck = ((System.Web.UI.WebControls.CheckBox)myControl).Checked;
                                itemid = ((System.Web.UI.WebControls.CheckBox)myControl).ToolTip;
                            }
                        }
                        iDtView.RowFilter = "itemid='" + itemid + "'";
                        if (iDtView.Count == 1)
                        {
                            if (cbCheck == true)
                            {
                                iDtView[0]["checkvalue"] = "true";
                                iDtView[0]["qtysj"] = Convert.ToDouble(((TextBox)row.FindControl("tbqtysj")).Text);
                                iDtView[0]["detailnote"] = ((TextBox)row.FindControl("tbnotedtl")).Text;
                            }
                        }
                    }
                }
            }
            iDtView.RowFilter = "";
            idata.AcceptChanges();
            Session["GetItem_Hist" + trnid.Text] = idata;
        }
    }

    private void GetDataMaterial()
    {
        DataTable sdata =ToDataTable( (from od in db.t_orderdtls
                join i in db.m_items on od.itemid equals i.itemid
                join om in db.t_orderhdrs on od.sysid equals om.sysid
                where om.transcode == 4 && od.sysid == int.Parse(soid.Text)
                && od.qty-od.qty_akum>0 && om.orderstatus=="APPROVED"
                orderby om.sysid ascending
                select new
                {
                    checkvalue = "False", om.sysid, od.sysiddtl, i.itemid, i.itemcode, i.itemname,
                    od.unitid, od.unitprice, orderqty = od.qty.GetValueOrDefault(0),
                    qty_akum = od.qty_akum.GetValueOrDefault(0),
                    qtysj = od.qty.GetValueOrDefault(0) - od.qty_akum.GetValueOrDefault(0),
                    detailnote = "" 
                }).ToList());
        for (int i = 0; i < sdata.Rows.Count; i++)
        {
            sdata.Rows[i]["orderqty"] = Convert.ToDouble(sdata.Rows[i]["orderqty"]).ToString("N2");
            sdata.Rows[i]["qty_akum"] = Convert.ToDouble(sdata.Rows[i]["qty_akum"]).ToString("N2");
            sdata.Rows[i]["qtysj"] = Convert.ToDouble(sdata.Rows[i]["qtysj"]).ToString("N2"); 
        }

        gvItem.DataSource = sdata;
        gvItem.DataBind();
        Session["GetItem"+ trnid.Text] = sdata;
        Session["GetItem_Hist" + trnid.Text] = sdata;
    }

    protected void BtnMultiItem_Click(object sender, EventArgs e)
    {
        if (soid.Text != "0")
        {
            try
            {
                this.GetDataMaterial();
                sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
            }
            catch (Exception ex)
            { 
                this.sMsg(ex.ToString(), "1");
                return;
            }
        }
    }

    protected void BtnFindItem_Click(object sender, EventArgs e)
    {
        try
        {
            this.UpdateCheckedValue();
           data = Session["GetItem_PO"] as DataTable;
           DtView =data.DefaultView;
            if (ClassGlobal.data.Rows.Count > 0)
            {
                if (txtfilterItem.Text != "")
                {
                   DtView.RowFilter = ddlfilterItem.SelectedValue.ToString() + " LIKE '%" + txtfilterItem.Text.ToString().Trim() + "%'";
                }
                Session["GetItem_PO"] =DtView.ToTable();
                gvItem.DataSource = Session["GetItem_PO"];
                gvItem.DataBind();
                sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, true);
            }
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void BtnCLoseItem_Click(object sender, EventArgs e)
    {
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
    }

    private static Boolean isOnDtl(int itemid, int sysiddtl, GridView GvData)
    {
        for (int Index = 0; Index < GvData.Rows.Count; Index++)
        {
            if (itemid == Convert.ToInt32(GvData.DataKeys[Index]["itemid"]) && sysiddtl == Convert.ToInt32(GvData.DataKeys[Index]["sysiddtl"]))
            {
                return true;
            }
        }
        return false;
    }

    protected void BtnAddToDtl_Click(object sender, EventArgs e)
    {
        this.UpdateCheckedValue();
        string sMsg = "";
        if (!string.IsNullOrEmpty(Session["GetItem" + trnid.Text].ToString()))
        {
            DataTable data = Session["GetItem" + trnid.Text] as DataTable;
            DataView DtView = data.DefaultView;
            DtView.RowFilter = "checkvalue='True'";
            if (DtView.Count <= 0)
            {
                this.sMsg("Maaf, data tidak bisa di delete..!!<br>", "1");
            }

            if (DtView.Count > 0)
            {
                for (int c1 = 0; c1 < DtView.Count; c1++)
                {
                    if (isOnDtl(Convert.ToInt32(DtView[c1]["itemid"]), Convert.ToInt32(DtView[c1]["sysiddtl"]), gvDtl) == true)
                    {
                        sMsg += "Item " + DtView[c1]["itemname"] + " has been added !<br />";
                    }

                    if (Decimal.Parse(DtView[c1]["qtysj"].ToString()) <= 0)
                    {
                        sMsg += "Qty Item " + DtView[c1]["itemname"] + " Cant be 0 !<br />";
                    } 
                }
            }
            DtView.RowFilter = "";
        }       

        if (sMsg != "")
        {
            this.sMsg(sMsg.ToString(), "2");
            return;
        }
         
        try
        {
            if (!string.IsNullOrEmpty(Session["GetItem" + trnid.Text].ToString()))
            {
                DataTable sdata = Session["GetItem" + trnid.Text] as DataTable;
                DataView sDtView = sdata.DefaultView;
                sDtView.RowFilter = "checkvalue='True'";
                Int32 iSeq = gvDtl.Rows.Count + 1;
                if (sDtView.Count > 0)
                {
                    for (int i = 0; i < sDtView.Count; i++)
                    {
                        t_indtl dtdtl = new t_indtl();
                        dtdtl.orderid = int.Parse(sDtView[i]["sysid"].ToString());
                        dtdtl.orderdtlid = int.Parse(sDtView[i]["sysiddtl"].ToString());
                        dtdtl.itemid = int.Parse(sDtView[i]["itemid"].ToString());
                        dtdtl.sysid = int.Parse(id_.Text);
                        dtdtl.seq = iSeq;
                        dtdtl.qtyin = 0;
                        dtdtl.qtyout = Decimal.Parse(sDtView[i]["qtysj"].ToString());
                        dtdtl.qtyin_small = 0;
                        dtdtl.qtyout_small = 0; 
                        dtdtl.qty_akum = 0;
                        dtdtl.hpp = Decimal.Parse(sDtView[i]["unitprice"].ToString());
                        dtdtl.totalhppdtl= Decimal.Parse(sDtView[i]["unitprice"].ToString()) * Decimal.Parse(sDtView[i]["qtysj"].ToString());
                        dtdtl.locationid = int.Parse( locationid.SelectedValue);
                        dtdtl.detailnote = sDtView[i]["detailnote"].ToString().Trim();
                        dtdtl.unitid = int.Parse(sDtView[i]["unitid"].ToString());                        
                        dtdtl.lastupdateby = Session["username"].ToString();
                        dtdtl.lastupdatetime = DateTime.Now;
                        db.t_indtls.InsertOnSubmit(dtdtl);
                        db.SubmitChanges();
                        iSeq += 1;
                    }
                    this.CountQtyHdr();
                }
                this.FillTexDtl(trnid.Text.ToString());
                this.Filltexbox(trnid.Text.ToString());
                sDtView.RowFilter = "";
            }
        }
        catch (InvalidOperationException ex)
        { 
            this.sMsg(ex.ToString(), "1");
            return;
        }
        txtfilterItem.Text = "";
        sVar.SetModalPopUp(MpeItem, PanelItem, BeItem, false);
    }

    //== End Get Data Item ==//
    private void Filltexbox(string idhdr)
    {
        try
        {
            TblHdr = db.t_inhdrs.Single(o => o.trnid == idhdr && o.transcode == trnCode);
            trnid.Text = TblHdr.trnid.ToString();
            id_.Text = TblHdr.sysid.ToString();
            trno.Text = TblHdr.trno;
            trdate.Text = TblHdr.trdate.Value.ToString("dd/MM/yyyy");
            locationid.SelectedValue = TblHdr.locationid.ToString();
            totalqty.Text = Convert.ToDouble(TblHdr.totalqtyin ?? 0).ToString("N2");
            orderstatus.Text = (TblHdr.trstatus ?? "");
            headernote.Text = (TblHdr.headernote ?? ""); 
            lblcreate.Text = TblHdr.createby.ToString();
            lblcreatetime.Text = TblHdr.createtime.Value.ToString("yyyy-MM-dd HH:mm:ss");
            lblupdate.Text = TblHdr.lastupdateby;
            lblupdatetime.Text = TblHdr.lastupdatetime.Value.ToString("yyyy-MM-dd HH:mm:ss");
            custid.Text = TblHdr.custsuppid.ToString();
            TblCust = db.m_customers.Single(c => c.custid == int.Parse(custid.Text));
            custname.Text = TblCust.custname.ToUpper();
            Btnsave.Visible = false;
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }

    }

    private void FillTexDtl(string idhdr)
    {
        try
        {
            DataTable objData =ToDataTable((
                from id in db.t_indtls                
                join irm in db.t_inhdrs on id.sysid equals irm.sysid
                join od in db.t_orderdtls on new { sysiddtl = (int)id.orderdtlid, sysid = (int)id.orderid } equals new { od.sysiddtl, od.sysid }
                join om in db.t_orderhdrs on od.sysid equals om.sysid
                join i in db.m_items on id.itemid equals i.itemid
                join g in db.m_generals on id.unitid equals g.genid
                where irm.trnid == idhdr
                select new
                {
                    checkvalue = "False",
                    irm.trnid,
                    id.sysiddtl,
                    id.orderdtlid,
                    id.orderid,
                    om.trno,
                    i.itemid,
                    itemcode = i.itemcode.ToString().ToUpper(),
                    itemname = i.itemname.ToString().ToUpper(),
                    id.unitid,
                    unit = g.genname,
                    orderqty = od.qty,
                    qtysj = id.qtyin.GetValueOrDefault(0),
                    unitprice = id.hpp.GetValueOrDefault(0),
                    totalhppdtl = id.totalhppdtl.GetValueOrDefault(0),
                    detailnote = id.detailnote
                }).ToList());

            for (int i = 0; i < objData.Rows.Count; i++)
            {
                objData.Rows[i]["detailnote"] = objData.Rows[i]["detailnote"].ToString().ToUpper();
                objData.Rows[i]["orderqty"] = Convert.ToDouble(objData.Rows[i]["orderqty"]).ToString("N2");
                objData.Rows[i]["qtysj"] = Convert.ToDouble(objData.Rows[i]["qtysj"]).ToString("N2");
                objData.Rows[i]["unitprice"] = Convert.ToDouble(objData.Rows[i]["unitprice"]).ToString("N2");
                objData.Rows[i]["totalhppdtl"] = Convert.ToDouble(objData.Rows[i]["totalhppdtl"]).ToString("N2"); 
            }
            gvDtl.DataSource = objData;
            gvDtl.DataBind();
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void Btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            var sMessage = "";
            if (custname.Text.Trim() == "")
            {
                sMessage += "- Customer belum dipilih! <br>";
            }

            if (flag_.Text.ToString() == "0")
            {
                //int CekDouble =CekDoubleClick("t_orderhdrs", trnid.Text);
                if (ClassGlobal.CekDoubleClick("t_inhdr", trnid.Text) > 0)
                {
                    sMessage += "- Maaf, anda sudah save data ini..!! <br>";
                }
            }

            if (sMessage != "")
            {
                this.sMsg(sMessage, "2");
                return;
            }

            if (flag_.Text.ToString() != "0")
            {
                TblHdr = db.t_inhdrs.Single(o => o.sysid == Int32.Parse(id_.Text));
            }
            else
            {
                //int idnew = db.t_inhdrs.Max(i => i.sysid) + 1;
                int idnew =GenerateID("t_inhdr") + 1;
                id_.Text = idnew.ToString();
            }

            if (trno.Text == "")
            {
                trno.Text =GenerateTransNo(trnCode);
            }

            TblHdr.trdate = toDate(trdate.Text);
            TblHdr.transcode = trnCode;
            TblHdr.trno = trno.Text;
            TblHdr.custsuppid = Convert.ToInt32(custid.Text);
            TblHdr.locationid = locationid.SelectedValue; 
            TblHdr.trstatus = orderstatus.Text;
            TblHdr.totalqtyin = Decimal.Parse(totalqty.Text);
            TblHdr.totalqtyout = 0;
            TblHdr.totalhpp = 0;
            TblHdr.trnextstatus = "On Progress";
            if (orderstatus.Text == "APPROVED")
            {
                TblHdr.approved_by = Session["username"].ToString();
                TblHdr.approved_time = DateTime.Now;
            }
            else if (orderstatus.Text == "REVISED")
            {
                TblHdr.revised_by = Session["username"].ToString();
                TblHdr.revised_reason = txtreason.Text.Trim();
                TblHdr.revised_time = DateTime.Now;
            }
            else if (orderstatus.Text == "REJECTED")
            {
                TblHdr.rejected_by = Session["username"].ToString();
                TblHdr.rejected_reason = txtreason.Text.Trim();
                TblHdr.rejected_time = DateTime.Now;
            }

            TblHdr.headernote = headernote.Text;
            TblHdr.lastupdateby = Session["username"].ToString();
            TblHdr.lastupdatetime = DateTime.Now;
            if (flag_.Text == "0")
            {
                TblHdr.trnid = trnid.Text.ToString();
                TblHdr.sysid = Convert.ToInt32(id_.Text);
                TblHdr.createby = Session["username"].ToString();
                TblHdr.createtime = DateTime.Now;
                db.t_inhdrs.InsertOnSubmit(TblHdr);
            }
            db.SubmitChanges(); 
        }
        catch (InvalidOperationException ex)
        {  
            this.sMsg(ex.ToString(), "1");
            return;
        }
        BtnSO.Enabled = true;
        Response.Redirect("~/Transaction/Shipment/F_Shipment.aspx?id=" + trnid.Text.ToString());
    }

    protected void Btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Transaction/Shipment/LF_Shipment.aspx?");
    } 

    protected void Btndel_Click(object sender, EventArgs e)
    {
        TblHdr = db.t_inhdrs.Where(o => o.sysid == Int32.Parse(id_.Text)).FirstOrDefault();
        Tbldtl = db.t_indtls.Where(d => d.sysid == Int32.Parse(id_.Text)).FirstOrDefault(); 
        try
        {
            if (db.t_indtls.Where(d => d.sysid == Int32.Parse(id_.Text)).Count() > 0)
            {
                db.t_indtls.DeleteOnSubmit(Tbldtl);
                db.SubmitChanges();
            }
            if (db.t_inhdrs.Where(o => o.sysid == Int32.Parse(id_.Text)).Count() > 0)
            {
                db.t_inhdrs.DeleteOnSubmit(TblHdr);
                db.SubmitChanges();
            } 
        }
        catch (Exception ex)
        { 
            this.sMsg(ex.ToString(), "1");
            return;
        }
        Response.Redirect("~/Transaction/Shipment/F_Shipment.aspx?");
    }

    protected void BtnSendApp_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "IN APPROVAL";
        Btnsave_Click(sender, e);
    }

    protected void BtnApproved_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "APPROVED";
        Btnsave_Click(sender, e);
    }

    protected void BtnRevise_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "REVISED";
        Btnsave_Click(sender, e);
    }

    protected void BtnReject_Click(object sender, EventArgs e)
    {
        orderstatus.Text = "REJECTED";
        Btnsave_Click(sender, e);
    }

    protected void gvDtl_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvDtl.EditIndex = e.NewEditIndex;
            this.FillTexDtl(trnid.Text.ToString());
            GridViewRow row = gvDtl.Rows[e.NewEditIndex];
            ((TextBox)row.FindControl("txtqtysj")).Enabled = true;
            ((TextBox)row.FindControl("txtDetailNote")).Enabled = true;
            ((CheckBox)row.FindControl("cbDtl")).Enabled = true;
            ((CheckBox)row.FindControl("cbDtl")).Checked = true;
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void gvDtl_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        gvDtl.EditIndex = -1;
        GridViewRow row = gvDtl.Rows[e.RowIndex];
        int sydIdDtl = int.Parse(gvDtl.DataKeys[e.RowIndex].Values[0].ToString());
        Tbldtl = db.t_indtls.Where(d => d.sysiddtl == sydIdDtl).FirstOrDefault();
        try
        {
            Tbldtl.qtyin = Decimal.Parse(((TextBox)row.FindControl("txtqtysj")).Text);
            Tbldtl.detailnote = ((TextBox)row.FindControl("txtDetailNote")).Text.ToString().Trim();
            Tbldtl.lastupdateby = Session["username"].ToString();
            Tbldtl.lastupdatetime = DateTime.Now;
            db.SubmitChanges();
            this.CountQtyHdr();
            this.FillTexDtl(trnid.Text.ToString());
            this.Filltexbox(trnid.Text.ToString());
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void gvDtl_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvDtl.EditIndex = -1;
            this.FillTexDtl(trnid.Text.ToString());
            GridViewRow row = gvDtl.Rows[e.RowIndex];
            ((TextBox)row.FindControl("txtqtysj")).Enabled = true;
            ((TextBox)row.FindControl("txtDetailNote")).Enabled = true;
            ((CheckBox)row.FindControl("cbDtl")).Enabled = true;
            ((CheckBox)row.FindControl("cbDtl")).Checked = true;
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
    }

    protected void gvDtl_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int sydIdDtl = int.Parse(gvDtl.DataKeys[e.RowIndex].Values[0].ToString());
        Tbldtl = db.t_indtls.Where(d => d.sysiddtl == sydIdDtl).FirstOrDefault();

        try
        {
            db.t_indtls.DeleteOnSubmit(Tbldtl);
            db.SubmitChanges();
            this.CountQtyHdr();
            this.FillTexDtl(trnid.Text.ToString());
            this.Filltexbox(trnid.Text.ToString());
        }
        catch (Exception ex)
        {
            this.sMsg(ex.ToString(), "1");
            return;
        }
        //Response.Redirect("~/Transaction/Receiving/F_Receiving.aspx?");
    }    

    protected void BtnChangeHdr_Click(object sender, EventArgs e)
    {
        Btnsave.Visible = true;
        BtnChangeHdr.Enabled = false;
        BtnMultiItem.Enabled = false;
    }
}